<?php
/*
Plugin Name: Team Thumbnail
Plugin URI: http://www.teamedc.com/
Description: Thumbnail with custom field template
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.00
Author URI: http://www.teamedc.com/
*/

//Fait le register et l'enqueue des javascripts 
function team_thumbnail_metabox() {

	ob_start();
	
	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_thumbnail'));
	
	global $post_type;
	global $post;

	$args=array(
	  'public'   => true,
	); 
	$output = '';
	$yy = 0;			
	
	
	
	//Lister tous les customs post type qui sont � ON
	if (count($settings)) {
		foreach($settings as $key => $setting  ) {
			$poskey = strrpos($key,'_');
			if (strpos($key,'label') === false) {
				$ma_key = substr($key,($poskey+1));
				$arraykey[] = substr($key,($poskey+1));
				$yy++;
				add_filter('manage_edit-'.$ma_key.'_columns', 'add_team_thumbnail_column');
				add_action('manage_'.$ma_key.'_posts_custom_column', 'manage_attachment_team_thumbnail_column', 1, 2);
			}
			
			
		}
	}
	
	$custom_post=get_post_types($args);
	//Voir si on trouve le custom post dans ceux coch�s
	if ($yy != 0) {
		foreach($custom_post as $single_post) {
			if ($single_post == $post_type) {	
				if (in_array($single_post,$arraykey)) { //On ajoute la metabox	

					add_meta_box('team_thumbnail_image_box', 'Ajouter des images', 'team_thumbnail_attachment_box', $single_post, 'normal', 'high');
			
				}
			}
		}	
	}
}

//////////////////////////////////////////////////////

function team_thumbnail_attachment_box($post) {

	$settings = unserialize(get_option('team_thumbnail'));
	global $post_type;
	
	foreach($settings as $key => $setting  ) {
		if (strpos($key,$post->post_type) == true  ) {
			if ($setting != 'on' && $setting != '') {
				$arraykey[] = $setting;
			}	
		}	
	}
	
	//print_r($arraykey);
	$output = '';
	$jj = 0;
	foreach ($arraykey as $un_image ) {
		$meta_titre = get_post_meta($post->ID, 'alt_team_thumbnail_image'.$jj, true);
		//$existing_image_id = get_post_meta($post->ID,'team_thumbnail_image'.$jj, true);
	
		$output .= '<div class="Meta_Thumbnail">';		
		$output .= '<div class="Image_Team_Thumbnail">'. team_the_thumb(false,false,'','team_thumbnail_image'.$jj,64,64,0,'',$post->ID) .'</div>';			
		$output .= '<div class="Input_Team_Thumbnail">'. $un_image . ' <br /><input type="file" name="team_thumbnail_image'.$jj.'" id="team_thumbnail_image'.$jj.'" /></div>';
		$output .= '<div class="Input_Team_Thumbnail"> Titre de l\'image (facultatif) <br /><input class="TitreThumbnail" type="text" name="alt_team_thumbnail_image'.$jj.'" id="alt_team_thumbnail_image'.$jj.'" value="'.$meta_titre.'" /></div>';
		$output .= '<div class="Input_Team_Thumbnail"> Supprimer l\'image? <br /><input class="DeleteThumbnail" type="checkbox" name="team_thumbnail_delete'.$jj.'"  id="team_thumbnail_delete'.$jj.'" /></div>';
		$output .= '<div class="clear"></div>';
		$output .= '</div>';
		
		$jj++;
	}
	$output .= '<input type="hidden" name="team_thumbnail_manual_save_flag" value="true" />';
	$output .= '<input type="hidden" name="team_thumbnail_nb_image" value="'.$jj.'" />';
	echo $output;
   // echo ': <input type="file" name="team_thumbnail_image" id="team_thumbnail_image" />';
/*
    // See if there's a status message to display (we're using this to show errors during the upload process, though we should probably be using the WP_error class)
    $status_message = get_post_meta($post->ID,'_xxxx_attached_image_upload_feedback', true);

    // Show an error message if there is one
    if($status_message) {

        echo '<div class="upload_status_message">';
            echo $status_message;
        echo '</div>';

    }

    // Put in a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    echo '<input type="hidden" name="team_thumbnail_manual_save_flag" value="true" />'
*/
}

function team_thumbnail_update_post($post_id, $post) {

    // Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
    // It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
    // post type in the passed object isn't "revision"
    $post_type = $post->post_type;

    // Make sure our flag is in there, otherwise it's an autosave and we should bail.
    if($post_id && isset($_POST['team_thumbnail_manual_save_flag']) && isset($_POST['team_thumbnail_nb_image'])) { 
		
        // Logic to handle specific post types
       if ($post_type != 'revisionz') {

            // If this is a post. You can change this case to reflect your custom post slug
          //  case 'post':

                // HANDLE THE FILE UPLOAD
				$ii = 0;
				while ($ii < $_POST['team_thumbnail_nb_image']) {

				
					//Supression de l'image!
					if (isset($_POST['team_thumbnail_delete'.$ii]) && $_POST['team_thumbnail_delete'.$ii] == 'on') {
						$existing_uploaded_image = (int) get_post_meta($post_id,'team_thumbnail_image'.$ii, true);
							if(is_numeric($existing_uploaded_image)) {
								wp_delete_attachment($existing_uploaded_image);
								update_post_meta($post_id,'team_thumbnail_image'.$ii,'');
							}
					}
					
					//Pour le titre de l'image
					if (isset($_POST['alt_team_thumbnail_image'.$ii])) {
						$file_title_for_media_library = utf8_decode($_POST['alt_team_thumbnail_image'.$ii]);
					}
					else {
						$file_title_for_media_library = 'Image'.$ii.'Post'.$post_id;
					}
					update_post_meta($post_id,'alt_team_thumbnail_image'.$ii,htmlentities($file_title_for_media_library));
				
					// If the upload field has a file in it
					if(isset($_FILES['team_thumbnail_image'.$ii]) && ($_FILES['team_thumbnail_image'.$ii]['size'] > 0)) {

					
						// Get the type of the uploaded file. This is returned as "type/extension"
						$arr_file_type = wp_check_filetype(basename($_FILES['team_thumbnail_image'.$ii]['name']));
						$uploaded_file_type = $arr_file_type['type'];

						// Set an array containing a list of acceptable formats
						$allowed_file_types = array('image/jpg','image/jpeg','image/gif','image/png');

						// If the uploaded file is the right format
						if(in_array($uploaded_file_type, $allowed_file_types)) {

							// Options array for the wp_handle_upload function. 'test_upload' => false
							$upload_overrides = array( 'test_form' => false ); 

							// Handle the upload using WP's wp_handle_upload function. Takes the posted file and an options array
							$uploaded_file = wp_handle_upload($_FILES['team_thumbnail_image'.$ii], $upload_overrides);

							// If the wp_handle_upload call returned a local path for the image
							if(isset($uploaded_file['file'])) {

								// The wp_insert_attachment function needs the literal system path, which was passed back from wp_handle_upload
								$file_name_and_location = $uploaded_file['file'];

								// Generate a title for the image that'll be used in the media library
								
								// Set up options array to add this file as an attachment
								$attachment = array(
									'post_mime_type' => $uploaded_file_type,
									'post_title' => htmlentities(addslashes($file_title_for_media_library)),
									'post_content' => '',
									'post_status' => 'inherit'
								);

								// Run the wp_insert_attachment function. This adds the file to the media library and generates the thumbnails. If you wanted to attch this image to a post, you could pass the post id as a third param and it'd magically happen.
								$attach_id = wp_insert_attachment( $attachment, $file_name_and_location, $post_id );
								require_once(ABSPATH . "wp-admin" . '/includes/image.php');
								$attach_data = wp_generate_attachment_metadata( $attach_id, $file_name_and_location );
								wp_update_attachment_metadata($attach_id,  $attach_data);

								// Before we update the post meta, trash any previously uploaded image for this post.
								// You might not want this behavior, depending on how you're using the uploaded images.
								$existing_uploaded_image = (int) get_post_meta($post_id,'team_thumbnail_image'.$ii, true);
								if(is_numeric($existing_uploaded_image)) {
									wp_delete_attachment($existing_uploaded_image);
								}

								// Now, update the post meta to associate the new image with the post
								update_post_meta($post_id,'team_thumbnail_image'.$ii,$attach_id);
								

								// Set the feedback flag to false, since the upload was successful
								$upload_feedback = false;


							} else { // wp_handle_upload returned some kind of error. the return does contain error details, so you can use it here if you want.

								$upload_feedback = 'Un probl&egrave;me est survenu lors du transfert.';
								//update_post_meta($post_id,'team_thumbnail_image'.$ii,$attach_id);

							}

						} else { // wrong file type

							$upload_feedback = 'T&eacute;l&eacute;verser seulement des images de format .jpg, .jpeg, .gif et .png.';
							//update_post_meta($post_id,'team_thumbnail_image'.$ii,$attach_id);

						}
						
					} else { // No file was passed

						$upload_feedback = false;

					}
					$ii++;
				}
        }
	}	
}
add_action('save_post','team_thumbnail_update_post',1,2);


/*function add_team_thumbnail_column($posts_columns) {
 
       // Add a new column
	$posts_columns['team_thumbnail'] = _x('Image(s) associ&eacute;(s)', 'column name');
 
	return $posts_columns;
}*/

	function add_team_thumbnail_column($columns) {

		$new_columns = array();
	
		// Rajoute la colonne pour indiquer si l'image mobile est pr�sente
		foreach($columns as $key => $column) {
			
			if($key == 'author') {
				$new_columns['team_thumbnail'] = 'Image(s) associ&eacute;(s)';
			}
			
			$new_columns[$key] = $column;
		}
		
		return $new_columns;
	}
	//add_filter('manage_edit-les-portfolios_columns', 'AddPortfolioColumn');



function manage_attachment_team_thumbnail_column($column_name, $id) {
	switch($column_name) {
	case 'team_thumbnail':
	
		$settings = unserialize(get_option('team_thumbnail'));

		global $post_type;
		global $post;
		
		foreach($settings as $key => $setting  ) {
			if (strpos($key,$post->post_type) == true  ) {
				if ($setting != 'on' && $setting != '') {
					$arraykey[] = $setting;
				}	
			}	
		}
		
		//if (!empty($arraykey)) {
			//print_r($arraykey);
			$output = '';
			$jj = 0;
			$output .= '<div class="Small_Thumbnail">';	
			foreach ($arraykey as $un_image ) {
				$meta_titre = get_post_meta($post->ID, 'alt_team_thumbnail_image'.$jj, true);
				//$existing_image_id = get_post_meta($post->ID,'team_thumbnail_image'.$jj, true);
			
					
				$output .= '<div class="Small_Team_Thumbnail">'. team_the_thumb(false,false,'','team_thumbnail_image'.$jj,32,32,1,1,$post->ID) .'</div>';			
				
				
				$jj++;			
			}
			
			$output .= '</div>';		
			$output .= '<div class="clear"></div>';

			echo $output;
		//}
		
		break;
	default:
		break;
	}
 
}

///////////////////////////////////////////////////////////


/**
 * The configuration form.
 */
function team_thumbnail_admin() {
?>
<div class="wrap"> 

	<h2>TEAM Thumbnail</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_thumbnail_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_thumbnail') === false) {
			add_option('team_thumbnail', $settings);
		} else {
			update_option('team_thumbnail', $settings);
		}	
		
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	<?php
		
		
	}
		$settings = maybe_unserialize(get_option('team_thumbnail'));
	?>
	<form action="admin.php?page=team-thumbnail/team-thumbnail.php&amp;action=save" method="post" id="thumbnailselector">
		
	<p><strong><?php _e( 'S&eacute;lectionner les  customs posts/page avec lesquelles vous voulez avoir des images meta :' ); ?></strong></p>
	<?php
	

			$args=array(
			  'public'   => true
			); 
			$output = '';
								
			$yy = 0;			
			$custom_single_post=get_post_types($args, 'object');
			
			//Lister tous les customs single_post type
			foreach($custom_single_post as $single_post) {
				//$output .= '<input type="checkbox" name="'.$single_post.'" id="'.$single_post.'" /> <label for="'.$single_post.'">'.$single_post.'</label><br />';
				$output .= '<div class="PostSelector">';
				$output .= '<label for="team_thumbnail_'. $single_post->name .'">';
				$output .= '<input type="checkbox" class="checkbox" id="team_thumbnail_'. $single_post->name.'" name="team_thumbnail_'. $single_post->name.'" '. checked( (bool) $settings["team_thumbnail_" . $single_post->name], true,false ).'/>';		
				$output .= $single_post->labels->name;
				$output .= '</label><br/>';		
				$output .= 'Label image 1 : <input type="text" id="team_thumbnail_label1_'. $single_post->name.'" name="team_thumbnail_label1_'. $single_post->name.'" value="'. esc_attr($settings["team_thumbnail_label1_$single_post->name"]) .'" ><br/>';
				$output .= 'Label image 2 : <input type="text" id="team_thumbnail_label2_'. $single_post->name.'" name="team_thumbnail_label2_'. $single_post->name.'" value="'. esc_attr($settings["team_thumbnail_label2_$single_post->name"]) .'" ><br/>';
				$output .= 'Label image 3 : <input type="text" id="team_thumbnail_label3_'. $single_post->name.'" name="team_thumbnail_label3_'. $single_post->name.'" value="'. esc_attr($settings["team_thumbnail_label3_$single_post->name"]) .'" ><br/>';
				$output .= 'Label image 4 : <input type="text" id="team_thumbnail_label4_'. $single_post->name.'" name="team_thumbnail_label4_'. $single_post->name.'" value="'. esc_attr($settings["team_thumbnail_label4_$single_post->name"]) .'" ><br/>';
				$output .= 'Label image 5 : <input type="text" id="team_thumbnail_label5_'. $single_post->name.'" name="team_thumbnail_label5_'. $single_post->name.'" value="'. esc_attr($settings["team_thumbnail_label5_$single_post->name"]) .'" ><br/>';
				$output .= '</div>';				
				$yy++;
			}
			if ($yy == 0) {
				$output .= 'Il n\'existe aucun custom single_post type';
			}
			
			$output .= '<div class="clear"></div>';
			echo $output;
			
		?>
		
		<br />
	
		<input type="hidden" id="check" name="check" value="OK" />
		<input type="submit" value="Save" class="SaveJavascript" />
		
	</form>
</div>
<?php

}

//Pour le fichier CSS de la partie admin
function load_custom_team_thumbnail_admin_style(){
        wp_register_style( 'custom_team_thumbnail_admin_css', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))  . 'thumbnail_admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_team_thumbnail_admin_css' );
}
add_action('admin_enqueue_scripts', 'load_custom_team_thumbnail_admin_style');

function team_thumbnail_admin_menu()
{
    //add_options_page('TEAM Javascript options', 'Fichiers javascript', 8, __FILE__, 'team_thumbnail_admin');
	add_submenu_page('team_teampress','TEAM Thumbnail options', 'Thumbnails', 8, __FILE__, 'team_thumbnail_admin');
}

add_filter('admin_menu', 'team_thumbnail_admin_menu');

//add_action('pre_get_posts', 'team_thumbnail_metabox');
add_action('admin_enqueue_scripts', 'team_thumbnail_metabox');


?>