<?php
/*
Plugin Name: Team Menu Acess
Plugin URI: http://www.teamedc.com/
Description: Get all available roles (default + custom) and allow to hide some part of the admin menu to selected roles. Menu are dynamics.
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.10
Author URI: http://www.teamedc.com/
*/

/*Changelog 
1.0 : Created the plugin because adminize wasn't working well with Capabilities

1.1 : Addres dynamic $menu, because you don,t want to add in the plugin new tab which comme in if you add plugins...
*/

function team_remove_admin_bar() {
	global $wp_admin_bar;
		
	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_menuaccess'));

	if ($settings{'team_menuaccess_actif'} == 'on') {		
		
		//$wp_admin_bar->remove_menu('get-shortlink');
		$wp_admin_bar->remove_menu('dashboard');
		//$wp_admin_bar->remove_menu('my-account-with-avatar');
		$wp_admin_bar->remove_menu('appearance');
		$wp_admin_bar->remove_menu('themes');
		$wp_admin_bar->remove_menu('widgets');
		$wp_admin_bar->remove_menu('menus');
		$wp_admin_bar->remove_menu('background');
		$wp_admin_bar->remove_menu('header');
		$wp_admin_bar->remove_menu('wrap');
		$wp_admin_bar->remove_menu('search');
		$wp_admin_bar->remove_menu('button');
		$wp_admin_bar->remove_menu('adminbarsearch');
		//$wp_admin_bar->remove_menu('wp-logo');
		//$wp_admin_bar->remove_menu('wp-logo-default');
		//$wp_admin_bar->remove_menu('wp-logo-external');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('about');
		$wp_admin_bar->remove_menu('wporg');
		$wp_admin_bar->remove_menu('documentation');
		$wp_admin_bar->remove_menu('support-forums');
		$wp_admin_bar->remove_menu('feedback');
		//$wp_admin_bar->remove_menu('site-name');
		//$wp_admin_bar->remove_menu('site-name-default');
		//$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('new-content');
		$wp_admin_bar->remove_menu('new-content-default');
		$wp_admin_bar->remove_menu('new-post');
		$wp_admin_bar->remove_menu('new-media');
		$wp_admin_bar->remove_menu('new-link');
		$wp_admin_bar->remove_menu('new-page');
		$wp_admin_bar->remove_menu('new-user');
		$wp_admin_bar->remove_menu('updates');
		//$wp_admin_bar->remove_menu('top-secondary');
		//$wp_admin_bar->remove_menu('my-account');
		//$wp_admin_bar->remove_menu('user-actions');
		//$wp_admin_bar->remove_menu('user-info');
		//$wp_admin_bar->remove_menu('edit-profile');
		//$wp_admin_bar->remove_menu('logout');
		$wp_admin_bar->remove_menu('search');
		$wp_admin_bar->remove_menu('network-admin');
		$wp_admin_bar->remove_menu('w3tc');
		$wp_admin_bar->remove_menu('w3tc-default');
		$wp_admin_bar->remove_menu('w3tc-empty-caches');
		$wp_admin_bar->remove_menu('w3tc-faq');
		$wp_admin_bar->remove_menu('w3tc-support');
		$wp_admin_bar->remove_menu('cloudflare');
		$wp_admin_bar->remove_menu('cloudflare-default');
		$wp_admin_bar->remove_menu('cloudflare-my-websites');
		$wp_admin_bar->remove_menu('cloudflare-analytics');
		$wp_admin_bar->remove_menu('cloudflare-account');
		//$wp_admin_bar->remove_menu('wp_memory_db_indicator');
	}
}

//Fait le register et l'enqueue des javascripts 
function team_menu_access_activation() {

	ob_start();
	
	
	global $menu, $submenu, $user_ID;
	global $current_user;
	
	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_menuaccess'));
	
	if ($settings{'team_menuaccess_actif'} == 'on') {
		foreach ($menu as $single_menu) {
			$restricted[] = $single_menu[2];
		}
		
		$restricted = str_replace('.','%2E',$restricted);
		//print_r($restricted);
		//print_r($menu);
		//Array o� l'on peut rajouter des fonctionnalit�s � cacher au plugin			
		/*$restricted = array(	
			'edit_php',
			'categories_php',
			'upload_php',
			'link-manager_php',
			'edit_php?post_type=page',
			'edit-comments_php',
			'themes_php',
			'plugins_php',
			'tools_php',
			'options-general_php',
			'wpcf7',
			'sitepress-multilingual-cms/menu/overview_php',
			'separator1',
			'separator2',
			'separator-last'
		);	*/	
		
		
		
		//Permet de connaitre le r�le de l'usager actuel
		if ( is_user_logged_in() ) {
		//$userRole = ($current_user->data->wp_capabilities);
		//$userRole = ($current_user->roles[0]);
		$role = ($current_user->roles[0]);		
		}
		
		$settings['team_menuaccess_client_profile%2Ephp'] = 'on';
		
		$ii = 0;
		if ($settings != '') {
			foreach ($settings as $key => $setting) {
				//				print_r('<br />');
				//	print_r($key);
				//	print_r('<br />');
				$key = substr($key,16); //cleanup identifiant
				$poskey = strpos($key,'_');
				$rolesetting = substr($key,0,$poskey); //Avoir le role � qui l'appliquer
				
				if ($rolesetting == $role ) { //Ce setting s'applique � l'usager actuel
				//print_r($rolesetting);
					$key = substr($key,($poskey+1)); //Avoir l'onglet � cacher
					//print_r($key);
					//print_r($restricted);

					if (in_array($key,$restricted)) {
						//On enleve le choix de l'array, car on l'utilisera pour savoir les onglets cach�s
						$poskey = array_search($key,$restricted);
						unset($restricted[$poskey]);
					}	
				}
			}
			
			//print_r($restricted);
			
			$restricted = str_replace('%2E','.',$restricted);
			$ii = 0;
			//On utilise les pointeurs de l'array pour enlever les pages...
			//print_r($restricted);
			reset($restricted); //Aller au d�but de l'array
			while (count($restricted) > $ii) {		
				remove_menu_page(current($restricted)); // Lecture de la valeur en cours
				next($restricted); //Avancer dans l'array
				$ii++;
			}
		}

		//global $wp_roles;
		//get_object_vars($wp_roles);

		
		//print_r($submenu['index.php']);
		
		/*if (count($restricted) >= 1) {
		
			$the_user = new WP_User($user_ID);
			//$restricted = array('wpcf7', 'sitepress-multilingual-cms/menu/overview.php', 'separator-last', 'separator1', 'separator2',  'edit.php', 'categories.php', 'upload.php', 'link-manager.php', 'edit-comments.php', 'themes.php', 'plugins.php', 'tools.php', 'options-general.php');
			
			//$restricted_str = 'widgets.php';
			$restricted_str = '';
			end ($menu);
			$menu_count = count($menu);

			for($j = $menu_count; $j > 0; $j--) {

				$menu_item = $menu[key($menu)];
				$restricted_str .= '|'.$menu_item[2];
				
				// Traitement sp�cial : dashboard devient accueil
				if($menu_item[2] == 'index.php') {
					$menu[key($menu)][0] = 'Accueil';
					unset($submenu[$menu_item[2]]);
				}
				
				if(in_array($menu_item[2] , $restricted)){
					$submenu_item = $submenu[$menu_item[2]];
					if($submenu_item != NULL){
						$tmp = $submenu_item;
						$max = array_pop(array_keys($tmp));
						for($i = $max; $i > 0;$i-=5){

							 if($submenu_item[$i] != NULL){
								$restricted_str .= '|'.$submenu[$menu_item[2]][$i][2];
								unset($submenu[$menu_item[2]][$i]);
							}
						}
					}
					
					unset($menu[key($menu)]);
					prev($menu);
					// Enl�ve un bug si on supprime le dernier item
					if(!current($menu)) end($menu);
				} else {
					prev($menu);
				}
			}	
		
		}*/
	}	
	
}

/**
 * The configuration form.
 */
function team_menu_access() {
	
	global $menu, $submenu, $user_ID;
	global $current_user;
	wp_enqueue_script("jquery");
	
	//print_r('<br /><br/><br/>');
	//print_r($menu);
?>
<div class="wrap"> 

	<h2>TEAM Menu Access</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;

	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_menuaccess_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_menuaccess') === false) {
			add_option('team_menuaccess', $settings);
		} else {
			update_option('team_menuaccess', $settings);
		}	
		
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	<?php

	}
		$settings = maybe_unserialize(get_option('team_menuaccess'));
	?>		
	<form action="admin.php?page=team-menu-access/team-menuaccess.php&amp;action=save" method="post" id="javascriptselector">
		
	<p><strong><?php _e( 'D&eacute;cocher les pages que vous voulez cacher aux usagers :' ); ?></strong></p>
	<?php
					
	/*Au cas ou d'un menu dynamique...ne fonctionne pas tr�s bien!*/				
	//print_r($menu);
	//echo '<pre>';
	//print_r($menu);
	
	foreach ($menu as $single_menu) {
		//print_r($single_menu);
		if ($single_menu[0] == '') {
			$single_menu[0] = 'S&eacute;parateur';
		}
		$restricted[] = array($single_menu[2],$single_menu[0]);
	}

	
	
	//Array o� l'on peut rajouter des fonctionnalit�s � cacher au plugin			
	/*$restricted = array();	
	$restricted[] = array('edit.php','Articles');
	$restricted[] = array('categories.php','Cat&eacute;gories');
	$restricted[] = array('upload.php','M&eacute;dias');
	$restricted[] = array('link-manager.php','Liens');
	$restricted[] = array('edit.php?post_type=page','Pages');
	$restricted[] = array('edit-comments.php','Commentaires');	
	$restricted[] = array('themes.php','Th&egrave;mes');
	$restricted[] = array('plugins.php','Plugin');
	$restricted[] = array('tools.php','Outils');
	$restricted[] = array('options-general.php','R&eacute;glages');	
	$restricted[] = array('wpcf7','Contact form 7');
	$restricted[] = array('sitepress-multilingual-cms/menu/overview.php','WPML');	
	$restricted[] = array('separator1','Premier s&eacute;parateur');
	$restricted[] = array('separator2','Second s&eacute;parateur');
	$restricted[] = array('separator-last','Dernier s&eacute;parateur');			*/		
	
	$liste_role = get_user_roles(); //Aller afficher la lsite des r�les avec les options pour chacun
	
	$output = '';
	$ii_typeuser = 0;
	$ii_restrict = 0;
	
	//On affiche les options pour chacun des types d'usagers possibles
	foreach ($liste_role as $role) {
		$output .= '<div class="TeamAccesMenu">';
		$output .= '<h2 class="TeamMenuAccesRole">'.$role.'</h2>';
		$ii_restrict = 0;
		foreach ($restricted as $restriction) {
			$restriction = str_replace('.','%2E',$restriction);
			$output .= '<label for="team_menuaccess_'. $role .'_'. $restriction[0] .'">';
			if ($role == 'administrator') {
				$output .= '<input readonly="readonly" type="checkbox" checked="checked" class="checkbox" id="team_menuaccess_'. $role .'_'. $restriction[0] .'" name="team_menuaccess_'. $role .'_'. $restriction[0].'" '. checked( (bool) $settings["team_menuaccess_". $role .'_'. $restriction[0]], true, false ) .'/>';
			}
			else {
				$output .= '<input type="checkbox" class="checkbox" id="team_menuaccess_'. $role .'_'. $restriction[0] .'" name="team_menuaccess_'. $role .'_'. $restriction[0].'" '. checked( (bool) $settings["team_menuaccess_". $role .'_'. $restriction[0]], true, false ) .'/>';
			}
			$output .= $restriction[1];
			$output .= '</label><br/>';
			$ii_restrict++;
		}
		$output .= '</div>';
		$ii_typeuser++;		
	}	
	
	echo $output;
	
	echo '<div class="clear"></div>';
	echo '<div class="ActivePlugin">';
	echo 'Team Menu Acces Actif : <input type="checkbox" class="checkbox" id="team_menuaccess_actif" name="team_menuaccess_actif" '.  checked( (bool) $settings["team_menuaccess_actif"], true, false ) .'/>';
	echo '</div><div class="clear"></div>';
	
	
	?>
		<input type="hidden" id="check" name="check" value="OK" />
		<input type="submit" value="Save" class="SaveJavascript" />
		
	</form>
</div>

<script type="text/javascript">
// Lorsque jQuery est pret, active les effets custom
jQuery(document).ready(function() {
	jQuery(':checkbox[readonly=readonly]').click(function(){
            return false;
        });
});
</script>

<?php
	
}

//Va chercher tous les roles (m�me custom capabilities)
function get_user_roles() {
	global $wp_roles;
	
	$user_roles = array();
	
	if ( isset( $wp_roles->roles) && is_array( $wp_roles->roles) ) {
		foreach ( $wp_roles->roles as $role => $data) {
			array_push( $user_roles, $role );
		}
	}
	
	return $user_roles;
}

//if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
//if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}

function team_menu_access_menu()
{
    //add_options_page('TEAM Menu Access options', 'Menu Admin', 8, __FILE__, 'team_menu_access');
	//add_users_page('TEAM Menu Access options', 'Menu Admin', 8, __FILE__, 'team_menu_access');
	 add_submenu_page('team_teampress','TEAM Menu Access options', 'Menu Admin', 8, __FILE__, 'team_menu_access');
}

//Pour le fichier CSS de la partie admin
function load_custom_wp_admin_style(){
        wp_register_style( 'custom_wp_admin_css_a', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))  . 'team-menuaccess.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css_a' );
}
add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');

add_filter('admin_menu', 'team_menu_access_menu',2);
add_action('admin_menu', 'team_menu_access_activation',20);
add_action('wp_before_admin_bar_render', 'team_remove_admin_bar');

?>