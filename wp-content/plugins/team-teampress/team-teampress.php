<?php
/*
Plugin Name: Team Teampress
Plugin URI: http://www.teamedc.com/
Description: Contain options which permit customization of Teampress theme. NE PAS D&Egrave;SACTIVER!
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.2
Author URI: http://www.teamedc.com/
*/

/*Changelog 
1.0 	 	- Allow to change BGColor of NoIE6 Page & automatically change color of associated image.
1.1			- Added TinyMCE style editor. You can now change de label!
1.2			- Addes a adress function for user in footer (or everywhere!), can always be useful.
*/

//NoIE6 - BG color 
function team_noie_six() {	
	if (is_page('noie6')) {
		ob_start();
		
		global $post;
		
		$settings = unserialize(get_option('team_teampress'));
							
		$bgcolor_hex = $settings{'team_teampress_bgcolor_noie6'};
		$_SESSION['bgcolor_hex'] = $bgcolor_hex;
		
		//Pour modification de l'image avec librarie GD
		$path = get_bloginfo('template_directory')  . '/images/noie6/logo.php' ;  
		$image_size = getimagesize($path);

		$fin_path = str_replace('http:/','http://',$path);
		$fin_path = str_replace('//','/',$fin_path);
		$fin_path = str_replace('\\','/',$fin_path);		
		
		//On change la bg color
		 ?>
		
		<script type="text/javascript">

		jQuery(document).ready(function() {	
		
			//Bg color
			var bgcolor = '<?php echo $bgcolor_hex; ?>';
			jQuery('body').css('background-color','#'+bgcolor);
			
			
			//Image logo
			var path_logo = '<?php echo $fin_path; ?>';
			var width_image = '<?php echo $image_size[0]; ?>';
			
			jQuery('div.Antiiepopup').css('width', width_image);
			jQuery('div.Antiiepopup').html('<img src="'+path_logo+'?bgcolorhex='+bgcolor+'" />');

		});


		</script>	
	
	<?php }
	
}

// Fonction pour les titre customs de tinymce
function team_custom_tinymce() {		
	add_filter( 'gettext', 'theme_change_tinymce_text', 20, 3 );
}

function theme_change_tinymce_text($translated_text, $text, $domain) {
		ob_start();
		$settings = maybe_unserialize(get_option('team_teampress'));
		
		$titre1 = $settings{'team_teampress_titre1'};
		$titre2 = $settings{'team_teampress_titre2'};
		$titre3 = $settings{'team_teampress_titre3'};
		$titre4 = $settings{'team_teampress_titre4'};
		$titre5 = $settings{'team_teampress_titre5'};
		$titre6 = $settings{'team_teampress_titre6'};

        switch ( $translated_text ) {

            case 'Titre 1' :
                $translated_text = $titre1;
                break;

            case 'Titre 2' :
                $translated_text = $titre2;
                break;
				
			case 'Titre 3' :
				$translated_text = $titre3;
				break;
			
			case 'Titre 4' :
				$translated_text = $titre4;
				break;
			
			case 'Titre 5' :
				$translated_text = $titre5;
				break;
				            
			case 'Titre 6' :
				$translated_text = $titre6;
				break;
				
        }

    return $translated_text;
}

// Fonction pour retourner l'adresse lorsque entr�e dans la partie admin
function team_infoadresse() {

	function team_adresse($info_adresse,$echo = true, $class= '') {
		ob_start();
		$settings = maybe_unserialize(get_option('team_teampress'));
		
		$langue = $language_code = icl_t('System', 'language_code', 'fr');
		
		$information = $settings{'team_teampress_'.$info_adresse .'_'. $langue};
		//Si on a entree une demande vide
		if ($information == '') {
			echo 'Cette information d\'adresse n\'existe pas !';
			return '';
		}
		$information = '<span class="'.$class.'">'.$information.'</span>';		

		if ($echo == true) {
			echo $information;
		}
		return $information;
		
	}	
}




/**
 * The configuration form.
 */
function team_teampress_admin() {
?>
<div class="wrap"> 

	<h1>TEAM Teampress</h1>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_teampress_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_teampress') === false) {
			add_option('team_teampress', $settings);
		} else {
			update_option('team_teampress', $settings);
		}	
		
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	<?php	
	}
		$settings = maybe_unserialize(get_option('team_teampress'));
	?>
	<form action="admin.php?page=team_teampress&amp;action=save" method="post" id="javascriptselector">
	
	<div class="BorderLimit">	
	<h2>No IE 6</h2>

	<p>
		<label for="team_teampress_bgcolor_noie6">
			<?php _e( ' Background color (code hexad&eacute;cimal) : ' ); ?><br/>
			#<input type="text" class="widefat" style="width:200px;" id="team_teampress_bgcolor_noie6" name="team_teampress_bgcolor_noie6" value="<?php echo esc_attr($settings["team_teampress_bgcolor_noie6"]); ?>" />
		</label><br/>
	</p>	
	</div>
	
	<div class="BorderLimit">	
	<h2>Titre custom TinyMCE</h2>

	<p>
		<label for="team_teampress_titre1">
			<?php _e( ' Titre 1 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre1" name="team_teampress_titre1" value="<?php echo esc_attr($settings["team_teampress_titre1"]); ?>" />
		</label><br/>
	</p>	
		<p>
		<label for="team_teampress_titre2">
			<?php _e( ' Titre 2 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre2" name="team_teampress_titre2" value="<?php echo esc_attr($settings["team_teampress_titre2"]); ?>" />
		</label><br/>
	</p>	
		<p>
		<label for="team_teampress_titre3">
			<?php _e( ' Titre 3 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre3" name="team_teampress_titre3" value="<?php echo esc_attr($settings["team_teampress_titre3"]); ?>" />
		</label><br/>
	</p>	
		<p>
		<label for="team_teampress_titre4">
			<?php _e( ' Titre 4 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre4" name="team_teampress_titre4" value="<?php echo esc_attr($settings["team_teampress_titre4"]); ?>" />
		</label><br/>
	</p>	
		<p>
		<label for="team_teampress_titre5">
			<?php _e( ' Titre 5 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre5" name="team_teampress_titre5" value="<?php echo esc_attr($settings["team_teampress_titre5"]); ?>" />
		</label><br/>
	</p>	
		<p>
		<label for="team_teampress_titre6">
			<?php _e( ' Titre 6 : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_teampress_titre6" name="team_teampress_titre6" value="<?php echo esc_attr($settings["team_teampress_titre6"]); ?>" />
		</label><br/>
	</p>	
	</div>
	
	<div class="BorderLimit">	
		<h2>Informations sur le client</h2>
		<div class="LeftContent">
			<h3>Fr</h3>
			<p>
				<label for="team_teampress_adresse_fr">
					<?php _e( ' Adresse : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_adresse_fr" name="team_teampress_adresse_fr" value="<?php echo esc_attr($settings["team_teampress_adresse_fr"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_telephone_fr">
					<?php _e( ' T&eacute;l&eacute;phone : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_telephone_fr" name="team_teampress_telephone_fr" value="<?php echo esc_attr($settings["team_teampress_telephone_fr"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_telephonesf_fr">
					<?php _e( ' T&eacute;l&eacute;phone sans frais : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_telephonesf_fr" name="team_teampress_telephonesf_fr" value="<?php echo esc_attr($settings["team_teampress_telephonesf_fr"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_fax_fr">
					<?php _e( ' Fax : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_fax_fr" name="team_teampress_fax_fr" value="<?php echo esc_attr($settings["team_teampress_fax_fr"]); ?>" />
				</label><br/>
			</p>
			<p>
				<label for="team_teampress_courriel_fr">
					<?php _e( ' courriel : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_courriel_fr" name="team_teampress_courriel_fr" value="<?php echo esc_attr($settings["team_teampress_courriel_fr"]); ?>" />
				</label><br/>
			</p>
		</div> <!-- LeftContent -->	
		
		<div class="RightContent">
			<h3>En</h3>
			<p>
				<label for="team_teampress_adresse_en">
					<?php _e( ' Adresse En : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_adresse_en" name="team_teampress_adresse_en" value="<?php echo esc_attr($settings["team_teampress_adresse_en"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_telephone_en">
					<?php _e( ' T&eacute;l&eacute;phone En : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_telephone_en" name="team_teampress_telephone_en" value="<?php echo esc_attr($settings["team_teampress_telephone_en"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_telephonesf_en">
					<?php _e( ' T&eacute;l&eacute;phone sans frais En : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_telephonesf_en" name="team_teampress_telephonesf_en" value="<?php echo esc_attr($settings["team_teampress_telephonesf_en"]); ?>" />
				</label><br/>
			</p>	
			<p>
				<label for="team_teampress_fax_en">
					<?php _e( ' Fax En : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_fax_en" name="team_teampress_fax_en" value="<?php echo esc_attr($settings["team_teampress_fax_en"]); ?>" />
				</label><br/>
			</p>
			<p>
				<label for="team_teampress_courriel_en">
					<?php _e( ' courriel En : ' ); ?><br/>
					<input type="text" class="widefat" style="width:300px;" id="team_teampress_courriel_en" name="team_teampress_courriel_en" value="<?php echo esc_attr($settings["team_teampress_courriel_en"]); ?>" />
				</label><br/>
			</p>
		</div> <!-- RightContent -->

		<div class="InfoFonction">
			<h3>Exemple</h3>
			<p>team_adresse('adresse');</p>
			<p>team_adresse('telephone');</p>
			<p>team_adresse('telephonesf');</p>
			<p>team_adresse('fax');</p>
			<p>team_adresse('courriel');</p>
		</div> <!-- InfoFonction -->
		
		<div class="clear"></div>
	
	</div> <!-- BorderLimit -->

	<input type="hidden" id="check" name="check" value="OK" />
	<input type="submit" value="Save" class="SaveJavascript" />
		
	</form>
</div>
<?php

}


function colormatch($image,$x,$y,$hex) {

	$rgb = imagecolorat($image,$x,$y);
	$r = ($rgb >> 16) & 0xFF;
	$g = ($rgb >> 8) & 0xFF;
	$b = $rgb & 0xFF;
	 
	$r2 = hexdec(substr($hex,0,2));
	$g2 = hexdec(substr($hex,2,2));
	$b2 = hexdec(substr($hex,4,6));

	if( $r == $r2 && $b == $b2 && $g == $g2 ) {
		return true;
	}
	else {
		return false;
	}
}

//Pour le fichier CSS de la partie admin
function load_custom_team_wp_admin_style(){
        wp_register_style( 'custom_team_wp_admin_css', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))  . 'team-teampress.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_team_wp_admin_css' );
		wp_register_style( 'custom_team_wp_admin_css2', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))  . 'team-admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_team_wp_admin_css2' );
}
add_action('admin_enqueue_scripts', 'load_custom_team_wp_admin_style');

function team_teampress_admin_menu()
{	
//Page gros menu Teampress
	add_menu_page('Teampress','Teampress','team_user','team_teampress','team_teampress_admin',plugin_dir_url( __FILE__ ).'medaillonteam.png',55);
    //add_submenu_page('team_teampress','TEAM Teampress options', 'Th&egrave;me Teampress', 8, __FILE__, 'team_teampress_admin');
}

add_filter('admin_menu', 'team_teampress_admin_menu',1);



//Filtre NoIE6
add_action('wp_head','team_noie_six');
add_action('admin_head','team_custom_tinymce');
add_action('wp_head','team_infoadresse');


?>