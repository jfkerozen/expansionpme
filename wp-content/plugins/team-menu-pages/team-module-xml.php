<?php 
/*
	Description : Module XML pour Team Menu Pagen servant � g�n�rer un XML SiteMap
	Author: Patrick Cyr
	Contributor : Nicolas Morin
	Version: 1.00
*/
	$frontpage_id = get_option('page_on_front');
	$pages_count = count($pages);

	if($pages_count) {
		//Balise pour SiteMap XML	
		$xml_sitemap = new SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"></urlset>');
		
		//URL avec Priorite, Frequency, etc.			
		$ii = 0;
		foreach($pages as $page) {
				
			$page_xml = $xml_sitemap->addChild('url');
			$page_xml->addChild('loc',get_page_link($page->ID));	 
			$page_xml->addChild('changefreq','monthly');
			$page_xml->addChild('lastmod',$page->post_modified);

			//Priorite selon la page
			if ($page->ID == $frontpage_id) { //Page Accueil
				$page_xml->addChild('priority','1.00');
			}
			else if ($page->post_parent != 0) { //Sous-page
				$page_xml->addChild('priority','0.5');
			}
			else { //Page normales
				$page_xml->addChild('priority','0.75');
			}
			$ii++;
		}	
	}

	echo $xml_sitemap->asXML();	
?>