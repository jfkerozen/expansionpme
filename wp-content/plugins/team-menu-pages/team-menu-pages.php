<?php
/*
Plugin Name: Team Menu (Pages)
Plugin URI: http://www.teamedc.com/
Description: Adds a widget that can display a special menu (display categories).
Author: Nicolas Morin
Contributor : Jimmy Lavoie (some updates)
Version: 1.03
Author URI: http://www.teamedc.com/
*/

class TeamMenuPages extends WP_Widget {

function TeamMenuPages() {
	// Constructeur
	parent::WP_Widget(false, $name='Team Menu (Pages)');
}

/**
 * Displays category posts widget on blog.
 */
function widget($args, $instance) {
	global $post;
	global $wp_query;
	global $term;
	global $taxonomy;
	global $cptll;
	
	extract( $args );
	
	// Construit la liste des pages � inclure
	$include = '';
	foreach($instance as $key => $value) {
		if(substr($key, 0, 13) == 'include_page_') {
			$include .= substr($key, 13) . ',';
		}
	}
	// Enl�ve la derni�re virgule
	$include = substr($include, 0, -1);
	
	$params = array(
			'exclude' => $instance["exclude"] . ',1',
			'include' => $include,
			'sort_column' => 'menu_order' //Edited by J.L.

				);
		
	// Get array of pages.
	// Seulement si au moins 1 item est coch�
	if($include) { 
		$pages = get_pages($params);
	} else {
		echo "[Aucun menu ne s'affiche puisqu'aucune page n'a &eacute;t&eacute; coch&eacute;e dans le widget.]";
	}
	
	$pages_count = count($pages);
	$ii = 0;
	$kk = 0;

	if($instance['xml_sitemap']){
		include_once('team-module-xml.php');
	}
	else {	
		echo $before_widget;
		
		// Widget title : N/A
		//echo $before_title;
		//echo $after_title;

			//2011-16-02 P.C : Ajout d'un titre aux colonnes selon la langue
			$langue = icl_t('System', 'language_code', utf8_encode('fr'));
			if ($langue == 'fr') {
				echo '<div id="TeamMenuPagesTitle-'. $cptll .'" class="TeamMenuPagesTitle">'. do_shortcode($instance['french_title']) .'</div>'; 
			}
			else if ($langue == 'en') {
				echo '<div id="TeamMenuPagesTitle-'. $cptll .'" class="TeamMenuPagesTitle">'. do_shortcode($instance['english_title']) .'</div>'; 
			}
			$cptll++;		
		
		// Construit le menu
		echo "<ul class='TeamMenuPages'>\n";
		
		if($pages_count) {		
			if($instance['columns'] == 1) {
				$instance['columns'] = 0;
			}
		
			if($instance['columns']) {
				$items_per_column = round($pages_count / $instance['columns']);
			}
		
			$root_term = GetRootTermSlug();
			$jj = 1;
			foreach($pages as $page) {
				$ii++;
				$kk++;
				$cssId = '';
				
				// Ajoute le pr�fix � l'ID (css) si d�fini
				if($instance['css_prefix']) {
					$cssId = $instance['css_prefix'] . "-";
				}
				$cssId .= $page->post_name;
				$cssIdii = $cssId + $ii;

				$firstPageClass = (($kk == 1) ? ' FirstMenuItem' : '');
				$lastPageClass = (($kk == $pages_count) ? ' LastMenuItem' : '');
				$liaison_post = get_post_meta(team_get_page_fr($page->ID)->ID, 'select_link_post_type', TRUE);
				$post_parent = get_post($post->post_parent);
				$post_grandparent = get_post($postparent->post_parent);
				

		?>
			<li class="MenuPrimaire Menu<?php echo do_shortcode($cssIdii); ?> MenuItem<?php echo ((team_get_page_fr($page->ID)->post_name == $taxonomy && $taxonomy != '') || is_page($page->ID) || $post->post_parent == $page->ID) || ($post_parent->post_parent == $page->ID) || ($liaison_post == $post->post_type) ? ' SelectedMenu' : ''; echo $firstPageClass . $lastPageClass; ?>" style="<?php echo ((!$instance['vertical_menu']) ? 'float:left;' : ''); ?>"><?php echo do_shortcode($instance["html_before_outside"]); ?><a title="<?php echo htmlentities(utf8_decode($page->post_title)); ?>" class="PrincipalMenu" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo do_shortcode($instance["html_before_inside"]); ?><?php echo $page->post_title; ?><?php echo do_shortcode($instance["html_after_inside"]); ?></a><?php echo do_shortcode($instance["html_after_outside"]); ?>
			
			<?php 

	// Si on utilise les pages enfants
				if($instance['show_children']){
					$pp = 0;
					$args = array( 'child_of' => $page->ID, 
								   'sort_column' => 'menu_order',
								   'hierarchical'	=> 0,
								   'parent'	=> $page->ID
								   );
				
					$MyPages = get_pages( $args );
					$count = count($MyPages);
					
					if ( $count ){
					
						$output = '';
						$output .= '<div class="FlecheMenu"></div><ul class="SousPages">';

							foreach( $MyPages as $SousPage ) {
							
								if ($pp % 2 == 0) {
									$ligne = 'Ligne1';
								}
								else {
									$ligne = 'Ligne2';
								}
								
								if($instance['show_third_children']){

									$args = array( 'child_of' => $SousPage->ID, 
									   'sort_column' => 'menu_order',
									   'hierarchical'	=> 0,
									   'parent'	=> $SousPage->ID
									   );
					
									$MyThirdPages = get_pages( $args );
									$countthird = count($MyThirdPages);

									if ( $countthird ){
										$qq = 0;

										if ($ii > 2) {
											$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
										}
										else {
											$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
										}
									
										$output .= '<ul class="TertiaryMenu">';
										foreach( $MyThirdPages as $SousThirdPage ) {
										
											if ($qq % 2 == 0) {
												$sous_ligne = 'Ligne1';
											}
											else {
												$sous_ligne = 'Ligne2';
											}
											
											$output .= '<li class="TertiaryLi '.$sous_ligne.'"><a href="'.get_page_link( $SousThirdPage->ID ).'">'.str_replace(' ','&nbsp;',$SousThirdPage->post_title).'</a></li>';
											$qq++;
										}
										$output .= '</ul><li class="clear"></li>';
									}									
									else {
										$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
									}

									$output .= '</li>';
								}
								else {
									if ($ii > 2) {
										$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
									}
									else {
										$output .= '<li class="SecondLi '.$ligne.'"><a href="'.get_page_link( $SousPage->ID ).'">'.$SousPage->post_title.'</a>';
									}
								}
								
								
								
								$pp++;
							}
						
						$output .= '</ul>';

						echo $output;
					}
				}
				
				if($instance['show_postscustom']) {
					// Conserve ancien post
					$old_post = clone($post);
				
					$args = array(
							'showposts'     => -1,
							'orderby'         => 'menu_order',
							'order'           => 'DESC',
							'post_type'       => $liaison_post
							 );
				
					$postsquery = new WP_Query($args);
					$postslist = $postsquery->posts;
					$count = count($postslist);
					
					if ( $count ){
					
						$output = '';
						$output .= '<ul class="SousPages">';

							foreach( $postslist as $post ) {
								setup_postdata($post);
								$output .= '<li><a href="'.get_permalink().'">'.$post->post_title.'</a></li>';
							}
						
						$output .= '</ul>';

						echo $output;
					}
					
					// R�active ancien post
					$post = clone($old_post);
				}
				
				?>
			</li>
			
			<?php if($ii < $pages_count && $instance['html_separator']) { // Affiche le s�parateur si sp�cifi�. Pas de s�parateur en dernier. ?>
			<li class="MenuSeparator MenuSeparator<?php echo $cssId; ?>" style="<?php echo ((!$instance['vertical_menu']) ? 'float:left;' : ''); ?>"><?php echo do_shortcode($instance['html_separator']); ?></li>
	<?php
				}
				if($instance['columns'] && $items_per_column == $ii && $ii != $pages_count) {
					$jj++;
					$kk = 0;
					echo '<li class="clear"></li></ul><ul class="TeamMenuPages Menu' . $jj . '">';
				}
			}
		}
		echo "<li class='clear'></li></ul><div class='clear'></div>\n";
		
		echo $after_widget;
	} // Fin IF XML Sitemap
	
	// Inclus le script jQuery de fit menu seulement si option activ�e
	if($instance['fit_sousmenu']) {
		$fit_sousmenu = 'true';
	}	
	else {
		$fit_sousmenu = 'false';
	}
	
	if($instance['fit_menu']) { ?>

	<script type="text/javascript">

	jQuery(document).ready(function() {

		var totalTextWidth = 0;
		var menuCount = 0;
		var totalListWidth = 0;
		var totalTextWidth2 = 0;
		var totalListWidth2 = 0;
		var fit_sousmenu = '<?php echo $fit_sousmenu; ?>';

		// Trouve l'espace occup� par le texte du menu
		jQuery('ul.TeamMenuPages').each(function() {
			totalTextWidth = 0;
			menuCount = 0;
			totalListWidth = jQuery(this).width();

			jQuery(this).find('li').each(function() {
				totalTextWidth += jQuery(this).width();
				menuCount++;
			});
	
			if(totalTextWidth < totalListWidth) {
				while(totalTextWidth < totalListWidth) {
					jQuery(this).find('li a.PrincipalMenu').each(function() {
						jQuery(this).width(jQuery(this).width() + 1);
						totalTextWidth++;
						if(totalTextWidth == totalListWidth) {
							return false;
						}
					});
				}
			} else if(totalTextWidth > totalListWidth) {
				while(totalTextWidth > totalListWidth) {
					jQuery(this).find('li a.PrincipalMenu').each(function() {
						jQuery(this).width(jQuery(this).width() - 1);
						totalTextWidth--;
						if(totalTextWidth == totalListWidth) {
							return false;
						}
					});
				}
			}
						
		}); 

		//Sous menu		
		if (fit_sousmenu == 'true') {
			//on ajuste la longeur de tous les sous-menus d�roulants
			jQuery('ul.TeamMenuPages li ul.SousPages').each(function() {
				jQuery(this).width(jQuery(this).parent().width());
			});
		}
		
		jQuery('ul.TeamMenuPages li ul.SousPages li').each(function() {
			jQuery(this).width(jQuery(this).parent().width());
		});
		
		/*
		var cptmenu = 0;
		jQuery('div#LowerMenuHeader ul.TeamMenuPages li ul.SousPages').each(function() {	
			jQuery(this).find('ul.TertiaryMenu').each(function() {
				if (cptmenu > 2) {
					LeftMenu = (jQuery(this).parent().parent().width() - 18);
					//jQuery(this).offset({right: LeftMenu });
					jQuery(this).css('right', LeftMenu );
				}
				else {
					LeftMenu = jQuery(this).parent().parent().width() - 18;
					jQuery(this).css('left', LeftMenu );
				}
				
			});
			
			cptmenu++;
		});
		*/
		
	});

	</script>		
<?php	
	}
	if($instance['mobile_menu']) {
	?>
	<script type="text/javascript">
	jQuery(document).ready(function() {
						
		//Menu Deroulant
		var window_width = window.innerWidth || document.documentElement.clientWidth;
		if (window_width <= 479) {
			//jQuery('div#MenuHeader').hide();
		}
		
		
		
	});

	</script>		

	<?php
	}
}


/**
 * Form processing... Dead simple.
 */
function update($new_instance, $old_instance) {
    //$new_instance["exclude"] = esc_attr($new_instance["exclude"]);
    //$new_instance["include"] = esc_attr($new_instance["include"]);
	$new_instance["english_title"] = $new_instance["english_title"];
	$new_instance["french_title"] = $new_instance["french_title"];
	$new_instance["fit_menu"] = (bool) $new_instance["fit_menu"];
	$new_instance["vertical_menu"] = (bool) $new_instance["vertical_menu"];
	$new_instance["show_children"] = (bool) $new_instance["show_children"];
	$new_instance["use_taxonomy"] = (bool) $new_instance["use_taxonomy"];
	$new_instance["mobile_menu"] = (bool) $new_instance["mobile_menu"];
	$new_instance["html_before_outside"] = $new_instance["html_before_outside"];
	$new_instance["html_after_outside"] = $new_instance["html_after_outside"];
	$new_instance["html_before_inside"] = $new_instance["html_before_inside"];
	$new_instance["html_after_inside"] = $new_instance["html_after_inside"];
	$new_instance["html_separator"] = $new_instance["html_separator"];
	$new_instance["css_prefix"] = $new_instance["css_prefix"];
	$new_instance["columns"] = (int)$new_instance["columns"];
	
	return $new_instance;
}

/**
 * The configuration form.
 */
function form($instance) {

	$params = array(
	
		'orderby' => 'menu_order'

			);
	
	// Get array of post info.
	$pages = get_pages($params);
	
?>
			<p><?php _e( 'Select pages to display in menu :' ); ?></p>
			<?php
				$team_languages = icl_get_languages();
				
				// Liste les pages disponibles dans chacune des langues
				foreach($team_languages as $team_language) {
			?>
			<p><img src="<?php echo $team_language['country_flag_url']; ?>" />&nbsp;<?php echo $team_language['native_name']; ?><br/>
			<?php  foreach($pages as $page) {
			
				// Trouve le page_id dans la langue courante $team_language
				$page_id = icl_object_id($page->ID, 'page', false, $team_language['language_code']);
				
				// Si page existe pas dans cette langue, ne l'affiche pas
				if($page_id != false) {
					if ($page->post_parent != 0) {
						$page_name = get_post($page->post_parent, 'OBJECT')->post_title . ' - ' . get_post($page_id, 'OBJECT')->post_title;
					}
					else {
						$page_name = get_post($page_id, 'OBJECT')->post_title;
					}	
			?>
				<label for="<?php echo $this->get_field_id("include_page_" . $page_id); ?>">
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("include_page_" . $page_id); ?>" name="<?php echo $this->get_field_name("include_page_" . $page_id); ?>"<?php checked( (bool) $instance["include_page_" . $page_id], true ); ?> />
					<?php echo $page_name; ?>
				</label><br/>
				
			<?php 	}
						} ?>
		<?php	} ?>
		</p>	

		<p> <!-- 2011-02-16 P.C : Ajout de titre FR/EN -->
			<label for="<?php echo $this->get_field_id("english_title"); ?>">
				<?php _e( 'English title' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("english_title"); ?>" name="<?php echo $this->get_field_name("english_title"); ?>" value="<?php echo esc_attr($instance["english_title"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("french_title"); ?>">
				<?php _e( 'French title' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("french_title"); ?>" name="<?php echo $this->get_field_name("french_title"); ?>" value="<?php echo esc_attr($instance["french_title"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("html_before_outside"); ?>">
				<?php _e( 'HTML Before (outside link)' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("html_before_outside"); ?>" name="<?php echo $this->get_field_name("html_before_outside"); ?>" value="<?php echo esc_attr($instance["html_before_outside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("html_after_outside"); ?>">
				<?php _e( 'HTML After (outside link)' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("html_after_outside"); ?>" name="<?php echo $this->get_field_name("html_after_outside"); ?>" value="<?php echo esc_attr($instance["html_after_outside"]); ?>" />
			</label>
		</p>	
		
		<p>
			<label for="<?php echo $this->get_field_id("html_before_inside"); ?>">
				<?php _e( 'HTML Before (inside link)' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("html_before_inside"); ?>" name="<?php echo $this->get_field_name("html_before_inside"); ?>" value="<?php echo esc_attr($instance["html_before_inside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("html_after_inside"); ?>">
				<?php _e( 'HTML After (inside link)' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("html_after_inside"); ?>" name="<?php echo $this->get_field_name("html_after_inside"); ?>" value="<?php echo esc_attr($instance["html_after_inside"]); ?>" />
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("html_separator"); ?>">
				<?php _e( 'HTML Separator (between items)' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("html_separator"); ?>" name="<?php echo $this->get_field_name("html_separator"); ?>" value="<?php echo esc_attr($instance["html_separator"]); ?>" />
			</label>
		</p>		
		
		<p>
			<label for="<?php echo $this->get_field_id("fit_menu"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("fit_menu"); ?>" name="<?php echo $this->get_field_name("fit_menu"); ?>"<?php checked( (bool) $instance["fit_menu"], true ); ?> />
				<?php _e( 'Fit menu into container' ); ?>
			</label>
		</p>	

		<p>
			<label for="<?php echo $this->get_field_id("fit_sousmenu"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("fit_sousmenu"); ?>" name="<?php echo $this->get_field_name("fit_sousmenu"); ?>"<?php checked( (bool) $instance["fit_sousmenu"], true ); ?> />
				<?php _e( 'Fit sous menu into container' ); ?>
			</label>
		</p>		

		<p>
			<label for="<?php echo $this->get_field_id("vertical_menu"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("vertical_menu"); ?>" name="<?php echo $this->get_field_name("vertical_menu"); ?>"<?php checked( (bool) $instance["vertical_menu"], true ); ?> />
				<?php _e( 'Vertical menu' ); ?>
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id("show_children"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_children"); ?>" name="<?php echo $this->get_field_name("show_children"); ?>"<?php checked( (bool) $instance["show_children"], true ); ?> />
				<?php _e( 'Show subpages' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_third_children"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_third_children"); ?>" name="<?php echo $this->get_field_name("show_third_children"); ?>"<?php checked( (bool) $instance["show_third_children"], true ); ?> />
				<?php _e( 'Tertiary menu' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("show_postscustom"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("show_postscustom"); ?>" name="<?php echo $this->get_field_name("show_postscustom"); ?>"<?php checked( (bool) $instance["show_postscustom"], true ); ?> />
				<?php _e( 'Show posts of custom post type (use with postlink!)' ); ?>
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("use_taxonomy"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("use_taxonomy"); ?>" name="<?php echo $this->get_field_name("use_taxonomy"); ?>"<?php checked( (bool) $instance["use_taxonomy"], true ); ?> />
				<?php _e( 'Show taxonomies' ); ?>
			</label>
		</p>		
		
		<p>
			<label for="<?php echo $this->get_field_id("mobile_menu"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("mobile_menu"); ?>" name="<?php echo $this->get_field_name("mobile_menu"); ?>"<?php checked( (bool) $instance["mobile_menu"], true ); ?> />
				<?php _e( 'Mobile menu' ); ?>
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("xml_sitemap"); ?>">
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id("xml_sitemap"); ?>" name="<?php echo $this->get_field_name("xml_sitemap"); ?>"<?php checked( (bool) $instance["xml_sitemap"], true ); ?> />
				<?php _e( 'XML Sitemap' ); ?>
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("css_prefix"); ?>">
				<?php _e( 'CSS menu id prefix' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("css_prefix"); ?>" name="<?php echo $this->get_field_name("css_prefix"); ?>" value="<?php echo esc_attr($instance["css_prefix"]); ?>" />
			</label>
		</p>		

		<p>
			<label for="<?php echo $this->get_field_id("columns"); ?>">
				<?php _e( 'Columns number' ); ?>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id("columns"); ?>" name="<?php echo $this->get_field_name("columns"); ?>" value="<?php echo (int) $instance["columns"]; ?>" />
			</label>
		</p>		
<?php

}

}

add_action( 'widgets_init', create_function('', 'return register_widget("TeamMenuPages");') );

?>
