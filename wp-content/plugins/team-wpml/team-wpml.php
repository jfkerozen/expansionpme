<?php
/*
Plugin Name: Team WPML
Plugin URI: http://www.teamedc.com/
Description: Adds an administrative section to add new WPML translation strings.
Author: Nicolas Morin
Version: 1.00
Author URI: http://www.teamedc.com/
*/

/**
 * The configuration form.
 */
function team_wpml_admin() {
?>
<div class="wrap"> 

	<h2>TEAM WPML</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 7) { 
			echo "You need at least Userlevel 7 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){
			echo "You need to be at least editor to modify the settings.";
			return;
		}
	}
	
	$context = '';
	$name = '';
	$value = '';
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
		
		// Copie les settings envoy�s dans le tableau, si leur clef est valide
		foreach($_POST as $setting => $value) {
			switch($setting) {
			
			case 'StringContext':
				$context = $value;
				break;
				
			case 'StringName':
				$name = strtolower($value);
				break;
				
			case 'StringString':
				$string = $value;
				break;
				
			case 'StringStringEn':
				$stringEn = $value;
				break;
				
			}
		}
		
		// Register la string
		if(!empty($context) && !empty($name) && !empty($string)) {
			$string_id = icl_register_string($context, $name, $string);
			
			if(!empty($stringEn)){
				icl_add_string_translation($string_id, 'en', $stringEn, 1);
			}
			
	?>
	<div id="messagewpml" class="updated">
		<p>String ajout&eacute;e. Contexte: [<?php echo $context; ?>], Nom: [<?php echo $name; ?>].<br/>
		Pour output: <input type="text" size="100" value="team_the_wpml('<?php echo $context; ?>', '<?php echo $name; ?>')"></input>
		</p>
	</div>
	<?php
		} else {
	?>
	<div id="message" class="error">
		<p>Erreur: un ou plusieurs param&egrave;tres sont vides.</p>
	</div>
<?php	
		}
	}	
?>
		<form action="admin.php?page=team-wpml/team-wpml.php&amp;action=save" method="post" id="stringadder">

		<p><strong><?php _e( "Ajouter une string pour WPML :" ); ?></strong></p>
		<table style="margin-bottom: 30px;"><tr><td style="padding-right:30px;">
			<label for="StringContext">
				<?php _e( 'Context' ); ?>: (System, Widgets, etc.)<br/>
				<input type="text" class="widefat" style="width:200px;" id="StringContext" name="StringContext" value="<?php echo $context; ?>" />
			</label>
		</td>
		<td style="padding-right:30px;">
			<label for="StringName">
				<?php _e( 'Name' ); ?>: (min. sans esp. ni acc.)<br/>
				<input type="text" class="widefat" style="width:200px;" id="StringName" name="StringName" value="<?php echo $name; ?>" />
			</label>
		</td>	
		<td style="padding-right:30px;">
			<label for="StringString">
				<?php _e( 'String' ); ?>: (Fran&ccedil;ais)<br/>
				<input type="text" class="widefat" style="width:200px;" id="StringString" name="StringString" value="<?php echo stripslashes($string); ?>" />
			</label>
		</td>
		<td style="padding-right:30px;">
			<label for="StringStringEn">
				<?php _e( 'StringEn' ); ?>: (Anglais)<br/>
				<input type="text" class="widefat" style="width:200px;" id="StringStringEn" name="StringStringEn" value="<?php echo stripslashes($stringEn); ?>" />
			</label>
		</td>
		</tr>
		</table>
		
		<input type="hidden" id="check" name="check" value="OK" />
		<input type="submit" value="Ajouter" class="SaveNewString" />
	</form>
</div>
<?php

}

function team_wpml_admin_menu()
{
    //add_options_page('TEAM WPML options', 'WPML Strings', 8, __FILE__, 'team_wpml_admin');
	add_submenu_page('team_teampress','TEAM WPML options', 'WPML Strings', 8, __FILE__, 'team_wpml_admin');
}

add_filter('admin_menu', 'team_wpml_admin_menu');

?>