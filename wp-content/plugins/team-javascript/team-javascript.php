<?php
/*
Plugin Name: Team Javascript
Plugin URI: http://www.teamedc.com/
Description: Permit to load easily javascript and jQuery files located in specified folder of the current theme used. Scripts will be loaded on each pages of the website via enqueue script.
Author: Patrick Cyr
Contributor : Nicolas Morin (some help)
Version: 1.00
Author URI: http://www.wordpress.teammwd.com/
*/

	/* Load requires files */	
	require_once WP_PLUGIN_DIR . '/team-javascript/includes/functions.php';
	require_once WP_PLUGIN_DIR . '/team-javascript/includes/metabox.php';
	require_once WP_PLUGIN_DIR . '/team-javascript/admin/admin_page.php';

	/* Add to menu */
	function tjs_admin_menu()
	{
		global $tjs_page;
		$tjs_page = add_options_page('TEAM Javascript options', 'Javascript Files', 'manage_options', 'team_javascript', 'team_javascript_admin');
		add_action( 'admin_enqueue_scripts', 'tjs_admin_css' );	 

	}
	add_filter('admin_menu', 'tjs_admin_menu');

	/* Load css only on the plugin page */
	function tjs_admin_css() {
	
		$screen = get_current_screen();
	
		if ( $screen->id != $my_admin_page ) {

			wp_register_style( 'tjs_admin_css', WP_PLUGIN_URL .'/team-javascript/css/team-javascript.css', false, '1.0.0' );
			wp_enqueue_style( 'tjs_admin_css' );
		}
	}
	
	add_action('get_header', 'tjs_activation');	


?>