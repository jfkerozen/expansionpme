<?php

/* Admin page */
function team_javascript_admin() {
?>
<div class="wrap"> 

	<h2>TEAM Javascript</h2>

	<?php
	//********************************************************//
	// Secure the admin area
	//********************************************************//
	
	global $user_level;
	get_currentuserinfo();
	if(get_option('db_version')< 4772){
		if ($user_level < 8) { //For old WP
			echo "You need at least Userlevel 8 to modify the settings.";
			return;
		}
	} else {
		global $current_user;
		if(!current_user_can('install_plugins')){  // Capabilities
			echo "You need to be at least administrator to modify the settings.";
			return;
		}
	}
	
	if($_GET['action'] == 'save' && $_POST['check'] == 'OK') {
	
		$settings = array();
	
		//Copy to array to be saved if keys are valid
		foreach($_POST as $setting => $value) {
			if(strpos($setting, 'team_javascript_') !== false) {
				$settings[$setting] = $value;
			}
		}
		
		$settings = serialize($settings);
		
		//Save options
		if(get_option('team_javascript') === false) {
			add_option('team_javascript', $settings);
		} else {
			update_option('team_javascript', $settings);
		}	
		
	?>
	<div id="message" class="updated">
		<p>Changes saved succesfully!</p>
	</div>	
	<?php
		
	}
		$settings = maybe_unserialize(get_option('team_javascript'));
	?>
	<form action="options-general.php?page=team_javascript&amp;action=save" method="post" id="javascriptselector">
		
	<?php
	
	//Construct the array of JS files found in the current theme
	$js_files = tjs_directory_loop();
	$file_ii = 0;
	$subfolder = '';
	
	$output = '';
	$output .= '<div class="BoxContent">';

	if (count($js_files) > 0) {
		echo '<h2>Javascript files found in current active theme</h2>';
		
		foreach ($js_files as $js_file) {
		
						$last_slash = strrpos($js_file['path'],'/') + 1;

						if ($subfolder != substr($js_file['path'],0, $last_slash) ) {
							$subfolder = substr($js_file['path'],0, $last_slash);
							$output .= '<h4>'. $subfolder .'</h4>';
						}
		
						$js_file_no_ext = substr($js_file['name'],0,-3); //Clean up name of files
						$js_file_no_ext = str_replace('.','-',$js_file_no_ext);
						//$js_file_no_ext = str_replace('-min','',$js_file_no_ext);
						
						$output .= '<input type="checkbox" class="checkbox" id="team_javascript_'. $js_file_no_ext .'" name="team_javascript_'. $js_file_no_ext .'" '.checked( (bool) $settings["team_javascript_" . $js_file_no_ext], true,false ).' >';
						$output .= '<label for="team_javascript_'. $js_file_no_ext .'" >';
						$output .= $js_file['name'];
						$output .= '</label>';						
						$output .= '<br />';

						$file_ii++;
		}
	}
	else {
		echo '<h3>No javascript files found. Does your current theme contain any Javascript file?</h3>';
	}
	
	$output .= '</div>';
	
	echo $output;
	submit_button();
	
	?>
	
	<input type="hidden" id="check" name="check" value="OK" />
	<!-- <input type="submit" value="Save" class="SaveJavascript" /> -->	
	
	</form>
</div>
<?php

}

?>