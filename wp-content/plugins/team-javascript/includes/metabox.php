<?php

add_action( 'save_post', 'tjs_metabox_save' );

/* Define the metabox. Need to be called after init so current_user_can function can be used */

function tjs_call_action() {


	if (current_user_can('administrator')) {
		add_action( 'add_meta_boxes', 'tjs_metabox' );

	}
}
add_action( 'plugins_loaded', 'tjs_call_action' );

/* Add the metabox */
function tjs_metabox() {


	
	add_meta_box(
		'tjs_box_id',
		__( 'Javascript files to load for this page', 'tjs_page_parameter' ), 
		'tjs_page_box',
		'page','normal'
	);
}

/* Print out the metabox */
function tjs_page_box( $post ) {
	/* Use nonce for verification */
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );

	ob_start();
	$settings = unserialize(get_option('team_javascript'));

	/* Construct an array of files to be loaded */
	if ($settings != '') {
		foreach($settings as $key => $value) {
			if(strpos($key, 'team_javascript_') !== false) {
				if($value) {
					$file_id = (substr($key, 16));
					if($file_id) {
						$include_files[] = $file_id;
						
					}
				}
			}
		}
	}	
	
	$main_post_id = tjs_main_postid($post->ID);

	/* Get saved meta */
	$tjs_saved_meta = get_post_meta($main_post_id,'tjs_meta',true);	
	$tjs_saved_meta = unserialize($tjs_saved_meta);
	
	$js_files = tjs_directory_loop();
	  
	$file_ii = 0;
	$subfolder = '';

	$output = '';
	$output .= '<div class="MetaboxContent">';

	if (count($js_files) > 0) {
		echo '<h2>Javascript files found in current active theme</h2>';
		
		foreach ($js_files as $js_file) {
		
						$last_slash = strrpos($js_file['path'],'/') + 1;

						if ($subfolder != substr($js_file['path'],0, $last_slash) ) {
							$subfolder = substr($js_file['path'],0, $last_slash);
							$output .= '<h4>'. $subfolder .'</h4>';
						}
						
		
						$js_file_no_ext = substr($js_file['name'],0,-3); //Clean up name of files
						$js_file_no_ext = str_replace('.','-',$js_file_no_ext);

						$checked = '';
						if ( in_array($js_file_no_ext, $include_files)) {
							$checked = 'checked="checked" disabled';
						}
						else if (tjs_array_search( 'team_javascript_'. $js_file_no_ext ,$tjs_saved_meta)) {
							$checked = 'checked="checked"';
						}
						
						$output .= '<input '. $checked .' type="checkbox" class="checkbox" id="team_javascript_'. $js_file_no_ext .'" name="team_javascript_'. $js_file_no_ext .'" >';
						$output .= '<label for="team_javascript_'. $js_file_no_ext .'" >';
						$output .= $js_file['name'];
						$output .= '</label>';						
						$output .= '<br />';

						$file_ii++;
		}
	}
	else {
		echo '<h3>No javascript files found. Does your current theme contain any Javascript file?</h3>';
	}

	$output .= '</div>';

	echo $output;  
	  
}

/* When the post is saved, saves our custom data */
function tjs_metabox_save( $post_id ) {

	/* Don't save on auto-save */
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
	  return;

	/* verify this came from the our screen and with proper authorization, because save_post can be triggered at other times */

	if ( !wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
	  return;

	//Check for WPML
	$post_id = tjs_main_postid($post_id);

	/* User must have permissions */
	if ( 'page' == $_POST['post_type'] ) 
	{
	if ( !current_user_can( 'edit_page', $post_id ) )
		return;
	}
	else
	{
	if ( !current_user_can( 'edit_post', $post_id ) )
		return;
	}

	foreach($_POST as $name => $value) {
		if (strpos($name, 'team_javascript') !== false) {
			$tjs_meta_table[] = array('tjs_file' => $name, 'tjs_value' => $value);
		}
		
		
	}	
	
	$tjs_meta = serialize($tjs_meta_table);


	/*Save our meta*/
	update_post_meta($post_id,'tjs_meta',$tjs_meta);

} 

?>