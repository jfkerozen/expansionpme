<?php

/* Fetch all javascript file found in current theme */
function tjs_directory_loop () {

	$di = new RecursiveDirectoryIterator(get_template_directory());
	foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
		if (preg_match('/^.+\.js$/i',$filename)) {			
			$tbl_files[] = array('name' => $file->getFilename(),'path' => str_replace(get_template_directory(),'',$filename));			
		}	
	}

	$tbl_filess = tjs_array_orderby($tbl_files,'path', SORT_ASC, 'name', SORT_ASC);	
					
	return $tbl_filess;
	
}

/* Reorder a multidimentionnal array */
function tjs_array_orderby()
{
	$args = func_get_args();
	$data = array_shift($args);
	foreach ($args as $n => $field) {
		if (is_string($field)) {
			$tmp = array();
			foreach ($data as $key => $row)
				$tmp[$key] = $row[$field];
			$args[$n] = $tmp;
			}
	}
	$args[] = &$data;
	call_user_func_array('array_multisort', $args);
	return array_pop($args);
}

/* Register and queue script to be loaded */
function tjs_activation() {

	global $post;

	ob_start();
	$settings = unserialize(get_option('team_javascript'));

	/* Construct an array of files to be loaded */
	if ($settings != '') {
		foreach($settings as $key => $value) {
			if(strpos($key, 'team_javascript_') !== false) {
				if($value) {
					$file_id = (substr($key, 16));
					if($file_id) {
						$include_files[] = $file_id;
						
					}
				}
			}
		}
	}

	
	/* Construct the array of JS files found in the current theme */
	$js_files = tjs_directory_loop();
	$file_ii = 0;
	$ii = 0;
	$subfolder = '';


	if (count($js_files) > 0) {
			
			
			
		$main_post_id = tjs_main_postid($post->ID); /*Check de la page ID */

		/* Get saved meta */
		$tjs_saved_meta = get_post_meta($main_post_id,'tjs_meta',true);	
		$tjs_saved_meta = unserialize($tjs_saved_meta);	
		
		foreach ($js_files as $js_file) {
		
						$current_path = str_replace('\\','/',$js_file['path']);	
						$last_slash = strrpos($current_path,'/') + 1;						

						if ($subfolder != substr($js_file['path'],0, $last_slash) ) {
							$subfolder = substr($js_file['path'],0, $last_slash);
						}
							
						$js_file_no_ext = substr($js_file['name'],0,-3); //Clean up name of files
						$js_file_no_ext = str_replace('.','-',$js_file_no_ext);
						$subfolder = str_replace('\\','/',$subfolder);						
						
						if (in_array($js_file_no_ext,$include_files) || (tjs_array_search( 'team_javascript_'. $js_file_no_ext ,$tjs_saved_meta))) {
							
							/* File is being added to register and enqueue */
							wp_register_script($js_file_no_ext, get_bloginfo('template_directory') . $subfolder . $js_file['name'], "jquery"); 
							wp_enqueue_script($js_file_no_ext);				
							$ii++;
						}

						$file_ii++;
		}
	}

}

/* search like in_array, but for multidimensional array */
function tjs_array_search ($needle, $mutli_array)
{
	if($mutli_array != false) {
	   foreach($mutli_array as $key => $value)
	   {	
		  if ( $value['tjs_file'] == $needle ) {
			 return true;
		  }
	   }
	}
    return false;
}

function tjs_main_postid ( $postid) {
	/* If WPML is activated, save all the meta only on the main language post */  
	if( function_exists('icl_object_id')) {
		global $sitepress;
		
		$default_lang = $sitepress->get_default_language();	  		
		return icl_object_id($postid, 'page', true, $default_lang);
		
	}
	else {
		return $postid;
	}
}

?>