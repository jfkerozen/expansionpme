// Plugin jQuery permettant de faire fader les liens en d�grad�
// Nicolas Morin 2010-02-10

(function($){  
	$.fn.fadeLink = function(options) {  
 
		// Valeurs par d�faut des param�tres
		var defaults = {  
			hoverColor: '#ff0000',  
			fadeTime: 200
		}; 
		
		// Prends la valeur par d�faut si param�tre non fourni
		var options = $.extend(defaults, options);  
	 	
		// M�morise la couleur actuelle de chaque lien affect� pour pouvoir y retourner
		this.each(function() {
			var oldColor = jQuery(this).css("color");
			jQuery(this).attr("oldcolor", oldColor);
		});			
			
			
		this.mouseover(function(){
			var oldColor = jQuery(this).attr("oldcolor");
			jQuery(this).css( {"color": oldColor} );
			jQuery(this).stop().animate({"color": options.hoverColor}, {duration: options.fadeTime});

		});
		
		this.mouseout(function(){
			var oldColor = jQuery(this).attr("oldcolor");
			jQuery(this).stop().animate({"color": oldColor}, {duration: options.fadeTime});
		});
			
		return true;
	
	};
})(jQuery);
