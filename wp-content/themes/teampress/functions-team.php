<?php
//require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/team-thumb/phpThumb.config.php');
/*Liste des fonctions TEAM
- team_get_page
- team_get_page_fr
- team_is_page
- team_the_file
- team_the_meta
- team_the_permalink
- team_the_a
- team_the_yapb
- team_the_thumb
- team_the_alt
- team_the_root_page
- team_language
- team_switch_language
*/


    // Fonction retournant le post donn� d'un post type donn�, � partir de son slug
    function team_get_custom_post_by_slug($slug, $post_type) {
        
        $inscription_post = '';
        
        $args = array(
                                    'name' => $slug,
                                    'post_type'     => $post_type
                                );
    
        $posts_match = get_posts($args);
        if(count($posts_match) == 1) {
            $post_match = $posts_match[0];
        }
        
        return $post_match;
    }
    
    
    // Fonction retournant le lien vers un post donn� d'un post type donn�, � partir de son slug
    function team_custom_post_the_permalink($slug, $post_type, $echo = true) {
        $custom_post = team_get_custom_post_by_slug($slug, $post_type);
        $custom_post_link = get_permalink($custom_post->ID);
        
        if($echo) {
            echo $custom_post_link;
        }
        
        return $custom_post_link;
    }


// Fonction retournant la page dans la bonne langue, en sp�cifiant le slug de la page francaise uniquement
	if(!function_exists('team_get_page')) {
		function team_get_page($pagePath) {
			$language_code = icl_t('System', 'language_code', 'fr');
			return get_page(icl_object_id(get_page_by_path($pagePath)->ID, 'page', true, $language_code));
		}
	}

	// Fonction retournant la page francaise toujours � partir d'une page dans n'importe quelle langue
	if(!function_exists('team_get_page_fr')) {
		function team_get_page_fr($post_id = 0) {
			global $post;
			$language_code = icl_t('System', 'language_code', 'fr');
			
			if($post_id == 0) {
				$post_id = $post->ID;
			}
			return get_page(icl_object_id($post_id, 'page', false, 'fr', $language_code));
		}
	}
	
	// Retourne true si la page en cours est celle sp�cifi�e, en tenant compte des langues
	if(!function_exists('team_is_page')) {
		function team_is_page($pagePath) {
			$language_code = icl_t('System', 'language_code', 'fr');
		
			return is_page(icl_object_id(get_page_by_path($pagePath)->ID, 'page', true, $language_code));
		}
	}
	
	// Fonction retournant le fichier en sp�cifiant le key du custom field
	if(!function_exists('team_the_file')) {
		function team_the_file($key, $echo = true) {
			global $post;
			if($attached_post = team_the_meta('', $key, false)) {
				$attached_file = get_option('siteurl') . '/wp-content/uploads/' . get_post_meta($attached_post, '_wp_attached_file', true);
				if($echo) {
					echo $attached_file;
				}
				return $attached_file;
			}
		}
	}
	
	// Fonction retournant notre meta post
	if(!function_exists('team_the_meta')) {
		function team_the_meta($key, $my_post = '', $echo = true) {
			global $post;
			if ($my_post != '') {
				$meta = get_post_meta($my_post, $key, true);
			}
			else {
				$meta = get_post_meta($post->ID, $key, true);
			}
			
			if($echo) {
				echo $meta;
			}
			return $meta;
		
		}
	}
	
	// Echo le permalink de la page sp�cifi�e, en tenant compte des langues
	if(!function_exists('team_the_permalink')) {
		function team_the_permalink($pagePath, $echo = true) {
			$language_code = icl_t('System', 'language_code', 'fr');
			$output = get_permalink(icl_object_id(get_page_by_path($pagePath)->ID, 'page', true, $language_code));
			if($echo) {
				echo $output;
			}
			return $output;
		}
	}
	
	//Fonction retournant une balise a contenant le href et le title du lien entre les balises
	if(!function_exists('team_the_a')) {
		function team_the_a($pagepath, $echo = true) {
			$page = team_get_page($pagepath);
			$output = '';
			$output .= '<a href="'.get_permalink($page->ID).'">' . apply_filters('the_title', $page->post_title) . '</a>';
			if($echo) {
				echo $output;
			}
			return $output;	
		}
	}
	
	// Fonction qui permet d'afficher une image provenant de yapb -- Liste des param�tres : echo, width, height, crop, link, class
	if (!function_exists('team_the_yapb')) {
		function team_the_yapb($echo = true, $width, $height, $zoomcrop = false, $link = false, $class = "") {
			if (yapb_is_photoblog_post()) :
				
				$zc = 'zc=0';
				if($zoomcrop == true) $zc = 'zc=1';
				
				$attributes = array();
				$attributes['alt'] = htmlentities(strip_tags(get_the_title())); // image tag alt attribute
				//$attributes['rel'] = $post->ID; // image tag rel attribute
				
				$output .= '<div class="Yapb_Img">';
				$output .= yapb_get_thumbnail(				 
					''. (($link == true) ? '<a class="'.$class.'" href="' . get_permalink() . '">' : '') .'', // HTML before image tag
					array(
					'alt' => strip_tags(get_the_title()), // image tag alt attribute
					'rel' => $post->ID // image tag rel attribute
					),
					(($link == true) ? '</a>' : ''),	// HTML after image tag
					array('w='.$width, 'h='.$height, 'q=100', $zc), // phpThumb configuration parameters
					'' // image tag custom css class
				);
				$output .= '</div>';
				if ($echo) {
					echo $output;
				}
				return $output;
			endif;
		}
	}
	
	//Fonction qui permet d'afficher une image PhPThumb -- Liste des param�tres : echo, url_only, path, key, width, height, zoom/crop, aoe, link, class
	//Exemple : <img src="team_the_thumb(false,$path,$AutreParametre)" ... />
	if (!function_exists('team_the_thumb')) {
		function team_the_thumb($echo = true, $url_only = false, $path, $key='', $width='', $height='', $zoomcrop='', $aoe='', $my_post = '', $f='', $link = false, $class = '', $inlinewh = false, $tableau = '') {
			global $post;
			
			$output = '';
			$debut_path = get_bloginfo('wpurl') . '/wp-content/plugins/team-thumb/phpThumb.php?';	
			

			
			if (($path == '') && ($key != '')) { //Si on veut utiliser la meta key
				if ($my_post != '') {			
					if($attached_post = get_post_meta($my_post, $key, true)) {
						$path = get_option('siteurl') . '/wp-content/uploads/' . get_post_meta($attached_post, '_wp_attached_file', true);
					}
				}
				else {
					$path = team_the_file($key,false);
				}
			
			}
			
			if ($path != '') {
				$url = get_bloginfo('wpurl');					
				//$fin_path = str_replace($url,ABSPATH,$path); //On modifie le path absolu par relatif
				$fin_path = str_replace('http://'.$_SERVER['HTTP_HOST'],'',$path); //On modifie le path absolu par relatif
				$fin_path = str_replace('//','/',$fin_path);
				$fin_path = 'src=' . $fin_path;
				
				if ($width !== '') {  $n_width = $width; $width = '&w='. $width; } else { $n_width = 0; } //On regarde les param�tres qui ont �t� d�finis
				if ($height !== '') {  $n_height = $height; $height = '&h='. $height; } else { $n_height = 0; }
				if ($zoomcrop !== '') { $zoomcrop = '&zc='. $zoomcrop; }
				if ($aoe !== '') { $aoe = '&aoe='. $aoe; }
				if ($f !== '') { $f = '&f='. $f; } else { $f = '&f=jpg'; }
				
				if( $tableau != '') {
				$zz = 0;
				$autre_parametre = '';
				
					while ($zz < count($tableau)) {
						$mykey = key($tableau);
						$myvalue = $tableau[$mykey];
						$autre_parametre .= '&'. $mykey . '=' . $myvalue;
						
						next($tableau);
						$zz++;
					}			
				}		
				
				//Pour qualit� JPEG
				//if ($format == 'JPEG') { $quality = '&q=90'; } else { $quality = ''; }
				
				$parametre = $fin_path . $width . $height . $zoomcrop . $aoe . $f . $autre_parametre;
				$parametre = htmlentities(phpThumbURL($parametre));
				
				if ($url_only == false) { //Avec balise img ($link et $class deviennent aussi possible...)
											
					//pour height et width dans la balise img
					$old_level = error_reporting();
					error_reporting(0);
					//$size = getimagesize($debut_path.$fin_path.$parametre);
					error_reporting($old_level);
				
					if (get_post_meta($post->ID, 'alt_'.$key, true)) {
						$meta_titre = get_post_meta($post->ID, 'alt_'.$key, true);
					}
					else {
						if (team_language() == 'fr') {
							$meta_titre = 'Image du site '. get_site_url();						
						}
						else {
							$meta_titre = 'Picture of '. get_site_url() .' website';					
						}

					}

					if (count($class) > 0 && $class != '' ) {
						$class = implode(' ',$class);
					}
					else {
						$class = '';
					}
					
					$url_encode =  $debut_path . $parametre;
						
					if ($link == true) { $output .= ' <a  href="' . get_permalink() . '">'; }
					if($inlinewh == true) { // Avec dimensions
						$output .= '<img width="'.$n_width.'" height="'.$n_height.'" class="'.$class.'" alt="'.$meta_titre.'" src="'.$url_encode.'" />';
					} else {  // Sans dimensions: pas trouv�...
						$output .= '<img class="'.$class.'" alt="'.$meta_titre.'" src="'.$url_encode.'" />';
					}
					if ($link == true) { $output .= '</a>'; }
				}
				else if ($url_only == true) { //Sans balise img
					$output .= $url_encode;
				}
				
				if ($echo) {
					echo $output;
				}
			}
			else {
				/*$output .= 'Aucune image';*/
			}						
			
			return $output;
		}
	}
	
	//Pour trouver le slug du premier parent
	function team_the_root_page($post_id, $echo = true) {
	
		if(is_single()) {
			if (strpos($_SERVER['HTTP_REFERER'],get_bloginfo('siteurl')) === false) {
				
			}
			else {
				print_r($_SERVER['HTTP_REFERER']);
			}
		}
		else {
			$parent_post_id = $post_id;		
			//boucle pour remonter jusqu'au premier parent
			//while ($parent_post_id != 0) {		
			while ($parent_post_id) { //2012-11-08 NM : modifi�		
				$the_post = get_post($parent_post_id);			
				$parent_post_id = $the_post->post_parent;
			}
			
			$page_slug = $the_post->post_name;		
		}
		
		if ($echo == true) {
			echo $page_slug;
		}
		
		return $page_slug;
	}
	
	//Fonction qui permet de retourner la langue de la page actuelle
	if (!function_exists('team_language')) {
		function team_language() {
			return icl_t('System', 'language_code', 'fr');
		}	
	}
	
	//Affiche la langue inverse, sans abr�viation, de la page sur laquelle on se retrouve (pour le header...)
	if (!function_exists('team_switch_language')) {
		function team_header_langue($echo = true, $full = false) {
			$output = '';
			$languages = icl_get_languages('skip_missing=0&orderby=id&order=dec');
			foreach($languages as $l){
				if(!$l['active']) {

					if ($full == false) {
						$output .= '<a href="'.$l['url'].'">'.$l['language_code'].'</a>';
					}
					else {
						$output .= '<a href="'.$l['url'].'">'.$l['native_name'].'</a>';					
					}
					
				}
			}
			if ($echo) {
				echo $output;
			}
			return $output;
		}
	}


	
?>