<?php 
	header("Content-type: text/css");
	
	$noir 			= "#000000";
	$blanc			= "#ffffff";
	
	/*style exemple bouton*/
	$orange			= "#fd6c01";
	$brunbouton     = "#291E17";
	
?>

/* Exemple FONT FACE*/
@font-face {
	font-family: 'TEAMDinRegular';
	src: url('fonts/din-regular-webfont.eot');
	src: local('?'), url('fonts/din-regular-webfont.woff') format('woff'), url('fonts/din-regular-webfont.ttf') format('truetype'), url('fonts/din-regular-webfont.svg#webfontkaFbJ8zs') format('svg');
	font-weight: normal;
	font-style: normal;
}

div#FontLoad {
	font-family: 'TEAMDinRegular';
	float: left;
	display: none;
}

/* Reset pour utilisation avec font-face */
h1, h2, h3, h4, h5, h6 {
	font-weight: normal!important;
	text-shadow: none;
}

strong {
	font-weight: normal!important;
}

/*****************************************/
/* CONTAINERS							 */
/*****************************************/

 html {
	width: 100%;
	height: 100%;
 }
 
 body{	
	background-color: #fff;
	color:#000;
	font-family: Arial, Helvetica, Sans-serif;
	margin:0;
	padding:0;
	width: 100%;
	height: 100%;
	min-width: 960px;
	-webkit-text-size-adjust: none;
	text-shadow: none;
}

sup, sub {
	height: 0;
	line-height: 1;
	vertical-align: middle;
	_vertical-align: middle;
	position: relative;
}

sup {
	bottom: 1ex;
	font-size: 8px;
}

sub {
	top: .5ex;
}

li {
	zoom: 1;
	list-style: none;
}

a {
	color: <?php echo $orange; ?>;
	text-decoration: none;
}

a:hover{
	color: <?php echo $noir; ?>;
	text-decoration: underline;
}

table {
	margin-bottom: 24px;
}

object embed, embed {
	margin-bottom: 24px;
}

span.LeftRoundedBorder{
	background: url("images/left_input.png") no-repeat scroll left top transparent;
	float: left;
	display: block;
	height: 35px;
	padding-left: 6px;
	margin-bottom: 12px;
}

span.RightRoundedBorder{
	background: url("images/right_input.png") no-repeat scroll right top transparent;
	float: left;
	display: block;
	height: 35px;
	padding-right: 6px;
}

span.LeftRoundedBorder span.RightRoundedBorder input {
	background: url("images/middle_input.png") repeat-x scroll 0 0 transparent;
	display: block;
	height: 35px;
	border: none!important;
	color: <?php echo $noir; ?>;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 18px;
	#margin-top: -1px!important;
	#padding-top: 6px;
	margin: 0;
	width: 420px;
	-webkit-border-radius: 0;
}

table textarea{
	border: none!important;
	background-color: #e8ebed;
	color: <?php echo $noir; ?>!important;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 18px;
	width: 421px!important;
	margin: 0;
	#margin-top: -1px!important;
	#padding-top: 6px;
	#margin-bottom: -1px!important;
	border: none!important;
	border-color: tansparent!important; 
	overflow: auto;
	-webkit-border-radius: 0;
}

.NoDisplay {
	display: none;
}

/*****************************************/
/* HEADER								 */
/*****************************************/

div#HeaderWrap{
	width: 100%;
}

div#Header {
	width: 960px;
	margin: 0 auto;
	
}

div#Logo {
	float: left;
	margin-top: 20px;
}

div#Logo a {
	background: url('./images/loginteam.png') no-repeat scroll left top transparent;
	width: 320px; 
	height: 130px;
	display: block;
}

/*Menu Mobile*/

div#Header div.BoutonMenu {
	display: none;
	background: url('./images/bouton_menu.jpg') no-repeat scroll left top transparent;
	width: 39px;
	height: 39px;
}

/* Menu */

div#Header div#MenuHeader {
	width: 960px;
}

div#Header div#MenuHeader ul li a.PrincipalMenu {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 24px;
	margin-bottom: 24px;
}

div#Header div#MenuHeader ul li a.PrincipalMenu:hover {
	text-decoration: none;
}

div#Header ul.TeamMenuPages li a {
	display: block;
}

/* Sous-menu */

div#Header div#MenuHeader ul.SousPages {
	margin: 0;
	padding: 0;
	background: <?php echo $blanc; ?>;
	display: none;
	position: absolute;
	z-index: 10;
	border: 1px solid <?php echo $noir; ?>;
}

div#Header div#MenuHeader ul.SousPages li {
	font-size: 14px;
	line-height: 24px;
	border-bottom: 1px solid <?php echo $noir; ?>;
}

div#Header div#MenuHeader ul.SousPages li a {
	display: block;
}

div#Header div#MenuHeader ul.SousPages li a:hover {
	color: <?php echo $orange; ?>;	
	text-decoration: none;
}

div#Header div#MenuHeader ul.SousPages li.LastListItem {
	border-bottom: none;
}

/*****************************************/
/* SLIDER								 */
/*****************************************/

div#Content div#Slider {
	max-width: 960px;
	height: 432px;
	margin-bottom: 48px;
}

div#Content div#Slider div#LayerSlider {
	height: 432px;
	width: 960px;
	position: relative;
}

div#Content div#Slider div#LayerSlider h1{
	font-family: TEAMOpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 36px;
	line-height: 38px;
	margin-bottom: 24px;
	color: <?php echo $noir; ?>;
}

div#Content div#Slider div#LayerSlider p {
	font-family: TEAMOpenSansSB, Arial, Helvetica, Sans-serif;
	font-size: 28px;
	line-height: 32px;
	margin-bottom: 24px;
	color: <?php echo $gris_moyen; ?>;
}

div#Content div#Slider div#LayerSlider p strong, div#Content div#Slider div#LayerSlider p bold {
	color: <?php echo $noir; ?>;
}

a.ls-nav-prev {
	background: url('./images/slide_back.png') no-repeat scroll left top transparent;
	display: block;
	width: 31px;
	height: 48px;
	position: absolute;
	left: 0px;
	top: 48%;
	z-index: 5000;
} 
 
a.ls-nav-next {
 	background: url('./images/slide_next.png') no-repeat scroll left top transparent;
	display: block;
	width: 31px;
	height: 48px;
	position: absolute;
	right: 0px;
	top: 48%;
	z-index: 5000;	
}
 
a:hover.ls-nav-prev , a:hover.ls-nav-next {
	background-position: 0px -49px;
}

div.ls-lt-container {
	overflow: hidden;
}
 
 div.ls-bottom-nav-wrapper {
	height: 12px;
	margin-top: -24px;
	margin-right: 48px;
	position: relative;
	z-index: 23000;
	float: right;	
}

span.ls-bottom-slidebuttons {
	height: 12px;
	display: block;
}

span.ls-bottom-slidebuttons a {
 	background: url('./images/bouton_slider.png') no-repeat scroll left top transparent;
	display: inline-block;
	width: 12px;
	height: 12px;
	margin-right: 4px;
	margin-left: 4px;
	
}

span.ls-bottom-slidebuttons a:hover, span.ls-bottom-slidebuttons a.ls-nav-active {
	background-position: 0px -13px;
}

div.ls-s2 {
	width: 384px;
	position: absolute;
	top: 48px;
	left: 528px;
}

/*****************************************/
/* CONTENT								 */
/*****************************************/

div#ContentWrap {
	width: 100%;
	overflow: hidden;
}

div#Content {
	width: 960px;
	margin: 0 auto;
}

div#Content h1 {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 24px;
	line-height: 24px;
	margin-bottom: 24px;
}

div#Content h2 {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 24px;
	line-height: 24px;
	margin-bottom: 24px;
}

div#Content p {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 24px;
	margin-bottom: 24px;
}

div#Content ul {
	margin-bottom: 24px;
}

div#Content ul li {
	background: url("images/liste_puce.png") no-repeat scroll left top transparent;
	list-style: none;
	padding-left: 18px;
	padding-top: 6px;
	margin-bottom: 6px;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 24px;
}

div#Content ol li {
	list-style: decimal;
	margin-left: 24px;
	padding-top: 6px;
	margin-bottom: 6px;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 24px;
}

/*****************************************/
/* SIDEBAR								 */
/*****************************************/

div#Content div#Sidebar {
	width: 288px;
	float: right;
	margin-bottom: 24px;
}

/*****************************************/
/* NOUS JOINDRE							 */
/*****************************************/

/* **** MapPress **** */

div#Content div.mapp-container {
	margin-bottom: 24px!important;
}

div#mapp0_poweredby {
	display: none!important;
}

div#Content div#Full_Google_Map {
	width: 100%;
	height: 300px;
}

/*****************************************/
/* NOUVELLES							 */
/*****************************************/

/*****************************************/
/* EMPLOIS								 */
/*****************************************/

/*****************************************/
/* RÉALISATIONS							 */
/*****************************************/

div#Content div#Realisations {
	width: 960px;
}

div#Content div#Realisations div.RealisationContent {
	float: left;
	margin-bottom: 24px;
	margin-right: 24px;
}

div#Content div#Realisations div.EndDiv {
	margin-right: 0px!important;
}

div#Content div#Realisations div.RealisationContent div.RealisationImage {
	width: 176px;
	height: 176px;
	margin-bottom: 12px;
}

/*Page single */

div#Content div#SingleRealisation {
	margin-bottom: 24px;
}

/*****************************************/
/* PLAN DU SITE							 */
/*****************************************/

div#Content li.pagenav {
	list-style: none;
}

/*****************************************/
/* FOOTER								 */
/*****************************************/

div#FooterWrap {
	width: 100%;
}

div#Footer {
	width: 960px;
	margin: 0 auto;
	padding-bottom: 48px;
}

/*****************************************/
/* CONTACT FORM 7						 */
/*****************************************/

div.wpcf7{
	position: relative;
}

div.EmploiUnePage div.wpcf7{
	display: none;
}

div#Content div#Contacts div.wpcf7{
	display: block;
}

div.wpcf7 div.HiddenInput {
	display:none;
}

/* Pour bouton file  */
div.FileSpecial input.file {
	display: none!important;
}

div.FileSpecial div input{
	#width: 232px!important;
	#display: block!important;
}

div.FileSpecial span.resume {
	float: left;
	height: 24px;
	overflow: hidden;
}

div.FileSpecial span.text {
	float: left;
	padding-left: 24px;
}

div.FileSpecial {
	height: 24px;
	line-height: 24px;
	margin-bottom: 3px;
}

div.FileSpecial input.wpcf7-file {
	#right: 0px!important;
}

/* Fin bouton file */

/*****************************************/
/* COMMONS								 */
/*****************************************/

/* Bouton */

p.Bouton{
	height: 24px;
	float: left;
}

p.BoutonDroite{
	float: right!important;
}

p.Bouton span{
	font-family: Arial, Helvetica, Sans-serif;
	line-height: 24px;
	font-size: 14px;
	display: block;
	cursor: pointer;
}

p.Bouton a{
	color: <?php echo $blanc; ?>!important;
	text-decoration: none!important;
}

p.Bouton a:hover span.BoutonMilieu {
	color: <?php echo $blanc; ?>;
	text-decoration: none!important;
}

p.Bouton a:hover span {
	cursor: pointer;
}

p.Bouton span.BoutonMilieu{
	background: url('./images/bouton_sprite_milieu.png') repeat left 0px transparent;
	height: 24px;
	line-height: 24px;
	padding-right: 6px;
	padding-left: 6px;
	color: <?php echo $blanc; ?>!important;
}

p.Bouton span.BoutonGauche{
	background: url('./images/bouton_sprite.png') no-repeat left 0px transparent;
	padding-left: 6px;
}

p.Bouton span.BoutonDroite{
	background: url('./images/bouton_sprite.png') no-repeat right -25px transparent;
	padding-right: 6px;
}

p.Bouton a:hover span.BoutonMilieu, span.BoutonMilieuOver{
	background: url('./images/bouton_sprite_milieu.png') repeat left -25px transparent!important;
}

p.Bouton a:hover span.BoutonGauche, span.BoutonGaucheOver{
	background-position: left -50px!important;
}

p.Bouton a:hover span.BoutonDroite, span.BoutonDroiteOver{
	background-position: right -75px!important;
}

/* style td */

td.CornerTopLeft {
	width: 6px;
	height: 6px;
	background: url('./images/corner-top-left.png') no-repeat bottom right transparent;
}

td.CornerTopRight {
	width: 6px;
	height: 6px;
	background: url('./images/corner-top-right.png') no-repeat bottom left transparent;
}

td.CornerBottomLeft {
	width: 6px;
	height: 6px;
	background: url('./images/corner-bottom-left.png') no-repeat top right transparent;
}

td.CornerBottomRight {
	width: 6px;
	height: 6px;
	background: url('./images/corner-bottom-right.png') no-repeat top left transparent;
}

td.SideTop {
	height: 6px;
	background: url('./images/border-top.png') repeat-x bottom left transparent;
}

td.SideBottom {
	height: 6px;
	background: url('./images/border-bottom.png') repeat-x top left transparent;
}

td.SideLeft {
	width: 6px;
	background: url('./images/border-left.png') repeat-y top right transparent;
}

td.SideRight {
	width: 6px;
	background: url('./images/border-right.png') repeat-y top left transparent;
}

td.MiddleCenter {
	background-color: <?php echo $blanc; ?>;
}

/* TIMYMCE */

.Capitale, .capitale {
	text-transform: uppercase;
}

/* autres */

.empty {
	line-height: 0px;
	font-size: 0px;
	overflow: hidden;
	width: 0;
	height: 0;
}

.clear {
	clear: both!important;
	line-height: 0px!important;
	font-size: 0px!important;
	overflow: hidden!important;
	width: 0!important;
	height: 0!important;
	float: none!important;
	margin: 0!important;
	padding: 0!important;
}	