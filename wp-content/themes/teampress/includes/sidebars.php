<?php

	//D�fini les sidebars
	if ( function_exists('register_sidebar') ) {
	
		register_sidebars((1),array(
			'name' => 'Widgets Menu Header',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));		

		register_sidebars((1),array(
			'name' => 'Widgets Subheader',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));			
		
		register_sidebars((1),array(
			'name' => 'Widgets Sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));		
		
		register_sidebars((1),array(
			'name' => 'Widgets Menu Footer',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));		
		
		register_sidebars((1),array(
			'name' => 'Widgets Plan site',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));	

		register_sidebars((1),array(
			'name' => 'Widgets XML Sitemap',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));			

		register_sidebars((1),array(
			'name' => 'Widgets Menu Mobile',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
            'before_title' => '<h3>',
			'after_title' => '</h3>',
		));		
		
	}
	
?>