<?php

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 GetPDFByCategory : Retourne un PDF d'une catégorie. 
						Pour les annonce de terrains 
						et développements
	 By :               Jimmy Lavoie
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	if(!function_exists('GetPDFByCategory')) {
		function GetPDFByCategory($Categorie) {
			global $posts;
			global $post;
			
			$pdf = false;
			/* Generer la boucle Post */
			wp_reset_query();	
			
			$args = array(
						'child_of' 			=> $Categorie
					);
			$LesCategories = get_categories( $args );		
			// Note:  category__in : exclue les posts des catégories enfants
			$args = array(
						'category__in' 		=> array($LesCategories[0]->cat_ID), 
						'posts_per_page' 	=> '-1'
					);
					
			//print_r($LesCategories);
			//echo $Categorie . " " . $LesCategories[0]->cat_ID;
			
			wp_reset_query();	
			$posts = query_posts($args);		
			
			while (have_posts()) : the_post(); setup_postdata($post);			
				$pdf = get_post_meta($post->ID, 'Plan', true);
				$pdf = get_post_meta($pdf, '_wp_attached_file', true);
				$pdf = get_option('siteurl') . '/wp-content/uploads/' .$pdf;
				//echo "<br/>pdf -> " . $pdf;		
			endwhile;
			
			return $pdf;
		}
	}

?>