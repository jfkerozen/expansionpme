<?php
	// Affichage breadcrumbs par Nicolas Morin 2011-03-17 / TEAM edc Inc.
	// Initialement cr�� pour le site laurasecord.ca

	//�crit le breadcrumbs si d�sir�
	if(!function_exists('team_the_breadcrumbs')) {
		function team_the_breadcrumbs($echo = true) {	
		
			global $post;
			global $paged;
			global $term;
			global $taxonomy;
			
			$output = '';
			$paged_output = '';
			//$separator = '<img src="'.get_bloginfo('template_directory').'/images/mini_fleche.png" width="14" height="16" alt="Separateur" />';
			$separator = ' &raquo; ';
			
			// Home
			$output .= '<a class="' . ((is_front_page()) ? 'SelectedBC' : '') . '" href="' . team_the_permalink('accueil', false) . '">' . team_get_page('accueil')->post_title . '</a>';

			if(!is_front_page()) {

				// Tient compte de la pagination
				if(is_paged()) {
					if(is_search()) {
						$current_link = get_search_link();
					} else {
						$current_link = get_permalink();
					}
					$paged_output = $separator . '<a class="SelectedBC" href="' . $current_link . 'page/' . $paged . '/">Page ' . $paged . '</a>';
				}
			
				// Traitement diff�rent si page ou taxonomie
				// Note: pour l'instant ne traite qu'un seul niveau de pages.
				// Ne traite pas les sous-pages et ce concept.
				if(is_page()) {
					$output .= GetPageTree($post, $separator);
					
					$output .= $separator;
					$output .= '<a class="' . ((!is_paged()) ? 'SelectedBC' : '') . '" href="' . get_permalink() . '">' . $post->post_title . '</a>';
					
				} elseif(is_404()) {
					$output .= $separator;
					$output .= '<a href="/404/" class="SelectedBC">404</a>';
					
				} elseif(is_tax()) {
					$term_obj = get_term_by('slug', $term, $taxonomy);
					
					// V�rifie si la taxonomie a un parent, et les imbriques	
					$output .= GetTaxTree($term_obj, $taxonomy, $separator);
					
					$output .= $separator;
					$output .= '<a class="' . ((!is_paged()) ? 'SelectedBC' : '') . '" href="' . get_term_link($term_obj, $taxonomy) . '">' . $term_obj->name . '</a>';
				
				} elseif(is_single()) {
					//$taxonomy = 'wpsc_product_category';
					//$taxonomies = get_object_taxonomies($post);
					$taxonomies = array_keys(get_taxonomies());
					//print_r($taxonomies);
					//$taxonomy = $taxonomies[0];
					$terms = wp_get_object_terms($post->ID, $taxonomies);
					if($terms) {
						$term_obj = $terms[0];
						
						// V�rifie si la taxonomie a un parent, et les imbriques	
						$output .= GetTaxTree($term_obj, $taxonomy, $separator);
						
						$output .= $separator;
						$output .= '<a href="' . get_term_link($term_obj, $taxonomy) . '">' . $term_obj->name . '</a>';
						$output .= $separator;
						$output .= '<a class="' . ((!is_paged()) ? 'SelectedBC' : '') . '" href="' . get_permalink() . '">' . $post->post_title . '</a>';
					}
				} elseif(is_search()) {
					$output .= $separator;
					$output .= '<a href="' . get_search_link() . '" class="' . ((!is_paged()) ? 'SelectedBC' : '') . '">' . team_the_wpml('Recherche', 'search', false) . '</a>';					
				}
			}
			if ($echo) {
				echo $output . $paged_output;
			}
			return $output . $paged_output;
		}
	}
	
	// Fonction retournant la partie du breadcrumb des taxonomies
	// en tenant compte des parents
	function GetTaxTree($child_term, $taxonomy, $separator) {
	
		$tax_tree = '';
		while($child_term->parent) {
			$parent_term = get_term($child_term->parent, $taxonomy);
			
			// Traitement cas sp�cial si cat�gorie root: redirection � la page du m�me slug
			if($parent_term->parent == 0) {
				$tax_tree = '<a href="' . get_permalink(get_page_by_path($parent_term->slug)->ID) . '">' . $parent_term->name . '</a>' . $tax_tree;
			
			} else {
				$tax_tree = '<a href="' . get_term_link($parent_term, $taxonomy) . '">' . $parent_term->name . '</a>' . $tax_tree;
			}
			$tax_tree = $separator . $tax_tree;
			
			$child_term = parent_term;
		}
		
		return $tax_tree;
	}
	
	// Fonction retournant la partie du breadcrumb des sous-pages
	// en tenant compte des pages parentes
	function GetPageTree($page_obj, $separator) {
	
		$page_tree = '';
		while($page_obj->post_parent) {
			$parent_page = get_post($page_obj->post_parent);

			$page_tree = '<a href="' . get_permalink($parent_page->ID) . '">' . $parent_page->post_title . '</a>' . $page_tree;
			$page_tree = $separator . $page_tree;
			
			$page_obj = $parent_page;
		}
		
		return $page_tree;
	}	
	
	
	
		
?>