<?php

	// Traduction explicite forc�e, si d�tecte seulement les shortcodes et pas les hooks WPML
	if(!function_exists('traduire')) {
		// Shortcode handler format
		function traduire($atts, $content = null) {
			extract(shortcode_atts(array(
				'item' => ''
			), $atts));

			return icl_t('Widgets', $item, $content);
		}
	}

	add_shortcode("translate", "traduire");

?>