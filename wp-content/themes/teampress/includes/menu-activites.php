<?php


if (isset($_POST['action']) && $_POST['action'] == 'generation') {
	GenerationExcelActivite();
}

if (isset($_POST['action']) && $_POST['action'] == 'sauvegarde_taxe') {
	SauvegardeTaxesActivite();
}

//Ajoute un menu à la partie administrative des activités
function custom_activites_menu(){
    add_submenu_page( 'edit.php?post_type=les-activites', 'Liste des inscriptions', 'Liste des inscriptions', 'client_user', 'generation_excel_activite', 'AfficheExcelActivite' );
    add_submenu_page( 'edit.php?post_type=les-activites', 'Taxes et administration', 'Taxes et administration', 'client_user', 'generation_taxes_activite', 'AfficheTaxesActivite' );
}
add_action( 'admin_menu', 'custom_activites_menu' );

// Affiche les choix pour générer un fichier Excel pour le post type des activités
function AfficheExcelActivite() {
	
	$args = array(
			'showposts'     => -1,
			'orderby'         => 'title',
			'order'           => 'ASC',
			'post_type'       => 'les-activites',
	);
		
	$postsquery = new WP_Query($args);
	$les_activites = $postsquery->posts;	
	$count = count($les_activites);
	
	$output = '';	
	$output .= '<h1>Génération d\'un fichier Excel</h1>';	
	
	if ($count >= 1) {
		$output .= '<form id="FormulaireExcelActivites" method="POST" action="#">';
		
		//Affiche la liste d'activités
		$output .= '<br /><h2>1. Choisir une activité.</h2>';
		$output .= '<select name="id_activite">';
		$output .= '<option value="0"> </option>';
		foreach ($les_activites as $une_activite) {
			$output .= '<option value="'.$une_activite->ID.'">'.$une_activite->post_title.'</option>';
		}
		$output .= '</select>';
	
		//Type d'inscription
		$output .= '<br /><br /><h2>2. Choisir les types d\'inscriptions à inclure.</h2>';
		$output .= '<input type="checkbox" name="type_inscription0" value="0" id="TI1"><label for="TI1">Inscritption payée via Paypal - Paiement en attente</label><br />';
		$output .= '<input type="checkbox" name="type_inscription1" value="1" id="TI2"><label for="TI2">Inscritption payée via Paypal - Paiement reçu</label><br />';
		$output .= '<input type="checkbox" name="type_inscription2" value="2" id="TI3"><label for="TI3">Inscritption payée via Chèque</label><br />';
		$output .= '<input type="checkbox" name="type_inscription3" value="3" id="TI4"><label for="TI4">Inscritption gratuite</label>';
		
		//Bouton pour générer
		$output .= '<br /><br /><h2>3. Générer le fichier!</h2>';
		$output .= '<input type="submit" value="Télécharger le fichier">';
		
		$output .= '<input type="hidden" value="generation" name="action" />';
		$output .= '</form>';
		
		$output .= "<i>N.B : Dans le cas d'une activité avec plusieurs ateliers, des doublons de participant seront présent, car le fichier EXCEL sera généré en fonction de chaque atelier pour l'activité donnée.<br />Ex.: Si une personne participe à plusieurs ateliers, elle aura une ligne pour chaque atelier.</i>";
	
	}
	else {
		$output .= '<h2>Aucune activité trouvée.</h2>';
	}
	echo $output;
	
}

//Fonction qui génère le fichier EXCEL 2007 à partir d'un template .XLSX
function GenerationExcelActivite() {
	
		//Aller chercher le post de l'activité
		$post_excel_id = $_POST['id_activite'];
		$arr_inscription = array($_POST['type_inscription0'],$_POST['type_inscription1'],$_POST['type_inscription2'],$_POST['type_inscription3'],6);
		$post_activite = get_post($_POST['id_activite']);
		
		set_include_path('phpexcel/Classes/');
		error_reporting(E_ALL);
		//Fichiers nécéssaires pour PHPExcel
		include_once('phpexcel/Classes/PHPExcel.php');
		include_once('phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
		include_once('phpexcel/Classes/PHPExcel/IOFactory.php');
		include_once('phpexcel/Classes/PHPExcel/RichText.php');
		
		//Faire un objet fichier PHPExcel!		
		$objPHPExcel = new PHPExcel();
			
		$inputFileType = 'Excel2007';
		//$inputFileName = 'C:/!_teamedc_www/expansionpme.org/wp-content/themes/teampress/includes/xlsx/rapport_activite.xlsx'; //Debug Local
		$inputFileName = ABSPATH . '/wp-content/themes/teampress/includes/xlsx/rapport_activite.xlsx'; //En ligne

		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$rapport_excel = $objReader->load($inputFileName);

		//Propriétés du fichier
		$rapport_excel->getProperties()->setCreator("Expansion PME");
		$rapport_excel->getProperties()->setLastModifiedBy("Expansion PME");
		if ($post_activite->ID != 0) { $rapport_excel->getProperties()->setTitle("Liste d'inscrit à l'activité - ".$post_activite->post_title); } else { $rapport_excel->getProperties()->setTitle("Liste d'inscrit à l'activité - Erreur"); }
		$rapport_excel->getProperties()->setSubject("Liste d'inscrit à une activité");
		if ($post_activite->ID != 0) { $rapport_excel->getProperties()->setDescription("Liste d'inscrit à l'activité - ".$post_activite->post_title); } else { $rapport_excel->getProperties()->setDescription("Liste d'inscrit à l'activité - Erreur"); }
		
		//Ajout des donnes
		$rapport_excel->setActiveSheetIndex(0);
		
		//Renomme la feuille
		$rapport_excel->getActiveSheet()->setTitle("Liste d'inscrit");
	
		//Sauvegarde du fichier
		$objWriter = new PHPExcel_Writer_Excel2007($rapport_excel);
		
		$ligne_excel = 3;
	
		//Requete SQL pour aller chercher les informations
		global $wpdb;
		
		//Information sur l'entreprise et les participants et leurs réservations
		$sql_reservations = "SELECT {$wpdb->prefix}act_entreprises.ID AS id_entreprise,
																{$wpdb->prefix}act_entreprises.entreprise AS nom_entreprise,
																{$wpdb->prefix}act_entreprises.adresse AS adresse_entreprise,
																{$wpdb->prefix}act_entreprises.ville AS ville_entreprise,
																{$wpdb->prefix}act_entreprises.code_postal AS codepostal_entreprise,
																{$wpdb->prefix}act_entreprises.telephone AS telephone_entreprise,
																{$wpdb->prefix}act_entreprises.secteur_activite AS secteuractivite_entreprise,
																{$wpdb->prefix}act_participants.ID AS id_participant,
																{$wpdb->prefix}act_participants.salutation AS salutation_participant,
																{$wpdb->prefix}act_participants.nom AS nom_participant,
																{$wpdb->prefix}act_participants.telephone AS telephone_participant,
																{$wpdb->prefix}act_participants.courriel AS courriel_participant,
																{$wpdb->prefix}act_participants.fonction AS fonction_participant,
																{$wpdb->prefix}act_participants.responsable AS is_responsable,
																{$wpdb->prefix}act_reservations.ID AS id_reservation,
																{$wpdb->prefix}act_reservations.post_id AS post_id_activite,
																{$wpdb->prefix}act_reservations.meta_key AS meta_key_conference,
																{$wpdb->prefix}act_transactions.type_inscrit AS type_inscrit,
																{$wpdb->prefix}act_transactions.status AS type_transaction
													FROM {$wpdb->prefix}act_reservations 
												 INNER JOIN {$wpdb->prefix}act_transactions ON ({$wpdb->prefix}act_reservations.transaction_id = {$wpdb->prefix}act_transactions.ID) 
												 INNER JOIN {$wpdb->prefix}act_participants ON ({$wpdb->prefix}act_reservations.participant_id = {$wpdb->prefix}act_participants.ID) 
												 INNER JOIN {$wpdb->prefix}act_entreprises ON ({$wpdb->prefix}act_entreprises.ID = {$wpdb->prefix}act_participants.entreprise_id)
												 WHERE {$wpdb->prefix}act_reservations.post_id = $post_excel_id ORDER BY id_participant ASC, id_reservation ASC";
		
		/* DEBUG NM */
		//$message .= "<br/>\r\n<br/>\r\n" . $sql_entreprise . "<br/>\r\n<br/>\r\n";
												 
		$resultats_activite = $wpdb->get_results($sql_reservations, OBJECT);	

		$arr_entreprise_id = array();	
		$id_entreprise_actuel = -1;
		//construire un tableau de entreprise id
		foreach ($resultats_activite as $un_resultat) {
			if ($un_resultat->id_entreprise != $id_entreprise_actuel) {
				$arr_entreprise_id[] = $un_resultat->id_entreprise;
				$id_entreprise_actuel = $un_resultat->id_entreprise;
			}
			
		}

		if (empty($arr_entreprise_id)) { //Au cas où...
			$arr_entreprise_id[] = 0;
		}
				
		$list_entreprise_id = implode(',',$arr_entreprise_id);
		
		//Information sur le responsable de l'entreprise et ses réservations
		$sql_responsable =  "SELECT {$wpdb->prefix}act_entreprises.ID AS id_entreprise, 
																{$wpdb->prefix}act_entreprises.entreprise AS nom_entreprise,
																{$wpdb->prefix}act_entreprises.adresse AS adresse_entreprise,
																{$wpdb->prefix}act_entreprises.ville AS ville_entreprise,
																{$wpdb->prefix}act_entreprises.code_postal AS codepostal_entreprise,
																{$wpdb->prefix}act_entreprises.telephone AS telephone_entreprise,
																{$wpdb->prefix}act_entreprises.secteur_activite AS secteuractivite_entreprise,
																{$wpdb->prefix}act_participants.ID AS id_participant,
																{$wpdb->prefix}act_participants.salutation AS salutation_participant,
																{$wpdb->prefix}act_participants.nom AS nom_participant,
																{$wpdb->prefix}act_participants.telephone AS telephone_participant,
																{$wpdb->prefix}act_participants.courriel AS courriel_participant,
																{$wpdb->prefix}act_participants.fonction AS fonction_participant,
																{$wpdb->prefix}act_participants.responsable AS is_responsable,
																{$wpdb->prefix}act_reservations.ID AS id_reservation,
																{$wpdb->prefix}act_reservations.post_id AS post_id_activite,
																{$wpdb->prefix}act_reservations.meta_key AS meta_key_conference,
																{$wpdb->prefix}act_transactions.type_inscrit AS type_inscrit,
																IF( {$wpdb->prefix}act_transactions.status IS NULL,6,{$wpdb->prefix}act_transactions.status) AS type_transaction
													FROM {$wpdb->prefix}act_participants
														INNER JOIN {$wpdb->prefix}act_entreprises ON ({$wpdb->prefix}act_entreprises.ID = {$wpdb->prefix}act_participants.entreprise_id)
														LEFT JOIN {$wpdb->prefix}act_reservations ON ({$wpdb->prefix}act_reservations.participant_id = {$wpdb->prefix}act_participants.ID)
														LEFT JOIN {$wpdb->prefix}act_transactions ON ({$wpdb->prefix}act_reservations.transaction_id = {$wpdb->prefix}act_transactions.ID) 													
													WHERE {$wpdb->prefix}act_participants.responsable = 1 AND ( {$wpdb->prefix}act_entreprises.ID IN ($list_entreprise_id) OR {$wpdb->prefix}act_reservations.post_id = $post_excel_id )";
													//GROUP BY id_entreprise";	
													//WHERE {$wpdb->prefix}act_reservations.post_id = $post_excel_id";														
													
		$resultats_reponsable = $wpdb->get_results($sql_responsable, OBJECT);	

		//$obj_merged = (object) array_merge((array) $resultats_activite, (array) $resultats_reponsable);

		//usort($obj_merged, 'compare_results_excel');
		
		//print_r($obj_merged);
		//die();
		
		
		foreach ($resultats_reponsable as $un_resultat) {
			if (in_array($un_resultat->type_transaction,$arr_inscription)) {
			
				$conference = get_post_meta($un_resultat->post_id_activite, $un_resultat->meta_key_conference, true);	
				if ($conference == false) {
					$conference = '-';
				}
			
				$rapport_excel->getActiveSheet()->SetCellValue('A'.$ligne_excel, $un_resultat->nom_entreprise); //Nom Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('B'.$ligne_excel, $un_resultat->adresse_entreprise); //Adresse Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('C'.$ligne_excel, $un_resultat->ville_entreprise); //Ville Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('D'.$ligne_excel, $un_resultat->codepostal_entreprise); //Code postal Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('E'.$ligne_excel, $un_resultat->telephone_entreprise); //Telephone Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('F'.$ligne_excel, $un_resultat->secteuractivite_entreprise); //Secteur Activite Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('G'.$ligne_excel, $un_resultat->salutation_participant); // Salutation
				$rapport_excel->getActiveSheet()->SetCellValue('H'.$ligne_excel, $un_resultat->nom_participant); // Nom
				$rapport_excel->getActiveSheet()->SetCellValue('I'.$ligne_excel, $un_resultat->fonction_participant); // Fonction
				$rapport_excel->getActiveSheet()->SetCellValue('J'.$ligne_excel, $un_resultat->telephone_participant); //Telephone
				$rapport_excel->getActiveSheet()->SetCellValue('K'.$ligne_excel, $un_resultat->courriel_participant); //Courriel
				$rapport_excel->getActiveSheet()->SetCellValue('N'.$ligne_excel, $conference); // Liste des conférences si il y a lieu			
			
				if ( $un_resultat->is_responsable == 1) {
					$rapport_excel->getActiveSheet()->SetCellValue('L'.$ligne_excel, "Oui"); //Courriel
				}
				else {
					$rapport_excel->getActiveSheet()->SetCellValue('L'.$ligne_excel, "Non"); //Courriel
				}
				
				switch($un_resultat->type_transaction) {

					case 0:
						$type_paiement = "Via Paypal - En attente du paiement";
					break;
				
					case 1:
						$type_paiement = "Via Paypal - Paiement reçu";
					break;
					
					case 2:
						$type_paiement = "Via Envoyez-moi une facture";
					break;
					
					case 3:
						$type_paiement = "Inscription Gratuite";
					break;
					
					case 6:
						$type_paiement = "Aucun - Ne participe pas à l'activité";
					break;
				
				}
				
				if ($un_resultat->type_inscrit == 0) {
					$type_inscrit = '-';
				}
				else {
					$post_inscrit = get_post($un_resultat->type_inscrit);
					$type_inscrit = $post_inscrit->post_title;
				}
				
				if(!empty($un_resultat->id_reservation)) {
					$rapport_excel->getActiveSheet()->SetCellValue('M'.$ligne_excel, "Oui"); // Participation du responsable
				}
				else {
					$rapport_excel->getActiveSheet()->SetCellValue('M'.$ligne_excel, "Non"); // Participation du responsable
				}
								
				$rapport_excel->getActiveSheet()->SetCellValue('O'.$ligne_excel, $type_inscrit ); // Type inscription
				$rapport_excel->getActiveSheet()->SetCellValue('P'.$ligne_excel, $type_paiement); // Type de paiement


			}
			$ligne_excel++;
		}			

		foreach ($resultats_activite as $un_resultat) {
			if (in_array($un_resultat->type_transaction,$arr_inscription) && $un_resultat->is_responsable == 0) {
						
				$conference = get_post_meta($un_resultat->post_id_activite, $un_resultat->meta_key_conference, true);			
			
				$rapport_excel->getActiveSheet()->SetCellValue('A'.$ligne_excel, $un_resultat->nom_entreprise); //Nom Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('B'.$ligne_excel, $un_resultat->adresse_entreprise); //Adresse Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('C'.$ligne_excel, $un_resultat->ville_entreprise); //Ville Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('D'.$ligne_excel, $un_resultat->codepostal_entreprise); //Code postal Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('E'.$ligne_excel, $un_resultat->telephone_entreprise); //Telephone Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('F'.$ligne_excel, $un_resultat->secteuractivite_entreprise); //Secteur Activite Entreprise
				$rapport_excel->getActiveSheet()->SetCellValue('G'.$ligne_excel, $un_resultat->salutation_participant); // Salutation
				$rapport_excel->getActiveSheet()->SetCellValue('H'.$ligne_excel, $un_resultat->nom_participant); // Nom
				$rapport_excel->getActiveSheet()->SetCellValue('I'.$ligne_excel, $un_resultat->fonction_participant); // Fonction
				$rapport_excel->getActiveSheet()->SetCellValue('J'.$ligne_excel, $un_resultat->telephone_participant); //Telephone
				$rapport_excel->getActiveSheet()->SetCellValue('K'.$ligne_excel, $un_resultat->courriel_participant); //Courriel
				$rapport_excel->getActiveSheet()->SetCellValue('N'.$ligne_excel, $conference); // Liste des conférences si il y a lieu			
			
				if ( $un_resultat->is_responsable == 1) {
					$rapport_excel->getActiveSheet()->SetCellValue('L'.$ligne_excel, "Oui"); //Courriel
				}
				else {
					$rapport_excel->getActiveSheet()->SetCellValue('L'.$ligne_excel, "Non"); //Courriel
				}
				
				switch($un_resultat->type_transaction) {

					case 0:
						$type_paiement = "Via Paypal - En attente du paiement";
					break;
				
					case 1:
						$type_paiement = "Via Paypal - Paiement reçu";
					break;
					
					case 2:
						$type_paiement = "Via Envoyez-moi une facture";
					break;
					
					case 3:
						$type_paiement = "Inscription Gratuite";
					break;
				
				}
				
				if ($un_resultat->type_inscrit == 0) {
					$type_inscrit = '-';
				}
				else {
					$post_inscrit = get_post($un_resultat->type_inscrit);
					$type_inscrit = $post_inscrit->post_title;
				}
				
				$rapport_excel->getActiveSheet()->SetCellValue('M'.$ligne_excel, "Oui" ); // Participation du responsable - Rien ici, que des participants
				$rapport_excel->getActiveSheet()->SetCellValue('O'.$ligne_excel, $type_inscrit ); // Type inscription
				$rapport_excel->getActiveSheet()->SetCellValue('P'.$ligne_excel, $type_paiement); // Type de paiement

				$ligne_excel++;
			}			
		}	
		
		//die();
		
		if ($post_activite->ID == 0) {
			$rapport_excel->getActiveSheet()->SetCellValue('A'.$ligne_excel,'Aucune activité sélectionnée!');
		}

		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition:inline;filename=Rapport_inscription_activite.xlsx");
		$objWriter->save('php://output');
		//$objWriter->save('/home/hormonal/public_html/wp-content/themes/twentyten/xlsx/test.xlsx');
		
		exit();
		
		

}

//Page pour les taxes et courriel d'administration
function AfficheTaxesActivite() {

	$output = '';
	
	$output .= '<form method="POST" enctype="multipart/form-data" action="#">';
	$output .= '<h1>Taxes</h1>';
	$output .= 'TPS : <input style="width:40px;" type="text" name="taxe_tps" value="'.get_option( "taxe_tps" , "" ).'"/>%<br/>';
	$output .= 'TVQ : <input style="width:40px;" type="text" name="taxe_tvq" value="'.get_option( "taxe_tvq" , "" ).'"/>%<br/>';
	$output .= 'TVH : <input style="width:40px;" type="text" name="taxe_tvh" value="'.get_option( "taxe_tvh" , "" ).'"/>%<br/>';
	$output .= '<p><i>Si une valeur est assigné à la TVH, les taxes seront toutes calculées à partir de celle-ci.</p></i><br/><br/>';
	
	$output .= '<h1>Administration</h1>';
	$output .= 'Courriel de l\'administrateur : <input type="text" style="width:250px;" name="courriel_administration" value="'.get_option( "courriel_administration" , "" ).'"/>';
	$output .= '<p><i>Un courriel est envoyé à cet adresse lors d\'une inscription à une activité via l\'option "Envoyez-moi un chèque".</p></i>';
	
	$output .= '<br/><input type="submit" value="Sauvegarder">';
	$output .= '<input type="hidden" name="action" value="sauvegarde_taxe"/>';
	$output .= '</form>';
	
	echo $output;
	
}

//Sauvegarde des options dans wp_options
function SauvegardeTaxesActivite() {

	$taxe_tps =  str_replace(',','.',mysql_real_escape_string($_POST['taxe_tps']));
	$taxe_tvq =  str_replace(',','.',mysql_real_escape_string($_POST['taxe_tvq']));
	$taxe_tvh =  str_replace(',','.',mysql_real_escape_string($_POST['taxe_tvh']));
	$courriel_administration =  str_replace(',','.',mysql_real_escape_string($_POST['courriel_administration']));

	update_option( 'taxe_tps', $taxe_tps );
	update_option( 'taxe_tvq', $taxe_tvq );
	update_option( 'taxe_tvh', $taxe_tvh );
	update_option( 'courriel_administration', $courriel_administration );

	add_action( 'admin_notices', 'my_admin_notice' );
	
}

//Notice de modification
function my_admin_notice() {
    ?>
    <div class="updated">
        <p>Modifications sauvegardées</p>
    </div>
    <?php
}

	function compare_results_excel($a, $b) { 
		if($a->id_entreprise == $b->id_entreprise) {
			return 0 ;
		} 
	  return ($a->id_entreprise < $b->id_entreprise) ? -1 : 1;
	} 

?>