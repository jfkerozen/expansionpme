<?php

function affiche_formulaire_activite() {

	?>
	 <script type="text/javascript">
	 var RecaptchaOptions = {
		theme : 'white'
	 };
	 </script>
	<?php
	//update_transaction_activites(1, 'Completed', 57.50);

	/* ///////////////////////////////////////// */
	/*	FORMULAIRE D'INSCRIPTION À UNE ACTIVITÉ  */
	/* ///////////////////////////////////////// */
	global $post;

	$output = '';	
	$output .= '<div id="FormulaireActivite">';
	$output .= '<form id="FormActivite" method="post" action="#" >';

	$output .= '<div class="SectionEntreprise">';
	//Champs d'entreprise	
	$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_entreprise',false).'</h2>';
	
	$output .= '<p><label for="entreprise">'.team_the_wpml('Contenu', 'contenu_formulaire_entreprise',false).'</label><input type="text" class="" value="" id="entreprise" name="entreprise" value=""></p>';
	$output .= '<p><label for="adresse">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_adresse',false).'</label><input type="text" class="" value="" id="adresse" name="adresse" value=""></p>';
	$output .= '<p><label for="ville">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_ville',false).'*</label><input type="text" class="Obligatoire" value="" id="ville" name="ville" value=""></p>';
	$output .= '<p><label for="code_postal">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_codepostal',false).'</label><input type="text" class="" value="" id="code_postal" name="code_postal" value=""></p>';
	$output .= '<p><label for="e_telephone">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_telephone',false).'*</label><input type="text" class="Obligatoire" value="" id="e_telephone" name="e_telephone" value=""></p>';
	//$output .= '<p><label for="e_telecopieur">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_fax',false).'</label><input type="text" value="" id="e_telecopieur" name="e_telecopieur" value=""></p>';
	$output .= '<p><label for="secteur_activite">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_secteuractivite',false).'</label><input type="text" class="" value="" id="secteur_activite" name="secteur_activite" value=""></p>';
	$output .= '<div class="clear"></div></div>';
	
	//Champs de la personne responsable
	$output .= '<div class="SectionResponsable">';
	$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_responsable',false).'</h2>';
	$output .= '<div class="Participant" id="Participant_0">';
	
	$output .= '<p class="Salutation">';
	$output .= '<input checked id="salutation_m" name="p_salutation_0" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_m',false).'" /><label for="salutation_m">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_m',false).'</label>';
	$output .= '<input id="salutation_mme" name="p_salutation_0" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mme',false).'" /><label for="salutation_mme">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mme',false).'</label>';
	//$output .= '<input id="salutation_mlle" name="p_salutation_0" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mlle',false).'" /><label for="salutation_mlle">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mlle',false).'</label>';	
	$output .= '</p>';
	
	/*$output .= '<select name="p_salutation[]"><option selected value="M.">M.</option><option value="Mme.">Mme.</option></select>';*/
	$output .= '<p><label for="r_nom">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_nom',false).'*</label><input type="text" class="Obligatoire" value="" id="r_nom" name="p_nom[] value=""></p>';
	$output .= '<p><label for="r_fonction">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_fonction',false).'</label><input type="text" class="" id="r_fonction" value="" name="p_fonction[]" value=""></p>';
	$output .= '<p class="DInput"><label class="LabelTelephone" for="r_telephone">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_telephone',false).'*</label><label class="LabelPoste" for="r_poste">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_poste',false).'</label><input class="CTelephone Obligatoire" type="text" class="Obligatoire" id="r_telephone" value="" name="p_telephone[]" value=""><input class="CPoste" type="text" id="r_poste" value="" name="p_poste[]" value=""></p>';
	$output .= '<p><label for="r_courriel">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_courriel',false).'*</label><input type="text" class="Obligatoire" id="r_courriel" value="" name="p_courriel[]" value=""></p>';
	$output .= '<div class="clear"></div><label for="commentaires">'.team_the_wpml('Contenu', 'commentaires_inscription',false).'</label><textarea id="commentaires" name="commentaires"></textarea>';

	$output .= '<div class="clear"></div>';
	
	$output .= '<input type="hidden" name="p_responsable[]" value="1" />';
	$output .= '<p class="RParticipe"><label for="r_participe">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_rparticipe',false).'</label><input type="checkbox" value="1" id="r_participe" name="participation_responsable" ></p>';
	$output .= '<div class="clear"></div><div id="ConteneurAjaxResponsable"></div>';
	//$output .= genere_conference($post->ID,0);
	$output .= '<div class="clear"></div>';
	$output .= '</div>';
	$output .= '</div>';

	//Type d'inscription - Tarification
	global $wp_query;
	
	$postslist = get_types_inscrits($post->ID);
	$count = count($postslist);
	$ll = 0;
	
	if (!empty($postslist)) {
	$output .= '<div class="SectionTarification">';
		$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_champs_tarification',false).'</h2>';		
		foreach( $postslist as $type_inscription ) {
			if ($ll == 0) {
				$output .= '<div class="BoiteTarif"><label for="type_inscription'.$type_inscription->ID.'">'.$type_inscription->post_title.'</label><div class="FondInput"><input checked type="radio" name="type_inscrit" id="type_inscription'.$type_inscription->ID.'" value="'.$type_inscription->ID.'"></div></div>';		
			}
			else {
				$output .= '<div class="BoiteTarif"><label for="type_inscription'.$type_inscription->ID.'">'.$type_inscription->post_title.'</label><div class="FondInput"><input type="radio" name="type_inscrit" id="type_inscription'.$type_inscription->ID.'" value="'.$type_inscription->ID.'"></div></div>';
		
			}
			$ll++;
		}	
	}	
	else {	
		$output .= '<div class="SectionTarification NoDisplay">';
		$output .= '<div class="BoiteTarif"><div class="FondInput"><input class="NoDisplay" type="radio" checked name="type_inscrit" id="type_inscription_tous" value=""></div></div>';		
	}
		
	
	$output .= '<div class="clear"></div></div>';
	$output .= '<hr/>';
	
	//Champs des autres participants
	$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_identification',false).'</h2>';
	$output .= '<div id="ConteneurAjax"></div>';
	
	$output .= '<p class="Bouton BoutonAjout"><a class="SubmitAjoutParticipant">'.team_the_wpml('Contenu', 'contenu_formulaire_ajout_participant',false).'</a></p>';	
	$output .= '<div class="ChargementAjax"></div><div class="clear"></div>';
	$output .= '<hr />';
	
	//Affichage du prix
	$output .= '<p class="Mini">'.team_the_wpml('Contenu', 'contenu_formulaire_soustotal',false).'<span class="SousTotal"></span></p>';
	if (get_option( "taxe_tvh" , "" ) != "") {
		$taxe_tvh = get_option( "taxe_tvh" , "" );	
	
		$output .= '<p class="Mini">TVH ('.$taxe_tvh.' %) : <span class="TaxesTVH"></span></p>';
	
	}
	else {
		$taxe_tps = get_option( "taxe_tps" , "" );
		$taxe_tvq = get_option( "taxe_tvq" , "" );	
	
		$output .= '<p class="Mini">TPS ('.$taxe_tps.' %) : <span class="TaxesTPS"></span></p>';
		$output .= '<p class="Mini">TVQ ('.$taxe_tvq.' %) : <span class="TaxesTVQ"></span></p>';	
	}

	$output .= '<p class="Medium">'.team_the_wpml('Contenu', 'contenu_formulaire_total',false).'<span class="Total"></span></p>';	
	
	$output .= '<hr />';
	
	//Champs cachés
	$output .= '<input type="Hidden" value="sauvegarde" name="action" />';
	$output .= '<input type="Hidden" value="0" name="status" />';
	$output .= '<input type="Hidden" value="'.$post->ID.'" name="post_id" />';
	$output .= '<input type="Hidden" value="0" name="nb_participant" />';
	$output .= '<input type="Hidden" value="'.team_the_wpml('System', 'language_code',false).'" name="langue" />';
	
	$output .= '<div class="ZonePaiement">';
	$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_choixpaiement',false).'</h2>';
	
	//Champs concernant le paiement (si activité payante)
	$output .= '<p class="ModePaiement" ><input name="type_paiement" type="radio" value="0" id="TPPaypal" ><label for="TPPaypal"><img src="'.get_bloginfo('template_directory').'/images/paypal_cartes.png" title="Paypal" /></label>';
	$output .= '<input name="type_paiement" type="radio" value="2" id="TPCheque" ><label for="TPCheque">'.team_the_wpml('Contenu', 'contenu_formulaire_paycheque',false).'</label>';
	
	//Affiche l'option de gratuité si l'utilisateur est connecté et de type client
	if (is_user_logged_in() && current_user_can( 'client_user' ) ) {
		$output .= '<input name="type_paiement" type="radio" value="3" id="TPGratuit" ><label for="TPGratuit">'.team_the_wpml('Contenu', 'contenu_formulaire_paygratuit',false).'</label>';
	}
	$output .= '</p>';
	$output .= '<div class="clear"></div></div>';

	//Faux bouton paiment temporaire
	$output .= '<div class="clear"></div>';
	
	$output .= '<p class="RParticipe PFacture"><label for="facturation">'.team_the_wpml('Contenu', 'contenu_formulaire_facture',false).'</label><input type="checkbox" value="1" id="facturation" name="facturation" ></p>';

	
    require_once('recaptchalib.php');
	if(team_language() == 'fr') {
	    $publickey = "6LfuOfISAAAAAKC_GhA82ezUnoxocGwnw8hyU4Qd&amp;hl=fr"; // you got this from the signup page
	}
	else {
	    $publickey = "6LfuOfISAAAAAKC_GhA82ezUnoxocGwnw8hyU4Qd&amp;hl=en"; // you got this from the signup page
	}

    $output .= recaptcha_get_html($publickey);

	
	$output .= '<p class="Bouton"><a class="SubmitSauvegarde">'.team_the_wpml('Contenu', 'contenu_formulaire_inscription',false).'</a></p><div class="clear"></div>';
	
	
	$output .= '</form>';
	
	//Bouton Expression Checkout Paypal
	//Bouton Achat Sandbox
	
	//Desactive tempo
		$output .= '<form class="FormPaypal NoDisplay" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="ibittar@expansionpme.org">
		<input type="hidden" name="lc" value="CA">
		<input type="hidden" name="item_name" value="'.$post->post_title.'">
		<input type="hidden" name="amount" value="0">
		<input type="hidden" name="currency_code" value="CAD">
		<input type="hidden" name="button_subtype" value="services">
		<input type="hidden" name="no_note" value="0">
		<input type="hidden" name="cn" value="Add special instructions to the seller:">
		<input type="hidden" name="no_shipping" value="2">
		<input type="hidden" class="HiddenQty" name="quantity" value="1">
		<input type="hidden" name="tax_rate" value="0.000">
		<input type="hidden" name="shipping" value="0.00">
		<input type="hidden" class="IDTransaction" name="custom" value="'.$_SESSION['TransactionID'].'" />
		<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHosted">
		<input type="image" src="https://www.paypalobjects.com/fr_CA/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
		<img alt="" border="0" src="https://www.paypalobjects.com/fr_XC/i/scr/pixel.gif" width="1" height="1">
		</form>';
	
	//<input type="hidden" name="tax_rate" value="14.975">
		
	//Bouton Achat								
	/*if (get_post_meta($_POST['IDActivite'],'PaypalActive',true) == 'yes') {
		$output .= '<form class="FormPaypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">';
		$output .= '<input type="hidden" name="cmd" value="_xclick">';
		$output .= '<input type="hidden" name="business" value="guylaine.lagace@dpme.ca">';
		$output .= '<input type="hidden" name="lc" value="CA">';
		$output .= '<input type="hidden" name="item_name" value="'.$_POST['TitreActivite'].'">';
		$output .= '<input type="hidden" name="amount" value="'.$prix_activite.'">';
		$output .= '<input type="hidden" name="currency_code" value="CAD">';
		$output .= '<input type="hidden" name="button_subtype" value="services">';
		$output .= '<input type="hidden" name="no_note" value="0">';
		$output .= '<input type="hidden" name="tax_rate" value="14.975">';
		$output .= '<input type="hidden" name="no_shipping" value="1" />';
		$output .= '<input type="hidden" name="shipping" value="0.00">';
		$output .= '<input type="hidden" class="HiddenQty" name="quantity" value="1">';
		$output .= '<input type="hidden" name="charset" value="utf-8" />';		
		$output .= '<input type="hidden" name="custom" value="'.$_SESSION['TransactionID'].'" />';
		$output .= '<input type="hidden" name="bn" value="PP-BuyNowBF:payez_paypal_fr.gif:NonHostedGuest">';
		$output .= '<input class="BtnPaypal" type="image" src="http://www.dpme.ca/wp-content/themes/teampress/images/payez_paypal_fr.png" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">';
		$output .= '<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">';
		$output .= '</form>';
	
	}*/
	
	$output .= '</div>';
	
	echo $output;
	
	//Aller chercher le post de l'ajax
	$url_ajax = team_custom_post_the_permalink('inscription-activite','ajax',false);
	$url_confirmation = team_the_permalink('confirmation-inscription',false);
	$activites_responsable = genere_conference($post->ID,0);
	
?>
<script type="text/javascript">

var url_confirmation = '<?php echo $url_confirmation; ?>';
var url_ajax = '<?php echo $url_ajax; ?>';
var post_id = '<?php echo $post->ID; ?>';
var activites_responsable = '<?php echo $activites_responsable; ?>';
var nb_participant = 0; //Contient le nombre de participant actuel
var no_participant = 1; //Contient le numéro du dernier participant créé
var gratuite = '<?php team_the_wpml('Contenu', 'contenu_formulaire_paygratuit'); ?>';

function RafraichirPrix(data) {		
		
		//alert(data);
		var tbl_prix = jQuery.parseJSON( data );

		if (tbl_prix['error'] == 'error') {
			//message_participant = 'Le nombre maximal de participants de '+  tbl_prix['max_participant'] +' a été dépassé.';
			//jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');
		}
		else if (tbl_prix['error_captcha'] == 'error') {
			//message_participant = 'Le captcha est incorrect. Veuillez réssayez.';
			//jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');		
		}
		else {
			if (jQuery('form#FormActivite input[name="langue"]').val() == 'fr') {
				
				if (parseInt(tbl_prix['total']) <= 0) {
					jQuery('span.Total').html(gratuite);
					jQuery('div.ZonePaiement input#TPGratuit').attr('checked','checked');
					jQuery('div.ZonePaiement').css('display','none');
					//jQuery('p.PFacture').css('display','none');
					
				}
				else {
					jQuery('span.Total').html(tbl_prix['total']+' $');
					jQuery('div.ZonePaiement').css('display','block');
					//jQuery('p.PFacture').css('display','block');
					if (jQuery('div.ZonePaiement input#TPGratuit').is(':checked')) {
						jQuery('div.ZonePaiement input#TPGratuit').attr('checked',false);
					}
					
				}
				
				jQuery('span.SousTotal').html(tbl_prix['sous_total']+' $');
				
				if (tbl_prix['taxes'][0]['nom'] == 'THV') {
					jQuery('span.TaxesTVH').html(tbl_prix['taxes'][0]['valeur']+' $');
				}
				else {
					jQuery('span.TaxesTPS').html(tbl_prix['taxes'][0]['valeur']+' $');
					jQuery('span.TaxesTVQ').html(tbl_prix['taxes'][1]['valeur']+' $');
				}
			
			}
			else {

				jQuery('span.Total').html('$ '+tbl_prix['total']);
				jQuery('span.SousTotal').html('$ '+tbl_prix['sous_total']);
				
				if (tbl_prix['taxes'][0]['nom'] == 'THV') {
					jQuery('span.TaxesTVH').html('$ '+tbl_prix['taxes'][0]['valeur']);
				}
				else {
					jQuery('span.TaxesTPS').html('$ '+tbl_prix['taxes'][0]['valeur']);
					jQuery('span.TaxesTVQ').html('$ '+tbl_prix['taxes'][1]['valeur']);
				}
			
			}
		}
		
}

	//Vérifie que les champs sont bien remplis avant de soumettre pour le paiement
	function verification_form() {
	
		var flag_valide = 1;
		var message_erreur = '<?php team_the_wpml('System', 'champ_obligatoire'); ?>';
	
		//Verification des inputs en lot
		jQuery('form#FormActivite p input.Obligatoire').each(function() {
		
			if (jQuery(this).val() == '') {				
				flag_valide = 0;
				//jQuery(this).parent().append('<div class="BoiteErreur">'+message_erreur+'</div>');					
				jQuery(this).css('border-color','red');					
			}
			
		});
		
		//Vérification des radio box
		if (! jQuery("form#FormActivite p input[name='type_paiement']").is(':checked')) {
				flag_valide = 0;
				//jQuery("form#FormActivite p input[name='type_paiement']").parent().append('<div class="BoiteErreur">'+message_erreur+'</div>');			
				jQuery("form#FormActivite div.ZonePaiement").css('border-color','red');			
		}
		
		if (! jQuery("form#FormActivite input[name='type_inscrit']").is(':checked')) {
				flag_valide = 0;
				//jQuery("form#FormActivite p input[name='type_paiement']").parent().append('<div class="BoiteErreur">'+message_erreur+'</div>');			
				jQuery("form#FormActivite div.SectionTarification").css('border-color','red');			
		}		
		
		//Vérification des select
		jQuery('form#FormActivite select.Obligatoire').each(function() {	
			if (jQuery(this).val() == "-1") {
				flag_valide = 0;
				//jQuery(this).parent().append('<div class="BoiteErreur">'+message_erreur+'</div>');	
				jQuery(this).css('border-color','red');					
			}
		});
			
			var cpt_participant = 0;
			var cpt_conference = 0;
			var cpt_selected = 0;
			
			//Boucle sur les participants pour connaitre leur conferences		
			while ( cpt_participant < no_participant) {			
				if (jQuery('div#Participant_'+cpt_participant).length >= 1) {				
					if (jQuery('div#Participant_'+cpt_participant+' input.ChoixConference').length >= 1) {
						cpt_conference = 0;
						jQuery('div#Participant_'+cpt_participant+' input.ChoixConference:checked').each(function() {
							cpt_conference++;
							cpt_selected++;
						});		
						
						if (cpt_conference == 0) {
							flag_valide = 0;
							//jQuery('div#Participant_'+cpt_participant+' p.ListingConference').append('<div class="BoiteErreur">'+message_erreur+'</div>');							
							jQuery('div#Participant_'+cpt_participant+' div.ListingConference').css('border-color','red');							
						}
						
					}
					
					
				}			
				cpt_participant++;
			}
			
			//Une conférence au minimum total
			if (cpt_selected == 0) {
				flag_valide = 0;
				//jQuery('div#Participant_0').append('<div class="BoiteErreur">Une activité doit être sélectionné au minimum</div>');	
			}
	
		return flag_valide;
	}

jQuery(document).ready(function() {

	//Ajouter un participant
	jQuery('a.SubmitAjoutParticipant').click(function(e) {
		e.preventDefault();

		jQuery('div.ChargementAjax').fadeIn(200);
	
		var arr_action = {};			
		arr_action["no_participant"] = no_participant;			
		arr_action["nb_participant"] = nb_participant;		
		arr_action["action"] = 'ajout_participant';
		arr_action["post_id"] = post_id;
			
		jQuery.ajax({url: url_ajax,cache: false, type: "POST", data: arr_action, success: function(data) {
		
			jQuery('div#ConteneurAjax').append(data);
			jQuery('div#Participant_'+no_participant).slideDown(600);
			jQuery('div.ChargementAjax').fadeOut(200);
			nb_participant++;
			no_participant++;
			jQuery('form#FormActivite input[name="nb_participant"]').val(nb_participant);			
						
			DemandeNouveauPrix();
		}});
	
	});
	
	//Participation du responsable aux activités
	jQuery('input[name="participation_responsable"]').click(function(e) {	
					
		if (jQuery('input[name="participation_responsable"]').is(':checked')) {
			jQuery('div#ConteneurAjaxResponsable').append(activites_responsable);							
		}
		else {
			jQuery('div#ConteneurAjaxResponsable div.ListingConference').remove();
		}
		
	});		

	//Supprimer un participant
	//.On pour capturer le contenu généré en AJAX
	jQuery('div#ConteneurAjax').on( 'click', 'a.SubmitSupressionParticipant', function(e) {
		e.preventDefault();
		
		var id_participant = jQuery(this).parent().parent().attr('id');
		
		jQuery('div#'+id_participant).slideUp(600,function() {
			jQuery('div#'+id_participant).remove();
			nb_participant--;
			jQuery('form#FormActivite input[name="nb_participant"]').val(nb_participant);
			jQuery.ajax({url: url_ajax+'?refresh=1',cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {
				RafraichirPrix(data);		
			}});	
		});
		
	});	

	//Sauvegarder les informations, puis envoyer vers Paypal
	jQuery('a.SubmitSauvegarde').click(function(e) {
		e.preventDefault();

		var form_valide = 0;
		var type_paiement = '';			
		form_valide = verification_form();
		
		//Type de paiement
		// 0 = Paiement via Paypal - En attente
		// 1 = Paiement via Paypal - Paiement reçu
		// 2 = Paiement via Chèque
		// 3 = Activité Gratuite	
		// 4 = Paiement via Paypal - Conflit

		if (form_valide == 1) {	
		
			type_paiement = jQuery("form#FormActivite p input[name='type_paiement']:checked").val();

			if (type_paiement == 0) {

				jQuery('div.ChargementAjax').fadeIn(200);
				jQuery('form#FormActivite input[name="status"]').val(0);
				jQuery.ajax({url: url_ajax+'?refresh=2',cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {				
						//alert(data);
						//Vu que les données ont étés enregistrées, envoyer vers Paypal
						RafraichirPrix(data);
						var tbl_transaction = jQuery.parseJSON( data );
						
						if (tbl_transaction['error'] == 'error') {
							message_participant = 'Le nombre maximal de '+  tbl_transaction['max_participant'] +' participants a été dépassé.';
							jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');
						}
						else if (tbl_transaction['error_captcha'] == 'error') {

							message_participant = 'Le captcha est incorrect. Veuillez réssayez.';
							jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');		
						}
						else {
							jQuery('input.IDTransaction').val(tbl_transaction['id_transaction']);	
							tbl_transaction['total'] = tbl_transaction['total'].replace(' ','');				
							tbl_transaction['total'] = tbl_transaction['total'].replace(',','.');						
							jQuery('input[name="amount"]').val(tbl_transaction['total']);						
						
							jQuery('form.FormPaypal').submit(); 
						}
						
											 
				}});	
				
			}
			else if (type_paiement == 2) {
				
				jQuery('form#FormActivite input[name="status"]').val(2);
				jQuery('div.ChargementAjax').fadeIn(200);
				jQuery.ajax({url: url_ajax+'?refresh=2',cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {				
					//alert(data);
					RafraichirPrix(data);
					var tbl_transaction = jQuery.parseJSON( data );
					jQuery('div.ChargementAjax').fadeOut(200);					
					//Message de confirmation sur la page web
					
					if (tbl_transaction['error'] == 'error') {
						message_participant = 'Le nombre maximal de '+  tbl_transaction['max_participant'] +' participants a été dépassé.';
						jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');
					}
					else if (tbl_transaction['error_captcha'] == 'error') {
						message_participant = 'Le captcha est incorrect. Veuillez réssayez.';
						jQuery('form#FormActivite').append('<div class="BoiteErreurParticipant">'+message_participant+'</div>');		
					}
					else {
						window.location.href = url_confirmation;
					} 
				}});
				
			
			}
			else if (type_paiement == 3) {

				jQuery('form#FormActivite input[name="status"]').val(3);
				jQuery('div.ChargementAjax').fadeIn(200);
				jQuery.ajax({url: url_ajax+'?refresh=2',cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {				
				
					if (data == false) {
						//Possibilité de mettre un message d'erreur
					}
					window.location.href = url_confirmation;
				
					jQuery('div.ChargementAjax').fadeOut(200);					
					//Message de confirmation sur la page web
					 
				}});
			
			}								
		}		
	});			
	
	function DemandeNouveauPrix() {
		jQuery.ajax({url: url_ajax+'?refresh=1',cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {
			//alert(data);
			RafraichirPrix(data);		
		}});
	}
	
	//Attrape les évènements pour recalculer le prix 
	jQuery('form#FormActivite').on( 'click', 'input.ChoixConference,form#FormActivite select.Obligatoire, div.FondInput input, a.SubmitSupressionParticipant', function(e) {
		DemandeNouveauPrix();
	});				
	
	jQuery('form#FormActivite').on('click','input#r_participe', function(e) {
		if (jQuery('input#r_participe').is(':checked')) {
			nb_participant++;
		}
		else {
			nb_participant--;			
		}
		jQuery('form#FormActivite input[name="nb_participant"]').val(nb_participant);
		DemandeNouveauPrix();
	});

	//Efface les message d'erreur sur un :hover
	jQuery(document).on({
		click: function () {
			jQuery(this).css('border-color','#AAAAAA');
			/*(400,function() {
				jQuery(this).remove();
			});*/
		}
	}, 'input');
	
	//Efface les message d'erreur sur un :hover
	jQuery(document).on({
		click: function () {
			jQuery(this).css('border-color','#AAAAAA');
			/*(400,function() {
				jQuery(this).remove();
			});*/
		}
	}, 'input');
		
	jQuery(document).on({
		mouseenter: function () {
			jQuery(this).fadeOut(400,function() {
				jQuery(this).remove();
			});
		}
	}, 'div.BoiteErreurParticipant');		
		
	
	jQuery(document).on({
		click: function () {	
			jQuery('div.ZonePaiement').css('border-color','transparent');
		}
	}, 'div.ZonePaiement input');	
	
	jQuery(document).on({
		click: function () {
			jQuery(this).parent().parent().css('border-color','transparent');
		}
	}, ' div.ListingConference input');	

	jQuery(document).on({
		click: function () {	
			jQuery('div.SectionTarification').css('border-color','transparent');
		}
	}, 'div.SectionTarification input');	
	
			
});

</script>
<?php
	}
	
	//Fonction pour faire le listing des conférences pour chaque participant (s'il y a lieu)
	function genere_conference($post_id, $cpt_nom) {
		
		$output = '';	
		$ii = 0; //Recherche dans les posts_meta
		$jj = 0; //Nombre trouvé
		
		$nb_conferences = get_post_meta( $post_id, 'nb_conferences' , true ); //On veut aller le nombre de conférence	

		if ($nb_conferences > 0) {
			$output .= '<div class="ListingConference">';
			$output .= '<h2>'.team_the_wpml('Contenu', 'contenu_formulaire_choix',false).'</h2>';
			$output .= '<p class="ListingConference">';
			while( $jj < $nb_conferences ) {
				$meta_value =  '';
				$meta_value = get_post_meta( $post_id, 'conference_'.$ii , true );
				
				if ($meta_value != '') {
					$output .= '<input class="ChoixConference" type="checkbox" value="conference_'.$ii.'" id="choix_conference_'.$cpt_nom.'_'.$ii.'" name="choix_conference_'.$cpt_nom.'[]" >';
					$output .= '<label for="choix_conference_'.$cpt_nom.'_'.$ii.'" >'.$meta_value.'</label>';			
					$jj++;
				}
				$ii++;
			}	
			$output .= '<input type="hidden" name="id_participant_'.$cpt_nom.'" value="1" />';
			$output .= '</p>';	
			$output .= '<div class="clear"></div></div>';
		}
		else {
			//conference_0 , c'est une activité qui ne peut pas être divisible en plusieurs section "conférences"
			$output .= '<div class="ListingConference NoDisplay"><p class="ListingConference"><input class="NoDisplay ChoixConference" type="checkbox" value="conference_0" name="choix_conference_'.$cpt_nom.'[]" checked ></p>';
			$output .= '<input type="hidden" name="id_participant_'.$cpt_nom.'" value="1" /><div class="clear"></div></div>';
			//$output .= $meta_value;
		}
		return $output;
	}	
	
	function get_types_inscrits($post_id) {
		// Charge toutes les conditions de cette activité et liste les conditions reliées au type d'inscrit.
		// Fournit les posts des types d'inscrits qui influencent le prix (sans doublon).
		

		
		$post_types_inscrits = array();
		$types_inscrits = array();
		$conditions_prix = get_conditions($post_id);
		
		foreach ($conditions_prix as $condition_prix) {
			$obj_condition = new conditionPrix();
			$obj_condition->load_condition_prix($condition_prix->ID);
			
			$type_inscrit = $obj_condition->get_condition_prix();
			$type_inscrit = $type_inscrit['type_inscrit'];
			
			if(!in_array($type_inscrit, $types_inscrits) && $type_inscrit != "" ) {
				$types_inscrits[] = $type_inscrit;
			}
		}
	
		// Remplace les slugs par les posts objects
		foreach($types_inscrits as $slug) {
			$post_types_inscrits[] = team_get_custom_post_by_slug($slug, 'types-inscrits');
		}
		
		return $post_types_inscrits;
	}
		
?>