<?php

	// Fonction permettant de faire fitter le texte "$textToFit" dans une hauteur "$height" donn�e
	// Note: on doit fournir les dimensions de l'image car si l'image prend du temps � se loader, elle ne prendra pas l'espace qu'elle requiert
	// et le calcul s'en trouvera affect� lorsque l'image sera load�e.
	function textFit($textToFit, $height, $viewMoreImage = '', $imageWidth = 0, $imageHeight = 0) {
		
		static $nextId = 1;
		
		$DivId = 'Fit_'. $nextId++;
		$output = '';
		
		if($viewMoreImage) {
			$viewMoreHTML = '... <a href="' . get_permalink() . '"><img src="' . get_bloginfo('template_directory') . '/images/' . $viewMoreImage . '" alt="..." width="' . $imageWidth . '" height="' . $imageHeight . '" /></a>';
		} else {
			$viewMoreHTML = '...';
		}
		
		$output .= "\n" . '<div class="FittedText" id="' . $DivId . '">' . "\n" . $textToFit . "\n" . '</div>' . "\n";
		$output .= '
			<script type="text/javascript">
				// Lorsque jQuery est pr�t, active le redimensionnement auto du texte
				jQuery(document).ready(function() {
					jQuery("div#' . $DivId . '").textFit({maxHeight: ' . $height . ', viewMoreHTML: \'' . $viewMoreHTML . '\'});
				});		
			</script>';
		
		echo $output;
	}

	wp_register_script('jquery-team-textfit', get_bloginfo('template_directory') . '/js/jquery.team-textfit.js', array('jquery'), '1.0');
	wp_enqueue_script('jquery-team-textfit');	
	
?>