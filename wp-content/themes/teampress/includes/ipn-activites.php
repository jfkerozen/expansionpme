<?php
/* ////////////////////////////////////////// */
/*	ENREGISTREMENT DU RETOUR DE L'IPN PAYPAL  */
/* ////////////////////////////////////////// */

	//Réception du paiment via paypal, affecter dans la BD
	function update_transaction_activites($transaction_id, $status, $total) {

		if ($status == 'Completed') { //Paiement accepté
		
			global $wpdb;
			//Verfier que l'on a paye le bon montant
			$sql_transaction = "SELECT total FROM {$wpdb->prefix}act_transactions WHERE ID = $transaction_id";
			$transaction = $wpdb->get_row($sql_transaction, OBJECT);
			$montant_recu = $transaction->total;
			
			if ($transaction->total == $total) {
				$valide = 1; // 1 = Paiement completé
			}
			else {
				$valide = 4; //Paiement avec conflit
			}

			$data_type = array('%d');			
			$data_transaction = array('status' 	=> $valide ); //Status paiement reçu via Paypal
			$data_where	= array ('ID' => $transaction_id);	
			$data_type_where = array('%d');
			
			$wpdb->update($wpdb->prefix.'act_transactions', $data_transaction,$data_where,$data_type,$data_type_where);			
			
			//Courriel pour Expansion PME + Accusé de réception
			if ($valide == 1) {
				courriel_paypal($transaction_id,$total);

			}
			else {
				//Conflit de paiemtn, courriel spécial appeler expansionpme?
				courriel_conflit($transaction_id,$total, $montant_recu);

			}
			
		
		}
		else if ($status == 'Denied') { // Paiement refusé
			//Avertir le client
			
		}
		else if ($status == 'Pending') { //En attente
			
		}
	
	}
	
	//Envoyé lors de l'inscription à une activité via le choix d'un chèque
	function courriel_conflit($transaction_id, $total, $montant_recu) {
	
		$subject = "Conflit paiement Paypal expansionpme.org";

		$message = 'Un conflit de paiement est survenu. Nous avons recu une somme de : ' . number_format((double)$montant_recu, 2) . " $\r\n<br/>";
		$message .= 'Montant qui aurait dû être reçu : ' . number_format((double)$total, 2) . " $\r\n<br/>";
	
		$to = get_option( "courriel_administration" , "inscription@expansionpme.org" ); //Comptabilité pour les factures		
		$message_html = '<html><body>' . $message . '</body></html>';
		
		$admin_email = get_bloginfo('admin_email');
		$headers = "MIME-Version: 1.0\n" .
							 "From: Expansion PME <$admin_email>\n" .
							 "Content-Type: text/html; charset=\"" .
							 get_option('blog_charset') . "\"\n";		
		wp_mail($to, $subject, $message_html, $headers);		
	}

	//Envoyé lors de l'inscription à une activité via Paypal et que le paiement a été reçu
	function courriel_paypal($transaction_id, $total) {

		global $wpdb;
	
		//Aller chercher la langue lors de la transaction
		$sql_transaction ="SELECT langue FROM {$wpdb->prefix}act_transactions WHERE ID = $transaction_id ";
		$info_transaction = $wpdb->get_row($sql_transaction, OBJECT);
		
		//Aller chercher les informations concernant l'activité pour le courriel
		$sql_activite = "SELECT {$wpdb->prefix}posts.post_title as nom_activite,
								{$wpdb->prefix}posts.ID as activite_id ,
								{$wpdb->prefix}act_transactions.facturation as facturation, 
								{$wpdb->prefix}act_transactions.commentaires as commentaires, 
								{$wpdb->prefix}act_transactions.status as type_transaction, 
								{$wpdb->prefix}act_transactions.sous_total as sous_total,
								{$wpdb->prefix}act_transactions.taxes as taxes, 
								{$wpdb->prefix}act_transactions.total as total FROM {$wpdb->prefix}posts
										INNER JOIN {$wpdb->prefix}act_reservations ON {$wpdb->prefix}posts.ID = {$wpdb->prefix}act_reservations.post_id
										INNER JOIN {$wpdb->prefix}act_transactions ON {$wpdb->prefix}act_reservations.transaction_id = {$wpdb->prefix}act_transactions.ID
										WHERE {$wpdb->prefix}act_transactions.ID = $transaction_id LIMIT 1";
		$info_activite = $wpdb->get_row($sql_activite, OBJECT);								
	
		//Avertir le vendeur de la réception d'un paiement
		//Information sur l'activité
		$message = '';
		//$message = "Un paiement via Paypal a &eacute;t&eacute; re&ccedil;u et approuv&eacute;.";			
		$message .= '<br/><br/>';
		$message .= '<b>-- Activit&eacute; --</b><br/><br/>';
		//$message .= 'Date de l\'activit&eacute; : '. $info_activite->date_activite .'<br/>';
		$message .= 'Titre de l\'activit&eacute; : '.  $info_activite->nom_activite .'<br/>';	
	
		if ($info_activite->type_transaction == 1) {
			$message .= 'Type de transaction : Via Paypal. Le paiement total a été reçu.<br/>';
			$message .= 'Paiement re&ccedil;u : '. number_format((double)$total, 2) . ' $<br/>';		
		}
		else if ($info_activite->type_transaction == 2) {
			$message .= 'Type de transaction : Via chèque.<br/>';
			$message .= 'Paiement à recevoir : '. number_format((double) $info_activite->total, 2) . ' $<br/>';	
		}
		else if ($info_activite->type_transaction == 3) {
			$message .= 'Type de transaction : Activité gratuite.<br/>';
		}
		
		$message .= 'Langue lors de la transaction : '. $info_transaction->langue .'<br/>';		

		//Information sur l'entreprise et les participants et leurs réservations
		$sql_reservations =  "SELECT	{$wpdb->prefix}act_entreprises.ID AS id_entreprise, 
																{$wpdb->prefix}act_entreprises.entreprise AS nom_entreprise,
																{$wpdb->prefix}act_entreprises.adresse AS adresse_entreprise,
																{$wpdb->prefix}act_entreprises.ville AS ville_entreprise,
																{$wpdb->prefix}act_entreprises.code_postal AS code_postal_entreprise,
																{$wpdb->prefix}act_entreprises.telephone AS telephone_entreprise,
																{$wpdb->prefix}act_entreprises.telecopieur AS telecopieur_entreprise,
																{$wpdb->prefix}act_entreprises.secteur_activite AS secteur_activite_entreprise,
																{$wpdb->prefix}act_participants.ID AS id_participant,
																{$wpdb->prefix}act_participants.salutation AS salutation_participant,
																{$wpdb->prefix}act_participants.nom AS nom_participant,
																{$wpdb->prefix}act_participants.telephone AS telephone_participant,
																{$wpdb->prefix}act_participants.courriel AS courriel_participant,
																{$wpdb->prefix}act_participants.fonction AS fonction_participant,
																{$wpdb->prefix}act_participants.responsable AS is_responsable_participant,
																{$wpdb->prefix}act_reservations.ID AS id_reservation,
																{$wpdb->prefix}act_reservations.post_id AS post_id_activite,
																{$wpdb->prefix}act_reservations.meta_key AS meta_key_conference
													FROM {$wpdb->prefix}act_reservations 
														INNER JOIN {$wpdb->prefix}act_participants ON ({$wpdb->prefix}act_reservations.participant_id = {$wpdb->prefix}act_participants.ID AND {$wpdb->prefix}act_reservations.transaction_id = $transaction_id)
														INNER JOIN {$wpdb->prefix}act_entreprises ON ({$wpdb->prefix}act_entreprises.ID = {$wpdb->prefix}act_participants.entreprise_id)
													WHERE {$wpdb->prefix}act_reservations.transaction_id = $transaction_id ORDER BY id_participant ASC, id_reservation ASC";
			
		/* DEBUG NM */
		//$message .= "<br/>\r\n<br/>\r\n" . $sql_entreprise . "<br/>\r\n<br/>\r\n";
												 
		$reservations = $wpdb->get_results($sql_reservations, OBJECT);
		
		
		$entreprise_id = $reservations[0]->id_entreprise;
		if ($entreprise_id == "") { //Au cas où...
			$entreprise_id = 0;
		}
		
		//Information sur le responsable de l'entreprise et ses réservations
		$sql_responsable =  "SELECT {$wpdb->prefix}act_entreprises.ID AS id_entreprise, 
																{$wpdb->prefix}act_participants.ID AS id_participant,
																{$wpdb->prefix}act_participants.salutation AS salutation_participant,
																{$wpdb->prefix}act_participants.nom AS nom_participant,
																{$wpdb->prefix}act_participants.telephone AS telephone_participant,
																{$wpdb->prefix}act_participants.courriel AS courriel_participant,
																{$wpdb->prefix}act_participants.fonction AS fonction_participant,
																{$wpdb->prefix}act_participants.responsable AS is_responsable_participant,
																{$wpdb->prefix}act_reservations.ID AS id_reservation,
																{$wpdb->prefix}act_reservations.post_id AS post_id_activite,
																{$wpdb->prefix}act_reservations.meta_key AS meta_key_conference
													FROM {$wpdb->prefix}act_participants
														INNER JOIN {$wpdb->prefix}act_entreprises ON ({$wpdb->prefix}act_entreprises.ID = {$wpdb->prefix}act_participants.entreprise_id AND {$wpdb->prefix}act_entreprises.ID = $entreprise_id AND {$wpdb->prefix}act_participants.responsable = 1)
														LEFT JOIN {$wpdb->prefix}act_reservations ON ({$wpdb->prefix}act_reservations.participant_id = {$wpdb->prefix}act_participants.ID)
													WHERE {$wpdb->prefix}act_entreprises.ID = $entreprise_id AND {$wpdb->prefix}act_participants.responsable = 1";		
													
		$reponsable = $wpdb->get_results($sql_responsable, OBJECT);
			
		//$message .= $sql_reservations;
		//$message .= $sql_responsable;
			
		$message .= '<br/><b>-- Informations sur l\'entreprise --</b><br/><br/>';
		$message .= 'Nom de l\'entreprise : '. $reservations[0]->nom_entreprise .'<br/>';		
		//$message .= 'Num&eacutero d\'entreprise du Qu&eacutebec (NEQ) : '. $reservations[0]->NEQ .'<br/>';		
		$message .= 'Adresse de l\'entreprise : '. $reservations[0]->adresse_entreprise .'<br/>';
		$message .= 'Ville : '. $reservations[0]->ville_entreprise .'<br/>';
		$message .= 'Code postal : '. $reservations[0]->code_postal_entreprise .'<br/>';
		$message .= 'T&eacute;l&eacute;phone : '. $reservations[0]->telephone_entreprise .'<br/>';
		//$message .= 'T&eacute;l&eacute;copieur : '. $reservations[0]->telecopieur_entreprise .'<br/>';
		$message .= 'Secteur d\'activit&eacute; : '. $reservations[0]->secteur_activite_entreprise .'<br/>';


		//Information sur le reponsable et leurs conférences choisies	
		$message .= '<br/><b>-- Responsable --</b><br />';		
		
		$last_id_participant = '';
		foreach ($reponsable as $reservation) {
			if($last_id_participant !== $reservation->id_participant) {
				if(empty($reservation->id_reservation)) {
					$message .= '<strong>Le responsable ne participe pas à l\'activité.</strong><br/>';
				} else {
					$message .= '<strong>Le responsable participe à l\'activité.</strong><br/>';
				}
				$message .= 'Nom : ' . $reservation->salutation_participant . ' ' . $reservation->nom_participant . '<br/>';
				$message .= 'Fonction : ' . $reservation->fonction_participant . '<br/>';
				$message .= 'T&eacute;l&eacute;phone : ' . $reservation->telephone_participant . '<br/>';
				$message .= 'Courriel : ' . $reservation->courriel_participant . '<br/><br/>';
				
				$courriel_client = $reservation->courriel_participant;
				
				$last_id_participant = $reservation->id_participant;
			}
			if($reservation->meta_key_conference != 'conference_0') { // Exclus les événements sans conférences: ne liste pas les conférences.
				$conference = get_post_meta($reservation->post_id_activite, $reservation->meta_key_conference, true);
				$message .= 'Conférence: ' . $conference . '<br/>';
			}
		}		
		
		//Information sur les participants et leurs conférences choisies	
		$message .= '<br/><b>-- Participants --</b><br />';		
				
		$last_id_participant = '';
		foreach ($reservations as $reservation) {
			if ($reservation->is_responsable_participant == 0) {
				if($last_id_participant !== $reservation->id_participant) {
					$message .= '<br/>';

					$message .= '<b>Participant</b><br />';
					
					$message .= 'Nom : ' . $reservation->salutation_participant . ' ' . $reservation->nom_participant . '<br/>';
					$message .= 'Fonction : ' . $reservation->fonction_participant . '<br/>';
					$message .= 'T&eacute;l&eacute;phone : ' . $reservation->telephone_participant . '<br/>';
					$message .= 'Courriel : ' . $reservation->courriel_participant . '<br/><br/>';
					
					$last_id_participant = $reservation->id_participant;
				}
				if($reservation->meta_key_conference != 'conference_0') { // Exclus les événements sans conférences: ne liste pas les conférences.
					$conference = get_post_meta($reservation->post_id_activite, $reservation->meta_key_conference, true);
					$message .= 'Conférence: ' . $conference . '<br/>';
				}
			}	
		}

		$message .= '<br/><b>-- Commentaires --</b><br />';		
		$message .= $info_activite->commentaires .'<br/>'; 
		
		/*
		$message .= '<b>-- Commentaires --</b><br/><br/>';
		$message .= $un_mail['message']->field_value .'<br /><br />';*/
		$message .= '<br />-- Fin du message --';				

		//Faire une liste de destinataire
		$destinataire_equipe = get_post_meta($info_activite->activite_id,'Cft_Courriel',false);	
		$nom_equipe = get_post_meta($info_activite->activite_id,'Cft_Nom',false);	
		$nb_destinataire = count($destinataire_equipe);

		$to = '';
		$ii = 1;
		if ($nb_destinataire == 0) {
			$to = 'inscription@expansionpme.org';
		}
		else {
			foreach ($destinataire_equipe as $un_destinataire) {
				if ($ii < $nb_destinataire) {
					$to .= $un_destinataire .',';
				}
				else {
					$to .= $un_destinataire;
				}
				$ii++;
				
			}
		}	

		$admin_email = "info@expansionpme.org" ;		

		if ($info_activite->type_transaction == 2 || $info_activite->type_transaction == 3) {
			$subject_facturation = "Facture à produire pour inscription à une activité sur le site de expansionpme.org";

			$message_facturation = "Une inscription pour une activité s'est produite avec succès. Vous trouverez plus bas les informations concernant l'inscription.<br/><br/>";
			$message_facturation .= 'Sous-total : '. number_format((double)$info_activite->sous_total, 2) . ' $<br />';
			$message_facturation .= 'Taxes : ' . number_format((double)$info_activite->taxes, 2) . ' $<br />';
			$message_facturation .= 'Total : ' . number_format((double)$info_activite->total, 2) .' $<br />';
			
			$message_facturation = $message_facturation . $message;
			
			$to_facture = get_option( "courriel_administration" , "inscription@expansionpme.org" ); //Comptabilité pour les factures	
			//$to_facture = 'p.cyr@equipeteam.com '; //Pour debug			
			$message_facturation_html = '<html><body>' . $message_facturation . '</body></html>';
			

			$headers = "MIME-Version: 1.0\n" .
								 "From: Expansion PME <$admin_email>\n" .
								 "Content-Type: text/html; charset=\"" .
								 get_option('blog_charset') . "\"\n";		
			wp_mail($to_facture, $subject_facturation, $message_facturation_html, $headers);	
		}
		
		//$to = 'p.cyr@equipeteam.com '; //Pour debug
		$subject = "Confirmation d'inscription à une activité d'Expansion PME";
		$message_html = '<html><body>' . $message . '</body></html>';
		

		$headers = "MIME-Version: 1.0\n" .
							 "From: Expansion PME <$admin_email>\n" .
							 "Content-Type: text/html; charset=\"" .
							 get_option('blog_charset') . "\"\n";		
		wp_mail($to, $subject, $message_html, $headers);

		$message_client = '';
		$message_client = get_post_meta($info_activite->activite_id, 'TexteActivite', true);
		if ($message_client == "") {
			$message_client = "Nous avons bien reçu votre demande d'inscription à l'activité.";
		}
		//$message_client = str_replace('[nom]',$un_participant['nom'],$message_client);
		
		if ($info_activite->type_transaction == 2) {
			$message_client .= '<br />-----<br /><b>Vous recevrez aussi une facture puisque vous avez choisi le mode de Paiement : Envoyez-moi une facture.</b>';
		}
		
		$attachments = '';
		if($attached_post = get_post_meta($info_activite->activite_id, 'FichierActivite', true)) {
			$attachments = array(WP_CONTENT_DIR . '/uploads/' . get_post_meta($attached_post, '_wp_attached_file', true));
		}		
		
		
		$to = $courriel_client;
		//$to = 'p.cyr@equipeteam.com '; //Pour debug
		$message_client_html = '<html><body>' . $message_client . '</body></html>';
		

		$headers = "MIME-Version: 1.0\n" .
							 "From: Expansion PME <$admin_email>\n" .
							 "Content-Type: text/html; charset=\"" .
							 get_option('blog_charset') . "\"\n";		
		wp_mail($to, $subject, $message_client_html, $headers, $$attachments);						

	}

	
?>