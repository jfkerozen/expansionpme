<?php
// Metabox pour les activités


include_once('classes/conditionPrix.class.php');


/* Définir la Metabox */
if (current_user_can('client') || current_user_can('administrator') ) {
	add_action( 'add_meta_boxes', 'team_activitesbox' );
	add_action( 'save_post', 'team_activitesbox_save' );
}

/* Ajoute la Metabox dans la partie principale */
function team_activitesbox() {	
    add_meta_box(
        'myplugin_sectionid',
        __( 'Informations de l\'activit&eacute;', 'team_parametre_page_activitesbox' ), 
        'team_parametre_page_activitesbox_box',
        'les-activites','normal','high'
    );
}

/* Affiche la Metabox */
function team_parametre_page_activitesbox_box( $post ) {

	//Verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );

	wp_enqueue_script('datePicker', get_template_directory_uri() . '/js/jquery.datePicker.js', array('jquery') );	  
	  
	// The actual fields for data entry
	$output .= '<h2 class="MetaTitre" >Informations obligatoires</h2>';

	//Informations de base
	$output .= '<table class="ActiviteTable" >';

	//Date Activité avec jQuery DatePicker
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'DateActivite', TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Date de l\'activité : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input class="datePicker" style="width:250px;" type="text" value="'.$current_data.'" name="DateActivite" id="DateActivite" /></td>';	
	$output .= '</tr>';

	//Prix normal de l'activité
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'PrixNormalActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Prix normal de l\'activité, sans signe (Ex.: 199.99) : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input style="width:250px;" type="text" value="'.$current_data.'" name="PrixNormalActivite" id="PrixNormalActivite" /></td>';	
	$output .= '</tr>';

	//Activité avec ou sans taxe? //1 = Taxes applicables TVQ/TPS  *** 0 = Taxes incluses
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'PrixTaxableActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Les taxes sont : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite">';
	$output .= ($current_data == 1 || $current_data == '') ? '<label for="ActiviteTaxable">Taxes applicables</label><input type="radio" id="ActivitePayante" name="PrixTaxableActivite" checked value="1"><label for="ActiviteGratuite">Taxes incluses</label><input type="radio" id="ActiviteGratuite" name="PrixTaxableActivite" value="0">' : '<label for="ActivitePayante">Taxes applicable</label><input type="radio" id="ActivitePayante" name="PrixTaxableActivite" value="1"><label for="ActiviteGratuite">Taxes incluses</label><input type="radio" id="ActiviteGratuite" name="PrixTaxableActivite" checked value="0">';
	$output .= '</td>';	
	$output .= '</tr>';	
	
	//Activité gratuite ou payante? //1 = Payant  *** 0 = Gratuit
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'TypeActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>L\'activité est : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite">';
	$output .= ($current_data == 1 || $current_data == '') ? '<label for="ActivitePayante">Payante</label><input type="radio" id="ActivitePayante" name="TypeActivite" checked value="1"><label for="ActiviteGratuite">Gratuite</label><input type="radio" id="ActiviteGratuite" name="TypeActivite" value="0">' : '<label for="ActivitePayante">Payante</label><input type="radio" id="ActivitePayante" name="TypeActivite" value="1"><label for="ActiviteGratuite">Gratuite</label><input type="radio" id="ActiviteGratuite" name="TypeActivite" checked value="0">';
	$output .= '</td>';	
	$output .= '</tr>';	
	
	
	
	$output .= '</table>';

	$output .= '<div style="width: 98%;margin: 24px auto; border-bottom: 4px solid black;"></div>';	
	$output .= '<h2 class="MetaTitre" >Rabais offerts</h2>';	
	
	//Information rabais	
	$output .= '<table class="ActiviteTable" >';

	//Date Activité avec un Rabais + jQuery DatePicker
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'DateRabaisActivite', TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Prix avant date (Laisser vide si aucun rabais) : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input class="datePicker" style="width:250px;" type="text" value="'.$current_data.'" name="DateRabaisActivite" id="DateRabaisActivite" /></td>';	
	$output .= '</tr>';

	//Prix de l'activité avec un rabais de date
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'PrixRabaisDateActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Prix de l\'activité avec un rabais de date, sans signe (Ex.: 169.99) : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input style="width:250px;" type="text" value="'.$current_data.'" name="PrixRabaisDateActivite" id="PrixRabaisDateActivite" /></td>';	
	$output .= '</tr>';
	
	$output .= '<tr><td colspan="2"><div style="width: 98%;margin: 6px auto; border-bottom: 2px solid #21759B;"></div></td></tr>';

	//Prix de l'activité avec un rabais de date
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'ForfaitActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Forfait billets, inscrire nombre (Ex.: 5) : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input style="width:250px;" type="text" value="'.$current_data.'" name="ForfaitActivite" id="ForfaitActivite" /></td>';	
	$output .= '</tr>';
	
	//Prix de l'activité avec un rabais de date
	$current_data = "";
	$current_data = get_post_meta($post->ID, 'PrixRabaisForfaitActivite'.$ii, TRUE);	
	$output .= '<tr>';
	$output .= '<td class="SmallTD MetaActivite" ><span><strong>Prix total du forfait (Ex.: 899.99) : </strong></span></td>';	
	$output .= '<td class="BigTD MetaActivite"><input style="width:250px;" type="text" value="'.$current_data.'" name="PrixRabaisForfaitActivite" id="PrixRabaisForfaitActivite" /></td>';	
	$output .= '</tr>';	

	
	$output .= '</table>';	
	$output .= '<div style="width: 98%;margin: 24px auto; border-bottom: 4px solid black;"></div>';	

	
	
	
	
	
// DÉBUT NM
	$output = '';
	$output .= '<div class="ActiviteBoxWrap">';
	
	// Ajout des détails de conférence/atelier (titre seulement)
	$nouvelle_conference_def = '<tr><td class="FirstColumn">Conférence / atelier :</td><td><input class="Conferences" name="conference[{ii}]" type="text" value="{desc_conference}" maxlength="255" /></td><td><div class="media-modal-close"><span class="media-modal-icon"></span></div></td></tr>';

	$nb_conferences = intval(get_post_meta($post->ID, 'nb_conferences', true));
	$conferences_inc = intval(get_post_meta($post->ID, 'conferences_inc', true));  // Next increment
	
	if($conferences_inc == 0) { // Next increment, commence à 1.
		$conferences_inc = 1;
	}
	
	$output .= '<h2 class="MetaTitre" >Événements (activités) à conférences multiples :</h2>';	
	$output .= '<p><input id="conferences_multiples" name="conferences_multiples" type="checkbox" value="1" ' . ($nb_conferences > 0 ? 'checked="checked"' : '') . ' /><label for="conferences_multiples">Il s\'agit d\'un événement à conférences multiples avec choix de conférences</label></p>';
	$output .= '<div id="FormulaireConferences">';
	$output .= '<table class="ListeConferences">';
	$output .= '<tr><th class="FirstColumn ColoredHeader">Conférences / ateliers</th><th class="ColoredHeader">Description</th><th class="ColoredHeader Delete">Suppr.</th></tr>';

	$ii = 1;
	for($ii = 1; $ii < $conferences_inc; $ii++) {
		$desc_conference = get_post_meta($post->ID, 'conference_' . $ii, true);
		if(!empty($desc_conference)) {
			$patterns = array('/\{ii\}/', '/\{desc_conference\}/');
			$replacements = array($ii, $desc_conference);
		
			$output .= preg_replace($patterns, $replacements, $nouvelle_conference_def);
		}
	}
		
	$output .= '</table>';
	$output .= '<p><a class="AjouterConference button">Ajouter un choix de conférence / atelier</a></p>';	
	$output .= '</div>';
	
	$output .= '<h2 class="MetaTitre" >Structure de prix applicables :</h2>';	
	$output .= '<h4 class="MetaTitre" >*Instructions et notes:<br/>
							Tous les prix sont des prix par personne, même ceux des forfaits.<br/>
							Le prix le plus bas s\'applique, par personne, si deux conditions valides sont rencontrés.<br/>
							Exception: un prix de forfait est prioritaire sur un prix sans forfait.<br/>
							Veiller à combler tous les intervalles, pour ne pas qu\'une situation hors de ces conditions soit possible, sinon le coût de cette situation sera 0.<br/>
							Les nombres dans les intervalles de nombre de participants et de nombre de conférences sont inclusifs.</h4>';	
	
	
	// Structure de calcul de prix
	$output .= "\r\n";
	$output .= '<table class="ConditionsPrix" >';

	$output .= '<tr>';
	$output .= '<th colspan="2" class="ColoredHeader">Prix</th><th colspan="2" class="ColoredHeader">Dates de validité<br/><em style="font-weight:normal;">(Laisser vide pour toujours)</em></th><th></th><th colspan="2" class="ColoredHeader">Forfaits</th><th colspan="3" class="ColoredHeader  MultipleConfOpt">&Eacute;v&eacute;nements &agrave; conf&eacute;rences multiples</th><th></th>';
	$output .= '</tr><tr>';
	$output .= '<th class="FirstColumn PrixCell">Prix par personne</th><th>Taxes incluses</th><th class="DatePickerCell">Date début</th><th class="DatePickerCell">Date fin</th><th>Type d\'inscrit</th><th>Nb. participants min.</th><th>Nb. participants max.</th><th class="MultipleConfOpt">Conf. à la pièce</th><th class="MultipleConfOpt">Nb. conférences min.</th><th class="MultipleConfOpt">Nb. conférences max.</th><th>Suppr.</th>';
	$output .= '</tr>';
	
	
	
	$nouvelle_ligne_def = '<tr>	<td class="FirstColumn"><input name="prix[{ii}]" type="text" value="{prix}" maxlength="8" size="8" />$</td> 
																				<td><input name="taxes_incluses[{ii}]" type="checkbox" value="1" {taxes_incluses} /></td> 
																				<td><input name="date_debut[{ii}]" class="datePicker" type="text" value="{date_debut}" maxlength="10" size="10" /></td> 
																				<td><input name="date_fin[{ii}]" class="datePicker" type="text" value="{date_fin}" maxlength="10" size="10" /></td> 
																				<td><select name="type_inscrit[{ii}]">{type_inscrit}</select></td> 
																				<td><input name="nb_participants_min[{ii}]" type="text" value="{nb_participants_min}" maxlength="2" size="2" /></td> 
																				<td><input name="nb_participants_max[{ii}]" type="text" value="{nb_participants_max}" maxlength="2" size="2" /></td> 
																				<td class="MultipleConfOpt"><select class="ListeConferencesUniques" name="conference_unique[{ii}]">{conference_unique}</select></td> 
																				<td class="MultipleConfOpt"><input class="nb_conferences_min" name="nb_conferences_min[{ii}]" type="text" value="{nb_conferences_min}" maxlength="2" size="2" /></td> 
																				<td class="MultipleConfOpt"><input class="nb_conferences_max" name="nb_conferences_max[{ii}]" type="text" value="{nb_conferences_max}" maxlength="2" size="2" /></td> 
																				<td><div class="media-modal-close"><span class="media-modal-icon"></span></div></td>
																	 </tr>';
	
	$conditions = get_conditions($post->ID);
	
	$condition_inc = 0;
	foreach($conditions as $condition) {
		$obj_condition_prix = new conditionPrix();

		$obj_condition_prix->load_condition_prix($condition->ID);
				
		$nouvelle_ligne = $obj_condition_prix->fill_input_text_with_values($nouvelle_ligne_def);
		$nouvelle_ligne = str_replace('{ii}', $condition_inc, $nouvelle_ligne);
		$output .= $nouvelle_ligne;

		$condition_inc++;
	}
	
	$output .= '</table>';
	
	$output .= '<p><a class="AjouterLigne button">Ajouter un prix</a></p>';
// FIN NM
	
	
	//DatePicker
	$output .= '<script type="text/javascript">' . "\n" .
			'// <![CDATA[' . "\n";
		
	// Remplace les valeurs dans la nouvelle ligne par les valeurs par défaut
	$default_condition_prix = new conditionPrix();
	$nouvelle_ligne = $default_condition_prix->fill_input_text_with_values($nouvelle_ligne_def);
	$nouvelle_conference = str_replace('{desc_conference}', '', $nouvelle_conference_def);
	
	// Extrait le select box pour en faire un modèle, qui sera modifié lui aussi par ajax et qu'on recopiera
	$matches = array();
	preg_match('/<select class="ListeConferencesUniques"(.|\r|\n)*<\/select>/', $nouvelle_ligne, $matches);
	$select_box_conferences_unique = $matches[0];
	$select_box_conferences_unique = preg_replace('/name="conference_unique\[\{ii\}\]"/', 'id="modele_conference_unique"', $select_box_conferences_unique);
		
	$default_condition_values = $default_condition_prix->get_condition_prix();
	
	$output .= "var condition_inc = $condition_inc;\r\n";
	$output .= "var conference_inc = $conferences_inc;\r\n";
	$output .= 'var nouvelleLigne = "' . str_replace("\n", "\\\r\n", addslashes($nouvelle_ligne)) . '";' . "\r\n";		
	$output .= 'var nouvelleConference = "' . str_replace("\n", "\\\r\n", addslashes($nouvelle_conference)) . '";' . "\r\n";
	$output .= 'var defaut_nb_conferences_min = ' . $default_condition_values['nb_conferences_min'] . ";\r\n";
	$output .= 'var defaut_nb_conferences_max = ' . $default_condition_values['nb_conferences_max'] . ";\r\n";
	
	$output .= "\r\n\r\n";
	$output .= 'Date.format = "dd-mm-yyyy"' . ";\n";
	$output .= 'function hideShowNbConferences(selectElement) {
								if(selectElement.val() == "") {
									nb_conferences_min = selectElement.parent().parent().find("input.nb_conferences_min").show();
									nb_conferences_max = selectElement.parent().parent().find("input.nb_conferences_max").show();
									
								} else {
									nb_conferences_min = selectElement.parent().parent().find("td input.nb_conferences_min");
									nb_conferences_max = selectElement.parent().parent().find("td input.nb_conferences_max");
									
									nb_conferences_min.val(defaut_nb_conferences_min);
									nb_conferences_max.val(defaut_nb_conferences_max);
									
									nb_conferences_min.hide();
									nb_conferences_max.hide();	
								}
							}';
	$output .= 'function enableGUI() {
									jQuery(".datePicker").css("float", "left"); jQuery(".datePicker").datePicker({});
									jQuery("input.datePicker").focus(function() {
										jQuery(this).next().click();
									});
									
									jQuery("table.ConditionsPrix div.media-modal-close").click(function() {
										jQuery(this).parent().parent().remove();
									});	
									
									jQuery("select.ListeConferencesUniques").change(function() {
										hideShowNbConferences(jQuery(this));
									});
									
									jQuery("table.ListeConferences div.media-modal-close").click(function() {
										conference_id = jQuery(this).parent().parent().find("input.Conferences").attr("name");
										conference_id = conference_id.replace(/\[/g, "_").replace(/\]/g, "");
										
										jQuery("select.ListeConferencesUniques option[value=\"" + conference_id + "\"]").remove();
										jQuery("select.ListeConferencesUniques").trigger("change");
										
										jQuery(this).parent().parent().remove();
									});		
									
									if(jQuery("input#conferences_multiples").is(":checked")) {
										jQuery(".MultipleConfOpt, #FormulaireConferences").show();
									} else {
										jQuery(".MultipleConfOpt, #FormulaireConferences").hide();
										jQuery("#FormulaireConferences table td").each(function() {
											if(jQuery(this).find("input.Conferences").length) {
												conference_id = jQuery(this).find("input.Conferences").attr("name");
												conference_id = conference_id.replace(/\[/g, "_").replace(/\]/g, "");
												
												jQuery("select.ListeConferencesUniques option[value=\"" + conference_id + "\"]").remove();
												jQuery("select.ListeConferencesUniques").trigger("change");
											}
											jQuery(this).remove();
										});
										jQuery(".nb_conferences_min").val(defaut_nb_conferences_min);
										jQuery(".nb_conferences_max").val(defaut_nb_conferences_max);
										
									}

									jQuery("input.Conferences").blur(function() {
										var new_text = jQuery(this).val();
										conference_id = jQuery(this).attr("name");
										conference_id = conference_id.replace(/\[/g, "_").replace(/\]/g, "");
										
										jQuery("select.ListeConferencesUniques option[value=\"" + conference_id + "\"]").each(function() {
											jQuery(this).text(new_text);
										});
									});
							}';				
							
	$output .=	'jQuery(document).ready(function() {';
	$output .= 	 	'jQuery("a.AjouterLigne").click(function(e) {
										e.preventDefault();  // Previent de monter au haut de page avec l\'ancre...
										// Ajoute la nouvelle ligne et remplace le contenu de la select box par le contenu de la select box modele.
										modele_select_box = jQuery("div.HiddenWrap select#modele_conference_unique").html();
										jQuery("table.ConditionsPrix").append(nouvelleLigne.replace(/\{ii\}/g, condition_inc)).find("tr:last-child td select.ListeConferencesUniques").html(modele_select_box);
										
										condition_inc++;
										enableGUI();
									});';
	$output .= 	 	'jQuery("a.AjouterConference").click(function(e) {
										e.preventDefault();  // Previent de monter au haut de page avec l\'ancre...
										jQuery("table.ListeConferences").append(nouvelleConference.replace(/\{ii\}/g, conference_inc));
										jQuery("select.ListeConferencesUniques").append("<option value=\"conference_" + conference_inc + "\"></option>");
										conference_inc++;
										enableGUI();
									});';								
	$output .= 		'jQuery("input#conferences_multiples").click(function(){
									enableGUI();
								});';
	$output .=  	'enableGUI();';
	$output .= 		'jQuery("select.ListeConferencesUniques").each(function() {
									hideShowNbConferences(jQuery(this));
								});';
	$output .= 	'});' . "\n" .
			'// ]]>' . "\n" .
			'</script>';	

	// Modèle select box conférence unique (wrapper caché)
	$output .= '<div class="HiddenWrap"><div>';  // 2 div de profond pour ne pas interferer avec autres fonctions "select.ListeConferencesUniques" .parent().parent() ...
	$output .= $select_box_conferences_unique;
	$output .= '</div></div>';
			
	$output .= '</div>'; // Fin ActiviteBoxWrap		
			
	echo $output;
}

/* Sauvegarde des données de la Metabox */
function team_activitesbox_save( $post_id ) {

	global $post;

	//Pas de autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return;

	//Vérification
	if (isset($_POST['myplugin_noncename'])) {
		if ( !wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
			return;
		}

	//Vérification des permissions de l'utilisateur
	if ( 'page' == $_POST['post_type'] ) 
	{
	if ( !current_user_can( 'edit_page', $post_id ) )
		return;
	}
	else
	{
	if ( !current_user_can( 'edit_post', $post_id ) )
		return;
	}
	
	if($post->post_type != 'les-activites') {
		return;
	}

	if(wp_is_post_revision($post_id) || wp_is_post_autosave($post_id)) {
		return;
	}
	
	
	/*********************************/
	/* ***Sauvegarde des données *** */
	/*********************************/
	
	// Supprime les conférences précédentes
	$conferences_inc = get_post_meta($post_id, 'conferences_inc', true);
	for($ii = 1; $ii < $conferences_inc; $ii++) {
		delete_post_meta($post_id, 'conference_' . $ii);
	}
	
	// Sauvegarde les conférences / ateliers
	$nb_conferences = 0;
	$max_conference_id = 0;
	foreach($_POST['conference'] as $key => $value) {
		$key = intval($key); // Casting et protection, au cas
		if(!empty($key) && !empty($value)) {
			update_post_meta($post_id, 'conference_' . $key, $value);
			
			// Mémorise le plus grand id trouvé.
			if($key > $max_conference_id) {
				$max_conference_id = $key;
			}
			
			$nb_conferences++;
		}
	}
	
	// Next increment: plus grand id trouvé + 1
	$conferences_inc = $max_conference_id + 1;
	
	update_post_meta($post_id, 'conferences_inc', $conferences_inc);
	update_post_meta($post_id, 'nb_conferences', $nb_conferences);
	
	
	// Sauvegarde les conditions de prix. Efface tout et re-sauvegarde.
	clear_conditions_prix($post_id);
	
	foreach($_POST['prix'] as $key => $value) {
		$prix = $_POST['prix'][$key];
		$taxes_incluses = (bool)(!empty($_POST['taxes_incluses'][$key]));
		$date_debut = $_POST['date_debut'][$key];
		$date_fin = $_POST['date_fin'][$key];
		$type_inscrit = $_POST['type_inscrit'][$key];
		$nb_participants_min = $_POST['nb_participants_min'][$key];
		$nb_participants_max = $_POST['nb_participants_max'][$key];
		$conference_unique = $_POST['conference_unique'][$key];
		$nb_conferences_min = $_POST['nb_conferences_min'][$key];
		$nb_conferences_max = $_POST['nb_conferences_max'][$key];
		
		$obj_condition_prix = new conditionPrix();
		
		$obj_condition_prix->set_condition_prix(0, $post_id, $prix, $taxes_incluses, $date_debut, $date_fin, $type_inscrit, $nb_participants_min, $nb_participants_max, $conference_unique, $nb_conferences_min, $nb_conferences_max);
		$obj_condition_prix->save_condition_prix();
	}
	
} 

	//Icone de custom post type
	function metabox_css() {
	   ?><link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/admin.css" />
	<?php }
	add_action( 'admin_head', 'metabox_css' );

?>