<?php
//Pour lier les post type aux pages
/* Define the custom box */
if (current_user_can('administrator')) {
	add_action( 'add_meta_boxes', 'team_metabox' );
	add_action( 'save_post', 'team_metabox_save' );
}

/* Adds a box to the main column on the Post and Page edit screens */
function team_metabox() {	
    add_meta_box(
        'myplugin_sectionid',
        __( 'Param&egrave;tre de la page', 'team_parametre_page' ), 
        'team_parametre_page_box',
        'page','side'
    );
}

/* Prints the box content */
function team_parametre_page_box( $post ) {

	  // Use nonce for verification
	  wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );

	  // The actual fields for data entry
	  echo '<p><strong>';
		   _e("Lier une page &agrave; un post type", 'team_parametre_page' );
	  echo '</strong></p> ';
	  
	  $args=array(
		  'public'   => true,
		  '_builtin' => false
		); 

	  $all_post_types = get_post_types($args,'names');
	  
	  $ii = 0;
	  $output .= '';	  
	  $current_data = get_post_meta($post->ID, 'select_link_post_type', TRUE);
	 
	 
	  
	  $output .= '<select id="select_link_post_type" name="select_link_post_type" >';
	  $output .= '<option value="aucun">Aucun</option>';
	  foreach ($all_post_types as $key => $un_post_type) {
			if ($current_data == $un_post_type) { $selected='selected="selected"'; } else { $selected= ''; }
			$output .= '<option '.$selected.' value="'. $un_post_type .'">'. $un_post_type .'</option>';
			$ii++;
	  }
	  $output .= '</select>';
	  
	  echo $output;
}

/* When the post is saved, saves our custom data */
function team_metabox_save( $post_id ) {

	  // verify if this is an auto save routine. 
	  // If it is our form has not been submitted, so we dont want to do anything
	  /*if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		  return;*/

	  // verify this came from the our screen and with proper authorization,
	  // because save_post can be triggered at other times

	  /*if ( !wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
		  return;*/

	  // Check permissions
	  if ( 'page' == $_POST['post_type'] ) 
	  {
		if ( !current_user_can( 'edit_page', $post_id ) )
			return;
	  }
	  else
	  {
		if ( !current_user_can( 'edit_post', $post_id ) )
			return;
	  }

	  // OK, we're authenticated: we need to find and save the data

	  $mydata = $_POST['select_link_post_type'];

	  // Do something with $mydata 
	  // probably using add_post_meta(), update_post_meta(), or 
	  // a custom table (see Further Reading section below)
	  
	  update_post_meta($post_id,'select_link_post_type',$mydata);
  
} 
?>