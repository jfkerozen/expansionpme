<?php

	// get_post_by_slug : ca dit tout ! Jimmy L.
	if(!function_exists('get_post_by_slug')) {
		function get_post_by_slug($page_title, $output = OBJECT) {
			global $wpdb;
			$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type='categories'", $page_title ));
			if ( $post )
				return get_post($post, $output);

			//return null;
			return "ERROR:get_post_by_slug('$page_title');";
		}
	}

?>