<?php

	// get_post_by_title : ca dit tout ! Jimmy L. Trouv� via : http://stackoverflow.com/questions/1536682/get-wordpress-post-id-from-post-title
	if(!function_exists('get_post_by_title')) {
		function get_post_by_title($page_title, $output = OBJECT) {
			global $wpdb;
			$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type='categories'", $page_title ));
			if ( $post )
				return get_post($post, $output);

			//return null;
			return "ERROR:get_post_by_title('$page_title');";
		}
	}

?>