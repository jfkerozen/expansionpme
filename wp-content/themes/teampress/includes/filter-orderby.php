<?php
	
	// Patch pour plugin re-order : trie par date, si menu_order pas trouv� ou �gal.
	// Les nouveaux post s'affichent maintenant dans l'ordre indiqu� dans reorder, nouveaux posts aussi.
	if(!function_exists('filter_orderby')) {	
		function filter_orderby($orderby = '') {
		
			global $wpdb;
		
			if(strpos($orderby, "{$wpdb->posts}.post_date") === false) {
				$orderby .= (($orderby == '') ? '' : ', ') . "{$wpdb->posts}.post_date DESC";
			}
			return $orderby;
		}
		add_filter('posts_orderby', 'filter_orderby');
	}

?>