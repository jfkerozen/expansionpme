<?php
/***
	Define conditionPrix() class
***/

class conditionPrix {

	private $id; // ID de condition dans database

	private $post_id; // Lien avec l'activité
	
	// Prix
	private $prix;
	private $taxes_incluses; // true ou false

	// Conditions
	private $date_debut; 	// Timestamp  ou empty
	private $date_fin; 		// Timestamp  ou empty
	
	// Prix selon type d'inscrit: ex.: participant PME, intervenant, etc.
	private $type_inscrit; // Propriété propre à l'entreprise, un seul statut par groupe d'inscrit.
	
	// Forfaits
	private $nb_participants_min;
	private $nb_participants_max;
	
	// Conférences/ateliers multiples
	private $conference_unique;
	private $nb_conferences_min;
	private $nb_conferences_max;
	
	public function __construct() {
		$this->id = 0;
		$this->post_id = 0;
		
		$this->prix = 0;
		$this->taxes_incluses = false;
		
		$this->date_debut = '';
		$this->date_fin = '';
		
		$this->type_inscrit = '';
		
		$this->nb_participants_min = 1;
		$this->nb_participants_max = 99;
		
		$this->conference_unique = '';
		$this->nb_conferences_min = 1;
		$this->nb_conferences_max = 99;
	}
	
	public function __destruct() {
	
	}
		
	// Fonction validant si les conditions sont remplies pour appliquer ce prix.
	// Retourne le prix (avec/sans taxes) si oui, sinon retourne false.
	public function valider_condition_prix($date_courante = 0, $type_inscrit_courant = '', &$participants = array()) {
				
		if($date_courante == 0) {
			$date_courante = time();
		}
		// De minuit à la date du début à 23:59:59 à la date de la fin (minuit + 86399 secondes)
		// Vérifier si tient compte du fuseau horaire EST. /////***********IMPORTANT
		if(($date_courante >= $this->date_debut || $this->date_debut == '') && ($date_courante <= $this->date_fin + 86399 || $this->date_fin == '') && ($type_inscrit_courant == $this->type_inscrit || $this->type_inscrit == '')) {
			
			// La date et le type d'inscrit est valide pour cette condition.
			// Recherche si les conditions de nombre de conférence par personne est valide, et pour combien de personnes.
			// Seulement si cette condition n'est pas un prix de conférence à la pièce
			if(empty($this->conference_unique)) {
				$nb_participants_valides = 0;
				foreach($participants as $participant) {
					$nb_conferences = count($participant['conferences']);
					if($nb_conferences >= $this->nb_conferences_min && $nb_conferences <= $this->nb_conferences_max) {
						// Le nombre de conférences choisies est valide pour cette condition. Ajoute un participant de plus afin de valider le nombre de participants.
						$nb_participants_valides++;
					}
				}				
				// Vérifie si le nombre de participants est valide.
				if($nb_participants_valides >= $this->nb_participants_min && $nb_participants_valides <= $this->nb_participants_max) {
					// Le nombre de conférences et de participants est valide. Affecte ce prix à chacun si plus bas que celui mémorisé.
					$prix_total = calculer_taxes($this->prix, $this->taxes_incluses);
					foreach($participants as &$participant) {
						$nb_conferences = count($participant['conferences']);
						// Seulement si le participant faisait partie du lot de participants de forfait.
						if($nb_conferences >= $this->nb_conferences_min && $nb_conferences <= $this->nb_conferences_max) {					
							if(!empty($participant['prix_min'])) {
								if($prix_total['total'] < $participant['prix_min']['total']) {
									$participant['prix_min'] = $prix_total;
								}
							// prix_min non défini, donc nouveau nécessairement plus bas.
							} else {
								$participant['prix_min'] = $prix_total;
							}
						}
					}
				}
			} else {
				// Il s'agit d'un prix de conférence à la pièce (prix pour conférence unique).
				// Valide, pour chaque personne, si celle-ci participe à cette conférence.
				// Lui affecte ce prix, si le nombre de participant est valide.
				$nb_participants_valides = 0;
				foreach($participants as $participant) {
					foreach($participant['conferences'] as $conference) {
						if($conference['ID'] == $this->conference_unique) {
							// La confénce choisi a un prix à la pièce. Ajoute un participant de plus afin de valider le nombre de participants.
							$nb_participants_valides++;
						}
					}
				}
				// Vérifie si le nombre de participants est valide.
				if($nb_participants_valides >= $this->nb_participants_min && $nb_participants_valides <= $this->nb_participants_max) {
					// Le nombre de conférences et de participants est valide. Affecte ce prix à chacun si plus bas que celui mémorisé.			
					$prix_total = calculer_taxes($this->prix, $this->taxes_incluses);
					foreach($participants as &$participant) {
						foreach($participant['conferences'] as &$conference) {
							if($conference['ID'] == $this->conference_unique) {
								if(!empty($conference['prix_min'])) {
									if($prix_total['total'] < $conference['prix_min']['total']) {
										$conference['prix_min'] = $prix_total;
									}
								// prix_min non défini, donc nouveau nécessairement plus bas.
								} else {
									$conference['prix_min'] = $prix_total;
								}
								break; // Sort de la boucle de conférences pour ce participant. La bonne conférence a été trouvée.
							}
						}
					}
				}
			}
			
		}
		return;
	}
	
	// Écriture dans la database (insert et update)
	public function save_condition_prix() {

		global $wpdb;
		
		$values = array(
										'post_id' => $this->post_id,
										'prix' => $this->prix,
										'taxes_incluses' => $this->taxes_incluses,
										'date_debut' => $this->date_debut,
										'date_fin' => $this->date_fin,
										'type_inscrit' => $this->type_inscrit,
										'nb_participants_min' => $this->nb_participants_min,
										'nb_participants_max' => $this->nb_participants_max,
										'conference_unique' => $this->conference_unique,
										'nb_conferences_min' => $this->nb_conferences_min,
										'nb_conferences_max' => $this->nb_conferences_max,
										);
		
		// Insert
		if(empty($this->id)) {
			$wpdb->insert($wpdb->prefix . 'act_conditions_prix', $values);
			$id = $wpdb->insert_id;
		
		// Update
		} else {
			$wpdb->update($wpdb->prefix . 'act_conditions_prix', $values, array('ID' => $this->id));
		
		}
	}
	
	// Chargement à partir de la database
	public function load_condition_prix($id_condition) {

		global $wpdb;
	
		$sql_load = "SELECT * FROM {$wpdb->prefix}act_conditions_prix WHERE ID = $id_condition";
	
		// Load
		$row = $wpdb->get_row($sql_load, OBJECT);
	
		$this->id = $row->ID;
		$this->post_id = $row->post_id;
		$this->prix = $row->prix;
		$this->taxes_incluses = (bool)$row->taxes_incluses;
		$this->date_debut = $row->date_debut;
		$this->date_fin = $row->date_fin;
		$this->type_inscrit = $row->type_inscrit;
		$this->nb_participants_min = $row->nb_participants_min;
		$this->nb_participants_max = $row->nb_participants_max;
		$this->conference_unique = $row->conference_unique;
		$this->nb_conferences_min = $row->nb_conferences_min;
		$this->nb_conferences_max = $row->nb_conferences_max;
		
	}
	
	// Pour lecture externe
	public function get_condition_prix() {
		
		$temp_date_debut = $this->date_debut;
		$temp_date_fin = $this->date_fin;
		
		if(!empty($temp_date_debut)) {
			$temp_date_debut = date('d-m-Y', $temp_date_debut);
		}
		
		if(!empty($temp_date_fin)) {
			$temp_date_fin = date('d-m-Y', $temp_date_fin);
		}
	
		$values = array(
										'ID' => $this->id,
										'post_id' => $this->post_id,
										'prix' => number_format($this->prix, 2, '.', ' '),
										'taxes_incluses' => ($this->taxes_incluses ? 'checked="checked"' :''),
										'date_debut' => $temp_date_debut,
										'date_fin' => $temp_date_fin,
										'type_inscrit' => $this->type_inscrit,
										'nb_participants_min' => $this->nb_participants_min,
										'nb_participants_max' => $this->nb_participants_max,
										'conference_unique' => $this->conference_unique,
										'nb_conferences_min' => $this->nb_conferences_min,
										'nb_conferences_max' => $this->nb_conferences_max,
										);
		
		return $values;
	}
	
	// Pour écriture externe
	public function set_condition_prix($val_id = 0, $val_post_id = 0, $val_prix = 0, $val_taxes_incluses = false, $val_date_debut = '', $val_date_fin = '', $val_type_inscrit = '', $val_nb_participants_min = 1, $val_nb_participants_max = 1, $val_conference_unique = '', $val_nb_conferences_min = 1, $val_nb_conferences_max = 99) {
	
		if(!empty($val_id)) {
			$this->id = $val_id;
		}
		
		if(!empty($val_post_id)) {
			$this->post_id = $val_post_id;
		}
		
		$price_patterns = array('/\$/', '/\s/', '/,/');
		$price_replacements = array('', '', '.');
		$this->prix = (double)preg_replace($price_patterns , $price_replacements, $val_prix);
		
		$this->taxes_incluses = (bool)$val_taxes_incluses;
		$this->date_debut = strtotime($val_date_debut);
		if($this->date_debut === false) {
			$this->date_debut = '';
		}
		$this->date_fin = strtotime( $val_date_fin);
		if($this->date_fin === false) {
			$this->date_fin = '';
		}
		
		// Validation ordre de grandeur logique de date
		if(!empty($this->date_debut) && !empty($this->date_fin)) {
			if($this->date_fin < $this->date_debut) {
				$this->date_fin = $this->date_debut;
			}
		}
		
		$this->type_inscrit = $val_type_inscrit;
		$this->nb_participants_min = $this->cast_nb($val_nb_participants_min);
		$val_nb_participants_max = $this->cast_nb($val_nb_participants_max);
		$this->nb_participants_max = $this->cast_nb($val_nb_participants_max);
		
		// Validation ordre de grandeur logique de nombre de participants
		if($this->nb_participants_max < $this->nb_participants_min) {
			$this->nb_participants_max = $this->nb_participants_min;
		}
		
		$this->conference_unique = $val_conference_unique;
		$this->nb_conferences_min = $this->cast_nb($val_nb_conferences_min);
		$this->nb_conferences_max = $this->cast_nb($val_nb_conferences_max);
		
		// Validation ordre de grandeur logique de nombre de conférences
		if($this->val_nb_conferences_max < $this->nb_conferences_min) {
			$this->val_nb_conferences_max = $this->nb_conferences_min;
		}
	}

	// Fonction utilitaire de cast de nombre
	private function cast_nb($value) {
		
		$value = (int)$value;
		
		if($value < 1) {
			$value = 1;
		}
		
		return $value;
	}
	
	public function fill_input_text_with_values($input_text) {
		
		static $types_inscrits = array();
		if(empty($types_inscrits)) {
			$types_inscrits =  get_posts(array('showposts' => -1, 'post_type' => 'types-inscrits'));
		}
				
		$type_inscrit_options = '';
		$type_inscrit_options .= "<option value=\"\" " . (empty($this->type_inscrit) ? 'selected="selected"' : '') . ">Tous</option>\n";
		foreach($types_inscrits as $type_inscrit) {
			$selected = '';
			if($type_inscrit->post_name == $this->type_inscrit) {
				$selected = 'selected="selected"';
			}
			$type_inscrit_options .= "<option $selected value=\"{$type_inscrit->post_name}\">{$type_inscrit->post_title}</option>\n";
		}		
		
		
		static $desc_conferences = array();
		if(empty($desc_conferences)) {
			$nb_conferences = intval(get_post_meta($this->post_id, 'nb_conferences', true));
			$conferences_inc = intval(get_post_meta($this->post_id, 'conferences_inc', true));  // Next increment
		
			if($conferences_inc == 0) { // Next increment, commence à 1.
				$conferences_inc = 1;
			}
		
			$ii = 1;
			for($ii = 1; $ii < $conferences_inc; $ii++) {
				$desc_conference = get_post_meta($this->post_id, 'conference_' . $ii, true);
				if(!empty($desc_conference)) {
					$desc_conferences['conference_' . $ii] = $desc_conference;
				}
			}
		}
		
		$conference_unique_options = '';
		$conference_unique_options .= "<option value=\"\" " . (empty($this->conference_unique) ? 'selected="selected"' : '') . ">Non</option>\n";
		foreach($desc_conferences as $key => $value) {
			$selected = '';
			if($key == $this->conference_unique) {
				$selected = 'selected="selected"';
			}
			$conference_unique_options .= "<option $selected value=\"{$key}\">{$value}</option>\n";
		}
		
		
		$condition_prix_fields = $this->get_condition_prix();
		$condition_prix_fields['type_inscrit'] = $type_inscrit_options;
		$condition_prix_fields['conference_unique'] = $conference_unique_options;
		$keys = array_keys($condition_prix_fields);
		$values = array_values($condition_prix_fields);
		
		foreach($keys as &$key) {
			$key = '/\{' . $key . '\}/';
		}
		
		$input_text = preg_replace($keys, $values, $input_text);
				
		return $input_text;
	}
}


/*** Fonctions utilitaires hors-classe ***/

// Fonction effaçant toutes les conditions reliées à un post_id (activité) donné.
function clear_conditions_prix($post_id) {
	
	global $wpdb;
	
	$wpdb->delete($wpdb->prefix . 'act_conditions_prix', array('post_id' => $post_id));
}

// Fonction retrouvant tous les id des conditions s'appliquant à un post_id (activité) donné.
function get_conditions($post_id) {

	global $wpdb;
	
	$sql_conditions = "SELECT ID FROM {$wpdb->prefix}act_conditions_prix WHERE post_id = $post_id";
	$conditions = $wpdb->get_results($sql_conditions, OBJECT);
	
	return $conditions;
}
	
//Fonction qui calcul les taxes
function calculer_taxes($prix, $taxes_incluses = false) {

	$arr_prix = array();
	$obj_tps = new stdClass;	
	$obj_tvq = new stdClass;	
	$obj_tvh = new stdClass;	

	$taxe_tps = get_option( "taxe_tps" , "" );
	$taxe_tvq = get_option( "taxe_tvq" , "" );
	$taxe_tvh = get_option( "taxe_tvh" , "" );

	$sous_total = $prix;
	
	//Calcul de la taxe et du total
	if ($taxes_incluses == false) {
		if ($tvh != "") {
			$total_tvh = round($prix * $taxe_tvh / 100,2);
			$total = $prix + $total_tvh;
			$obj_tvh->nom = "TVH";
			$obj_tvh->pourcentage = $taxe_tvh;
			$obj_tvh->valeur = $total_tvh;
			$arr_prix['taxes'][] = $obj_tvh;
		}
		else {
			$total_tps = round($prix * $taxe_tps / 100,2);	
			$total_tvq = round($prix * $taxe_tvq / 100,2);
			$total = $prix + $total_tvq + $total_tps;
			$obj_tps->nom = "TPS";
			$obj_tps->pourcentage = $taxe_tps;
			$obj_tps->valeur = $total_tps;
			$obj_tvq->nom = "TVQ";
			$obj_tvq->pourcentage = $taxe_tvq;
			$obj_tvq->valeur = $total_tvq;	
			$arr_prix['taxes'][] = $obj_tps;
			$arr_prix['taxes'][] = $obj_tvq;	
		}
	}
	else {
		$total = $prix;
		$obj_tps->nom = "TPS";
		$obj_tps->pourcentage = $taxe_tps;
		$obj_tps->valeur = 0;
		$obj_tvq->nom = "TVQ";
		$obj_tvq->pourcentage = $taxe_tvq;
		$obj_tvq->valeur = 0;	
		$arr_prix['taxes'][] = $obj_tps;
		$arr_prix['taxes'][] = $obj_tvq;			
	}
	
	$arr_prix['sous_total'] = $sous_total;
	$arr_prix['total'] = $total;
	
	return $arr_prix;
}

//Additionne toutes les valeurs entre elle
function ajouter_prix_taxes($prix_taxes1, $prix_taxes2) {
	
	$prix_taxes_add = array();
	$obj_tps = new stdClass;	
	$obj_tvq = new stdClass;	
	$obj_tvh = new stdClass;	

	if (empty($prix_taxes1)) {
		return $prix_taxes2;
	}
	else if (empty($prix_taxes2)) {
		return $prix_taxes1;
	}
	else {
		$prix_taxes_add['sous_total'] = $prix_taxes1['sous_total'] + $prix_taxes2['sous_total'];
		$prix_taxes_add['total'] = $prix_taxes1['total'] + $prix_taxes2['total'];

		if ($prix_taxes1['taxes'][0]->nom == 'TVH') { // TVH, prioritaire
			
			$obj_tvh->nom = $prix_taxes1['taxes'][0]->nom;
			$obj_tvh->pourcentage = $prix_taxes1['taxes'][0]->pourcentage;
			$obj_tvh->valeur = $prix_taxes1['taxes'][0]->valeur + $prix_taxes2['taxes'][0]->valeur;
			
			$prix_taxes_add['taxes'][0] = $obj_tvh;
			
		}
		elseif($prix_taxes1['taxes'][0]->nom == 'TPS') { // TPS, TVQ
			
			$obj_tps->nom = $prix_taxes1['taxes'][0]->nom;	
			$obj_tvq->nom = $prix_taxes1['taxes'][1]->nom;	
			$obj_tps->pourcentage = $prix_taxes1['taxes'][0]->pourcentage;	
			$obj_tvq->pourcentage = $prix_taxes1['taxes'][1]->pourcentage;					
			$obj_tps->valeur = $prix_taxes1['taxes'][0]->valeur + $prix_taxes2['taxes'][0]->valeur;	
			$obj_tvq->valeur = $prix_taxes1['taxes'][1]->valeur + $prix_taxes2['taxes'][1]->valeur;	
			
			$prix_taxes_add['taxes'][0] = $obj_tps;
			$prix_taxes_add['taxes'][1] = $obj_tvq;
			
		}
		return $prix_taxes_add;
	}
	

}


?>