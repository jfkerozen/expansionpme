<?php
/***
	Define condition_prix() class
***/

class condition_prix {

	private $id; // ID de condition dans database

	// Prix
	private $prix;
	private $taxes_incluses; // true ou false

	// Conditions
	private $date_debut; 	// Timestamp à 00:00 00s le jour du début
	private $date_fin; 		// Timestamp à 23:59 59s le jour de la fin
	
	// Prix selon type d'inscrit: ex.: participant PME, intervenant, etc.
	private $type_inscrit;
	
	// Forfaits
	private $nb_participants_min;
	private $nb_participants_max;
	
	// Conférences/ateliers multiples
	private $nb_conferences_min;
	private $nb_conferences_max;
	
	private function __construct() {
		$id = 0;
		
		$prix = 0;
		$taxes_incluses = false;
		
		$date_debut = 0;
		$date_fin = 0;
		
		$type_inscrit = '';
		
		$nb_participants_min = 1;
		$nb_participants_max = 1;
		
		$nb_conferences_min = 1;
		$nb_conferences_max = 1;
	}
	
	private function __destruct() {
	
	}
	
	// Fonction validant si les conditions sont remplies pour appliquer ce prix.
	// Retourne le prix (avec/sans taxes) si oui, sinon retourne false.
	public function valider_condition_prix($date_courante = 0, $type_inscrit_courant = '', $nb_participants = 1, $nb_conferences = 1) {
		
		if($date_courante == 0) {
			$date_courante = time();
		}
		
		if($date_courante >= $date_debut && $date_courante <= $date_fin &&
			$type_inscrit_courant == $type_inscrit &&
			$nb_participants >= $nb_participants_min && $nb_participants <= $nb_participants_max &&
			$nb_conferences >= $nb_conferences_min && $nb_conferences <= $nb_conferences_max) {
			
			return array('prix' => $prix, 'taxes_incluses' => $taxes_incluses);
			
		} else {
			return false;
			
		}
	
	}
	
	// Écriture dans la database (insert et update)
	public function save_condition_prix() {

		global $wpdb;
		
		$values = array(
										'prix' => $prix,
										'taxes_incluses' => $taxes_incluses,
										'date_debut' => $date_debut,
										'date_fin' => $date_fin
										'type_inscrit' => $type_inscrit,
										'nb_participants_min' => $nb_participants_min,
										'nb_participants_max' => $nb_participants_max,
										'nb_conferences_min' => $nb_conferences_min,
										'nb_conferences_max' => $nb_conferences_max,
										);
		
		// Insert
		if(empty($id)) {
			$wpdb->insert($wpdb->prefix . 'act_conditions_prix', $values);
			$id = $wpdb->insert_id;
		
		// Update
		} else {
			$wpdb->update($wpdb->prefix . 'act_conditions_prix', $values, array('ID' => $id));
		
		}
	}
	
	// Chargement à partir de la database
	public function load_condition_prix($id_condition) {

		global $wpdb;
	
		$sql_load = "SELECT * FROM {$wpdb->prefix}act_conditions_prix WHERE ID = $id_condition";
	
		// Load
		$row = $wpdb->get_row($sql_load, OBJECT);
	
		$id = $row->ID;
		$prix = $row->prix;
		$taxes_incluses = (bool)$row->taxes_incluses;
		$date_debut = $row->date_debut;
		$date_fin = $row->date_fin;
		$type_inscrit = $row->type_inscrit;
		$nb_participants_min = $row->nb_participants_min;
		$nb_participants_max = $row->nb_participants_max;
		$nb_conferences_min = $row->nb_conferences_min;
		$nb_conferences_max = $row->nb_conferences_max;
		
	}
	
	// Pour lecture externe
	public function get_condition_prix() {
		$values = array(
										'ID' => $id,
										'prix' => $prix,
										'taxes_incluses' => $taxes_incluses,
										'date_debut' => $date_debut,
										'date_fin' => $date_fin
										'type_inscrit' => $type_inscrit,
										'nb_participants_min' => $nb_participants_min,
										'nb_participants_max' => $nb_participants_max,
										'nb_conferences_min' => $nb_conferences_min,
										'nb_conferences_max' => $nb_conferences_max,
										);
		
		return $values;
	}
	
	// Pour écriture externe
	public function set_condition_prix($val_id = 0, $val_prix = 0; $val_taxes_incluses = false, $val_date_debut = 0, $val_date_fin = 0, $val_type_inscrit = '', $val_nb_participants_min = 1, $val_nb_participants_max = 1, $val_nb_conferences_min = 1, $val_nb_conferences_max = 1) {
	
		if(!empty($val_id)) {
			$id = $val_id;
		}
	
		$prix = $val_prix;
		$taxes_incluses = $val_taxes_incluses;
		$date_debut = $val_date_debut;
		$date_fin = $val_date_fin;
		$type_inscrit = $val_type_inscrit;
		$nb_participants_min = cast_nb($val_nb_participants_min);
		$nb_participants_max = cast_nb($val_nb_participants_max);
		$nb_conferences_min = cast_nb($val_nb_conferences_min);
		$nb_conferences_max = cast_nb($val_nb_conferences_max);
	}
	
	// Fonction utilitaire de cast de nombre
	private function cast_nb($value) {
		
		$value = (int)$value;
		
		if($value < 1) {
			$value = 1;
		}
		
		return $value;
	}
}










?>