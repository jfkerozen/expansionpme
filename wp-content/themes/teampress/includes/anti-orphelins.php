<?php

	if(!function_exists('anti_orphelins')) {
		function anti_orphelins($TheParagraph) {
			if (substr_count($TheParagraph," ") > 1) {
			$lastspace = strrpos($TheParagraph," ");
			$TheParagraph = substr_replace($TheParagraph,"&nbsp;",$lastspace,1);
			}
			return $TheParagraph;
		}
	}
	
	// Anti-orphelins dans les titres
	add_filter('the_title','anti_orphelins');
	//add_filter('the_content', 'anti_orphelins');

?>