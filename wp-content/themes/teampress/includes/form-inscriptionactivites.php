<?php
/* ///////////////////////////////////////// */
/*	FORMULAIRE D'INSCRIPTION À UNE ACTIVITÉ  */
/* ///////////////////////////////////////// */

	$output = '';
	
	$output .= '<div id="FormulaireActivite">';
	$output .= '<form id="FormActivite" method="post" action="#" >';

	//Champs d'entreprise	
	$output .= 'ENTREPRISE';
	
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="entreprise" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="adresse" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="ville" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="code_postal" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="e_telephone" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" value="" name="e_telecopieur" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="secteur_activite" value=""></span></span></p>';
	
	//Champs de la personne responsable
	$output .= 'RESPONSABLE';
	
	//$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="r_salutation" value=""></span></span></p>';
	$output .= '<select class="Obligatoire" name="r_salutation"><option selected value="M.">M.</option><option value="Mme.">Mme.</option></select>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="r_nom" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="r_telephone" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="r_courriel" value=""></span></span></p>';
	$output .= '<p><span class="LeftRoundedBorder"><span class="RightRoundedBorder"><input type="text" class="Obligatoire" value="" name="r_fonction" value=""></span></span></p>';
	
	$output .= 'AUTRES PARTICIPANTS';
	$output .= '<div id="ConteneurAjax"></div>';
	
	$output .= '<p class="Bouton BoutonAjout"><a class="SubmitAjoutParticipant"><span class="BoutonGauche"><span class="BoutonDroite"><span class="BoutonMilieu">Ajouter un participant</span></span></span></a></p><div class="clear"></div>';	
	$output .= '<div class="ChargementAjax"></div>';
	
	//Champs des autres participants
	
	//Faux bouton paiemnt temporaire
	$output .= '<p class="Bouton"><a class="SubmitSauvegarde"><span class="BoutonGauche"><span class="BoutonDroite"><span class="BoutonMilieu">Enregistrement BD!</span></span></span></a></p><div class="clear"></div>';
	$output .= '<input type="Hidden" value="sauvegarde" name="action" />';
	
	$output .= '</form>';
	
	//Bouton Expression Checkout Paypal
	//Bouton Achat Sandbox
	
	//Desactive tempo
		$output .= '<form class="FormPaypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="programmeurs_buisness@equipeteam.com">
		<input type="hidden" name="lc" value="CA">
		<input type="hidden" name="item_name" value="'.$post->post_title.'">
		<input type="hidden" name="amount" value="12.00">
		<input type="hidden" name="currency_code" value="CAD">
		<input type="hidden" name="button_subtype" value="services">
		<input type="hidden" name="no_note" value="0">
		<input type="hidden" name="cn" value="Add special instructions to the seller:">
		<input type="hidden" name="no_shipping" value="2">
		<input type="hidden" class="HiddenQty" name="quantity" value="1">
		<input type="hidden" name="tax_rate" value="0.000">
		<input type="hidden" name="shipping" value="0.00">
		<input type="hidden" name="custom" value="'.$_SESSION['TransactionID'].'" />
		<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHosted">
		<input type="image" src="https://www.paypalobjects.com/fr_CA/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
		<img alt="" border="0" src="https://www.paypalobjects.com/fr_XC/i/scr/pixel.gif" width="1" height="1">
		</form>';
	
	//<input type="hidden" name="tax_rate" value="14.975">
	
	
	//Bouton Achat								
	/*if (get_post_meta($_POST['IDActivite'],'PaypalActive',true) == 'yes') {
		$output .= '<form class="FormPaypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">';
		$output .= '<input type="hidden" name="cmd" value="_xclick">';
		$output .= '<input type="hidden" name="business" value="guylaine.lagace@dpme.ca">';
		$output .= '<input type="hidden" name="lc" value="CA">';
		$output .= '<input type="hidden" name="item_name" value="'.$_POST['TitreActivite'].'">';
		$output .= '<input type="hidden" name="amount" value="'.$prix_activite.'">';
		$output .= '<input type="hidden" name="currency_code" value="CAD">';
		$output .= '<input type="hidden" name="button_subtype" value="services">';
		$output .= '<input type="hidden" name="no_note" value="0">';
		$output .= '<input type="hidden" name="tax_rate" value="14.975">';
		$output .= '<input type="hidden" name="no_shipping" value="1" />';
		$output .= '<input type="hidden" name="shipping" value="0.00">';
		$output .= '<input type="hidden" class="HiddenQty" name="quantity" value="1">';
		$output .= '<input type="hidden" name="charset" value="utf-8" />';		
		$output .= '<input type="hidden" name="custom" value="'.$_SESSION['TransactionID'].'" />';
		$output .= '<input type="hidden" name="bn" value="PP-BuyNowBF:payez_paypal_fr.gif:NonHostedGuest">';
		$output .= '<input class="BtnPaypal" type="image" src="http://www.dpme.ca/wp-content/themes/teampress/images/payez_paypal_fr.png" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">';
		$output .= '<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">';
		$output .= '</form>';
	
	}*/

	
	$output .= '</div>';
	
	echo $output;
	
	//Aller chercher le post de l'ajax
	$url_ajax = team_custom_post_the_permalink('inscription-activite','ajax');
?>
<script type="text/javascript">
jQuery(document).ready(function() {

		var url_ajax = '<?php echo $url_ajax; ?>';
		var nb_participant = 0; //Contient le nombre de participant actuel
		var no_participant = 0; //Contient le numéro du dernier participant créé

		//Ajouter un participant
		jQuery('a.SubmitAjoutParticipant').click(function(e) {
			e.preventDefault();

			jQuery('div.ChargementAjax').fadeIn(200);
		
			var arr_action = {};			
			arr_action["no_participant"] = no_participant;			
			arr_action["nb_participant"] = nb_participant;		
			arr_action["action"] = 'ajout_participant';
				
			jQuery.ajax({url: url_ajax,cache: false, type: "POST", data: arr_action, success: function(data) {
			
				 jQuery('div#ConteneurAjax').append(data);
				 jQuery('div#Participant_'+no_participant).slideDown(600);
				 jQuery('div.ChargementAjax').fadeOut(200);
				 nb_participant = nb_participant + 1;
				 no_participant = no_participant + 1;
				 			
			}});
		
		});

		//Supprimer un participant
		//.On pour capturer le contenu générer en AJAX
		jQuery('div#ConteneurAjax').on( 'click', 'a.SubmitSupressionParticipant', function(e) {
			e.preventDefault();
			
			var id_participant = jQuery(this).parent().parent().attr('id');
			
			jQuery('div#'+id_participant).slideUp(600,function() {
				jQuery('div#'+id_participant).remove();
				nb_participant = nb_participant - 1;
			});
			
			
		});	

		//Sauvegarder les informations, puis envoyer vers Paypal
		jQuery('a.SubmitSauvegarde').click(function(e) {
			e.preventDefault();

			var form_valide = 0;					
			form_valide = Verification();

			if (form_valide == 1) {	
			
				jQuery('div.ChargementAjax').fadeIn(200);
			
				jQuery.ajax({url: url_ajax,cache: false, type: "POST", data: jQuery('form#FormActivite').serialize(), success: function(data) {				
					 jQuery('div#ConteneurAjax').html(data);				 				
				}});
			}
			
		});		
		
		
		//Vérifie que les champs sont bien remplis avant de soumettre pour le paiement
		function Verification() {
		
			var flag_valide = 1;
			var message_erreur = '<?php team_the_wpml('System', 'champ_obligatoire'); ?>';
		
			jQuery('form#FormActivite p input.Obligatoire,form#FormActivite p select.Obligatoire').each(function() {
			
				if (jQuery(this).val() == '') {
				
					flag_valide = 0;
					jQuery(this).parent().append('<div class="BoiteErreur">'+message_erreur+'</div>');
					
				}
			});
		
			return flag_valide;
		}

		//Efface les message d'erreur sur un :hover
		jQuery(document).on({
			mouseenter: function () {
				jQuery(this).fadeOut(400,function() {
					jQuery(this).remove();
				});
			}
		}, 'div.BoiteErreur');
		
		
});
</script>