<?php
/*
Template Name: Nouvelles
*/
	get_header();
	the_post();
?>

<div id="ContentWrap">
	<div id="Content">
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">

			<?php if (!have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				
					<?php	
						echo '<h1>'. apply_filters('the_title',get_the_title()) .'</h1>';
						global $wp_query;

						$args = array(
							'showposts'     => -1,
							'orderby'         => 'post_date',
							'order'           => 'DESC',
							'post_type'       => 'les-nouvelles'
							 );

						$postsquery = new WP_Query($args);
						$postslist = $postsquery->posts;

						$ii = 1;
						
						$output = '';
						//$output .= '<div class="Archives">';
						$langue = icl_t('System', 'language_code', utf8_encode('fr')); 
						
						if(count($postslist)){
							foreach ($postslist as $une_nouvelle){
								
								if($ii <= 2){

									$output .= '<div class="Titre">';
									$output .=	'<h2>'. $une_nouvelle->post_title .'</h2>';
									if ($langue == 'fr') {
										$output .= '<div class="DateNouvelle">'.get_the_time('j F Y', $une_nouvelle->ID).'</div>';
									}
									else {
										$output .= '<div class="DateNouvelle">'.get_the_time('F j, Y', $une_nouvelle->ID).'</div>';
									}
									$output .= '</div>';
									
									$output .= '<div class="Texte">';
									$output .= '<div class="TextFitH">'. apply_filters('the_content',$une_nouvelle->post_content) .'</div>';
									$output .= '<p class="Bouton"><a title="'.get_permalink($une_nouvelle->ID,false).'" href="'. get_permalink($une_nouvelle->ID,false) .'"><span class="BoutonGauche"><span class="BoutonDroite"><span class="BoutonMilieu">'. team_the_wpml('Buttons', 'button_more',false) .'</span></span></span></a></p>';
									$output .= '</div>';
									$output .= '<div class="clear"></div>';

								}else{ //Archives
									if ($ii == 3) {
										$output .= '<h3 class="Archives">'. icl_t('Widgets', 'widget title', utf8_encode('Archives')) .'</h3>';
										$output .= '<ul class="SelecteurArchives">';
									}
									if ($langue == 'fr') {
										$output .= '<li class="Archives"><div class="Date_Arch">'.get_the_time('j F Y', $une_nouvelle->ID).'</div><a title="'.get_permalink($une_nouvelle->ID,false).'" href="'.get_permalink($une_nouvelle->ID,false).'">'. apply_filters('the_title',$une_nouvelle->post_title) .'</a></li>';										
									} else {
										$output .= '<li class="Archives"><div class="Date_Arch">'.get_the_time('F j, Y', $une_nouvelle->ID).'</div><a title="'.get_permalink($une_nouvelle->ID,false).'" href="'.get_permalink($une_nouvelle->ID,false).'">'. apply_filters('the_title',$une_nouvelle->post_title) .'</a></li>';
									}
								}
					
							$ii++;
							}
							if ($ii >=2) { $output .= '</ul>'; }
						}						
						
						echo $output;										
					?>

				<?php endwhile; ?>			
			<?php endif; ?>
			
		<?php get_sidebar(); ?>
		
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

<script type="text/javascript">
	// Lorsque jQuery est pr�t, active le bon bouton...
	jQuery(document).ready(function() {
		
	});
</script>

<?php get_footer(); ?>