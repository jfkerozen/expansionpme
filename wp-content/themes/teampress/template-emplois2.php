<?php
/*
Template Name: Emplois une page
*/
?>
<?php get_header(); 

the_post();
?>

<div id="ContentWrap">
	<div id="Content">
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			
			<?php if (!have_posts()) : ?>		
				<?php while (have_posts()) : the_post(); ?>
				
					<?php
						//echo '<h1>'. apply_filters('the_title',get_the_title()) .'</h1>';		
						the_content(); 
					?>
					<div id="ContainerTitreCV">						
						<h2 class="NoDisplay"><?php echo icl_t('Careers', 'career_cv', utf8_encode('Banque de CV')); ?></h2>					
						<div id="ContainerBanqueCV">				
							<p class="FadeButton"><a href="#" class="ToggleAccordion CV"><span><?php echo icl_t('Buttons', 'send_us_your_resume', utf8_encode('Faites-nous parvenir votre C.V.')); ?></span></a></p>
							
							<?php
								if(team_language() == 'fr') {
									echo do_shortcode('[contact-form 3 "Emploi Fr"]');
								} else {
									echo do_shortcode('[contact-form 4 "Emploi En"]');
								}
							?>
						</div>
					</div>	
					<h1><?php echo icl_t('Titles', 'available_positions', utf8_encode('Postes disponibles')); ?></h1>	
					
					<?php
					
						global $wp_query;
						global $post;
					
					// Save old query
					$old_query = clone($wp_query);
					

					$args = array(
						'showposts'     => -1,
						'orderby'         => 'menu_order',
						'order'           => 'ASC',
						'post_type'       => 'les-emplois'
					);
								
					$postsquery = new WP_Query($args);
					$postslist = $postsquery->posts;
					
					$output = '';
					$ii = 1;
					$post_count = count($postslist);
					
					if($post_count) {
						foreach ($postslist as $un_emploi) {
							$output .= '<div class="LeSousPost Emploi' . (($ii == 1) ? ' PremierEmploi' : '') . (($post_count == $ii) ? ' DernierEmploi' : '') . '">';
											
							$output .= '<div class="PostContent">';
							$output .= '<h2>' . apply_filters('the_title', $un_emploi->post_title) . '</h2>';  // PostTitle
							$output .= apply_filters('the_content', $un_emploi->post_content);
							
							$output .= '</div>';
								
							if($attached_post = get_post_meta($un_emploi->ID, 'FichierPDF', true)) {
								$attached_file = get_option('siteurl') . '/wp-content/uploads/' . get_post_meta($attached_post, '_wp_attached_file', true);
								$output .= '<p class="FadeButton"><a href="' . $attached_file . '" target="_blank" rel="external"><span>';
								$output .= icl_t('Buttons', 'details', utf8_encode('D�tails (PDF)'));
								$output .= '</span></a></p><div class="clear"></div>';
							}
							
							$output .= '<div class="EmploiUnePage">';
							
							$output .= '<p class="FadeButton"><a href="#" class="ToggleAccordion"><span>';
							$output .= icl_t('Buttons', 'apply', utf8_encode('Postuler'));
							$output .= '</span></a></p><div class="clear"></div>';
							
							if(icl_t('System', 'language_code', 'fr') == 'fr') {
								$output .= do_shortcode('[contact-form 3 "Emploi Fr"]');
							} else {
								$output .= do_shortcode('[contact-form 4 "Emploi En"]');
							}

							$output .= '</div>';
							
							$output .= '</div>';
							
							$output .= '<div class="clear"></div>';
							
							$ii++;
						}
					} else {
						$output .= '<div class="LeSousPost"><p>' . icl_t('Messages', 'no_position_available', utf8_encode('Il n\'y a aucun poste disponible pour l\'instant.')) . '</p></div>';
					}
					echo $output;
					
					?>
					
				<?php endwhile; ?>		
			<?php endif; ?>
			
		</div><!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

	<script type="text/javascript">
		// Lorsque jQuery est pret, active les effets custom
		jQuery(document).ready(function() {	

			// Rempli le lien emploi vs. formulaire
			jQuery('div.HiddenInput input').map(function(){
				var jobTitle = jQuery(this).parent().parent().parent().parent().parent().parent().parent().find('h2').html();
				jobTitle = jobTitle.replace(/(&nbsp;)/g, ' ');
				jQuery(this).val(jobTitle);
			});
		
			// Click ouvre accordeon formulaire
			jQuery('a.ToggleAccordion').click(function(e) {
				e.preventDefault();
				// Ouvre
				if(!jQuery(this).hasClass('Opened')) {
					jQuery(this).addClass('Opened');
					jQuery(this).find('span').html('<?php echo icl_t('Buttons', 'close', utf8_encode('Fermer')); ?>');
					jQuery(this).parent().parent().find('div.wpcf7').slideDown(400);
				
				// Ferme
				} else {
					jQuery(this).removeClass('Opened');
					if(jQuery(this).hasClass('CV')) {
						jQuery(this).find('span').html('<?php echo icl_t('Buttons', 'send_us_your_resume', utf8_encode('Faites-nous parvenir votre C.V.')); ?>');
					} else {
						jQuery(this).find('span').html('<?php echo icl_t('Buttons', 'apply', utf8_encode('Postuler')); ?>');
					}
					jQuery(this).parent().parent().find('div.wpcf7').slideUp(400);
				}
			});
		});
	</script>
	
<?php get_footer(); ?>