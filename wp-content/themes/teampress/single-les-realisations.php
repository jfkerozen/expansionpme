<?php 
	get_header(); 
	the_post();	
?>
	<div id="ContentWrap">
		<div id="Content">	
			<div id="Template" class="template-single-realisation">	
				<?php
					$output = '';
					
					$output .= '<div id="SingleRealisation">';
					$output .= team_the_thumb(false,false,'','team_thumbnail_image0',352,352,1,1);
					$output .= '</div>';
					$output .= '<p class="Bouton "><a href="'.team_the_permalink("realisations",false).'"><span class="BoutonRetour"></span></a></p><div class="clear"></div>';
					
					echo $output;
				?>
			</div> <!-- Template -->
		</div><!-- Content -->
	</div><!-- ContentWrap -->
	
<?php 
	get_footer();
 ?>