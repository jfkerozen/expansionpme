<?php
/*
Template Name: Accueil
*/

if(is_page(get_page_by_path('about-us')->ID)) {
	include('template-commun.php');
	exit;
}

	get_header();
?>
<div id="ContentWrap">
	<div id="Content">	
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
		
			<?php if (have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>

					<?php
						echo '<h1 class="NoDisplay">'. apply_filters('the_title',get_the_title()) .'</h1>';	
						global $wp_query;

						$args = array(
								'showposts'     => -1,
								'orderby'         => 'menu_order',
								'order'           => 'ASC',
								'post_type'       => 'les-sliders'
						);
							
						$postsquery = new WP_Query($args);
						$postslist = $postsquery->posts;
						$count = count($postslist);
						$ii = 1;

						$output = '';						
						$output .= '<div id="Slider">';
						$output .= '<div id="LayerSlider" style="width: 948px; height: 335px;">';
						
						foreach( $postslist as $une_slide ) {
						
							//Chercher le type d'évènement pour triangle coloré
							$term_post = wp_get_post_terms( $une_slide->ID, 'champs-expertises');						
							$image_url = get_taxonomy_image_url($term_post[0]->term_id);
							
							//Couleur des textes
							$CouleurFond = get_post_meta($une_slide->ID, 'CouleurFond', true);
							$CouleurTexte = get_post_meta($une_slide->ID, 'CouleurTexte', true);

							//Transition entre les slides, delai etc.
							$output .= '<div class="ls-layer" style="transition2d: 5; slidedelay: 6000; timeshift: -1000;">';						
							
							//Slide principale
							$classes = array('ls-bg');
							$output .= team_the_thumb(false,false,'','team_thumbnail_image0',948,335,1,1,$une_slide->ID,'jpeg&q=100',false,$classes);			
							
							//Slide et texte supplementaire qui s'ajoutent par dessus la slide principale
							$output .= '<div class="ls-s2 '.$CouleurFond.' '.$CouleurTexte.'" style="delayin: 0; slidedirection : fade; slideoutdirection: fade;" >';
							$output .= '<div class="TypeEvenement">'. $term_post[0]->name .'&nbsp;'. team_the_thumb(false,false,$image_url[0],'',18,18,1,1,'','png') .'</div>';
							$output .= apply_filters('the_content',$une_slide->post_content) .'</div>';
						
							if (get_post_meta($une_slide->ID,'Lien',true) != '') {
								$output .= '<a class="ls-linkto" href="'.get_post_meta($une_slide->ID,'Lien',true).'"></a>';
							}
							$output .= '</div>';					
							
							$ii++;
						}
						
						$output .= '</div>';
						$output .= '</div>';
						$output .= '<div class="LigneVerteSlider"></div>';
						
						$output .= '<div class="ResumeAccueil"><div class="ZoneTexte">'.apply_filters('the_content',get_the_content()).'</div></div>';
						
						//Champs d'expertises , activités et nouvelles
						
						$output .= '<div id="BottomAccueil">';
						$output .= '<div class="ZoneTitre ZoneTitreExpertise"><div class="TitreAccueil">'.team_the_wpml('Accueil', 'accueil_expertise',false).'</div><a href="'.team_the_permalink('a-propos',false).'" class="MiniTitreAccueil">'.team_the_wpml('Accueil', 'accueil_apropos',false).'</a><div class="clear"></div></div>';
						
						$output .= '<div id="ZoneExpertiseWrap">';
						$output .= '<a href="'.team_the_permalink('services-conseils',false).'" class="ZoneExpertise Expertise1"><div class="TexteExpertise">'.apply_filters('the_content',get_post_meta($post->ID,'Expertise1',true)).'</div><div class="ZoneHoverExpertise"><div class="ShapePlus">'.team_the_wpml('Accueil', 'accueil_savoirplus',false).'<img src="'.get_bloginfo('template_directory').'/images/bouton_plus.png" width="20" height="20" /></div></div></a>';
						$output .= '<a href="'.team_the_permalink('aide-financiere',false).'" class="ZoneExpertise Expertise2"><div class="TexteExpertise">'.apply_filters('the_content',get_post_meta($post->ID,'Expertise2',true)).'</div><div class="ZoneHoverExpertise"><div class="ShapePlus">'.team_the_wpml('Accueil', 'accueil_savoirplus',false).'<img src="'.get_bloginfo('template_directory').'/images/bouton_plus.png" width="20" height="20" /></div></div></a>';
						$output .= '<a href="'.team_the_permalink('activites',false).'" class="ZoneExpertise Expertise3"><div class="TexteExpertise">'.apply_filters('the_content',get_post_meta($post->ID,'Expertise3',true)).'</div><div class="ZoneHoverExpertise"><div class="ShapePlus">'.team_the_wpml('Accueil', 'accueil_savoirplus',false).'<img src="'.get_bloginfo('template_directory').'/images/bouton_plus.png" width="20" height="20" /></div></div></a>';
						$output .= '<div class="clear"></div>';
						$output .= '</div>';
						
						$output .= '<div id="BigZoneActivitesWrap">';
						$output .= '<div id="ZoneActivitesWrap">';
						$output .= '<div class="ZoneTitre"><div class="TitreAccueil">'.team_the_wpml('Accueil', 'accueil_activite',false).'</div><a href="'.team_the_permalink('activites',false).'" class="MiniTitreAccueil">'.team_the_wpml('Accueil', 'accueil_calendrier',false).'</a><div class="clear"></div></div>';														
						$output .= '<div id="ZoneActivitesViewport">';	
				
						$args = array(
								'showposts'     => -1,
								'orderby'       => 'menu_order',
								'order'         => 'ASC',
								'post_type'     => 'les-activites',
								'tax_query' => array(
													array(
														'taxonomy' => 'attributs',
														'field' => 'slug',
														'terms' => 'en-vedette'
													)
												)
						);
							
						$postsquery = new WP_Query($args);
						$postslist_vedette = $postsquery->posts;
						$count_vedette = count($postslist);

						if ($count_vedette < 6) {
							$args = array(
									'showposts'     => -1,
									'orderby'       => 'menu_order',
									'order'         => 'ASC',
									'post_type'     => 'les-activites',
									'tax_query' => array(
														array(
															'taxonomy' => 'attributs',
															'field' => 'slug',
															'terms' => 'en-vedette',
															'operator' => 'NOT IN'
														)
													)
							);

								$postsquery = new WP_Query($args);
								$postslist_normal = $postsquery->posts;
								$count_normal = count($postslist);		
						}
						
						$cpt_activite = 0;
						
						if ($count_vedette > 0 || $count_normal > 0) {
						
							foreach($postslist_vedette as $une_activite ) {
								if ($cpt_activite < 6) {
								
									$term_post = wp_get_post_terms( $une_activite->ID, 'champs-expertises');	
									$image_url = get_taxonomy_image_url($term_post[0]->term_id);
								
									$output .= '<div class="ZoneActivite">';
									
									$output .= '<div class="BoiteImage"><a href="'.get_permalink($une_activite->ID,false).'">'. team_the_thumb(false,false,'','team_thumbnail_image0',189,125,1,1,$une_activite->ID,'jpeg&q=100') .'</a><div class="ChampActivite">'.team_the_thumb(false,false,$image_url[0],'',18,18,1,1,'','png').'</div></div>';
									if (team_the_wpml('System', 'language_code',false) == 'fr') {
		
										$output .= '<div class="DateActivite">'.get_post_meta($une_activite->ID,'DateActivite',true).'</div>';
									}
									else {
										$output .= '<div class="DateActivite">'.get_post_meta($une_activite->ID,'DateActivite',true).'</div>';
									}				
									$output .= '<div class="TypeActivite">'.$term_post[0]->name.'</div>';
									$output .= '<a href="'.get_permalink($une_activite->ID,false).'" class="TitreActivite">'.$une_activite->post_title.'</a>';
									
									$output .= '</div>';
									
								}
								$cpt_activite++;
							}		

							if ($cpt_activite < 6) {
								foreach($postslist_normal as $une_activite ) {
									if ($cpt_activite < 6) {
									
										$term_post = wp_get_post_terms( $une_activite->ID, 'champs-expertises');	
										$image_url = get_taxonomy_image_url($term_post[0]->term_id);
									
										$output .= '<div class="ZoneActivite">';
										
										$output .= '<div class="BoiteImage"><a href="'.get_permalink($une_activite->ID,false).'">'. team_the_thumb(false,false,'','team_thumbnail_image0',189,125,1,1,$une_activite->ID,'jpeg&q=100') .'</a><div class="ChampActivite">'.team_the_thumb(false,false,$image_url[0],'',18,18,1,1,'','png').'</div></div>';
										if (team_the_wpml('System', 'language_code',false) == 'fr') {
											//date_i18n("j F Y", strtotime(get_post_meta($une_activite->ID,'DateActivite',true)))
											//$output .= '<div class="DateActivite">'.date_i18n("j F Y", strtotime(get_post_meta($une_activite->ID,'DateActivite',true))).'</div>';
										}
										else {
											//$output .= '<div class="DateActivite">'.date_i18n("F, j Y", strtotime(get_post_meta($une_activite->ID,'DateActivite',true))).'</div>';
										}		
										$output .= '<div class="DateActivite">' . get_post_meta($une_activite->ID,'DateActivite',true) . '</div>';
										$output .= '<div class="TypeActivite">'.$term_post[0]->name.'</div>';
										$output .= '<a href="'.get_permalink($une_activite->ID,false).'" class="TitreActivite">'.$une_activite->post_title.'</a>';
										
										$output .= '</div>';
										
									}
									$cpt_activite++;
								}
							}
						}
						else {
							//Aucune activité!
						}
						
						
						$output .= '</div>'; //ZoneActiviteViewport
												
						$output .= '<a href="#" id="ui-carousel-next"><span></span></a>';
						$output .= '<a href="#" id="ui-carousel-prev"><span></span></a>'; //Boutons du Carousel
								
						$output .= '<a href="'.team_the_permalink('activites',false).'" class="BoutonMobileActivites" >'.team_the_wpml('Accueil', 'accueil_btn_activites',false).'</a>';
								
						$output .= '</div>'; //ZoneActiviteWrap 

						//Propriétaire PME
						$output .= '<a href="'.team_the_permalink('equipe',false).'" id="ProprietairePME"><div class="TitreProprietaire">'.team_the_wpml('Accueil', 'accueil_propriopme',false).'</div><div class="ContenuProprietaire">'.team_the_wpml('Accueil', 'accueil_proprioconsulte',false).'</div></a>';
						
						$output .= '</div>'; //BigZoneActiviteWrap
						
						$output .= '<div id="ZoneNouvellesWrap">';
						$output .= '<div class="ZoneTitre"><div class="TitreAccueil">'.team_the_wpml('Accueil', 'accueil_nouvelles',false).'</div><a href="'.team_the_permalink('blogue',false).'" class="MiniTitreAccueil">'.team_the_wpml('Accueil', 'accueil_toutelesnouvelles',false).'</a><div class="clear"></div></div>';														

							$args = array(
									'showposts'     => 2,
									'orderby'         => 'menu_order',
									'order'           => 'ASC',
									'post_type'       => 'les-nouvelles'
							);
								
							$postsquery = new WP_Query($args);
							$postslist = $postsquery->posts;
							$count = count($postslist);
							$ii = 1;
							
							foreach( $postslist as $une_nouvelle ) {	
								
								$output .= '<div class="ZoneNouvelle">';
								$output .= '<div class="DateNouvelle">';
								if (team_the_wpml('System', 'language_code',false) == 'fr') {
									$output .= '<span class="JourNouvelle">'.date_i18n("j", strtotime($une_nouvelle->post_date)).'</span><br/>';
									$output .= date_i18n("M", strtotime($une_nouvelle->post_date)).'.<br/>';
									$output .= date_i18n("Y", strtotime($une_nouvelle->post_date)).'<br/>';
								}
								else {
									$output .= date_i18n("M", strtotime($une_nouvelle->post_date)).'.<br/>';
									$output .= '<span class="JourNouvelle">'.date_i18n("j", strtotime($une_nouvelle->post_date)).'</span><br/>';
									$output .= date_i18n("Y", strtotime($une_nouvelle->post_date)).'<br/>';
								}	
								$output .= '</div>';
								$output .= '<div class="WrapContenuNouvelle"><a class="nouvelleTitre" href="'.get_permalink($une_nouvelle->ID,false).'" ><div class="TitreNouvelle">'.$une_nouvelle->post_title.'</a></div>';
								$output .= '<div class="ExtraitNouvelle"><p>' . strip_tags(apply_filters('the_content',$une_nouvelle->post_content)) . '</p></div>';
								$output .= '<a class="SuiteNouvelle" href="'.get_permalink($une_nouvelle->ID,false).'" >'.team_the_wpml('Accueil', 'accueil_suitenouvelle',false).'</a>';
								$output .= '</div><div class="clear"></div>';								
								
								$output .= '</div>';
								
							}							
						
						$output .= '<a href="'.team_the_permalink('blogue',false).'" class="BoutonMobileNouvelles" >'.team_the_wpml('Accueil', 'accueil_btn_nouvelles',false).'</a>';
						$output .= '</div>'; //ZoneNouvelleWrap
						$output .= '<div class="clear"></div>';

						$output	.= '<div id="LignePartenaire"></div>';		
						$output .= '<div class="DescriptifPartenaires"><p>'.team_the_wpml('Accueil', 'accueil_partenaires',false).'</p></div>';
						
						$args = array(
								'showposts'     => 3,
								'orderby'         => 'menu_order',
								'order'           => 'ASC',
								'post_type'       => 'les-partenaires'
						);
							
						$postsquery = new WP_Query($args);
						$postslist = $postsquery->posts;
						$count = count($postslist);
						$ii = 1;
						$parametre = array('far' => 'C');
						
						$output .= '<div id="ImagesPartenaireOuter"><div id="ImagesPartenaireInner">';
						foreach( $postslist as $un_partenaire ) {						
							$output .= '<div class="ImagePartenaire">'. team_the_thumb(false,false,'','team_thumbnail_image0',197,95,0,0,$un_partenaire->ID,'jpeg&q=100',false,'',false,$parametre) .'</div>';
						}
						$output .= '</div></div>';
						
						$output .= '</div>'; //BottomAccueil
						
						echo $output;				
					?>
			
				<?php endwhile; ?>
			<?php endif; ?>
			
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

<script src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.core.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.widget.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.rcarousel.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() {

		jQuery('div#LayerSlider').layerSlider({
			skinsPath : '../layerslider/skins/',
			navPrevNext : false,
			showCircleTimer : false,
			responsive : false,
			thumbnailNavigation : 'disabled',
			skin : 'defaultskin'
		});


		w_width = jQuery(window).width();	
		
		if (w_width > 640) {
			jQuery( "#ZoneActivitesViewport ").rcarousel({
				visible: 2,
				step: 1,
				width: 191,
				height: 270,
				margin: 32,
				next: "#ui-carousel-next", 
				prev: "#ui-carousel-prev"
			});
		}
		else {
			jQuery( "#ZoneActivitesViewport ").rcarousel({
				visible: 1,
				step: 1,
				width: 191,
				height: 270,
				margin: 32,
				next: "#ui-carousel-next", 
				prev: "#ui-carousel-prev"
			});
		
		}

		
});
</script>
<?php
	get_footer(); 
?>