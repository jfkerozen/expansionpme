<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	Par : Jimmy Lavoie selon code fait par MapPress et Nicolas Morin.
	Date : 2010/01/04
	N.B.  - Utilise le pluging MapPress comme saisie dans l'interface WP-Admin.
	      - Utilise la version 3 de l'api Google Maps. Pas besoin de cl�s.
		  - Permet un meilleur controle sur l'affichage de la carte.
	URL d'appel : 
		  <iframe src="<?php bloginfo('template_url');?>/map.php?PostID=<?php the_ID();?>&mappress_size=<?php echo $team_mappress["size"];?>&mappress_zoom=<?php echo $team_mappress["zoom"];?>&mappress_center_lat=<?php echo $team_mappress["center_lat"];?>&mappress_center_lng=<?php echo $team_mappress["center_lng"];?>&mappress_auto_center=<?php echo $team_mappress["auto_center"];?>" name="frame1" scrolling="no" frameborder="no" align="center" width="700" height="500"></iframe>
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	//Convertir les donn�es $_REQUEST dans des variables distinctes. Attention pour le d�bogage.
	extract($_REQUEST, EXTR_PREFIX_SAME, "TEAM_");
	
	/*
	echo $PostID . "<br/>. $mappress_size . "<br/>" . $mappress_zoom . "<br/>" . $mappress_center_lat . "<br/>" . $mappress_center_lng . "<br/>" . $mappress_auto_center . "<br/>";
	*/

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<script type='text/javascript' src='http://www.nouvelledemeure.com/wp-includes/js/jquery/jquery.js?ver=1.3.2'></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			initialize();			
		}); 
	</script>
</head>
<body>
	<div style="width:700px;height:500px;background:transparent;">
		<div id="map_canvas" style="width:700px; height:500px;">&nbsp;</div>
		<script type="text/javascript">
		  function initialize() {
			var latlng = new google.maps.LatLng(<?php echo $mappress_center_lat;?>, <?php echo $mappress_center_lng;?>);
			var myOptions = {
			  zoom: <?php echo $mappress_zoom;?>,
			  center: latlng,
			  mapTypeControl: false,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			
			var logoIcon = new google.maps.MarkerImage("http://www.nouvelledemeure.com/wp-content/themes/nouvelledemeure/images/pin_ndemeure.png",
			  // This marker is 66 pixels wide by 70  pixels tall.
			  new google.maps.Size(64, 64),
			  // The origin for this image is 0,0.
			  new google.maps.Point(0,0),
			  // The anchor for this image is the base of the flagpole at 33,70.
			  new google.maps.Point(64/2, 64/2));
			
			var marker = new google.maps.Marker({
			  position: latlng, 
			  map: map, 
			  title: "Nouvelle Demeure",
			  icon: logoIcon
			});
		  }
						  
		</script>
		
	</div> <!--  -->
</body>
</html>