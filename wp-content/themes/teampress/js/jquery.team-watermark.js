// Plugin jQuery permettant de faire afficher un texte dans une input box lorsque d�sactiv�e
// Nicolas Morin 2010-06-07
(function($){  
	$.fn.watermark = function(options) {  

		// Valeurs par d�faut des param�tres
		var defaults = {  
			text: 'Texte',  
			color: '',
			defaultClass : ''
		}; 
		
		// Prends la valeur par d�faut si param�tre non fourni
		var options = $.extend(defaults, options);  
	 
	 // Got focus
    this.focus(function()
    {
		if (jQuery(this).val() == options.text)
        {
					jQuery(this).css("color", "");
					jQuery(this).val("");
					if(options.defaultClass != '') {
						jQuery(this).removeClass(options.defaultClass);
					}
        }
    });
    
		// Lost focus
    this.blur(function()
    {
        if (jQuery(this).val() == "")
        {
					if(options.defaultClass != '') {
						jQuery(this).addClass(options.defaultClass);
					}
  				jQuery(this).css("color", options.color);
					jQuery(this).val(options.text);
        }
    });

		// Normalement vide lors du load...
		this.blur();
		
		// Pour le reload
		if(this.val() == options.text) {
			this.css("color", options.color); 
			if(options.defaultClass != '') {
				jQuery(this).addClass(options.defaultClass);
			}
		}

		return true;
	
	};
})(jQuery);
