(function($){
	$.fn.fontReady = function(handler) {
		return jQuery(document).bind('fontReady', function(ev) {
			handler(ev);
		});
	}; 
})(jQuery);


var preWidth = 0;
var hndWidthChecker = 0;

jQuery(document).ready(function() {

	preWidth = jQuery('div#FontLoad').width();
	
	hndWidthChecker = setInterval(function() {
			if(jQuery('div#FontLoad').width() != preWidth || jQuery.browser.msie) {
				clearInterval(hndWidthChecker);
				hndWidthChecker = 0;
				jQuery(document).triggerHandler('fontReady');
			}
	}, 100);
});