jQuery.extend({
	delay: function(msec, callback) {
		if(typeof callback == 'function'){
			setTimeout(callback, msec);
		}
	}
});