/*
 * Style File - jQuery plugin for styling file input elements
 *  
 * Copyright (c) 2007-2008 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Based on work by Shaun Inman
 *   http://www.shauninman.com/archive/2007/09/10/styling_file_inputs_with_css_and_the_dom
 *
 * Revision: $Id: jquery.filestyle.js 303 2008-01-30 13:53:24Z tuupola $
 *
 */

(function($) {
    
    $.fn.filestyle = function(options) {
                
        /* TODO: This should not override CSS. */
        var settings = {
            width : 600
        };
                
        if(options) {
            $.extend(settings, options);
        };
                        
        return this.each(function() {
            
            var self = this;
            var wrapper = $("<div />")
                            .css({
                                "width": settings.imagewidth + "px",
                                "height": settings.imageheight + "px",
                                "background": "url(" + settings.image + ") 0 0 no-repeat",
                                "background-position": "right",
                                "display": "inline",
                                "position": "absolute",
                                "overflow": "hidden"
                            });

			//wrap pour le bouton extensible
			var wrapperspan = $('<p class="Bouton"><a><span class="BoutonGauche"><span class="BoutonDroite"><span class="BoutonMilieu TexteParcourir"></span></span></span></a></p><div class="clear"></div>');				

			var filename = $('<input class="file">')
                             .addClass($(self).attr("class"))
                             .css({
                                 "display": "inline",
                                 "width": settings.width + "px"
                             });
			

            $(self).before(filename);
			$(self).before(wrapperspan);
			

            $(self).css({
                        "position": "relative",
                        "height": settings.imageheight + "px",
                        "width": settings.width + "px",
                        "display": "inline",
                        "cursor": "pointer",
                        "opacity": "0.0",
						"height": "50px",
						"top": -52 + "px"
                    });
					

            if ($.browser.mozilla) {
                if (/Win/.test(navigator.platform)) {
                    $(self).css("margin-left", "-142px");
					$(self).css("position", "absolute"); 		
					$(self).css("top", "0px"); 
					$(self).css("width:", "600px"); 
					$(self).css("right", "90px");    
					$(self).css("font-size", "29px");   					
                } else {
                    //$(self).css("margin-left", "-168px");                    
                };
            } else {
                $(self).css("margin-left", settings.imagewidth - settings.width + "px");
				$(self).css("width", "100px");      
				$(self).css("font-size", "21px"); 					
            };

            $(self).bind("change", function() {
                filename.val($(self).val());
				
				// Modif de Team pour enlever C:\fakepath\ Sur IE et Chrome
				var path = encodeURI($(self).val());
				path = path.replace("C:%5Cfakepath%5C","");
				path = decodeURI(path);
				
				// Affiche le nom du fichier dans le span
				$('div.FileSpecial span.text').html('');
				$('div.FileSpecial span.text').html(path);
				
				
            });
      
        });
        
    };

})(jQuery);