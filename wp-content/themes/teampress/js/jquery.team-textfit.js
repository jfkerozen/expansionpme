// Plugin jQuery permettant de faire fitter le texte dans une hauteur donn�e
// Nicolas Morin 2010-02-10

(function($){  
	$.fn.textFit = function(options) {  
 
		// Valeurs par d�faut des param�tres
		var defaults = {  
			maxHeight: 300,  
			trimText: false,
			viewMoreHTML: '...'
		}; 
		
		// Prends la valeur par d�faut si param�tre non fourni
		var options = $.extend(defaults, options);  

		this.map(function() {
			// Si le redimensionnement n'est pas n�cessaire, sort et retourne false
			if(jQuery(this).height() <= options.maxHeight) {
				return false;
			}
			
			// Cache le texte pendant l'ex�cution
			jQuery(this).css('visibility', 'hidden');
			var parContent = jQuery(this).html().split(' ');

			jQuery(this).html(parContent.join(' ') + options.viewMoreHTML);
			
			while(jQuery(this).height() > options.maxHeight) {
				parContent.pop();
				jQuery(this).html(parContent.join(' ') + options.viewMoreHTML);
			}
			
			//R�affiche le texte apr�s l'ex�cution
			jQuery(this).css('visibility', 'visible');
		});
		
		return true;
	
	};
})(jQuery);
