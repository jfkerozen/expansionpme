<?php
	/*
	Template Name: Contact
	*/
	get_header();
?>

<div id="ContentWrap">
	<div id="Content">
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<?php if (have_posts()) : ?>			
				<?php while (have_posts()) : the_post(); ?>
				
					<div class="LeftContactContent">
						<?php 								
							the_content();
						?>
					</div>
					
					<div class="RightContactContent">
					
						<p class="Bouton BoutonGoogleMapMobile"><a target="_blank" href="https://maps.google.ca/maps?q=470+boul+wilfrid+laurier+mont+saint+hilaire&ie=UTF-8&hq=&hnear=0x4cc9ab2f16a3d6df:0xc1edd53a25154ad2,470+Boulevard+Sir-Wilfrid-Laurier,+Mont-Saint-Hilaire,+QC+J3H+4X6&gl=ca&ei=sh0GU4yXDOad0QH3goCQDw&ved=0CC0Q8gEwAA"><?php team_the_wpml('Contact', 'contact_voir_carte'); ?></a></p>
					
						<div class="CadreGoogleMap">
							<?php					
								include_once('google_map.php');						
							?>
						</div>
					</div>
					<div class="clear"></div>
					
					<?php						
						if(team_language() == 'fr') {
							echo do_shortcode('[contact-form 1 "Contact Fr"]');
						} else {
							echo do_shortcode('[contact-form 2 "Contact En"]');
						}
					?>

					<?php					
						include_once('google_map.php');						
					?>
					
				<?php endwhile; ?>			
			<?php endif; ?>
			
		</div><!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

<script type="text/javascript">
	// Lorsque jQuery est pret, active les effets custom
	jQuery(document).ready(function() {		

	});
</script>
<?php
	get_footer(); 
?>