<?php
	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-search">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<div class="ZoneRecherche">
			<h1><?php team_the_wpml('Recherche', 'results_for'); ?></h1>
			<h2 class="KeyWords"><?php echo esc_attr(apply_filters('the_search_query', get_search_query())); ?></h2>
			</div>
			<?php if (have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<p><?php echo strip_tags(get_the_excerpt()); ?></p>
				<?php endwhile; ?>
			<?php else: ?>
				<h2><?php team_the_wpml('Recherche', 'no_result_found'); ?></h2>
			<?php endif; ?>
			<hr />
			<div class="Pagination">
				<?php
					$args = array(
													'title' => '',
													'nextpage' => team_the_wpml('Pagination', 'next', false),
													'previouspage' => team_the_wpml('Pagination', 'previous', false)													
												);
					wp_paginate($args);
				?>
			</div>
			<div class="clear"></div>
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
