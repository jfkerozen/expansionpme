<?php
/*
Template Name: Realisations Slider
*/
	get_header();
	the_post();
?>
<div id="ContentWrap">
		<div id="Content">	
			<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
					<?php
						//apply_filters('the_title',the_title()); 	
						//apply_filters('the_content',the_content());
						

						
						$args = array(
								'orderby'         => 'menu_order',
								'order'           => 'DESC',
								'post_type'       => 'les-realisations',
								'paged' => $paged
								 );
					
						$postsquery = new WP_Query($args);
						$postslist = $postsquery->posts;
						$count = count($postslist);
						$ii = 1;

						$output = '';						
						$output .= '<div id="Slider">';
						$output .= '<div id="RhinoSlider">';
						
						foreach( $postslist as $une_realisation ) {				
						
								//Informations sur la realisations
								$output .= '<div class="Slide">'. team_the_thumb(false,false,'','team_thumbnail_image0',960,400,1,1,$une_realisation->ID) .'</div>';
								$ii++;
						}
						
						$output .= '</div>';
						$output .= '</div>';
						echo $output;
					
						if(function_exists('wp_paginate')) {
							wp_paginate();
						}

				
					?>
			</div> <!-- Template -->
		</div><!-- Content -->
</div><!-- ContentWrap -->

<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#RhinoSlider').rhinoslider({
		effect: 'shuffle',
		easing: 'easeInCubic',
		shiftValue: '300',
		parts: '4',
		showTime: 3000,
		effectTime: 500,
		controlsMousewheel: false,
		controlsKeyboard: false,
		controlsPlayPause: false,
		animateActive: false,
		autoPlay: true,
		showBullets: 'always',
		showControls: 'always',
		prevText: '',
		nextText: ''
	});
});
</script>

<?php
	get_footer(); 
?>