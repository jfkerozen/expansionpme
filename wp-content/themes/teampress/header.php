<?php
	header('X-UA-Compatible: IE=EmulateIE8');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta charset='utf-8'> 
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="format-detection" content="telephone=no" />
<!-- <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" /> -->
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
<?php echo utf8_encode('<meta name="Author" content="TEAM marketing.web.design http://www.equipeteam.com/" />'); ?>

<?php
	global $post;
	/*
	if (team_is_page('nouvelles') || (get_post_type($post) == 'les-nouvelles')) {
		?>
		<link rel="alternate" type="application/rss+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-nouvelles" />
		<link rel="alternate" type="application/atom+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-nouvelles" />
		<?php
	}
	else if (team_is_page('carrieres') || (get_post_type($post) == 'les-emplois')) {
		?>
		<link rel="alternate" type="application/rss+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-emplois" />
		<link rel="alternate" type="application/atom+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-emplois" />
		<?php
	}
	else {
		?>
		<link rel="alternate" type="application/rss+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-realisations" />
		<link rel="alternate" type="application/atom+xml" title="RSS feed TEAM marketing.web.design" href="<?php echo icl_get_home_url(); ?>feed/?post_type=les-realisations" />
		<?php
	}*/
?>

<?php /*<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>*/ ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-safari.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-firefox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-responsive.php" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/layerslider.css" type="text/css" media="screen" /> <!-- LayerSlider -->
<!-- <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/images/icone_57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/icone_72x72.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/icone_114x114.png" />

<meta name="viewport" content="width=device-width; initial-scale=1.0; />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<?php //wp_enqueue_script("jquery"); ?>
<?php //wp_enqueue_script("jquery-hoverflow"); ?>

<?php wp_head(); ?>

<script type="text/javascript">	
	
	// Path pour le pointer personalise
	path = '<?php bloginfo('template_directory'); ?>/images/map/';
		
	jQuery(document).fontReady(function() {
		//alert('Font load fini');		

			jQuery('div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.ExtraitNouvelle').textFit({
				maxHeight: 120,
				viewMoreHTML: '(...)'
			});	
		
	});
	
	// Lorsque jQuery est pret, active les effets custom
	jQuery(document).ready(function() {
	
		jQuery('a.PrintThisPage').click(function(e) {
			e.preventDefault();
			window.print();
		});
	
		jQuery('a.EmailThisPage').click(function(e) {
			e.preventDefault();
			window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
		});	
	
			// Check for device screen size
			//if(jQuery.mobile.media("screen and (max-device-width: 480px)")) {
				// Change viewport for smaller devices
			//	jQuery('meta[name=viewport]').attr('content','width=device-width, initial-scale=1');
			//}
	
		//Bouton parcourir personnalise
			jQuery("input.wpcf7-file").filestyle({ 
		//	image: "<?php bloginfo('template_directory'); ?>/images/parcourir_<?php echo icl_t('System', 'language_code', 'fr'); ?>.png",
		//	imageheight : 24,
		//	imagewidth : 96
		});
		
		jQuery('span.resume').hover(function(){
			jQuery(this).find('span.BoutonGauche').addClass('BoutonGaucheOver');
			jQuery(this).find('span.BoutonDroite').addClass('BoutonDroiteOver');
			jQuery(this).find('span.BoutonMilieu').addClass('BoutonMilieuOver');
		},
		function() {
			jQuery(this).find('span.BoutonGauche').removeClass('BoutonGaucheOver');
			jQuery(this).find('span.BoutonDroite').removeClass('BoutonDroiteOver');
			jQuery(this).find('span.BoutonMilieu').removeClass('BoutonMilieuOver');
		});
		
		var langue = '<?php echo icl_t('Buttons', 'parcourir', utf8_encode('Joindre votre CV')); ?>';
		jQuery('span.TexteParcourir').prepend(langue);
		
		jQuery("div.FileSpecial p.Bouton").mouseover(function(){
			jQuery('div.FileSpecial p.Bouton span.BoutonMilieu').addClass('BoutonMilieuOver');
		}).mouseout(function(){
			jQuery('div.FileSpecial p.Bouton span.BoutonMilieu').removeClass('BoutonMilieuOver');
		});
	
		//Detecte la version et le browser puis ajoute la classe au body
		/*Browser = jQuery.browser.browser();
		Version = jQuery.browser.version.string();
		Version = Version.replace(/\./g,"-");		
		jQuery('body').addClass(Browser + '-' + Version);
		jQuery('body').addClass(Browser);*/
		
		//Detecte la langue actuelle puis ajoute la classe au body
		var langue = 'Language<?php echo ucfirst(icl_t('System', 'language_code', utf8_encode('fr'))); ?>';
		jQuery('body').addClass(langue);
	
		// Ajoute la classe "FirstListItem" aux premiers items des listes li
		jQuery('li:first-child').addClass('FirstListItem');	

		// Ajoute la classe "LastListItem" aux derniers items des listes li
		// jQuery('li:last-child').addClass('LastListItem');
		// 2011-03-10 NM : Modifie le selecteur precedent car selectionne tous les li qui sont
		// last-child de leur parent, ce qui n'est pas le cas lorsqu'un div clear est ajoute a la fin
		// d'une liste a l'interieur du ul. Parcours donc tous les ul, trouve les li, et assigne une class au dernier li
		jQuery('ul').each(function() {
			jQuery(this).find('li').last().addClass('LastListItem');
		});	
		
		jQuery('div#ListeItems').each(function() {
			jQuery(this).find('div.ListeItemWrap').last().addClass('LastListItem');
		});			
		
		// Utilisation spéciale blockquote pour exigences design
		jQuery('div#Content blockquote').each(function() {
			doc_link = jQuery(this).find('a').first().clone().empty();
			jQuery(this).append(jQuery('div.HiddenImage').html());
			if(doc_link) {
				jQuery(this).find('div.ImageCitation img').wrap(doc_link);
			}
		});
		
		//Va chercher le premier form pour les element "a" afin de declancher leur 'submit' (pour les boutons)
		//Ne pas mettre de id/name 'submit' pour eviter des problemes
		jQuery('a.SubmitSpecial').click(function(e) {
			e.preventDefault();  // Previent de monter au haut de page avec l'ancre...
			jQuery(this).closest('form').submit();
		});
		
		jQuery('a.FlecheSlide').click(function(e) {
			e.preventDefault();
			if(jQuery(this).hasClass('Open')) {
				jQuery(this).removeClass('Open');
				jQuery(this).parent().parent().find('div#CategoryListWrap, div.FormWrap, div.ContactForm7Wrap, div.Formulaire').slideUp();
			} else {
				jQuery(this).parent().parent().find('div#CategoryListWrap, div.FormWrap, div.ContactForm7Wrap, div.Formulaire').slideDown();
				jQuery(this).addClass('Open');
			}
		});
		
		//jQuery('#select').sb();
		
		// Smooth scroll
		jQuery("div.ScrollTop a").click(function(event){
			//prevent the default action for the click event
			event.preventDefault();

			//get the full url - like mysitecom/index.htm#home
			var full_url = this.href;

			//split the url by # and get the anchor target name - home in mysitecom/index.htm#home
			var parts = full_url.split("#");
			var trgt = parts[1];

			//get the top offset of the target anchor
			var target_offset = jQuery("#"+trgt).offset();
			if(target_offset != null) {  // Pour tenir compte de si l'element n'existe pas...
				var target_top = target_offset.top;

				//goto that anchor by setting the body scroll top to anchor top
				jQuery('html, body').animate({scrollTop:target_top}, 500);
			}
		});

		// Hover Menu
		jQuery('div#MenuHeader ul.TeamMenuPages li a').mouseover(function(){
			jQuery(this).stop().animate({backgroundPosition: '0px -80px'},750);
		});
		jQuery('div#MenuHeader ul.TeamMenuPages li a').mouseout(function(){
			jQuery(this).stop().animate({backgroundPosition: '0px 0px'},750);
		});
		
		jQuery('div#UpperMenuHeader ul.TeamMenuPages li, div#BottomMenuHeader ul.TeamMenuPages li, div#MenuMobileHeader ul li').hover(function(e){
			
			jQuery(this).children('ul').hoverFlow(e.type, {
				'height': 'show',
				'marginTop': 'show',
				'marginBottom': 'show',
				'paddingTop': 'show',
				'paddingBottom': 'show' 
			});
			jQuery(this).addClass('HoverMenu');
			
			}, function(e) {
		
			jQuery(this).children('ul').hoverFlow(e.type, {
				'height': 'hide',
				'marginTop': 'hide',
				'marginBottom': 'hide',
				'paddingTop': 'hide',
				'paddingBottom': 'hide' 
			});
			jQuery(this).removeClass('HoverMenu');

		 });	

		 
		var opened_sous_menu = false; 
		//Sous-Menu
		jQuery('div#BoutonMenuMobile, div#BoutonMenuMobileBlanc').click(function() {
		
			if (opened_sous_menu == false) {
				jQuery('div#MenuMobileHeader').slideDown(400, function() {
				
				});
				jQuery('div#LigneHeader').slideUp();
				opened_sous_menu = true;
			}
			else {
				jQuery('div#MenuMobileHeader').slideUp(400, function() {
					
				});
			jQuery('div#LigneHeader').slideDown();
				opened_sous_menu = false;
			}
		
			
		});

			
		
				
		jQuery( window ).resize(function() {
			w_width = jQuery(window).width();	

			if (w_width > 640) {	
				if (opened_sous_menu == true) {
					jQuery('div#MenuMobileHeader').slideUp(400, function() {
						
					});
				jQuery('div#LigneHeader').slideDown();
					opened_sous_menu = false;
				}
			}			
		});	
			
		//Watermark
		  jQuery.watermark.options = {
			  hideBeforeUnload: false
		   };
		 
		
		 jQuery('input#s').watermark('<?php echo team_the_wpml('System', 'recherche',false); ?>'); 
		 jQuery('input#courriel_infolettre').watermark('<?php echo team_the_wpml('System', 'exemple_courriel',false); ?>'); 
		 
	     jQuery('select').sb();
		 
	});

</script>
</head>

<body <?php echo (is_page(get_page_by_path('accueil')->ID) ? 'class="IsHome"' : ''); /*echo 'onunload=""';*/ ?>>

	<div id="MenuMobileHeader">
		<div id="SearchForm"><?php get_search_form(); ?></div><div id="BoutonMenuMobileBlancWrap"><div id="BoutonMenuMobileBlanc"></div></div>
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Widgets Menu Mobile') ) : endif; ?>
		<div id="SelecteurLangue"><?php team_header_langue(true,true); ?></div>
	</div>

	<div id="HeaderWrap">		
		<div id="LigneHeader"></div>
		<div id="Header">

			<div id="UpperHeader">
				<div id="Logo">
					<a title="Logo" href="<?php echo icl_get_home_url(); ?>"></a>
				</div>
				
				<div id="BoutonMenuMobile"></div>

				
				<div id="UpperMenuHeader">			
					<div id="SearchForm"><?php get_search_form(); ?></div>
					<div id="SelecteurLangue"><?php team_header_langue(); ?></div>
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Widgets Menu Header') ) : endif; ?>				
				</div>				
				<div class="clear"></div>				
			</div>

			<div id="BottomHeader">
				<div id="BottomMenuHeader">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Widgets Subheader') ) : endif; ?>
					
					<div id="MediaSociauxHeader">
						<a href="http://www.linkedin.com/company/expansion-pme" target="_blank" class="BoutonLinkedin"></a>
						<a href="http://www.youtube.com/user/ExpansionPME" target="_blank" class="BoutonYoutube"></a>
					</div>
					
				</div>
			</div>	
		</div><!-- header -->
	</div><!-- headerWrap -->
