<?php
/* ///////////////////////////////////////// */
/*	AJOUT/SUPPRESSION CHAMPS PARTICIPANTS	 */
/* ///////////////////////////////////////// */
	
	$action = mysql_real_escape_string($_POST['action']);	 	 	
	if (isset($_GET['refresh'])) {
		$refresh = mysql_real_escape_string($_GET['refresh']);	
	}
	else {
		$refresh = "";		
	}
	$output = '';
	
	//Ajouter des participants au formulaire d'inscription
	if ($action == 'ajout_participant') {
	
		$nb_participant = $_POST['nb_participant'];	
		$no_participant = $_POST['no_participant'];	 
		
		$output .= '<div class="NoDisplay Participant ParticipantBox" id="Participant_'.$no_participant.'">';

		$output .= '<h3>Participant '.$no_participant.'</h3>';		
		
		$output .= '<p class="Salutation">';
		$output .= '<input checked id="salutation_m" name="p_salutation_'.$no_participant.'" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_m',false).'" /><label for="salutation_m">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_m',false).'</label>';
		$output .= '<input id="salutation_mme" name="p_salutation_'.$no_participant.'" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mme',false).'" /><label for="salutation_mme">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mme',false).'</label>';
		//$output .= '<input id="salutation_mlle" name="p_salutation_'.$no_participant.'" type="radio" value="'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mlle',false).'" /><label for="salutation_mlle">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_salutation_mlle',false).'</label>';	
		$output .= '</p>';
		$output .= '<div class="clear"></div>';
		
		$output .= '<div class="ZoneChamps">';
		/*$output .= '<select name="p_salutation[]"><option selected value="M.">M.</option><option value="Mme.">Mme.</option></select>';*/
		//$output .= '<p><label for="r_nom">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_nom',false).'*</label><label for="r_fonction">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_fonction',false).'*</label><label for="r_courriel">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_courriel',false).'*</label>';
		//$output .= '<input type="text" class="Obligatoire" value="" id="r_nom" name="p_nom[] value="" /><input type="text" class="Obligatoire" id="r_fonction" value="" name="p_fonction[]" value="" /><input type="text" class="Obligatoire" id="r_courriel" value="" name="p_courriel[]" value="" /></p><div class="clear"></div>';
		
		$output .= '<div class="BoiteChampsParticipant"><p><label for="r_nom">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_nom',false).'*</label><input type="text" class="Obligatoire" value="" id="r_nom" name="p_nom[] value="" /></p></div>';
		$output .= '<div class="BoiteChampsParticipant"><p><label for="r_fonction">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_fonction',false).'*</label><input type="text" class="Obligatoire" id="r_fonction" value="" name="p_fonction[]" value="" /></p></div>';
		$output .= '<div class="BoiteChampsParticipant"><p><label for="r_courriel">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_courriel',false).'*</label><input type="text" class="Obligatoire" id="r_courriel" value="" name="p_courriel[]" value="" /></p></div>';
		$output .= '<div class="clear"></div>';
				
		
		
		$output .='</div>';
		
		$output .= '<p class="DInput"><label class="LabelTelephone" for="r_telephone">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_telephone',false).'*</label><label class="LabelPoste" for="r_poste">'.team_the_wpml('Contenu', 'contenu_formulaire_champs_poste',false).'</label><input class="Obligatoire CTelephone" type="text" id="r_telephone" value="" name="p_telephone[]" value=""><input class="CPoste" type="text" id="r_poste" value="" name="p_poste[]" value=""></p>';			

		$output .= '<input type="hidden" value="" name="p_responsable[]" value="0">';	
		$output .= genere_conference($_POST['post_id'],$no_participant);

		$output .= '<p class="Bouton BoutonSupression"><a class="SubmitSupressionParticipant"></a></p><div class="clear"></div>';	
		
		$output .= '</div>';
	}
	else if ($action == 'sauvegarde') { //Sauvegarde dans la BD des informations avant de procéder soit à paypal ou à l'envoi de courriels

		$nb_participant = $_POST['nb_participant'];	
	
		//Vérification inscription gratuite
		if ($_POST['type_paiement'] == '3' ) {
			//Toujours s'assurer que la demande de gratuité est fait avec un complet client
			if (is_user_logged_in() && current_user_can( 'client_user' ) ) {
				//Action spécial?	
			}
			else {
				return false; //Ne rien sauvegarde et sortir
			}
		}	
		

		

		//Empeche le nombre de participant de dépasser les conditions
		$max_participant = get_max_nb_participants_max($_POST['post_id']);
		if	($nb_participant > $max_participant)	{
			$tbl_prix = array();
			$tbl_prix['max_participant'] = $max_participant; 
			$tbl_prix['error'] = 'error'; 
			$output .= json_encode($tbl_prix); //Ne rien sauvegarde et sortir
		}		
		else {
			if ($refresh == 1) { //Demande de vérification de prix seulement
				$tbl_prix = calcul_prix(); //temporaire pour echo dans le data			
				
				$tbl_prix = format_prix($tbl_prix); //Formatte le prix selon la langue
				$output .= json_encode($tbl_prix);
				
			}
			else if ($refresh == 2) { //Vérifier le prix et enregistrer dans la BD

				  require_once('includes/recaptchalib.php');
				  $privatekey = "6LfuOfISAAAAAAP3YmpZEG-wORWniPLRTNDDZhVu";
				  $resp = recaptcha_check_answer ($privatekey,
												$_SERVER["REMOTE_ADDR"],
												$_POST["recaptcha_challenge_field"],
												$_POST["recaptcha_response_field"]);	
			
				if (!$resp->is_valid) {	
					$tbl_prix = array();
					//$tbl_prix['captcha'] = 'Le captcha inscrit est incorrect.'; 
					$tbl_prix['error_captcha'] = 'error';
					$output .= json_encode($tbl_prix); //Ne rien sauvegarde et sortir
				} else {

						
					$tbl_prix = calcul_prix();
					$tbl_prix['error'] = 'succes'; 	
			
					global $wpdb;

					//Sauvegarde dans la table wp_act_entreprises
					$data_entreprise = array('NEQ' 				=> '',
										'entreprise'		=> stripslashes_deep($_POST['entreprise']),
										'adresse'			=> stripslashes_deep($_POST['adresse']),
										'ville'				=> stripslashes_deep($_POST['ville']),
										'code_postal'		=> stripslashes_deep($_POST['code_postal']),
										'telephone'			=> stripslashes_deep($_POST['e_telephone']),
										//'telecopieur'		=> stripslashes_deep($_POST['e_telecopieur']),
										'telecopieur'		=> "",
										'secteur_activite'	=> stripslashes_deep($_POST['secteur_activite'])									
						);
					
					$wpdb->insert($wpdb->prefix.'act_entreprises', $data_entreprise,'%s');						
					$id_entreprise = mysql_insert_id();


					//date_default_timezone_set('America/Montreal');
					//$date_transaction = strtotime("now");		
					
					$total_taxe = $tbl_prix['total'] - $tbl_prix['sous_total'];
					//Sauvegarde dans la table wp_act_transactions
					$data_type = array('%f','%f','%f','%d','%d','%s','%d','%s','%s');
					$data_transaction = array('sous_total' 		=> $tbl_prix['sous_total'],
											'taxes' 			=> $total_taxe,
											'total'				=> $tbl_prix['total'],
											'status'			=> stripslashes_deep($_POST['status']),
											'type_inscrit'		=> stripslashes_deep($_POST['type_inscrit']),
											'langue'			=> stripslashes_deep($_POST['langue']),
											'facturation'		=> stripslashes_deep($_POST['facturation']),
											'commentaires'		=> stripslashes_deep($_POST['commentaires']),
											'date_transaction'	=> current_time('mysql')	
						);
					$wpdb->insert($wpdb->prefix.'act_transactions', $data_transaction,$data_type);
					$id_transaction = mysql_insert_id();
				
					//Sauvegarde dans la table wp_act_participants
					$jj = 0;
					$pp = 0;
					
					for ($ii=0; $ii<count($_POST['p_nom']); $ii++)
					{

						$flag_salutation = false;

						//On sauvegarde les conférences pour chaque participant
						while ($flag_salutation == false) {			
							if(!empty($_POST['p_salutation_'.$pp])) {

								$salutation_p = $_POST['p_salutation_'.$pp];
								$flag_salutation = true;
							}
							$pp++;
						}			
					
						$data_type = array('%d','%s','%s','%s','%s','%s','%d');
						$data_participant = array('entreprise_id' 	=> $id_entreprise,
												'salutation' 	=> stripslashes_deep($salutation_p),
												'nom'			=> stripslashes_deep($_POST['p_nom'][$ii]),
												'telephone'		=> stripslashes_deep($_POST['p_telephone'][$ii] . ' poste : ' .$_POST['p_poste'][$ii]),
												'courriel'		=> stripslashes_deep($_POST['p_courriel'][$ii]),
												'fonction'		=> stripslashes_deep($_POST['p_fonction'][$ii]),
												'responsable'	=> stripslashes_deep($_POST['p_responsable'][$ii])					
							);						
						$wpdb->insert($wpdb->prefix.'act_participants', $data_participant,$data_type);
						
						$id_participant = mysql_insert_id();
						$flag_conference = false;

						//On sauvegarde les conférences pour chaque participant
						if (($_POST['p_responsable'][$ii] == 1 && $_POST['participation_responsable'] == 1) || $_POST['p_responsable'][$ii] == 0) {
							while ($flag_conference == false) {			
								if(!empty($_POST['choix_conference_'.$jj])) {
									foreach($_POST['choix_conference_'.$jj] as $une_conference) {

										//Sauvegarde dans la table wp_act_reservations
										$data_type = array('%d','%s','%d','%d');
										$data_transaction = array('post_id' 		=> stripslashes_deep($_POST['post_id']),
																'meta_key' 			=> $une_conference,
																'participant_id'	=> $id_participant,	
																'transaction_id'	=> $id_transaction	
											);
										$wpdb->insert($wpdb->prefix.'act_reservations', $data_transaction,$data_type);											
									}							
									$flag_conference = true;
								}
								$jj++;
							}
						}	
					}			
						
					//Cas facture ou gratuit, on envoi aussi des courriels
					if ($_POST['type_paiement'] == '2') { 
						courriel_paypal($id_transaction,'Completed',$tbl_prix['total']);
						/*if ($_POST['facturation'] == 1) {
							courriel_facturation($id_transaction,$_POST['total']);
						}	*/			
								
					}
					else if ($_POST['type_paiement'] == '3' ) {
						//Toujours s'assurer que la demande de gratuité est fait avec un complet client
						if (is_user_logged_in() && current_user_can( 'client_user' ) || ($tbl_prix['total'] == 0) ) {
							courriel_paypal($id_transaction,$tbl_prix['total']);	
						}
					}
					
					$tbl_prix = format_prix($tbl_prix); //Formatte le prix en fonction de la langue
					
					$tbl_prix['id_transaction'] = $id_transaction; 
					
					//$output = $id_entreprise; //Puisque la transaction sera créée en même temps que l'entreprise			
					$output .= json_encode($tbl_prix);
					
				
				}	
			}		
		}
	}	
	echo $output;
	
	//Calcul le meilleur prix selon les conférences et/ou activités choisies par les participants
	function calcul_prix() {
	
		//$_POST['type_inscrit']
		//$nb_participant_total = $_POST['nb_participant'];
		//current_time( 'timestamp' )

		$participants = array();
		$conditions_prix = array();
		$ii = 0;
		$jj = 0;
		$kk = 0;
		
		//print_r($_POST);
		//die();
		
		// Remplir le tableau de toutes les conférences
		// 500 itérations max (prévient boucle infinie au cas)
		while($jj < 100 || ($ii < count($_POST['p_nom']) - (($_POST['participation_responsable']) ? 0 : 1))) {
		//for ($ii=0; $ii<count($_POST['p_nom']); $ii++) {
			$flag_conference = false;
			//On regarde les conferences pour chaque participant
			if(isset($_POST['id_participant_'.$jj])) {
				if(isset($_POST['choix_conference_'.$jj])) {
					$participants[$ii] = array();
					$participants[$ii]['prix_min'] = array();

					while ($flag_conference == false) {	
						$kk = 0;
						foreach($_POST['choix_conference_'.$jj] as $une_conference) {			
							$participants[$ii]['conferences'][$kk]['ID'] = $une_conference;
							$participants[$ii]['conferences'][$kk]['prix_min'] = 0;
							$kk++;
						}							
						$flag_conference = true;
					}	
				}
				$ii++;
			}
			$jj++;
		}
		
		// Convertit l'ID de type inscrit en slug (comme dans les conditions).
		$type_inscrit = '';
		if($_POST['type_inscrit']) {
			$type_inscrit = get_post($_POST['type_inscrit']);
			if($type_inscrit) {
				$type_inscrit = $type_inscrit->post_name;
			}
		}
			
		//Chercher les conditions
		$conditions_prix = get_conditions($_POST['post_id']);
		foreach ($conditions_prix as $id_condition_prix) {
			$obj_condition = new conditionPrix();
			$obj_condition->load_condition_prix($id_condition_prix->ID);
			
			$obj_condition->valider_condition_prix(time(), $type_inscrit, $participants);
		}
		
		
		// Calcule le prix pour chaque participant.
		// Si un prix de participant est présent, prend celui-ci, sinon calcule le prix individuel par conférence.
		$prix_taxes_total = array();
		foreach($participants as $participant) {
			if(!empty($participant['prix_min'])) {
				$prix_taxes_total = ajouter_prix_taxes($prix_taxes_total, $participant['prix_min']);
				
			} else {
				$total_conferences = array();
				foreach($participant['conferences'] as $conference) {
					if(!empty($conference['prix_min'])) {
						$total_conferences = ajouter_prix_taxes($total_conferences, $conference['prix_min']);
					} else {
						// Ici: pas supposé... Toutes les conférences devraient avoir un prix individuel si au moins 1 prix individuel est défini.
					}
				}
				$prix_taxes_total = ajouter_prix_taxes($prix_taxes_total, $total_conferences);
			}
		}

		/* *** NM DEBUG *** */
		/*
		echo "Calcul des taxes : \r\n";
		print_r($prix_taxes_total);

				echo "Structure de participants : \r\n";
		print_r($participants);
		*/
			
			
		//La fonction devrait retourner un array contenant (sous-total, total et taxes) pour pouvoir faire l'insertion dans la table de transaction
		//print_r($arr_conditions[0]->get_condition_prix());

		//print_r($prix_taxes_total);

		//return $arr_conditions[0]->get_condition_prix();
		
		//Retourne le calcul du prix
		return $prix_taxes_total;
	}
	
	//Parse le prix en fonction de la langue!
	function format_prix($prix_taxes_total) {

		// Si prix nul = 0.00, ou prix pas trouvé, array vide.
		if(empty($prix_taxes_total)) {
			$prix_taxes_total = calculer_taxes(0, false);
		}
	
		if ($_POST['langue'] == 'fr') {
			$prix_taxes_total['total'] = number_format($prix_taxes_total['total'],2,',',' ');
			$prix_taxes_total['sous_total'] = number_format($prix_taxes_total['sous_total'],2,',',' ');

			if ($prix_taxes_total['taxes'][0]->nom == 'TVH') {
				$prix_taxes_total['taxes'][0]->valeur = number_format($prix_taxes_total['taxes'][0]->valeur,2,',',' ');
			}
			else {
				$prix_taxes_total['taxes'][0]->valeur = number_format($prix_taxes_total['taxes'][0]->valeur,2,',',' ');
				$prix_taxes_total['taxes'][1]->valeur = number_format($prix_taxes_total['taxes'][1]->valeur,2,',',' ');				
			}
			
		}
		else {
			$prix_taxes_total['total'] = number_format($prix_taxes_total['total'],2,'.',',');
			$prix_taxes_total['sous_total'] = number_format($prix_taxes_total['sous_total'],2,'.',',');	
			
			if ($prix_taxes_total['taxes'][0]->nom == 'TVH') {
				$prix_taxes_total['taxes'][0]->valeur = number_format($prix_taxes_total['taxes'][0]->valeur,2,'.',',');
			}
			else {
				$prix_taxes_total['taxes'][0]->valeur = number_format($prix_taxes_total['taxes'][0]->valeur,2,'.',',');
				$prix_taxes_total['taxes'][1]->valeur = number_format($prix_taxes_total['taxes'][1]->valeur,2,'.',',');				
			}
							
		}	
		
		return $prix_taxes_total;
	}
	
	function get_max_nb_participants_max($post_id) {
		// Fonction qui retourne le maximum de participants pour savoir s'il est possible de s'inscrire ou non 

		$conditions_prix = get_conditions($post_id);
		$high_max_participant = 0;
		
		foreach ($conditions_prix as $condition_prix) {
			$obj_condition = new conditionPrix();
			$obj_condition->load_condition_prix($condition_prix->ID);
			
			$liste_condition = $obj_condition->get_condition_prix();
			$max_participant = $liste_condition['nb_participants_max'];
			
			
			if ($max_participant > $high_max_participant) {
				$high_max_participant = $max_participant;
			}
		}		
		
		return $high_max_participant;
	}
	
?>