<?php
	//Fonction pour convertir code hexad�cimal en RGB
	function &hex2rgb($hex)
	{
		if (0 === strpos($hex, '#')) {
		$hex = substr($hex, 1);
		} else if (0 === strpos($hex, '&H')) {
		$hex = substr($hex, 2);
		}

		$cutpoint = ceil(strlen($hex) / 2)-1;
		$rgb = explode(':', wordwrap($hex, $cutpoint, ':', $cutpoint), 3);

		$rgb[0] = (isset($rgb[0])? hexdec($rgb[0]) : 0);
		$rgb[1] = (isset($rgb[1])? hexdec($rgb[1]) : 0);
		$rgb[2] = (isset($rgb[2])? hexdec($rgb[2]) : 0);

		return $rgb;
	} 

	if(isset($_GET['bgcolorhex'])) {
		$bg_color_hex = $_GET['bgcolorhex'];
	}
	
	//Pour afficher le logo dans le header
	if (!@getimagesize("logo.png")) {		
		if (!@getimagesize("logo.jpg")) {			
			if (!@getimagesize("logo.gif")) {
				echo 'Aucun logo trouv� / No Logo found';
			}
			else {
				//On travaille un gif						
				$logo_image = imagecreatefromgif('logo.gif');
				header('Content-Type: image/gif');
				imagegif($logo_image);		
				imagedestroy($logo_image_gif);	
			}
		}
		else {
				//On travaille un jpg
				$logo_image = imagecreatefromjpeg('logo.jpg');
				header('Content-Type: image/jpeg');
				imagegif($logo_image);					
		}
	}
	else {
		//On travaille un png
		$logo_image = imagecreatefrompng('logo.png');
		$color = $bg_color_hex;
		list($sx, $sy) = getimagesize('logo.png');

		//colored background image
		$logo_image_gif = imagecreatetruecolor($sx,$sy);
		list($R, $G, $B)= (hex2rgb($color));
		$mycolor = ImageColorAllocate($logo_image_gif, $R,$G,$B);
		imagefill($logo_image_gif, 0, 0, $mycolor);

		//the 24bit png with an alpha channel
		imagealphablending($logo_image, true);
		imagesavealpha($logo_image, false);

		imagecopy($logo_image_gif, $logo_image, 0, 0, 0, 0, $sx, $sy);
		
		header('Content-Type: image/gif');
		imagegif($logo_image_gif);		
		imagedestroy($logo_image_gif);		
	}
	
	imagedestroy($logo_image);
	

?>