<?php
	global $root_page_name;
	global $is_activites;
?>
<div id="Sidebar">

	<?php // Recherche ?>
	<div class="InputBlock">
		<div class="BlockTitle"><?php team_the_wpml('Recherche', 'search'); ?></div>
		<div class="FormWrap">
			<form role="search" method="get" id="SearchInPostType" action="<?php team_the_permalink($root_page_name); ?>" >
				<div>
					<input type="text" value="<?php echo esc_attr(apply_filters('the_search_query', trim($_GET['ss']))); ?>" name="ss" id="ss" /><?php // ss : sub search ?>
					<a class="SubmitSpecial" href="#"></a>
				</div>
			</form>
		</div>
	</div>
	
	<?php // Type d'activité ?>
	<div id="CategoryBlock">
		<div class="BlockTitle"><?php
			if($is_activites) {
				team_the_wpml('Activites', 'event_type');
			}	else { // Blogue
				team_the_wpml('Actualites', 'news_type');
			}
			
		?>
			<a href="#" class="FlecheSlide"></a>		
		</div>
		<div id="CategoryListWrap">
			<ul>

				<?php
					if($is_activites) {
						$current_taxonomy = 'types-activites';
					} else {
						$current_taxonomy = 'types-actualites';
					}
				
					$args = array('hide_empty' => false);
					$all_terms = get_terms($current_taxonomy, $args);
					
					$output = '';
					if($is_activites) {
						$output .= '<li '. ((empty($term)) ? 'class="SelectedItem"' : '') .'><a href="'.team_the_permalink($root_page_name,false).'">'.team_the_wpml('Activites', 'all_events',false).'</a></li>';
					}
					else {
						$output .= '<li '. ((empty($term)) ? 'class="SelectedItem"' : '') .'><a href="'.team_the_permalink($root_page_name,false).'">'.team_the_wpml('Actualites', 'all_news',false).'</a></li>';
					}
					foreach($all_terms as $current_term) {
						$output .= '<li ' . (($term == $current_term->slug) ? 'class="SelectedItem"' : '') . '><a href="' . get_term_link($current_term, $current_taxonomy) . '">' . ((!empty($current_term->description)) ? $current_term->description : $current_term->name) . '</a></li>';
					}
					
					echo $output;
				?>
			</ul>
		</div>
	</div>
	
	<?php // Archives ?>
	<div id="ArchivesBlock" class="InputBlock">
		<div class="BlockTitle"><?php team_the_wpml('Activites', 'archives'); ?><a href="#" class="FlecheSlide"></a></div>
		<div class="FormWrap">
			<form action="<?php team_the_permalink($root_page_name); ?>" method="get">
				<?php	
				if($is_activites) {
					team_months_dropdown('les-activites');
				} else {
					team_months_dropdown('les-nouvelles');
				}
				?>
			</form>
			<script type="text/javascript">
				// Lorsque jQuery est pret
				jQuery(document).ready(function() {
					jQuery('div#ArchivesBlock select').change(function() {
						jQuery(this).closest('form').submit();
					});
				});
			</script>
		</div>
	</div>
	
</div>