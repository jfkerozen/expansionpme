<?php
/*
Template Name: Emplois deux pages
*/
	get_header(); 
?>

	<div id="ContentWrap">
		<div id="Content">	
			<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			
				<?php if (!have_posts()) :?>
					<?php while (have_posts()) : the_post(); ?>

						<?php 
							echo '<h1>'. apply_filters('the_title',get_the_title()) .'</h1>';
							the_content(); 
						?>
						
						<div id="ContainerTitreCV">						
							<h2 class="NoDisplay"><?php echo icl_t('Careers', 'career_cv', utf8_encode('Banque de CV')); ?></h2>					
							<div id="ContainerBanqueCV">	
								<?php
											
									if(team_language() == 'fr') {
										echo do_shortcode('[contact-form 3 "Emploi Fr"]');
									} else {
										echo do_shortcode('[contact-form 4 "Emploi En"]');
									}

								?>
							</div>
						</div>

								
				
					<?php endwhile; ?>
				<?php endif; ?>			
				
				<h1><?php echo icl_t('Titles', 'available_positions', utf8_encode('Postes disponibles')); ?></h1>	
				<?php			
					global $wp_query;

					$args = array(
						'showposts'     => -1,
						'orderby'         => 'menu_order',
						'order'           => 'ASC',
						'post_type'       => 'les-emplois'
					);
				
					$postsquery = new WP_Query($args);
					$postslist = $postsquery->posts;

					$output = '';				
				
					
					if(count($postslist)) {
						$output .= '<ul>';
						foreach ($postslist as $un_emploi) {
							$output .= '<li><a title="'. get_permalink($un_emploi->ID) .'" href="'. get_permalink($un_emploi->ID) .'">' . apply_filters('the_title',$un_emploi->post_title) . '</a></li>';
						}	
						$output .= '</ul>';
					} 
					else {				
						$output .= '<p>' . icl_t('Messages', 'no_position_available', utf8_encode('Il n\'y a aucun poste disponible pour l\'instant.')) . '</p>';					
					}
					
					echo $output;
				?>

			</div> <!-- Template -->
		</div><!-- Content -->
	</div><!-- ContentWrap -->

	<script type="text/javascript">
		// Lorsque jQuery est pret, active les effets custom
		jQuery(document).ready(function() {

			// Rempli le lien emploi vs. formulaire
			jQuery('div.HiddenInput input').map(function(){
				var jobTitle = jQuery(this).parent().parent().parent().parent().parent().parent().parent().find('h2').html();
				jobTitle = jobTitle.replace(/(&nbsp;)/g, ' ');
				jQuery(this).val(jobTitle);
			});
			
		});
	</script>
<?php get_footer(); ?>