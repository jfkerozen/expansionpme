<?php
/*
Template Name: Popup IE6
*/
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php echo utf8_encode('<meta name="Author" content="Team �quipe de cr�ation. http://www.equipeteam.com/">'); ?>
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/../../../wp-includes/js/jquery/jquery.js"></script>
<style type="text/css">

body {
	background-color: white;
}

.Message {
	width: 768px;
	margin: auto;		
}

.ImageIE6 {
	border: none;
	
}

.LogoSite {
	border: none;
}

.Antiiepopup {
	margin: auto;
	width: 320px;
}

</style>

<script type="text/javascript">
jQuery(document).ready(function() {
	var Maheight = jQuery(window).height(); // returns height of browser viewport
	var Z = (Maheight - 584)/2;
	jQuery('div.Antiiepopup').css('margin-top', Z);
});
</script>

</head>
<body>
	<?php
		wp_head();
	if (have_posts()) {
		the_post();
		the_content();
	}
	
	$langue = icl_t('System', 'language_code', utf8_encode('fr'));
	($langue == 'fr') ? $lienchrome='http://www.google.com/chrome?hl=fr' : $lienchrome='http://www.google.com/chrome?hl=en';
	($langue == 'fr') ? $lienff='http://www.mozilla.com/fr/firefox/' : $lienff='http://www.mozilla.com/en-US/firefox/';
	($langue == 'fr') ? $liensafari='http://support.apple.com/kb/DL1531?viewlocale=fr_FR' : $liensafari='http://support.apple.com/kb/dl1531';
	($langue == 'fr') ? $lienie='http://www.microsoft.com/canada/fr/windows/internet-explorer/default.aspx' : $lienie='http://www.microsoft.com/windows/internet-explorer/default.aspx';
	//Image du logo dynamique
	echo '<div class="Antiiepopup"><img class="LogoSite" id="ImageLogo" usemap="#Map" src="'. get_bloginfo('template_directory') .'/images/noie6/logo.png" /></div>'; 
	//Image IE6 avec liens vers navigateurs
	if ($langue == 'fr') {
		echo '<table class="Message"><tr valign="center"><td><img class="ImageIE6" id="ImageIE6" usemap="#Map" src="'. get_bloginfo('template_directory') .'/images/noie6/ie6_page_info.gif" /></td></tr></table>'; 
	}
	else {
		echo '<table class="Message"><tr><td><img class="ImageIE6" id="ImageIE6" usemap="#Map" src="'. get_bloginfo('template_directory') .'/images/noie6/ie6_page_infoen.gif" valign="middle"/></td></tr></table>'; 
	}
	?>

	<MAP Name="Map">
		<area shape="rect" href="<?php echo $lienchrome ?>" coords="90,375,178,459">
		<area shape="rect" href="<?php echo $lienff ?>" coords="278,374,338,459">
		<area shape="rect" href="<?php echo $liensafari ?>" coords="445,372,500,459">
		<area shape="rect" href="<?php echo $lienie ?>" coords="610,374,669,459">
	</MAP>
	<?php
		wp_footer();
	?>
</body>
</html>