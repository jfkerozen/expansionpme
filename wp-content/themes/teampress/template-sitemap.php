<?php
/*
Template Name: Plan de site
*/
	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
				<?php if (have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>

						<?php 
							echo '<h1>'. apply_filters('the_title',get_the_title()) .'</h1>';	
							the_content(); 
						?>
						<?php //echo do_shortcode('[team_plan_site]'); ?>
						<div id="PlanSite">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Widgets Plan site') ) : endif; ?>
						</div>
				
				<?php endwhile; ?>
			<?php endif; ?>	
			
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
