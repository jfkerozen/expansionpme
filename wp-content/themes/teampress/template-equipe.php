<?php
/*
Template Name: Équipe
*/

	$posts_per_page = 6;

	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<div id="PreContent">
				<div id="LeftPreContent">

					<?php $current_page = get_post($post->ID);
						
						$output = '';
						$output .= apply_filters('the_content', $current_page->post_content);
						
						echo $output;
					?>
				</div>
				<div id="RightPreContent">
					<div class="MoreInformation"><?php team_the_wpml('Contact', 'more_information'); ?></div>
					<?php
						$contactez_nous = team_get_page('contactez-nous');
					?>
					<div class="PageButton"><a href="<?php echo get_permalink($contactez_nous->ID); ?>"><?php echo apply_filters('the_title', $contactez_nous->post_title); ?></a></div>
				</div>
				<div class="clear"></div>
			</div>
				<?php
				$args = array(
												'post_type' => 'les-membres',
												'orderby'       => 'menu_order',
												'order'         => 'ASC',
												'posts_per_page' => -1,
												'tax_query' => array(
													array(
														'taxonomy' => 'types-equipes',
														'field' => 'slug',
														'terms' => $current_page->post_name
													)
												)
											);
				
				query_posts($args);
			?>
			<hr class="SeparateurPlein" />
			<div id="FullContent">
				<?php if (have_posts()) :?>
					<div id="ListeItems" class="ListeEquipe">
					<?php while (have_posts()) : the_post();
						$terms_post = wp_get_post_terms($post->ID, 'champs-expertises');
					?>
						<div class="ListeItemWrap">
							<div class="ListeItem" id="membre-<?php echo $post->post_name; ?>">
								<?php
									if(!empty($terms_post)) {
										$image_url = get_taxonomy_image_url($terms_post[0]->term_id);
										$output = '<div class="CategoriePost">' . $terms_post[0]->name . team_the_thumb(false,false,$image_url[0],'',18,18,1,1,'','png') . '</div><div class="clear"></div>';
										echo $output;
									}
								?>
								<div class="ItemImage"><?php	
									if(get_post_meta($post->ID, 'team_thumbnail_image0', true)) {
										team_the_thumb(true, false,'','team_thumbnail_image0',285,176,1,1,$post->ID,'jpeg&q=100');
									} else { ?>
										<img src="<?php bloginfo('template_directory'); ?>/images/default_membre.png" />
									<?php	}	?>
								</div>
								<div class="ItemContenu">
									<h2><?php the_title(); ?></h2>
									<div class="Titres"><div class="TitreMembre Titre1Membre"><?php echo get_post_meta($post->ID, 'Titre', true); ?></div><div class="TitreMembre Titre2Membre"><?php echo get_post_meta($post->ID, 'TitreComplementaire', true); ?></div></div>
									<?php if(get_post_meta($post->ID, 'Courriel', true)) { ?>
										<div class="CourrielMembre"><a href="mailto:<?php echo get_post_meta($post->ID, 'Courriel', true); ?>"><?php echo get_post_meta($post->ID, 'Courriel', true); ?></a></div>
									<?php } ?>
									<div class="Telephones"><p class="TelephoneMembre Telephone1Membre"><?php echo get_post_meta($post->ID, 'Telephone1', true); ?></p><p class="TelephoneMembre Telephone2Membre"><?php echo get_post_meta($post->ID, 'Telephone2', true); ?></p></div>
									<?php the_content(); ?>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
