<?php
	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-single-nouvelle">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<?php if (have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>
					<div id="RightContent">
						<div class="BlogPostWrap">
							<h1><?php the_title(); ?></h1>
							<div class="PostDate"><?php echo get_the_date(); ?> par <span><?php echo get_the_author(); ?></span></div>
							<div class="ShareSocial">
								<a class="ShareFacebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>"></a>
								<a class="ShareGooglePlus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>"></a>
								<a class="ShareLinkedIn" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&summary=<?php echo urlencode(get_the_title()); ?>&source=<?php echo urlencode(site_url()); ?>"></a>
								<div class="clear"></div>
							</div>
							
							<?php the_content(); ?>
						
							<?php
								$previous_title = team_the_wpml('Pagination', 'previous', false);
								$next_title = team_the_wpml('Pagination', 'next', false);
								
								$terms_post = wp_get_post_terms($post->ID, 'types-actualites');
								
								$previous_post = get_adjacent_post(true, '', true, 'types-actualites');
								$next_post = get_adjacent_post(true, '', false, 'types-actualites');
								
								if(!empty($terms_post)) {
									if(team_language() == 'fr') {
										$previous_title = $terms_post[0]->name . ' ' . $previous_title;
										$next_title = $terms_post[0]->name . ' ' . $next_title;
									} else {
										$previous_title = $previous_title . ' ' . $terms_post[0]->name;
										$next_title = $next_title . ' ' . $terms_post[0]->name;
									}
								
									$previous_link = '';
									if($previous_post) {
										$previous_link = '<a href="' . get_permalink($previous_post->ID) . '">' . $previous_title . '</a>';
									}
									
									$next_link = '';
									if($next_post) {
										$next_link = '<a href="' . get_permalink($next_post->ID) . '">' . $next_title . '</a>';
									}
							?>
							<div class="TermNavigation">
								<div class="PreviousLink"><?php echo $previous_link; ?></div>
								<div class="NextLink"><?php echo $next_link; ?></div>
								<div class="clear"></div>
							</div>
						</div>
						<?php	} ?>
						<div class="SendEmail SendAction"><a class="EmailThisPage" href="#"><?php team_the_wpml('Messages', 'send_email'); ?></a></div>
						<div class="SendPrinter SendAction"><a class="PrintThisPage" href="#"><?php team_the_wpml('Messages', 'print'); ?></a></div>
					</div>
					
					<div id="LeftContent">
						<?php if(get_post_meta($post->ID, 'team_thumbnail_image0', true)) { ?>
							<div class="ItemImage"><?php team_the_thumb(true, false,'','team_thumbnail_image0',254,264,1,1,$post->ID,'jpeg&q=100') ?></div>
						<?php } ?>
						<?php if(($attached_post = get_post_meta($post->ID, 'FichierPDF', true)) && get_post_meta($post->ID, 'TitreFichier', true)) {
										$upload_dir = wp_upload_dir();
										$attached_file = $upload_dir['baseurl'] . '/' . get_post_meta($attached_post, '_wp_attached_file', true);
						?>
							<div class="DownloadFile"><a href="<?php echo $attached_file; ?>" target="_blank"><?php echo get_post_meta($post->ID, 'TitreFichier', true); ?></a></div>
						<?php } ?>
						<div class="MoreInformationWrap">
							<div class="MoreInformation"><?php team_the_wpml('Contact', 'more_information'); ?></div>
							<?php
								$contactez_nous = team_get_page('contactez-nous');
							?>
							<div class="PageButton"><a href="<?php echo get_permalink($contactez_nous->ID); ?>"><?php echo apply_filters('the_title', $contactez_nous->post_title); ?></a></div>
							<div class="clear"></div>
						</div>
						
						<div class="SendEmail SendAction"><a class="EmailThisPage" href="#"><?php team_the_wpml('Messages', 'send_email'); ?></a></div>						
						<div class="ShareSocial">
							<a class="ShareFacebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>"></a>
							<a class="ShareGooglePlus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>"></a>
							<a class="ShareLinkedIn" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&summary=<?php echo urlencode(get_the_title()); ?>&source=<?php echo urlencode(site_url()); ?>"></a>
							<div class="clear"></div>
						</div>
						
					</div>
					
					<div class="clear"></div>
					<div class="RetourBlogue"><a href="<?php team_the_permalink('blogue'); ?>"><?php team_the_wpml('Messages', 'back_blog'); ?></a></div>
				<?php endwhile; ?>
			<?php endif; ?>
			
			<?php //get_sidebar(); ?>
			
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
