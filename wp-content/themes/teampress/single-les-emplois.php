<?php 
	get_header(); 
	the_post();
?>

<div id="ContentWrap">
	<div id="Content">	
		<div id="Template" class="template-single-emplois">	

			<h1><?php the_title(); ?></h1>
			<h2><?php echo icl_t('Buttons', 'apply', utf8_encode('Postuler')); ?></h2>
			<div class="clear"></div>
			<?php
			
				if(team_language() == 'fr') {
					$output .= do_shortcode('[contact-form 3 "Emploi Fr"]');
				} else {
					$output .= do_shortcode('[contact-form 4 "Emploi En"]');
				}
				
				echo $output;

			?>
				
		</div <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

	<script type="text/javascript">
		// Lorsque jQuery est pret, active les effets custom
		jQuery(document).ready(function() {

			// Rempli le lien emploi vs. formulaire
			jQuery('div.HiddenInput span input').map(function(){
				var jobTitle = jQuery(this).parent().parent().parent().parent().parent().parent().find('h1').html();
				jobTitle = jobTitle.replace(/(&nbsp;)/g, ' ');
				jQuery(this).val(jobTitle);
			});
			
		});
	</script>
<?php get_footer(); ?>



























<?php
	$LaCategorie = "";
	
	if(in_category('equipe-fr') || in_category('equipe-en'))
	{
		$LaCategorie = "agents";
	
	} elseif(in_category('nouvelles') || in_category('news')) {
		$LaCategorie = "nouvelles";
	}
	

	switch ($LaCategorie)
	{
		case "agents":	
			include ("template-equipe.php");
			break;
			
		case "nouvelles":
			include ("template-nouvelles.php"); 
			break;
			
		default:
			//include ("single_default.php");
			break;
	}
?><?php
/*
<div id="content">
	
		<div id="LesPosts">
			<?php if (have_posts()) :?>
				<?php $postCount=0; ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php $postCount++;?>

			<div class="LePost entry-<?php echo $postCount ;?>">
				
				<div class="entrytitle">
					<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
				</div>
				<div class="entrybody">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
				
			</div>
			
			<?php endwhile; ?>
			<?php
				if(function_exists('wp_paginate')) {
					wp_paginate();
				}
			?>
				
			<?php else : ?>

				<?php $postCount++;?>
				<div class="LePost entry-<?php echo $postCount ;?>">
					<div class="Square"></div>
					
					<div class="entrytitle">
						<h2><?php echo icl_t('System', 'not_found', 'Page non-trouv&eacute;e'); ?></h2>
					</div>
					<div class="entrybody">
						<?php useful404s(); ?>
					</div>
				</div>

			<?php endif; ?>
		</div> <!-- LesPosts -->

		<?php get_sidebar('right'); ?>

</div><!-- content -->
*/?>