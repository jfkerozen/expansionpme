<?php
	include_once 'includes/shortcodes.php';
	include_once 'includes/sidebars.php';
	include_once 'includes/team-textfit.php';
	include_once 'includes/breadcrumbs.php';
	include_once 'includes/trim-text.php';
	include_once 'includes/anti-orphelins.php';
	include_once 'includes/filter-orderby.php';
	include_once 'includes/GetPDFByCategory.php';
	include_once 'includes/get-post-by-title.php';
	include_once 'includes/get-post-by-slug.php';
	include_once 'includes/team-metabox.php';
	include_once 'includes/team-activitesbox.php';
	include_once 'functions-team.php';
	include_once 'includes/menu-activites.php';	//Fonctions de g�n�ration Excel activit�s
	include_once 'includes/ipn-activites.php'; //Fonctions de courriel et IPN activit�s
	include_once('includes/form-activites.php'); //Fonctions du formulaire d'activit�	

	// Fonction retournant true si on est dans la page blogue fr/en
	// ou si on est dans une page de taxonomie associ�e au blogue
	function is_blogue() {
		global $taxonomy;
		
		if(team_is_page('blogue')) {
			return true;
			
		} elseif(is_tax() && $taxonomy == 'types-actualites') {
			return true;
		
		} else {
			return false;
		}
	}
	
	// Fonction retournant true si on est dans la page activites fr/en
	// ou si on est dans une page de taxonomie associ�e aux activites
	function is_activites() {
		global $taxonomy;

		if(team_is_page('activites')) {
			return true;
			
		} elseif(is_tax() && $taxonomy == 'types-activites') {
			return true;
		
		} else {
			return false;
		}
	}
	
	function team_months_dropdown( $post_type ) {
		global $wpdb, $wp_locale;

		$months = $wpdb->get_results( $wpdb->prepare( "
			SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
			FROM $wpdb->posts
			WHERE post_type = %s
			ORDER BY post_date DESC
		", $post_type ) );

		$month_count = count( $months );

		if ( !$month_count || ( 1 == $month_count && 0 == $months[0]->month ) )
			return;

		$ym = isset( $_GET['ym'] ) ? (int) $_GET['ym'] : 0;
?>
		<select name='ym'>
			<option<?php selected( $ym, 0 ); ?> value='0'><?php team_the_wpml('Activites', 'select_month'); ?></option>
<?php
		foreach ( $months as $arc_row ) {
			if ( 0 == $arc_row->year )
				continue;

			$month = zeroise( $arc_row->month, 2 );
			$year = $arc_row->year;

			printf( "<option %s value='%s'>%s</option>\n",
				selected( $ym, $year . $month, false ),
				esc_attr( $arc_row->year . $month ),
				/* translators: 1: month name, 2: 4-digit year */
				sprintf( __( '%1$s %2$d' ), ucfirst($wp_locale->get_month( $month )), $year )
			);
		}
?>
		</select>
<?php
	}	
	
	function get_posts_of($post_count, $posts_per_page, $paged) {
		$premier_article = ($paged - 1) * $posts_per_page + 1;
		$premier_article = ($post_count == 0) ? 0 : $premier_article;
		$dernier_article = $paged * $posts_per_page;
		$dernier_article = ($dernier_article < $post_count) ? $dernier_article : $post_count;
	
		return $premier_article . ' - ' . $dernier_article . ' ' . team_the_wpml('Pagination', 'of', false) . ' ' . $post_count;
	}
	
	
	// 6 articles par page pour les activites ou les actualit�s (blogue)
	function change_posts_per_page($query) {
		if($query->is_main_query() && ($query->is_tax && ($query->tax_query->queries[0]['taxonomy'] == 'types-activites' || $query->tax_query->queries[0]['taxonomy'] == 'types-actualites')) || $query->is_search) {
				$query->set('posts_per_page', 6);
		}
		return $query;
	}
	add_action('pre_get_posts', 'change_posts_per_page');
	
	function filter_posts_per_month($query) {
		if(($query->query_vars['post_type'] == 'les-activites' || $query->query_vars['post_type'] == 'les-nouvelles') && isset($_GET['ym']) && !empty($_GET['ym'])) {
			$ym = $_GET['ym'];
		
			/*$year = substr($ym, 0, 4);
			$month = substr($ym, 4, 2);
			$query->set('year', $year);
			$query->set('monthnum', $month);*/
			
			$query->set('m', $ym);
		}
		return $query;
	}
	add_action('pre_get_posts', 'filter_posts_per_month');
	
	function filter_posts_per_search($query) {
		if(($query->query_vars['post_type'] == 'les-activites' || $query->query_vars['post_type'] == 'les-nouvelles' ) && isset($_GET['ss']) && !empty($_GET['ss'])) {
			$ss = trim($_GET['ss']);
			
			$query->set('s', $ss);
		}
		return $query;
	}
	add_action('pre_get_posts', 'filter_posts_per_search');	
	
	//Fonction qui renvoi l'Url d'une image du plugin de taxonomy-image
	if (!function_exists('get_taxonomy_image_url')) {
		function get_taxonomy_image_url($term_id) {
		
			$id_image = '';
			$associations = taxonomy_image_plugin_sanitize_associations( get_option( 'taxonomy_image_plugin' ) );
			if ( array_key_exists( $term_id, $associations ) ) {
				$id_image = absint( $associations[$term_id] );
			}							
			$image_url = wp_get_attachment_image_src( $id_image, 'full' );	
			
			return $image_url;
		}	
	}
	
	
	//Trouve l'ID d'un meta avec sa cl� et sa valeur
	function get_mid_by_key( $post_id, $meta_key, $meta_value ) {
		 global $wpdb;
		 $mid = $wpdb->get_var( $wpdb->prepare("SELECT meta_id FROM
	$wpdb->postmeta WHERE post_id = %d AND meta_key = %s AND meta_value = %s", $post_id,
	$meta_key,$meta_value) );
		 if( $mid != '' )
			 return (int)$mid;

		 return false;
	}
	
	
	// 2013-06-20 NM: Corrige un bug avec la validation w3c feed: wfw:commentRss must be a full and valid URL.
	function modify_post_comments_feed_link($url) {
		return get_bloginfo('wpurl');
	}	
	add_filter('post_comments_feed_link', 'modify_post_comments_feed_link', 100);
	
	// 2013-06-20 NM: Enl�ve la mention Alkivia Framework du plugin capability manager.
	remove_action('wp_head', '_ak_framework_meta_tags');	
	
	function DisplaySiteMapURL() {
		echo "Sitemap: ".team_get_page('xml-sitemap') ."\r\n";
	}
	add_action('do_robotstxt','DisplaySiteMapURL');	
	
	
	// Extend the hierarchy NM 2013-02-14
	function add_posttype_slug_template( $templates = array())
	{
		$object = get_queried_object();

		// New
			$new_templates[] = "single-{$object->post_type}-{$object->post_name}.php";
		// Like in core
			$new_templates[] = "single-{$object->post_type}.php";
			$new_templates[] = "single.php";

		return locate_template( $new_templates );    
	}
	// Now we add the filter to the appropriate hook
	function intercept_template_hierarchy()
	{
		add_filter( 'single_template', 'add_posttype_slug_template', 10, 1 );
	}
	add_action( 'template_redirect', 'intercept_template_hierarchy', 20 );

	
	//Fonction pour trouver le nom du fichier template en cours
	function template_name($file) {
		$template_data = implode( '', file( $file ));

		$template_name = '';
		if ( preg_match( '|Template Name:(.*)$|mi', $template_data, $template_name ) ) {
			$template_name = _cleanup_header_comment($template_name[1]);
		}
		
		$template_name = sanitize_title($template_name);

	return $template_name;
	}
	
	//Fonction STRPOS, mais pour toutes les $needle possible dans le $haystack
	function strallpos($haystack,$needle,$offset = 0){
		$result = array();
		for($i = $offset; $i<strlen($haystack); $i++){
			$pos = strpos($haystack,$needle,$i);
			if($pos !== FALSE){
				$offset =  $pos;
				if($offset >= $i){
					$i = $offset;
					$result[] = $offset;
				}
			}
		}
		return $result;
	} 
	
	// Fonction retournant le slug du term racine (root term),
	// � partir du post si c'est un post, ou du term courant si la page est taxonomy
	// NOTE: TAXONOMIE HARDCOD�E POUR POST: � AJUSTER � CHAQUE FOIS OU � AM�LIORER �VENTUELLEMENT
	function GetRootTermSlug() {

		global $term;
		global $taxonomy;
		global $post;

		if(is_tax()) {
			$term_slug = $term;
			$term_taxonomy = $taxonomy;
			
		} elseif(is_single()) {
			/*$terms = wp_get_object_terms($post->ID, 'wpsc_product_category');
			if($terms) {
				$term_slug = $terms[0]->slug;
				$term_taxonomy = $terms[0]->taxonomy;
			}*/
			
		}
	
		// Seulement si term et taxonomie trouv�es
		if($term_slug && $term_taxonomy) {
			$term_obj = get_term_by('slug', $term_slug, $term_taxonomy);
		}

		if($term_obj) {
			// Remonte jusqu'� la racine (parent = 0)
			while($term_obj->parent != 0) {
				$term_obj = get_term($term_obj->parent, $term_taxonomy);
			}

			// Retourne le slug du dernier term trouv�
			return $term_obj->slug;
		
		} else {
			return '';
		}
	}
	
	//Pour faire une redirection si l'usager � IE6 vers la page NoIE6
	if(!function_exists('no_IE6')) {
		function no_IE6() {	
			if((substr($_SERVER['HTTP_USER_AGENT'],0,34)=="Mozilla/4.0 (compatible; MSIE 6.0;") && (!team_is_page('noie6'))) {	
				$lienIE = team_the_permalink('noie6',false);		
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $lienIE);	
				exit();
			}
			
		}
	}
	add_action('template_redirect','no_IE6');
	
	
	//pour taxonomies dans les get_posts, ne pas oublie :
	/*  $postsquery = new WP_Query($args);
        $postslist = $postsquery->posts;
        foreach ($postslist as $post) :  setup_postdata($post);
	*/
	// USAGE: dans args() : 'tax_terms' => 'taxonomy1:term1,taxonomy2:term2'
	function tax_terms_where($where,$wp_query) {
      if (isset($wp_query->query)) {
        $query = $wp_query->query;
        if (is_string($query))
          parse_str($query,$query);
        if (is_array($query) && isset($query['tax_terms'])) {
          global $wpdb;
          $tax_terms = explode(',',$query['tax_terms']);
          foreach($tax_terms as $tax_term) {
            list($taxonomy,$term) = explode(':',$tax_term);
            $sql = "
    AND $wpdb->posts.ID IN (
      SELECT tr.object_id
      FROM $wpdb->term_relationships AS tr
      INNER JOIN $wpdb->term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
      INNER JOIN $wpdb->terms AS t ON tt.term_id = t.term_id
      WHERE tt.taxonomy='%s' AND t.slug='%s'
    )";

            $where .= $wpdb->prepare($sql,$taxonomy,$term);
          }
        }
      }
      return $where;
    }
    add_action('posts_where','tax_terms_where',10,2);
	

//Fonction qui permet de trier par cat�gorie de taxonomie les posts d'un custom post type	
add_action( 'restrict_manage_posts', 'my_restrict_manage_posts' );
function my_restrict_manage_posts() {

    // only display these taxonomy filters on desired custom post_type listings
    global $typenow;
	
	$args=array(
	  'public'   => true,
	  '_builtin' => false
	); 	
	$custom_postypes = get_post_types($args);	
	
	$ii  = 0;
	$ll = 0;
	foreach ($custom_postypes as $post_type) { //Aller chercher tous les customs post type cr��s par Team
		$post_types[$ii] = $post_type;
		$ii++;			
	}
	
	while ($ll <= $ii) {
		if ($typenow == $post_types[$ll]) { //S'assurer d'avoir le bon post type sur la page que l'on est actuellement		
			
			$args=array(
			  'public'   => true,
			  '_builtin' => false
			  
			); 
		
			$filters  = get_taxonomies($args); //Aller chercher toutes les taxonomies cr��es par Team	
			// create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list

			foreach ($filters as $tax_slug) {
			
				// retrieve the taxonomy object
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				// retrieve array of term objects per taxonomy
				$terms = get_terms($tax_slug);
				
				if (($tax_obj->object_type[0]) == $typenow ) { //Bonne taxonomie associ� au bon post
					// output html for taxonomy dropdown filter
					echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
					echo "<option value=''>Show All $tax_name</option>";
					foreach ($terms as $term) {
						// output each select option line, check against the last $_GET to show the current option selected
						echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
					}
					echo "</select>";
					
					/*wp_dropdown_categories(array(
					'show_option_all' =>  __("Show All"),
					'taxonomy'        =>  $tax_slug,
					'name'            =>  'Categories',
					'id'			  =>  $tax_slug,
					'orderby'         =>  'name',
					'selected'        =>  $_GET[$tax_slug],
					'hierarchical'    =>  true,
					'depth'           =>  3,
					'show_count'      =>  true, // Show # listings in parens
					'hide_empty'      =>  true, // Don't show businesses w/o listings
					));*/
				}

			}
		}
		$ll++;
	}
}

//Fonction qui permet de retourner la string WPML 
if (!function_exists('team_the_wpml')) {
	function team_the_wpml($context, $name, $echo = true) {
		
		global $wpdb;
		$row = $wpdb->get_row("SELECT value FROM wp_icl_strings WHERE context = '$context' AND name = '$name'");

		if($row){
			if($echo === true){
				echo icl_t($context, $name, stripslashes($row->value));
			}else{
				return icl_t($context, $name, stripslashes($row->value));
			}
		// Si aucun resultat affiche message erreur
		}else{
			$erreur = 'Erreur, cette chaine existe pas!';
			if($echo === true){
				echo $erreur;
			}else{
				return $erreur;
			}
		}
	}	
}

// Fonction qui recheche le premier h1 et y ajoute un lien vers la page en cours
// 2010-11-12 NM
if(!function_exists('h1_link')) {
   function h1_link($text) {
				   global $post;

				   // Charge le contenu dans le parser DOM pour le travailler
				   $dom = new DOMDocument();

				   $dom->formatOutput = false;
				   $dom->preserveWhiteSpace = true;
				   
				   // Load seulement si texte existant (�vite erreur)
				   if($text) {
								   // utf8_decode : r�sout probl�mes de charset
								   $dom->loadHTML(utf8_decode($text));
				   }
				   
				   $h1Elements = $dom->getElementsByTagName('h1');
				   // Ins�re le link dans le premier h1 si au moins 1 h1 trouv�
				   if(count($h1Elements)) {
								   // Seulement si h1 a du contenu
								   if($h1Elements->item(0)->nodeValue) {
												  // Cr�e l'�l�ment et assigne le lien
												  $linkElement = $dom->createElement('a');
												  $linkElement->setAttribute('href', get_permalink($post->ID));
												  $linkElement->setAttribute('alt', $h1Elements->item(0)->nodeValue);
												  
												  $children = $h1Elements->item(0)->childNodes;
												  $count = $children->length;
												  while($ii < $count) {
																  $linkElement->appendChild($children->item(0));
																  $ii++;
												  }
												  
												  // D�truit le contenu de la node existante avant d'ajouter celle avec le lien
												  $h1Elements->item(0)->nodeValue = '';
												  $h1Elements->item(0)->appendChild($linkElement);
								   }
				   }
				   
				   return preg_replace(array("/<!DOCTYPE [^>]+>/", "/<body>/", "/<\/body>/", "/<html>/", "/<\/html>/"), "", $dom->saveHTML());
   }
  //add_filter('the_content', 'h1_link', 20);  // Last priority
  // add_filter('the_title', 'h1_link', 20);  // Last priority
}     

	/*///////////////////////////*/
	/* FONCTION ADMIN ACTIV�E    */
	/*///////////////////////////*/

	//Enl�ve les champs personalis�s dans les pages et posts
	function remove_field_column($posts_columns) {
			// Delete an existing column
		unset($posts_columns['custom-fields']);

		return $posts_columns;
	}
	add_filter('manage_edit-page_columns', 'remove_field_column');
	add_filter('manage_edit-post_columns', 'remove_field_column');

	//Enleve le tag cloud dans les taxonomies
	function remove_tag_cloud($content) {
		return;
	}
	add_filter('wp_tag_cloud','remove_tag_cloud');
	 
	//Enleve flux RSS
	function remove_comments_rss( $for_comments ) {
		return;
	}
	add_filter('post_comments_feed_link','remove_comments_rss');
		
	//Enleve le quick edit	  
	function remove_quick_edit(){
		?>
		<style type="text/css">
		#the-list .hide-if-no-js,a.editinline{display:none;}
		</style>
		<?php
	}
	add_action('admin_print_styles-edit.php', 'remove_quick_edit');  
	
	//Icone de custom post type
	function cpt_icons() {
	   ?>
	    <style type="text/css" media="screen">
	        #menu-posts-les-nouvelles .wp-menu-image {
            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_nouvelles.png) no-repeat 2px 6px !important;
	        }
			
	        #menu-posts-les-emplois .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_emplois.png) no-repeat 2px 6px  !important;
	        }
			
			#menu-posts-les-realisations .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_realisations.png) no-repeat 2px 6px !important;
	        }
			
			#menu-posts-les-sliders .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_sliders.png) no-repeat 2px 6px !important;
	        }
			
			#menu-posts-ajax .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_ajaxs.png) no-repeat 2px 6px !important;
	        }
			
			#menu-posts-les-partenaires .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/images/icones/icone_partenaires.png) no-repeat 2px 6px !important;
	        }
			
	        #menu-posts-les-nouvelles:hover .wp-menu-image, #menu-posts-les-nouvelles.wp-has-current-submenu .wp-menu-image,
			#menu-posts-les-emplois:hover .wp-menu-image, #menu-posts-les-emplois.wp-has-current-submenu .wp-menu-image,
			#menu-posts-les-realisations:hover .wp-menu-image, #menu-posts-les-realisations.wp-has-current-submenu .wp-menu-image,
		    #menu-posts-ajax:hover .wp-menu-image, #menu-posts-ajax.wp-has-current-submenu .wp-menu-image,
			 #menu-posts-les-partenaires:hover .wp-menu-image, #menu-posts-les-partenaires.wp-has-current-submenu .wp-menu-image,
	        #menu-posts-les-sliders:hover .wp-menu-image, #menu-posts-les-sliders.wp-has-current-submenu .wp-menu-image {
	            background-position: 2px -20px !important;
	        }
	    </style>
	<?php }
	add_action( 'admin_head', 'cpt_icons' );

	// From : http://feedproxy.google.com/~r/nettuts/~3/mzMTAvljPtw/
	// Clean up wp_head
	// Remove Really simple discovery link
	remove_action('wp_head', 'rsd_link');
	// Remove Windows Live Writer link
	remove_action('wp_head', 'wlwmanifest_link');
	// Remove the version number
	remove_action('wp_head', 'wp_generator');	
		
	remove_action( 'wp_head',             'feed_links',                    2     );
	remove_action( 'wp_head',             'feed_links_extra',              3     );
	remove_action( 'wp_head',             'rsd_link'                             );
	remove_action( 'wp_head',             'wlwmanifest_link'                     );
	remove_action( 'wp_head',             'index_rel_link'                       );
	remove_action( 'wp_head',             'parent_post_rel_link',          10, 0 );
	remove_action( 'wp_head',             'start_post_rel_link',           10, 0 );

	// Active les shortcodes dans les widgets
	// Aussi widget2 pour 1 widget sp�cial
	add_filter('widget_title', 'do_shortcode', 1); 
	add_filter('widget_title2', 'do_shortcode'); 
	add_filter('widget_text', 'do_shortcode'); 
	add_filter('post_photo', 'do_shortcode'); 
	
	//Ajout les extraits de page aux pages
	add_post_type_support('page', 'excerpt');

	//2011-02-15 P.C : disable les alertes updates de Wordpress
	//add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	//add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
	//add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
	//show_admin_bar(false);

	
	//Enleve custom post metabox
	function remove_post_custom_fields() {
		remove_meta_box( 'postcustom' , 'page' , 'normal' ); 
	}
	 if(!current_user_can('administrator')) {
		add_action( 'admin_menu' , 'remove_post_custom_fields' );
	}
	
	//Enleve le Logo Wordpress
	function admin_bar_remove() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');
	}
	add_action('wp_before_admin_bar_render', 'admin_bar_remove', 0);	
	
    function gkp_remove_dashboard_widgets() {
		global $wp_meta_boxes;
		 
		// Tableau de bord g�n�ral
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // Presse-Minute
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Commentaires r�cents
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // Extensions
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // Liens entrant
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // Billets en brouillon
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // Blogs WordPress
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // Autres actualit�s WordPress
    }
	add_action('wp_dashboard_setup', 'gkp_remove_dashboard_widgets' );

	/*///////////////////////////*/
	/* FONCTION ADMIN D�SACTIV�E */
	/*///////////////////////////*/
	
	// Function that outputs the contents of the dashboard widget
	function dashboard_widget_function() {
		echo 'Pour mieux comprendre le fonctionnement de la partie administrative de votre site Internet, n\'h&eacutesitez pas &agrave; consulter ce <a target="_blank" href="../wp-content/uploads/WordPress_STD_guide.pdf">PDF</a>.';
	}

	// Function used in the action hook
	function add_dashboard_widgets() {
		wp_add_dashboard_widget('dashboard_widget', 'PDF Explicatif - Partie administrative', 'dashboard_widget_function');
	}
	//add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

	//Ajouter un twitter feed au dashboard avec le plugin Twitter Widget Pro
	function twitterfeed_dashboard_widget_function() {
		// Display whatever it is you want to show
		echo do_shortcode('[twitter-widget username="TEAMEquipe" before_widget="<div class=\'dashtwitter-box\'>" after_widget="</div>" before_title="<h1>" after_title="</h1>" errmsg="Twitter n\'est pas disponible pour l\'instant!" showintents="false" hiderss="true" hidereplies="true" targetBlank="true" avatar="1" showXavisysLink="0" items="3" showts="60"]TEAM marketing &bull; web &bull; design[/twitter-widget]');
	} 

	function twitterfeed_add_dashboard_widgets() {
		wp_add_dashboard_widget('twitterfeed_dashboard_widget', 'TEAM marketing &bull; web &bull; design Twitter Feed', 'twitterfeed_dashboard_widget_function');	
	} 
	//add_action('wp_dashboard_setup', 'twitterfeed_add_dashboard_widgets' );	

	function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo_wordpress.png) !important; background-size: 320px 199px!important; height: 130px!important; }
    </style>';
	}
	//add_action('login_head', 'my_custom_login_logo');
		
	function w4_login_headerurl(){
		return 'http://www.equipeteam.com/';
	}
	//add_filter( 'login_headerurl', 'w4_login_headerurl');
	
	function w4_login_headertitle(){
		return 'TEAM marketing &bull; web &bull; design';
	}
	//add_filter( 'login_headertitle', 'w4_login_headertitle');
	
	function dashboard_footer_team () {
		echo '<a href="http://www.equipeteam.com/" target="_blank">Merci de faire confiance &agrave; l\'&eacute;quipe TEAM marketing &bull; web &bull; design</a>';
	}
	//add_filter('admin_footer_text', 'dashboard_footer_team');	
	
?>