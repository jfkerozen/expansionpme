<?php
	/*
	Template Name: Pre-diagnostic
	*/
	get_header();
?>

<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<?php if (have_posts()) : ?>			
				<?php while (have_posts()) : the_post(); ?>
				
					<?php 				

						the_content();
						
						//echo do_shortcode('[mappress mapid="1"]');

						echo '<div class="BoutonFormulaire">'. team_the_wpml('Contenu', 'contenu_formulaire_prediag',false) .'<a href="#" class="FlecheSlide"></a></div>';
						echo '<div class="ContactForm7Wrap">';
						if (team_is_page('outils/pre-diagnostic-exportation')) {
							if(team_language() == 'fr') {
								echo do_shortcode('[contact-form-7 id="1714" title="Pre-diagnostic exportation"]');
							} else {
								echo do_shortcode('[contact-form 2 "Contact En"]');
							}						
						}
						else if (team_is_page('outils/pre-diagnostic-innovation')) {

							if(team_language() == 'fr') {
								echo do_shortcode('[contact-form-7 id="1717" title="Pre-diagnostic Innovation"]');
							} else {
								echo do_shortcode('[contact-form 2 "Contact En"]');
							}						
						}
						echo '</div>';
					

					?>

					
				<?php endwhile; ?>			
			<?php endif; ?>
			
		</div><!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->

<script type="text/javascript">
	// Lorsque jQuery est pret, active les effets custom
	jQuery(document).ready(function() {		

	});
</script>
<?php
	get_footer(); 
?>