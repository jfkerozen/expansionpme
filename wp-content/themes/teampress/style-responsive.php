<?php 
	header("Content-type: text/css");
	
	$noir 			   = "#000000";
	$blanc			   = "#ffffff";
	$turquoise		 = "#37b9c6";
	$vert_fonce		 = "#7f9400";
	$vert_pale		 = "#a7bc29";
	$vert_hover    = "#4e5b00";
	$gris_fonce		 = "#231f20";
	$gris_moyen		 = "#6e6e6e";
	$gris_pale		 = "#727272";
	$gris_hover		 = "#545454";
	$gris_footer	 = "#aaaaaa";
	$gris_bordure  = "#afafaf";
	$gris_fond_bte = "#ebebeb";
	$orange			 	 = "#eea139";
	$brun			  	 = "#998677";
	$rouge				 = "#ac1627";

	//$site_url 		= "http://localhost/expansionpme.org";
	//$site_url 		= "http://www.votrenouveausite.ca/expansionpme.org";
	
	$template_url = 'http://' . $_SERVER['SERVER_NAME'] . pathinfo($_SERVER['PHP_SELF']);
	$template_url = $template_url['dirname'];
	
	
?>

/*FONT-FACE*/

@font-face {
    font-family: 'OpenSansRegular';
    src: url('fonts/opensans-regular-webfont.eot');
    src: url('fonts/opensans-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-regular-webfont.woff') format('woff'),
         url('fonts/opensans-regular-webfont.ttf') format('truetype'),
         url('fonts/opensans-regular-webfont.svg#open_sansregular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansItalic';
    src: url('fonts/opensans-italic-webfont.eot');
    src: url('fonts/opensans-italic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-italic-webfont.woff') format('woff'),
         url('fonts/opensans-italic-webfont.ttf') format('truetype'),
         url('fonts/opensans-italic-webfont.svg#open_sansitalic') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansBold';
    src: url('fonts/opensans-bold-webfont.eot');
    src: url('fonts/opensans-bold-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-bold-webfont.woff') format('woff'),
         url('fonts/opensans-bold-webfont.ttf') format('truetype'),
         url('fonts/opensans-bold-webfont.svg#open_sansbold') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansBoldItalic';
    src: url('fonts/opensans-bolditalic-webfont.eot');
    src: url('fonts/opensans-bolditalic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-bolditalic-webfont.woff') format('woff'),
         url('fonts/opensans-bolditalic-webfont.ttf') format('truetype'),
         url('fonts/opensans-bolditalic-webfont.svg#open_sansbold_italic') format('svg');
    font-weight: normal;
    font-style: normal;
}


@font-face {
    font-family: 'OpenSansSemiBold';
    src: url('fonts/opensans-semibold-webfont.eot');
    src: url('fonts/opensans-semibold-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-semibold-webfont.woff') format('woff'),
         url('fonts/opensans-semibold-webfont.ttf') format('truetype'),
         url('fonts/opensans-semibold-webfont.svg#open_sanssemibold') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'OpenSansSemiBoldItalic';
    src: url('fonts/opensans-semibolditalic-webfont.eot');
    src: url('fonts/opensans-semibolditalic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-semibolditalic-webfont.woff') format('woff'),
         url('fonts/opensans-semibolditalic-webfont.ttf') format('truetype'),
         url('fonts/opensans-semibolditalic-webfont.svg#open_sanssemibold_italic') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansExtraBold';
    src: url('fonts/opensans-extrabold-webfont.eot');
    src: url('fonts/opensans-extrabold-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-extrabold-webfont.woff') format('woff'),
         url('fonts/opensans-extrabold-webfont.ttf') format('truetype'),
         url('fonts/opensans-extrabold-webfont.svg#open_sansextrabold') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansExtraBoldItalic';
    src: url('fonts/opensans-extrabolditalic-webfont.eot');
    src: url('fonts/opensans-extrabolditalic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-extrabolditalic-webfont.woff') format('woff'),
         url('fonts/opensans-extrabolditalic-webfont.ttf') format('truetype'),
         url('fonts/opensans-extrabolditalic-webfont.svg#open_sansextrabold_italic') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansLight';
    src: url('fonts/opensans-light-webfont.eot');
    src: url('fonts/opensans-light-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-light-webfont.woff') format('woff'),
         url('fonts/opensans-light-webfont.ttf') format('truetype'),
         url('fonts/opensans-light-webfont.svg#open_sanslight') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'OpenSansLightItalic';
    src: url('fonts/opensans-lightitalic-webfont.eot');
    src: url('fonts/opensans-lightitalic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/opensans-lightitalic-webfont.woff') format('woff'),
         url('fonts/opensans-lightitalic-webfont.ttf') format('truetype'),
         url('fonts/opensans-lightitalic-webfont.svg#open_sanslight_italic') format('svg');
    font-weight: normal;
    font-style: normal;
}


@font-face {
    font-family: 'CabinRegular';
    src: url('fonts/cabin-regular-webfont.eot');
    src: url('fonts/cabin-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/cabin-regular-webfont.woff') format('woff'),
         url('fonts/cabin-regular-webfont.ttf') format('truetype'),
         url('fonts/cabin-regular-webfont.svg#cabinregular') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'CabinItalic';
    src: url('fonts/cabin-italic-webfont.eot');
    src: url('fonts/cabin-italic-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/cabin-italic-webfont.woff') format('woff'),
         url('fonts/cabin-italic-webfont.ttf') format('truetype'),
         url('fonts/cabin-italic-webfont.svg#cabinitalic') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'CabinBold';
    src: url('fonts/cabin-bold-webfont.eot');
    src: url('fonts/cabin-bold-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/cabin-bold-webfont.woff') format('woff'),
         url('fonts/cabin-bold-webfont.ttf') format('truetype'),
         url('fonts/cabin-bold-webfont.svg#cabinbold') format('svg');
    font-weight: normal;
    font-style: normal;

}

div#FontLoad {
	font-family: 'CabinRegular';
	float: left;
	display: none;
}

/* Reset pour utilisation avec font-face */
h1, h2, h3, h4, h5, h6 {
	font-weight: normal!important;
	text-shadow: none;
}

strong {
	font-weight: normal!important;
}

/*****************************************/
/* CONTAINERS							 */
/*****************************************/

 html {
	width: 100%;
	height: 100%;
 }
 
 body{	
	color:#000;
	font-family: Arial, Helvetica, Sans-serif;
	margin:0;
	padding:0;
	width: 100%;
	height: 100%;
	/*min-width: 960px;*/
	min-width: 320px;
	-webkit-text-size-adjust: none;
	background: url('./images/fond_header.jpg') no-repeat center top #fff;
}

sup, sub {
	height: 0;
	line-height: 1;
	vertical-align: middle;
	_vertical-align: middle;
	position: relative;
}

sup {
	bottom: 1ex;
	font-size: 8px;
}

sub {
	top: .5ex;
}

li {
	zoom: 1;
	list-style: none;
}

a {
	color: <?php echo $noir; ?>;
	text-decoration: none;
}

a:hover{
	color: <?php echo $noir; ?>;
	text-decoration: underline;
}

img {
	max-width: 100%;
	height: auto;
}

table {
	margin-bottom: 24px;
}

input[type=text] {
    -webkit-appearance: none;
    border-radius: 0;
	border: 1px solid <?php echo $gris_footer; ?>;
	height: 38px;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 38px;
	color: <?php echo $gris_moyen; ?>;
	padding: 0px 6px;
}

textarea {
    -webkit-appearance: none;
    border-radius: 0;
	border: 1px solid <?php echo $gris_footer; ?>;
	height: 120px;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 24px;
	color: <?php echo $gris_moyen; ?>;
	padding: 6px;
	margin-top: 3px;
}

hr {
	border: none;
	height: 2px;
	background-color: <?php echo $gris_bordure; ?>;
	margin-top: 0;
	margin-bottom: 24px;
}

object embed, embed {
	margin-bottom: 24px;
}

span.LeftRoundedBorder{
	background: url("images/left_input.png") no-repeat scroll left top transparent;
	float: left;
	display: block;
	height: 35px;
	padding-left: 6px;
	margin-bottom: 12px;
}

span.RightRoundedBorder{
	background: url("images/right_input.png") no-repeat scroll right top transparent;
	float: left;
	display: block;
	height: 35px;
	padding-right: 6px;
}

span.LeftRoundedBorder span.RightRoundedBorder input {
	background: url("images/middle_input.png") repeat-x scroll 0 0 transparent;
	display: block;
	height: 35px;
	border: none!important;
	color: <?php echo $noir; ?>;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 18px;
	#margin-top: -1px!important;
	#padding-top: 6px;
	margin: 0;
	width: 100%;
	-webkit-border-radius: 0;
}

table textarea{
	border: none!important;
	background-color: #e8ebed;
	color: <?php echo $noir; ?>!important;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 18px;
	width: 100%!important;
	margin: 0;
	#margin-top: -1px!important;
	#padding-top: 6px;
	#margin-bottom: -1px!important;
	border: none!important;
	border-color: tansparent!important; 
	overflow: auto;
	-webkit-border-radius: 0;
}

.NoDisplay {
	display: none;
}

.NoVisibility {
	visibility: hidden;
}

/*****************************************/
/* HEADER								 */
/*****************************************/

div#HeaderWrap{
	width: 100%;
	position: relative;
	/*overflow: hidden;*/
	z-index: 500;		
}

div#FondHeader {
	background: url('./images/fond_header.jpg') no-repeat center top transparent;
	/*width: 1920px;*/
	height: 757px;
	width: 100%;
	/*height: 100%;*/
	position: absolute;
	/*margin-left: -960px;
	left: 50%;*/
	left: 0px;
	top: 0px;
	z-index: 100;
}

div#HeaderWrap div#LigneHeader {	
	background-color: <?php echo $gris_moyen; ?>;
	width: 100%;
	height: 18px;
	position: relative;
	z-index: 200;		
}

div#Header {
	width: 100%;
	max-width: 100%;
	margin: 0 auto;
	position: relative;
	z-index: 500;	
}

div#Header div#UpperHeader {
	 margin: 0 auto;
	 max-width:960px;
	 width:100%;
}

div#Header div#UpperHeader div#Logo {
	float: left;
	max-width: 264px;	
	width: 27.78947%; /* 264/950 */
	margin-top: 20px;
}

div#Header div#UpperHeader  div#Logo a {
	background: url('./images/logo-expension-pme.png') no-repeat scroll left top transparent;
	background-size: contain!important;	
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
	src='<?php echo $template_url; ?>/images/logo.png',
	sizingMethod='scale');			
	width: 100%; 
	height: 0px;
	padding-bottom: 25.37878%; /* 67/264 */
	display: block;
}

/* Upper Menu */

div#Header div#UpperMenuHeader {
	float: right;
	width: 72.21052%;
	max-width: 686px;
	margin-top: 40px;
}

div#Header div#UpperMenuHeader div.widget_teammenupages {
	float: right;
}

div#Header div#UpperMenuHeader div.widget_teammenupages li {
	position: relative;
}

div#Header div#UpperMenuHeader ul li a.PrincipalMenu, div#Header div#UpperMenuHeader div#SelecteurLangue  a {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 35px;
	color: <?php echo $gris_fonce; ?>;
	text-transform: uppercase;
	padding-right: 12px;
	padding-left: 12px;
}

div#Header div#UpperMenuHeader ul li a.PrincipalMenu:hover, div#Header div#UpperMenuHeader ul li.SelectedMenu a, div#Header div#UpperMenuHeader div#SelecteurLangue  a:hover {
	color: <?php echo $vert_fonce; ?>;
	text-decoration: none;
}

div#Header div#UpperMenuHeader ul li div.FlecheMenu {
	background: url('./images/fleche_upper_header.png') no-repeat scroll left top transparent;
	position: absolute;
	right: 0px;
	top: 17px;
	width: 8px;
	height: 4px;
}

div#Header div#UpperMenuHeader ul li a.PrincipalMenu:hover + div.FlecheMenu {
	background-position: 0px -5px;
}

div#Header ul.TeamMenuPages li a {
	display: block;
}

/* Upper Sous-menu */

div#Header div#UpperMenuHeader ul.SousPages {
	background: url('./images/bande_sousmenu_principal.png') no-repeat right top <?php echo $gris_hover; ?>;
	width: 239px;
	margin: 0;
	padding: 12px 0 24px 0;
	display: none;
	position: absolute;
	z-index: 10;
}

div#Header div#UpperMenuHeader ul.SousPages li {
	font-size: 14px;
	line-height: 24px;
}

div#Header div#UpperMenuHeader ul.SousPages li a {
	display: block;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 24px;
	color: <?php echo $blanc; ?>;	
	padding: 6px 24px;
	
}

div#Header div#UpperMenuHeader ul.SousPages li a:hover {
	color: <?php echo $vert_pale; ?>;	
	text-decoration: none;
}

div#Header div#UpperMenuHeader ul.SousPages li.LastListItem {
	border-bottom: none;
}

/* Langue */

div#Header div#UpperMenuHeader div#SelecteurLangue {
	float: right;
	margin-right: 12px;
}

div#Header div#UpperMenuHeader div#SelecteurLangue a  {
	border-left: 1px solid <?php echo $gris_fonce; ?>;
}

/* Searchform */

div#Header div#UpperMenuHeader div#SearchForm {
	float: right;
}

div#Header div#UpperMenuHeader div#SearchForm input#s {
	float: left;
	border: none;
	width: 178px;
	padding: 0px 6px;
	height: 35px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 35px;
	color: <?php echo $gris_moyen; ?>;
	border:1px solid <?php echo $gris_moyen; ?>;
}

div#Header div#UpperMenuHeader div#SearchForm a {
	background: url('./images/bouton_recherche.png') no-repeat scroll left top transparent;
	display: block;
	width: 40px;
	height: 35px;
	float: left;	
}

div#Header div#UpperMenuHeader div#SearchForm a:hover {
	background-position: 0px -36px;
}

/* MEDIA QUERIES UPPER HEADER */

@media screen and (min-width: 641px) and (max-width: 949px) {

	div#UpperHeader {
		width: 98%;
		margin: 0 auto;
	}
	
	div#Header div#UpperHeader {
		margin-bottom: 3.15789%; /* 30/950 */
	}

	div#Header div#UpperMenuHeader ul li a.PrincipalMenu, div#Header div#UpperMenuHeader div#SelecteurLangue a {
		font-size: 11px;
		padding-left: 6px;
	}
	
	div#Header div#UpperMenuHeader div#SelecteurLangue {
		margin-right: 1.74927%; /* 12/686 */
	}
	
	div#Header div#UpperMenuHeader div#SelecteurLangue a {
		padding-right: 0px;
	}
	
	div#Header div#UpperMenuHeader div#SearchForm {
		width: 32.79883%;
	}
	
	div#Header div#UpperMenuHeader div#SearchForm input#s {
		width: 67%;
	}
	
	div#Header div#UpperMenuHeader div#SearchForm a {
		width: 24%;
	}

}

@media screen and (min-width: 320px) and (max-width: 640px) {

	body {
		background-image: none;
	}


	div#HeaderWrap div#LigneHeader {
		height: 12px;
	}
	
	div#HeaderWrap {
		background: #6e6e6e;
		background-size: cover!important;		
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/mobile/fond_header.jpg',
		sizingMethod='scale');				
	}
	
	body.IsHome div#ContentWrap {
		padding-top: 0px!important;
	}
	
	div#Header div#UpperHeader {
		 margin: 0 auto;
		padding-bottom: 30px;
	}

	div#Header div#UpperHeader div#Logo {
		float: left;
		max-width: 366px;
		max-height: 93px;
		height: 0px;
		padding-bottom: 14.53%;
		width: 57.1875%; /* 366/640 */
		margin-left: 7.1875%; /* 46/640 */	
		margin-right: 20.15625%; /* 129/640 */
		margin-top: 20px;
	}

	div#Header div#UpperHeader  div#Logo a {
		background: url('./images/logo-expension-pme.png') no-repeat scroll left top transparent;
		background-size: contain!important;	
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/logo.png',
		sizingMethod='scale');					
		width: 100%; 
		height: 0px;
		padding-bottom: 27%;
		display: block;
	}

	div#UpperMenuHeader, div#BottomHeader {
		display: none;
	}
	
}

/* MEDIA QUERIES UPPER HEADER */

/* Bottom Menu */

div#Header {
	background: #fff none repeat scroll 0 0;
    height: 100px;
	width: 100%;
	max-width: 100%;
	
}
	div#Header div#BottomHeader {
		background:<?php echo $gris_moyen; ?>!important;
		height:70px;
	}
		div#Header div#BottomMenuHeader {
			margin: 12px auto;
			width: 100%;
			max-width: 960px;
		}

div#Header div#BottomMenuHeader ul li {
	position: relative;
}

div#Header div#BottomMenuHeader div.widget_teammenupages {
	float: left;
	border-right: 3px solid <?php echo $gris_moyen; ?>;	
}

div#Header div#BottomMenuHeader ul li a.PrincipalMenu {
	background: url('./images/hover_menu_principal.png') repeat left top transparent;
	display: block;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 17px;
	line-height: 24px;
	color: <?php echo $blanc; ?>;
	text-transform: uppercase;
	padding: 14px 22px 24px 22px;
}

div#Header div#BottomMenuHeader ul li a.PrincipalMenu:hover, div#Header div#BottomMenuHeader ul li.HoverMenu a.PrincipalMenu, div#Header div#BottomMenuHeader ul li.SelectedMenu a.PrincipalMenu {
	background-position: 0px -63px;
	text-decoration: none;
	-webkit-box-shadow: inset 0px 0px 5px 2px rgba(79,79,79,1);
	-moz-box-shadow: inset 0px 0px 5px 2px rgba(79,79,79,1);
	box-shadow: inset 0px 0px 5px 2px rgba(79,79,79,1);	
}

div#Header div#BottomMenuHeader ul li div.FlecheMenu {
	background: url('./images/fleche_menu_principal.png') no-repeat left top transparent;
	position: absolute;
	width: 13px;
	height: 7px;
	left: 50%;
	top: 40px;
	margin-left: -7px;
}

div#Header div#BottomMenuHeader div#MediaSociauxHeader {
	float: left;
	height: 62px;
}

div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonLinkedin {
	background: url('./images/bouton_linkedin.png') no-repeat left top transparent;
	display: block;
	float: left;
	width: 31px;
	height: 31px;
	margin: 16px 6px 10px 18px;
}

div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonLinkedin:hover {
	background-position: 0px -32px;
} 

div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonYoutube {
	background: url('./images/bouton_youtube.png') no-repeat left top transparent;
	display: block;
	float: left;	
	width: 30px;
	height: 36px;
	margin: 12px 0px 14px 18px;	
}

div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonYoutube:hover {
	background-position: 0px -37px;
}

/* Sous-menu */

div#Header div#BottomMenuHeader ul.SousPages {
	background: url('./images/bande_sousmenu_principal.png') no-repeat right top <?php echo $gris_hover; ?>;
	width: 239px;
	margin: 0;
	padding: 12px 0 24px 0;
	display: none;
	position: absolute;
	z-index: 10;
}

div#Header div#BottomMenuHeader ul.SousPages li {
	font-size: 14px;
	line-height: 24px;
}

div#Header div#BottomMenuHeader ul.SousPages li a {
	display: block;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 24px;
	color: <?php echo $blanc; ?>;	
	padding: 6px 24px;
}

div#Header div#BottomMenuHeader ul.SousPages li a:hover {
	color: <?php echo $vert_pale; ?>;	
	text-decoration: none;
}

div#Header div#BottomMenuHeader ul.SousPages li.LastListItem {
	border-bottom: none;
}



@media screen and (min-width: 641px) and (max-width: 949px) {
	
	div#Header div#BottomMenuHeader div.widget_teammenupages {
		width: 84.8421% /* 806/950 */
	}
	
	div#Header div#BottomMenuHeader ul li a.PrincipalMenu {
		font-size: 13px;
		padding-left: 8px;
		padding-right: 8px;
	}
	
	div#MediaSociauxHeader {
		width: 14.5%;
	}
	
	div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonLinkedin {
		margin-left: 9%;
		margin-right: 9%;
	}
	
	div#Header div#BottomMenuHeader div#MediaSociauxHeader a.BoutonYoutube {
		margin-left: 9%;
		margin-right: 0px;	
	}
	
	div#Header div#BottomMenuHeader li.LastMenuItem ul.SousPages {
		width: 180px;
	}
	
}

@media screen and (min-width: 760px) and (max-width: 949px) {
	div#Header div#BottomMenuHeader ul li a.PrincipalMenu {
		font-size: 15px;
		padding-left: 11px;
		padding-right: 11px;

	}
}

/* Menu Mobile */

div#Header div#BoutonMenuMobile {
	display: none;
}

div#MenuMobileHeader {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 2000;
	display: none;
}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Header div#BoutonMenuMobile {
		background: url('./images/mobile/bouton_menu.png') no-repeat left top transparent;
		background-size: contain!important;	
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/mobile/bouton_menu.png',
		sizingMethod='scale');			
		display: block;
		float: left;
		width: 9.84375%; /* 63/640 */
		height: 0px;
		padding-bottom: 8%;
		margin-top: 8%;
		cursor: pointer;
	}
	
	div#BoutonMenuMobileBlancWrap{ 
		background: url('./images/mobile/fond_bouton_menu_blanc.png') no-repeat left bottom <?php echo $gris_moyen ?>;
		width: 20%;			
		height: 93px;
		float: left;	
	}
	
	div#BoutonMenuMobileBlanc{
		background: url('./images/mobile/bouton_menu_blanc.png') no-repeat left bottom transparent;	
		/*width: 64px;*/
		height: 64px;
		margin-top: 29px;
		cursor: pointer;
		width: 100%;	
	}	
	
	div#MenuMobileHeader {
		background-color: <?php echo $gris_fonce; ?>;
		position: absolute;
		left: 0px;
		top: 0px;
		width: 100%;
		padding-bottom: 36px;
	}
	
	div#MenuMobileHeader ul li  {
		float:none!important;
	}
	
	div#MenuMobileHeader ul li a {
		display: inline-block!important;
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 14px;
		line-height: 24px;
		color: <?php echo $blanc; ?>;	
		padding: 12px 6px 12px 24px;	
	}
	
	div#MenuMobileHeader ul li a.PrincipalMenu:hover, div#MenuMobileHeader ul li.SelectedMenu a {
		text-decoration: none;
		color: <?php echo $vert_pale; ?>;
	}
	
	div#MenuMobileHeader ul li div.FlecheMenu {
		background: url('./images/mobile/fleche_menu.png') no-repeat left top transparent;	
		width: 14px;
		height: 14px;
		display: inline-block;

	}
	
	div#MenuMobileHeader ul li.HoverMenu div.FlecheMenu {
		background-position: 0px -15px;
	}
	
	div#MenuMobileHeader ul li ul.SousPages {
		display: none;
	}
	
	div#MenuMobileHeader ul li ul.SousPages li {
		background-color: <?php echo $gris_pale; ?>;
		padding-left: 36px;		
	}
	
	div#MenuMobileHeader ul li ul.SousPages li a {
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
		font-size: 14px;
		line-height: 24px;
		color: <?php echo $blanc; ?>;	
		padding: 6px 6px 6px 24px;	
	}
	
	div#MenuMobileHeader ul li ul.SousPages li a:hover, div#MenuMobileHeader ul li ul.SousPages li.SelectedMenu a {
		text-decoration: none;
		color: <?php echo $vert_pale; ?>;	
	}
	
	div#MenuMobileHeader div#SelecteurLangue {
		display: inline-block!important;
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 14px;
		line-height: 24px;

		padding: 12px 6px 12px 24px;	
	}
		
	div#MenuMobileHeader div#SelecteurLangue  a {
		color: <?php echo $blanc; ?>;	
	}	
	
	div#MenuMobileHeader div#SelecteurLangue  a:hover {
		color: <?php echo $vert_pale; ?>;
		text-decoration: none;
	}
	
	div#SearchForm {
		background-color: <?php echo $gris_moyen; ?>;
		padding-top: 44px;
		padding-bottom: 14px;
		float: left;
		width: 80%;
	}
	
	div.NoLigneHeader {
		height: 0px!important;
	}
	
	div#MenuMobileHeader div#SearchForm input#s {
		border: none;
		width: 150px;
		padding: 0px 6px;
		height: 35px;
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
		font-size: 16x;
		line-height: 35px;
		color: <?php echo $gris_moyen; ?>;
	}
	
	div#MenuMobileHeader div#SearchForm a.SubmitSpecial {
		background: url('./images/mobile/bouton_recherche.png') no-repeat scroll left top transparent;
		display: block;
		width: 40px;
		height: 35px;
		float: left;
		margin-left: 12px;
	}
		
}

@media screen and (min-width: 320px) and (max-width: 480px) { 

	div#Header div#BoutonMenuMobile {
		margin-top: 10%;
	}

}

/*****************************************/
/* SLIDER								 */
/*****************************************/


div#Content div#Slider {
	width: 100%;
	max-width: 948px;
	height: 335px;
	border: 1px solid <?php echo $gris_pale; ?>;
}

div#Content div#Slider div.ls-s2 {
	width: 55%!important;
	position: absolute;
	top: 0px;
	right: -1px;
}

div#Content div#Slider a.ls-linkto {
	display: block;
	width: 100%;
	height: 100%;
}	

div#Content div#Slider div#LayerSlider {
	/*width: 100%;
	max-width: 960px;	
	height: 100%;*/
	position: relative;
}

div#Content div#Slider div#LayerSlider div.TypeEvenement {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 18px;
	line-height: 18px;
	text-transform: uppercase;	
	color: <?php echo $gris_pale; ?>;
	text-align: right;
	margin: 24px 24px 30px 0px;
}

div#Content div#Slider div#LayerSlider  div.FondTurquoise h1 {
	background-color: <?php echo $turquoise; ?>;
}

div#Content div#Slider div#LayerSlider  div.FondBrun h1 {
	background-color: <?php echo $brun; ?>;
}

div#Content div#Slider div#LayerSlider  div.FondOrange h1 {
	background-color: <?php echo $orange; ?>;
}

div#Content div#Slider div#LayerSlider  div.FondVert h1 {
	background-color: <?php echo $vert_pale; ?>;
}

div#Content div#Slider div#LayerSlider  div.TexteTurquoise h2,div#Content div#Slider div#LayerSlider  div.TexteTurquoise h3 {
	color: <?php echo $turquoise; ?>;
}

div#Content div#Slider div#LayerSlider  div.TexteBrun h2,div#Content div#Slider div#LayerSlider  div.TexteBrun h3 {
	color: <?php echo $brun; ?>;
}

div#Content div#Slider div#LayerSlider  div.TexteOrange h2,div#Content div#Slider div#LayerSlider  div.TexteOrange h3 {
	color: <?php echo $orange; ?>;
}

div#Content div#Slider div#LayerSlider  div.TexteVert h2,div#Content div#Slider div#LayerSlider  div.TexteVert h3 {
	color: <?php echo $vert_pale; ?>;
}

div#Content div#Slider div#LayerSlider h1{
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 32px;
	line-height: 32px;
	color: <?php echo $blanc; ?>;
	text-transform: uppercase;
	padding: 24px 24px 24px 60px;
}

div#Content div#Slider div#LayerSlider h2 {
	font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;
	font-size: 30px;
	line-height: 32px;
	text-transform: uppercase;	
	margin-bottom: 0px;
	margin-left: 60px;
}

div#Content div#Slider div#LayerSlider h3 {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 18px;
	line-height: 24px;
	text-transform: uppercase;	
	margin-bottom: 24px;
	margin-left: 60px;	
}

div#Content div#Slider div#LayerSlider p {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;
	line-height: 20px;
	margin-bottom: 6px;
	color: <?php echo $gris_pale; ?>;
	text-transform: uppercase;
	margin-left: 60px;	
}

div#Content div#Slider div#LayerSlider p strong, div#Content div#Slider div#LayerSlider p bold {
	font-size: 20px;
	line-height: 24px;
	color: <?php echo $gris_pale; ?>;
}

div.ls-lt-container {
	overflow: hidden;
}
 
 div.ls-bottom-nav-wrapper {
	height: 13px;
	position: relative;
	z-index: 23000;
	float: right;	
	margin-top: 20px;
}

span.ls-bottom-slidebuttons {
	height: 13px;
	display: block;
}

span.ls-bottom-slidebuttons a {
 	background: url('./images/bouton_slider.png') no-repeat scroll left top transparent;
	display: inline-block;
	width: 13px;
	height: 13px;
	margin-right: 6px;
	
}

span.ls-bottom-slidebuttons a:hover, span.ls-bottom-slidebuttons a.ls-nav-active {
	background-position: 0px -14px;
}

div#Content div.LigneVerteSlider {
	border-left: 1px solid <?php echo $gris_pale; ?>;
	border-right: 1px solid <?php echo $gris_pale; ?>;
	border-bottom: 1px solid <?php echo $gris_pale; ?>;
	width: 99.78947%; /* 948/950 */
	height: 12px;
	background-color: <?php echo $vert_pale; ?>;
	margin-bottom: 30px;
}

/* MEDIA QUERIES SLIDER */

@media screen and (min-width: 640px) and (max-width: 949px) {

	div#Content div#Slider div#LayerSlider div.TypeEvenement {
		margin: 18px 12px 18px 0px;
	}	
	
	div#Content div#Slider div#LayerSlider h1{
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 24px;
		line-height: 24px;
		color: <?php echo $blanc; ?>;
		text-transform: uppercase;
		padding: 18px 18px 18px 36px;
	}

	div#Content div#Slider div#LayerSlider h2 {
		font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;
		font-size: 26px;
		line-height: 28px;
		text-transform: uppercase;	
		margin-bottom: 0px;
		margin-left: 24px;
		margin-right: 6px;			
	}

	div#Content div#Slider div#LayerSlider h3 {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 21px;
		line-height: 24px;
		text-transform: uppercase;	
		margin-bottom: 18px;
		margin-left: 24px;
		margin-right: 6px;			
	}

	div#Content div#Slider div#LayerSlider p {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 21px;
		line-height: 24px;
		margin-bottom: 6px;
		color: <?php echo $gris_pale; ?>;
		text-transform: uppercase;
		margin-left: 24px;
		margin-right: 6px;		
	}	

	div#Content div#Slider {
		width: 100%!important;
		max-width: 949px!important;
		height: 325px!important;
		border-color: <?php echo $gris_footer; ?>;
	}

	.ls-layer {
		height: 325px!important;
	}
	
	div#Content div#Slider img.ls-bg {
		max-width: 949px!important;
		max-height: 325px!important;
		width: 100%:
		height: 100%;
		/*margin-left: -372px!important;*/
		/*margin-top: 0px!important;*/
		/*left: 45%;*/
	}		
	
	.ls-nexttile img{
		margin-top: 0px!important;
		/*left: 5%;*/
	}

	div#Content div#Slider div#LayerSlider {
		max-width: 949px!important;
		max-height: 325px!important;	
		width: 100%!important;
		height: 100%!important;
	}
	
	div#Content div#Slider div.ls-s2 {
		width: 60%!important;
		position: absolute;
		top: 0px!important;
		right: -1px!important;
	}		
	
	div.ls-bottom-nav-wrapper  {
		float: none;
		width: 100%;
		margin-top: 36px;
	}
	
	span.ls-bottom-slidebuttons {
		text-align: center;
	}	
	
	span.ls-bottom-slidebuttons a { 
		margin-right: 12px;
	}	
	
	div#Content div.LigneVerteSlider {
		margin-bottom: 60px;
		border-color: <?php echo $gris_footer; ?>;
	}
	
}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content div#Slider div#LayerSlider div.TypeEvenement {
		margin: 6px 6px 12px 0px;
	}	
		
	div#Content div#Slider div#LayerSlider h1{
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 14px;
		line-height: 16px;
		color: <?php echo $blanc; ?>;
		text-transform: uppercase;
		padding: 6px 6px 6px 10px;
	}

	div#Content div#Slider div#LayerSlider h2 {
		font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;
		font-size: 15px;
		line-height: 16px;
		text-transform: uppercase;	
		margin-bottom: 0px;
		margin-left: 24px;
		margin-right: 6px;			
	}

	div#Content div#Slider div#LayerSlider h3 {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 12px;
		line-height: 16px;
		text-transform: uppercase;	
		margin-bottom: 6px;
		margin-left: 24px;
		margin-right: 6px;			
	}

	div#Content div#Slider div#LayerSlider p {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 11px;
		line-height: 16px;
		margin-bottom: 6px;
		color: <?php echo $gris_pale; ?>;
		text-transform: uppercase;
		margin-left: 24px;
		margin-right: 6px;		
	}

	div#Content div#Slider div#LayerSlider p strong, div#Content div#Slider div#LayerSlider p bold {
		font-size: 12px;
		line-height: 16px;
		color: <?php echo $gris_pale; ?>;
	}	

	div#Content div#Slider {
		width: 100%!important;
		max-width: 640px!important;
		height: 225px!important;
		border-color: <?php echo $gris_footer; ?>;
	}

	.ls-layer {
		height: 225px!important;
	}
	
	div#Content div#Slider img.ls-bg {
		max-width: 640px!important;
		max-height: 225px!important;
		width: 100%:
		height: 100%;
		/*margin-left: -372px!important;*/
		/*margin-top: 0px!important;*/
		/*left: 45%;*/
	}		
	
	.ls-nexttile img{
		margin-top: 0px!important;
		/*left: 5%;*/
	}

	div#Content div#Slider div#LayerSlider {
		max-width: 640px!important;
		max-height: 225px!important;	
		width: 100%!important;
		height: 100%!important;
	}
	
	div#Content div#Slider div.ls-s2 {
		width: 60%!important;
		position: absolute;
		top: 0px!important;
		right: -1px!important;
	}		
	
	div.ls-bottom-nav-wrapper  {
		float: none;
		width: 100%;
		margin-top: 36px;
	}
	
	span.ls-bottom-slidebuttons {
		text-align: center;
	}	
	
	span.ls-bottom-slidebuttons a { 
		margin-right: 12px;
	}	
	
	div#Content div.LigneVerteSlider {
		margin-bottom: 60px;
		border-color: <?php echo $gris_footer; ?>;
	}
	
}

/* MEDIA QUERIES SLIDER */

/*****************************************/
/* CONTENT								 */
/*****************************************/

div#ContentWrap {
	width: 100%;
	overflow: hidden;
	position: relative;
	z-index: 400;
	background-color: #fff;
}

body.IsHome div#ContentWrap {
	background-color: transparent;
	padding-top: 12px;
}

div#BandeVerte {
	margin: 0 auto;
	
	border-left: 1px solid <?php echo $gris_pale; ?>;
	border-right: 1px solid <?php echo $gris_pale; ?>;
	border-bottom: 1px solid <?php echo $gris_pale; ?>;
	width: 99.78947%; /* 948/950 */
	height: 12px;
	background-color: <?php echo $vert_pale; ?>;
}

div#Content {
	width: 100%;
	max-width: 950px;
	margin: 0 auto;
}

div#Content h1 {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 20px;
	line-height: 24px;
	margin-bottom: 12px;
	color: <?php echo $gris_fonce; ?>;
	text-transform: uppercase;
}

div#Content h2 {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 18px;
	line-height: 24px;
	margin-bottom: 24px;
	color: <?php echo $gris_fonce; ?>;
	text-transform: uppercase;	
}

div#Content h3 {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 18px;
	line-height: 24px;
	margin-bottom: 24px;
	color: <?php echo $gris_pale; ?>;
	text-transform: uppercase;	
}

div#Content p {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 16px;
	line-height: 20px;
	margin-bottom: 24px;
	color: <?php echo $gris_fonce; ?>;
}

div#Content a, div#Content p a {
	text-decoration: underline;
	color: <?php echo $vert_fonce; ?>;
}

div#Content a:visited, div#Content p a:visited {
	text-decoration: underline;
	color: <?php echo $gris_pale; ?>;
}

div#Content a:hover, div#Content p a:hover {
	text-decoration: none;
	color: <?php echo $vert_hover; ?>;
}

div#Content ul {
	margin-bottom: 24px;
}

div#Content ul li {
	background: url("images/liste_puce.png") no-repeat scroll left top transparent;
	padding-left: 18px;
	padding-top: 6px;
	margin-left: 18px;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 16px;
	line-height: 24px;
}

div#Content strong {
	font-family: CabinBold, Helvetica, Sans-serif;
	font-weight: bold;
}

div#RightContent {
	float: left;
	margin-bottom: 48px;
}

@media screen and (min-width: 320px) and (max-width: 949px) {

	div#Content div#Template {
		width: 94%;
		padding-left: 3%;
		padding-right: 3%;
	}
	
	div#Content div.template-accueil {
		width: 100%!important;
		padding-left: 0%!important;
		padding-right: 0%!important;
	}	
	
	div#Content table img{
		width: 100%;
	}

}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content h1 {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 18px;
		line-height: 21px;
		margin-bottom: 12px;
		color: <?php echo $gris_fonce; ?>;
		text-transform: uppercase;
		text-align: center;
	}

	div#Content h2 {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 16px;
		line-height: 18px;
		margin-bottom: 24px;
		color: <?php echo $gris_fonce; ?>;
		text-transform: uppercase;	
	}

	div#Content h3 {
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 14px;
		line-height: 18px;
		margin-bottom: 24px;
		color: <?php echo $gris_pale; ?>;
		text-transform: uppercase;	
	}

	div#Content p {
		font-family: CabinRegular, Arial, Helvetica, Sans-serif;
		font-size: 13px;
		line-height: 18px;
		margin-bottom: 24px;
		color: <?php echo $gris_fonce; ?>;
	}
	
	div#Content ul li {
		background: url("images/liste_puce.png") no-repeat scroll left top transparent;
		padding-left: 18px;
		padding-top: 6px;
		margin-bottom: 3px;
		margin-left: 18px;
		font-family: CabinRegular, Arial, Helvetica, Sans-serif;
		font-size: 13px;
		line-height: 29px;
	}
	
	div#Content div.template-commun p, div#Content div.template-commun ul {
		width: 100%!important;
	}
	
	div#Content blockquote p, div#Content div.template-commun blockquote p{
		width: 65%!important;
	}

	div#Content blockquote div.ImageCitation {
		right: 10px!important;
		top: 25px!important;
	}
	
	object embed, embed {
		margin-bottom: 24px;
		max-width: 320px;
		width: 100%;
	}

}


/*****************************************/
/* BREADCRUMS							 */
/*****************************************/

div#Content div#Breadcrumbs {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	text-transform: uppercase;
	font-size: 13px;
	line-height: 16px;
	margin-bottom: 48px;
	padding-top: 24px;
}

div#Content div#Breadcrumbs a {
	color: <?php echo $gris_pale; ?>;
	text-decoration: underline;
}

div#Content div#Breadcrumbs a.SelectedBC {
	color: <?php echo $gris_fonce; ?>;
	text-decoration: none;
}

div#Content div#Breadcrumbs a:hover {
	color: <?php echo $vert_hover; ?>;
	text-decoration: none;
}

@media screen and (min-width: 320px) and (max-width: 949px) {
	
	div#Breadcrumbs {
		font-size: 11px!important;
		margin-bottom: 24px!important;
	}

}

/*****************************************/
/* ACCUEIL								 */
/*****************************************/

div#Content div.ResumeAccueil {
	background: url("images/shades/50.png") repeat scroll left top transparent;
	border: 4px solid <?php echo $gris_pale; ?>;
	width: 68%; /* 646/950 */
	margin: 0 auto 18px;
}

div#Content div.ResumeAccueil div.ZoneTexte {
	background: url("images/ligne_grise.png") no-repeat scroll center 90% transparent;
	padding: 24px;
	text-align: center;
}

div#Content div.ResumeAccueil div.ZoneTexte p {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 24px;
	font-size: 20px;
	text-transform: uppercase;
	color: <?php echo $gris_fonce; ?>;
	margin-bottom: 0px;
}

div#Content div.ResumeAccueil div.ZoneTexte p strong {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 24px;
	font-size: 20px;
	text-transform: uppercase;
}

div#Content div#BottomAccueil {
	padding-top: 18px;
}

div#Content div#BottomAccueil div.ZoneTitre {
	width: 100%;
	height: 32px;
	border-bottom: 2px solid <?php echo $gris_pale; ?>;
	margin-bottom: 24px;
}

div#Content div#BottomAccueil div.ZoneTitre div.TitreAccueil {
	float: left;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 24px;
	font-size: 20px;
	color: <?php echo $gris_fonce; ?>;	
	text-transform: uppercase;
}

div#Content div#BottomAccueil div.ZoneTitre a.MiniTitreAccueil {
	background: url("images/bouton_mini_plus.png") no-repeat scroll right center transparent;
	float: right;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 32px;
	font-size: 12px;
	color: <?php echo $gris_fonce; ?>;	
	text-decoration: none;
	text-transform: uppercase;
	padding-right: 24px;
}

div#Content div#BottomAccueil div.ZoneTitre a.MiniTitreAccueil:hover {
	color: <?php echo $vert_pale; ?>;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap {
	width: 100%;
	margin-bottom: 48px;
}

/*Expertises*/

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise {
	float: left;
	width: 309px;
	height: 237px;
	display: block;
	text-decoration: none;	
	position: relative;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise:hover div.ZoneHoverExpertise  {
	display: block!important;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise {
	background: url("images/expertise_hover.png") repeat-x left top transparent;
	width: 309px;
	height: 219px;
	position: absolute;
	z-index: 100;
	left: 0px;
	top: 0px;
	display: none;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise div.ShapePlus {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 219px;
	font-size: 18px;
	color: <?php echo $vert_pale; ?>;	
	text-transform: uppercase;
	text-align: center;	
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise div.ShapePlus img {
	position: relative;
	top: 3px;
	left: 6px;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise1 {
	background: url("images/expertise1c.png") no-repeat left top transparent;
	margin-right: 11px;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise2 {
	background: url("images/expertise2c.png") no-repeat left top transparent;
	margin-right: 11px;	
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise3 {
	background: url("images/expertise3c.png") no-repeat left top transparent;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise {
	margin-top: 106px;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise h2 {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 24px;
	font-size: 23px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 24px;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise hr {
	background-color: <?php echo $blanc; ?>;
	margin: 0 auto;
	width: 30%;
	height: 2px;
	border: none;
}

div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise p, div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneExpertise div.TexteExpertise p strong  {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 18px;
	font-size: 13px;
	color: <?php echo $blanc; ?>;	
	text-align: center;
	padding: 12px 18px 18px 18px;
}

/* MEDIA QUERIES R�SUM� ET EXPERTISES */

@media screen and (min-width: 641px) and (max-width: 949px) {

	div#Content div.ResumeAccueil div.ZoneTexte p {
		line-height: 21px;
		font-size: 16px;
	}

	div#Content div.ResumeAccueil div.ZoneTexte p strong {
		line-height: 21px;
		font-size: 16px;
	}	

	div#Content div#BottomAccueil {
		width: 98%;
		margin: 0 auto;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise {
		width: 32.545653%;
		height: 0px;
		padding-bottom: 32.545653%;
		
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise {
		margin-top: 44.88996%;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise h2 {
		font-size: 16px;
		line-height: 18px;
		margin-bottom: 4.76699%;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise p, div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneExpertise div.TexteExpertise p strong {
		font-size: 11px;
		padding: 6px; 12px; 6px;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise1 {
		margin-right: 1.18152%;
		background: url('./images/expertise1c.png') no-repeat center top transparent;
		background-size: cover!important;	
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/expertise1c.png',
		sizingMethod='scale');			
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise2 {
		margin-right: 1.18152%;	
		background: url('./images/expertise2c.png') no-repeat center top transparent;
		background-size: cover!important;	
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/expertise2c.png',
		sizingMethod='scale');			
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise3 {
		background: url('./images/expertise3c.png') no-repeat center top transparent;
		background-size: cover!important;	
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/expertise3c.png',
		sizingMethod='scale');			
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise {
		width: 100%;
		height: 0px;
		padding-bottom: 93%;	
	}
	
}

@media screen and (min-width: 800px) and (max-width: 949px) {

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise {
		margin-top: 44.88996%;
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise h2 {
		font-size: 18px;
		line-height: 21px;
		margin-bottom: 7.76699%;
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise p, div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneExpertise div.TexteExpertise p strong {
		font-size: 13px;
		padding: 12px; 18px; 12px;
	}

}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content div#BottomAccueil {
		width: 100%;
		margin: 0 auto;
	}

	div#Content div.ResumeAccueil {
		background: url("images/shades/50.png") repeat scroll left top transparent;
		border: 4px solid <?php echo $gris_pale; ?>;
		width: 90%; /* 288/320 */
		margin: 0 auto 18px;
	}
	
	
	div.template-accueil {
		background: url('./images/mobile/fond_body.jpg') no-repeat scroll left 5% transparent;	
	}	
	
	div#Content div.ResumeAccueil div.ZoneTexte p {
		line-height: 18px;
		font-size: 14px;
	}

	div#Content div.ResumeAccueil div.ZoneTexte p strong {
		line-height: 18px;
		font-size: 14px;
	}	

	div#Content div.ResumeAccueil div.ZoneTexte {
		padding: 18px 18px 24px 18px;
	}
	
	div#Content div#BottomAccueil div.ZoneTitre div.TitreAccueil {
		float: none;
		text-align: center;
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
		font-size: 18px;		
	}
	
	div#Content div#BottomAccueil div.ZoneTitre a.MiniTitreAccueil {
		display: none;
	}
	
	div#Content div#BottomAccueil div.ZoneTitreExpertise {
		width: 90%;
		margin: 0 auto 24px;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap {
		width: 90%;
		margin: 0 auto 24px;	
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise {
		width: 100%;
		height: 0px;
		padding-bottom: 22.94372%; /* 106 / 462 */
		margin-bottom: 5.1948%; /* 24 / 462 */
		background-size: contain!important;	
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise1 {
		background: url("images/mobile/expertise1c.png") no-repeat left top transparent;
		margin-right: 0px;
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/mobile/expertise1c.png',
		sizingMethod='scale');			
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise2 {
		background: url("images/mobile/expertise2c.png") no-repeat left top transparent;
		margin-right: 0px;
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/mobile/expertise2c.png',
		sizingMethod='scale');		
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.Expertise3 {
		background: url("images/mobile/expertise3c.png") no-repeat left top transparent;
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/mobile/expertise3c.png',
		sizingMethod='scale');			
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise {
		margin-top: 8%;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise h2 {
		padding-left: 24px;
		text-align: left;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise hr, div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise p, div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise li {
		display: none;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise {
		width: 100%;
		height: 0px;
		padding-bottom: 21.5%; /* 106 / 462 */
		margin-bottom: 5.1948%; /* 24 / 462 */
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise div.ShapePlus {
		margin-top: 9%;
		line-height: 24px;
		font-size: 24px;
	}
	
}

@media screen and (min-width: 320px) and (max-width: 480px) {

	
	div.template-accueil {
		background: url('./images/mobile/fond_body.jpg') no-repeat scroll left 6.5% transparent;	
	}

	div#Content div.ResumeAccueil div.ZoneTexte p {
		line-height: 18px;
		font-size: 12px;
	}

	div#Content div.ResumeAccueil div.ZoneTexte p strong {
		line-height: 18px;
		font-size: 12px;
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap div.ZoneHoverExpertise div.ShapePlus {
		margin-top: 6%;
		line-height: 24px;
		font-size: 18px;	
	}

	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise {
		margin-top: 8%;
	}
	
	div#Content div#BottomAccueil div#ZoneExpertiseWrap a.ZoneExpertise div.TexteExpertise h2 {
		font-size: 16px;
		line-height: 20px;
	}	

}

/* MEDIA QUERIES R�SUM� ET EXPERTISES */

/*Activit�s*/

div#Content div#BottomAccueil div#BigZoneActivitesWrap {
	float: left;
	width: 448px;	
	margin-bottom: 24px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap {
	background-color: #f7f7f7;
	width: 412px;
	padding: 18px;
	overflow: hidden;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div#ZoneActivitesViewport  {
	width: 800px;
	height: auto!important;
	margin-bottom: 18px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite {
	float: left;
	width: 191px;
	height: auto!important;
	min-height: 250px!important;
	margin-right: 32px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite div.BoiteImage {
	height: 125px;
	position: relative;
	border: 1px solid <?php echo $gris_pale; ?>;
	margin-bottom: 6px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite div.ChampActivite {
	position: absolute;
	bottom: -2px;
	right: 0px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite div.DateActivite {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 18px;
	font-size: 16px;
	color: <?php echo $gris_pale; ?>;	
	text-transform: uppercase;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite div.TypeActivite {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 18px;
	font-size: 12px;
	color: <?php echo $gris_pale; ?>;	
	text-transform: uppercase;
	margin-bottom: 12px;
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite a.TitreActivite {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 18px;
	font-size: 16px;
	color: <?php echo $vert_fonce; ?>;	
	text-transform: uppercase;
	text-decoration:none;	
}

div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite a.TitreActivite:hover {
	color: <?php echo $vert_fonce; ?>;	
	text-decoration:underline;	
}

a#ui-carousel-prev {
	background: url("images/bouton_fleche_precedent.png") no-repeat left top transparent;
	float: right;
	display: block;
	width: 18px;
	height: 18px;
	margin-right: 12px;
}

a#ui-carousel-next {
	background: url("images/bouton_fleche_suivant.png") no-repeat left top transparent;
	float: right;	
	display: block;
	width: 18px;
	height: 18px;	
}

a#ui-carousel-prev:hover, a#ui-carousel-next:hover {
	background-position: 0px -19px;
}

.ui-carousel {
	height: 100px;
	margin: 0;
	overflow: auto;
	padding: 0;
	position: relative; /*for ie7 to work e.g in html carousels*/
	width: 300px
}

.ui-carousel > .wrapper {
    margin: 0;
    padding: 0;
    width: 9999px;
}

.ui-carousel > .wrapper > * {
	border: 0;
	display: block;
	float: left;
	height: 100px;
	overflow: hidden;
	width: 100px;	
}

div#Content div#BottomAccueil a#ProprietairePME {
	background: url("images/banniere_proprietaire_pme.png") no-repeat left top transparent;
	display: block;
	width: 412px;
	height: 57px;
	padding: 18px;
	margin-top: 24px;
	text-decoration: none;
	color: <?php echo $blanc; ?>;		
}

div#Content div#BottomAccueil a#ProprietairePME div.TitreProprietaire {
	float: left;
	width: 200px;	
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 27px;
	font-size: 25px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;	
}

div#Content div#BottomAccueil a#ProprietairePME div.ContenuProprietaire {
	float: left;
	width: 200px;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	line-height: 18px;
	font-size: 15px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;
}

/* Nouvelles */

div#Content div#BottomAccueil div#ZoneNouvellesWrap {
	float: left;
	margin-left: 84px;
	padding-top: 18px;
	width: 416px;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle {
	margin-bottom: 24px;
	border-bottom: 2px dotted <?php echo $gris_pale; ?>;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.DateNouvelle {
	float: left;
	width: 54px;
	margin-right: 18px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;	
	line-height: 21px;
	text-transform: uppercase;
	color: <?php echo $gris_pale; ?>;		
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.DateNouvelle span.JourNouvelle {
	font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;
	font-size: 20px;	
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.WrapContenuNouvelle {
	float: right;
	width: 344px;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.TitreNouvelle {
	font-family: CabinBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;	
	line-height: 21px;
	text-transform: uppercase;
	color: <?php echo $vert_fonce; ?>;	
	margin-bottom: 12px;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle a.nouvelleTitre {
	font-family: CabinBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;	
	line-height: 21px;
	text-transform: uppercase;
	color: <?php echo $vert_fonce; ?>;	
	margin-bottom: 12px;
	text-decoration:none;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle h3, div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle h2 {
	font-size: 13px;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle a.SuiteNouvelle {
	background: url("images/bouton_fleche_suivant_vert.png") no-repeat right top transparent;
	float: right;
	display: block;
	padding-right: 24px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 12px;	
	line-height: 18px;
	text-transform: uppercase;
	color: <?php echo $gris_fonce; ?>;		
	text-decoration: none;
	margin-bottom: 18px;
}

div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle a.SuiteNouvelle:hover {
	text-decoration: none;
	color: <?php echo $vert_fonce; ?>;			
}

div#Content div#BottomAccueil a.BoutonMobileActivites, div#Content div#BottomAccueil a.BoutonMobileNouvelles {
	display: none;
}

/* Partenaires */

div#Content div#BottomAccueil div#LignePartenaire {
	background-color: <?Php echo $vert_pale; ?>;
	width: 100%;
	height: 2px;
	margin-top: 24px;
	margin-bottom: 18px;
}

div#Content div#BottomAccueil  div.DescriptifPartenaires p {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;	
	line-height: 18px;
	text-transform: uppercase;
	color: <?Php echo $gris_fonce; ?>;
}

div#Content div#BottomAccueil  div.DescriptifPartenaires p strong {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
}

div#Content div#BottomAccueil div#ImagesPartenaireOuter {
	position:relative;
	left:50%;
	float:left;
	clear:both;
	margin-bottom: 48px;
	text-align:left;
}

div#Content div#BottomAccueil div#ImagesPartenaireInner {
	position:relative;
	left:-50%;
	text-align:left;
}

div#Content div#BottomAccueil div.ImagePartenaire {
	float: left;
	width: 25.83312%; /* 82.666/320 */
	height: auto;
	margin-left: 3.75%; /* 12/320*/
	margin-right: 3.75%; /* 12/320*/
}

/* MEDIA QUERIES ACTIVIT�S, NOUVELLES ET PARTENAIRES */

@media screen and (min-width: 641px) and (max-width: 949px) {

	div#Content div#BottomAccueil div#BigZoneActivitesWrap {
		width: 100%;
		float: none;
	}

	div#Content div#BottomAccueil div#ZoneActivitesWrap {
		width: 100%;
		padding-left: 0px;
		padding-right: 0px;
		position: relative;
	}
	
	div#Content div#BottomAccueil div.ZoneTitre {
		width: 98%;
		margin: 0 auto 24px;
	}
	
	div#ZoneActivitesViewport {
		margin: 0 auto;
	}
	
	a#ui-carousel-next {
		background: url("images/mobile/bouton_fleche_suivant.png") no-repeat right top transparent;
		width: 29px;
		height: 29px;
		position: absolute;
		right: 48px;
		top: 120px;
	}
	
	a#ui-carousel-prev {
		background: url("images/mobile/bouton_fleche_precedent.png") no-repeat right top transparent;
		width: 29px;
		height: 29px;
		position: absolute;	
		left: 48px;
		top: 120px;

	}
	
	a#ui-carousel-next:hover {
		background-position: 0px -30px;
	}
	
	a#ui-carousel-prev:hover { 
		background-position: 0px -30px;	
	}	
	
	div#Content div#BottomAccueil a#ProprietairePME {
		margin: 24px auto 0;
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap {
		float: none;
		margin: 0 auto;
	}
	
}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content div#BottomAccueil div#BigZoneActivitesWrap {
		background-color: #f7f7f7;
		float: none;
		width: 100%; /* 288 / 320 */
		margin: 0 auto 24px;
	}
	
	div#Content div#BottomAccueil div#ZoneActivitesWrap {
		width: 90%;
		margin: 0 auto;
		padding: 18px 0px 24px 0px;
		position: relative;
	}
	
	div#Content div#BottomAccueil div#ZoneActivitesWrap div#ZoneActivitesViewport {
		max-width: 192px;
		margin: 0 auto;
	}
	
	div#Content div#BottomAccueil div#ZoneActivitesWrap div.ZoneActivite {
		margin-bottom: 24px;
	}
	
	a#ui-carousel-next {
		background: url("images/mobile/bouton_fleche_suivant.png") no-repeat right top transparent;
		width: 29px;
		height: 29px;
		position: absolute;
		right: 0px;
		top: 120px;
	}
	
	a#ui-carousel-prev {
		background: url("images/mobile/bouton_fleche_precedent.png") no-repeat right top transparent;
		width: 29px;
		height: 29px;
		position: absolute;	
		left: 0px;
		top: 120px;

	}
	
	a#ui-carousel-next:hover {
		background-position: 0px -30px;
	}
	
	a#ui-carousel-prev:hover { 
		background-position: 0px -30px;	
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap {
		float: none;
		margin: 0 auto;
		width: 90%;
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.DateNouvelle {
		width: 18.75%; /* 54/288 */
		margin-right: 8.33333%; /* 24/288 */
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.WrapContenuNouvelle {
		width: 72.91666%; /* 210/288 */
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle div.TitreNouvelle {
		font-size: 14px;
	}
	
	div#Content div#BottomAccueil a.BoutonMobileActivites, div#Content div#BottomAccueil a.BoutonMobileNouvelles {
		background-color: <?php echo $vert_fonce; ?>;
		display: block;
		width: 80%;
		height: 42px;
		margin: 0 auto;
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 13px;	
		line-height: 42px;
		text-transform: uppercase;
		color: <?Php echo $blanc; ?>;	
		text-align: center;
		text-decoration: none;
	}

	div#Content div#BottomAccueil a.BoutonMobileActivites:hover, div#Content div#BottomAccueil a.BoutonMobileNouvelles:hover {
		background-color: <?php echo $gris_pale; ?>;
		text-decoration: none;
		color: <?php echo $blanc; ?>;
	}
	
	div#Content div#BottomAccueil a#ProprietairePME {
		display: none;
	}
	
	div#Content div#BottomAccueil div.DescriptifPartenaires  {
		width: 90%; /* 288/320 */
		margin: 0 auto;
		text-align: center;
	}
	
	div#Content div#BottomAccueil div.ImagePartenaire {
		width: 25.83312%; /* 82.666/320 */
		height: auto;
		margin-left: 3.75%; /* 12/320*/
		margin-right: 3.75%; /* 12/320*/
	}
	
	div#Content div#BottomAccueil div#ZoneNouvellesWrap div.ZoneNouvelle h1 {
		text-align: left!important;
	}
	
}

/*****************************************/
/* SIDEBAR								 */
/*****************************************/

div#Content div#Sidebar {
	width: 26.7%;
	float: left;
	margin-bottom: 48px;
	padding-top: 12px;
}

div#Content div#Sidebar div.InputBlock {
	width: 84%;
	padding-left: 8%;
	padding-right: 8%;
	background: url("images/fond_boite_recherche.png") no-repeat left top <?php echo $gris_pale; ?>;
	margin-bottom: 18px;
}

div#Content div#Sidebar form#SearchInPostType {
	position: relative;
}

div#Content div#Sidebar div.FormWrap {
	height: 51px;
}

div#Content div#Sidebar form#SearchInPostType, div#Content div#Sidebar div#ArchivesBlock form {
	padding: 0;
	/*background-color: <?php echo $blanc; ?>;*/
	height: 15px;
	width: 100%;
	display: block;
}

div#Content div#Sidebar div#ArchivesBlock form .selectbox {
	width: 100%!important;
}

div#Content div#Sidebar form#SearchInPostType input {
	float: left;
	border: none;
	width: 95%;
	padding: 0px 6px;
	height: 35px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 35px;
	color: <?php echo $gris_moyen; ?>;
}

div#Content div#Sidebar form#SearchInPostType a.SubmitSpecial {
	display: block;
	width: 15px;
	padding-right: 9px;
	height: 35px;
	position: absolute;
	top: 0;
	right: 0px;
	background: url("images/loupe_recherche.png") no-repeat left top transparent;
}

div#Content div#Sidebar div.BlockTitle {
	text-transform: uppercase;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 20px;
	line-height: 46px;
	color: <?php echo $blanc; ?>;
}

div#Content div#Sidebar div#CategoryBlock {
	background: url("images/fond_boite_titre.png") no-repeat left top <?php echo $gris_fond_bte; ?>;
	padding-left: 20px;
	margin-bottom: 18px;
}

div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul {
	margin: 0;
	padding: 10px 0;
	list-style: none;
}

div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li {
	margin: 0;
	padding: 0;
	list-style: none;
	background: none;
}

div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li a {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 35px;
	color: <?php echo $gris_fonce; ?>;
	text-decoration: none;
}

div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li a:hover, div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li.SelectedItem a {
	color: <?php echo $vert_hover; ?>;
	text-decoration: none;
}

@media screen and (min-width: 641px) and (max-width: 949px) {

	div#Content div#Sidebar div#CategoryBlock { 
		padding-left: 7%;
		padding-right: 7%;
	}

	div#Content div#Sidebar div.BlockTitle {
		font-size: 14px;
	}

	div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li a {
		font-size: 13px;
	}
	
}

/*****************************************/
/* ACTIVITES, BLOGUE ET EQUIPE   				 */
/*****************************************/

div#Content div#FormulaireActivite p {
	min-height: 24px;
	position: relative;
}

div#Content div#RightContent {
	width: 670px;
	padding-top: 12px;
}

div#PreContent div#LeftPreContent {
	width: 65%;
	float: left;
}

div#PreContent div#LeftPreContent h2 {
	text-transform: uppercase;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 20px;
	line-height: 24px;
	margin-bottom: 12px;
}

div#PreContent div#RightPreContent {
	float: right;
	margin-bottom: 24px;
}

div#Content div.MoreInformation {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 24px;
	margin-bottom: 10px;
	color: <?php echo $gris_fonce; ?>;
	text-transform: uppercase;
}

div#Content div.PageButton {
	width: 205px;
	height: 42px;
}

div#Content div.PageButton a {
	width: 182px;
	height: 42px;
	display: block;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;
	line-height: 42px;
	text-transform: uppercase;
	text-decoration: none!important;
	text-align: center;
	padding-right: 23px;
	color: <?php echo $blanc; ?>!important;
	background: url("images/bouton_vert.png") no-repeat scroll 0px 0px transparent;
}

div#Content div.PageButton a:hover {
	color: <?php echo $blanc; ?>!important;
	text-decoration: none!important;
	background: url("images/bouton_vert.png") no-repeat scroll 0px -43px transparent;
}

div#Content hr.SeparateurPlein {
	height: 2px;
	width: 100%;
	border: none;
	background-color: <?php echo $gris_bordure; ?>;
	margin: 0 0 24px 0;
}

div#Content h2.ListTitle {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 20px;
	line-height: 24px;
	text-transform: uppercase;
	float: left;
	margin-bottom: 6px;
	color: <?php echo $gris_fonce; ?>;
}

div#Content div#RightContent h4.Legende {
	text-transform: uppercase;
	font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_fonce; ?>;
	font-size: 12px;
	line-height: 15px;
	float: left;
	padding-bottom: 84px;
}

div#Content div#RightContent div.ItemLegende {
	text-transform: uppercase;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_fonce; ?>;
	font-size: 12px;
	line-height: 15px;
	float: left;
	padding-left: 24px;
	padding-bottom: 10px;
	margin-top: -6px;
}

div#Content div#RightContent div.ItemLegende img {
	margin-left: 3px;
}

div#Content div#ListeItems {

}

div#Content div#ListeItems div.ListeItemWrap {
	margin-bottom: 17px;
	position: relative;
}

div#Content div#ListeItems div.ListeItemWrap div.ImageCategorie {
	position: absolute;
	right: 0;
	bottom: 0;
	width: 52px;
	height: 52px;
}

div#Content div#ListeItems div.ListeItemWrap div.ItemLabel,div#Content div#LeftContent div.ItemImage div.ItemLabel {
	position: absolute;
	left: -2px;
	top: 10px;
	width: 142px;
	padding-right: 2px;
	height: 42px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	text-transform: uppercase;
	font-size: 13px;
	line-height: 42px;
	text-align: center;
}

div#Content div#ListeItems div.ListeItemWrap div.LabelComplete, div#Content div#LeftContent div.ItemImage div.LabelComplete {
	background: url("images/label_complete.png") no-repeat scroll left top transparent;
	color: <?php echo $blanc; ?>;
}

div#Content div#ListeItems div.ListeItemWrap div.LabelPast, div#Content div#LeftContent div.ItemImage div.LabelPast {
	background: url("images/label_past.png") no-repeat scroll left top transparent;
	color: <?php echo $blanc; ?>;
}


div#Content div#ListeItems div.ListeItem {
	position: relative;
}

div#Content div#ListeItems div.ListeItem div.ItemImage {
	width: 176px;
	height: 143px;
	border: 1px solid <?php echo $gris_bordure; ?>;
	float: left;
	background-color: <?php echo $gris_fond_bte; ?>;
}

div#Content div#ListeItems div.ListeItem div.ItemImage a {
	text-decoration: none!important;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu {
	display: table;
	height: 1%;
	padding-left: 14px;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu div.CategoriePost {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_pale; ?>;
	font-size: 15px;
	line-height: 18px;
	margin-bottom: 12px;
	text-transform: uppercase;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu h2 {
	text-transform: uppercase;
	margin-bottom: 6px;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu h2 a {
	text-decoration: none!important;
	color: <?php echo $gris_fonce; ?>;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu h2 a:hover {
	text-decoration: none!important;
	color: <?php echo $vert_hover; ?>;
}

div#Content div.ListeNouvelles div.ListeItem div.ItemContenu h2 {
	margin-bottom: 3px!important;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu div.ExtraitActivite p {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_pale; ?>;
	font-size: 14px;
	line-height: 17px;
	margin-bottom: 12px;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu div.DateActivite {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_pale; ?>;
	text-transform: uppercase;
	font-size: 16px;
	line-height: 20px;
	margin-bottom: 0px;
}

div#Content div#ListeItems div.ListeItem div.ItemContenu div.LieuActivite {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	color: <?php echo $gris_pale; ?>;
	text-transform: uppercase;
	font-size: 12px;
	line-height: 15px;
	margin-bottom: 0px;
}

div#Content div.ListeEvenements div.ListeItem {
	border: 4px solid <?php echo $gris_bordure; ?>;
	padding: 11px 19px 40px 19px;
	min-height: 186px;
}

div#Content div.ListeNouvelles div.ListeItem {
	border-bottom: 2px dotted <?php echo $gris_bordure; ?>;
	padding: 0px 19px 11px 19px;
	margin-bottom: 38px;
}

div#Content div#ListeItems div.ListeItem a.MoreDetails {
	background: url("images/bouton_mini_plus.png") no-repeat scroll right center transparent;
	float: right;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 12px;
	line-height: 32px;
	color: <?php echo $gris_fonce; ?>;	
	text-decoration: none;
	text-transform: uppercase;
	padding-right: 24px;
	position: absolute;
	bottom: 6px;
	right: 40px;
	display: block;
}

div#Content div.ListeNouvelles div.ListeItem a.MoreDetails {
	position: static!important;
	margin-right: 30px;
}

div#Content div.ListeNouvelles div.ListeItem div.PostDate {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 12px;
	line-height: 15px;
	text-transform: uppercase;
	color: <?php echo $gris_pale; ?>;
	margin-bottom: 16px;
}

div#Content div.ListeNouvelles div.ListeItem div.PostDate span {
	color: <?php echo $vert_fonce; ?>;
}

div#Content div#ListeItems div.ListeItem a.MoreDetails:hover {
	color: <?php echo $vert_hover; ?>;	
	text-decoration: none;
}

div#Content div.ListeEquipe div.ListeItem div.CategoriePost {
	font-family: Arial, Helvetica, Sans-serif;
	font-weight: bold;
	font-size: 16px;
	line-height: 20px;
	text-transform: uppercase;
	color: <?php echo $gris_fonce; ?>;
	margin-bottom: 18px;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem {
	border-bottom: 2px dotted <?php echo $gris_bordure; ?>;
	padding: 0px 0px 11px 0px;
	margin-bottom: 24px;
}

div#Content div#FullContent div.ListeEquipe div.LastListItem div.ListeItem {
	border: none;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.CategoriePost img {
	margin-left: 3px;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.ItemImage {
	width: 30%;
	padding-bottom: 18.5%;
	height: 0px;
	margin-bottom: 13px;
	float: left;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.ItemContenu {
	padding-left: 3.78947%;
	width: 65.21052%;
	float: right;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.Titres {
	margin-bottom: 30px;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.TitreMembre {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 18px;
	text-transform: uppercase;
	color: <?php echo $gris_pale; ?>;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.CourrielMembre {
	margin-bottom: 6px;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.Telephones {
	margin-bottom: 12px;
}

div#Content div#FullContent div.ListeEquipe div.ListeItem div.Telephones p {
	margin-bottom: 0;
	line-height: 36px;
}
	
/* Pagination */

div#Content div.PostsOf {
	float: right;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 12px;
	line-height: 16px;
	padding-top: 7px;
	color: <?php echo $gris_fonce; ?>;
}

div.Pagination {
	float: left;
	padding-top: 7px;
}

div.Pagination .wp-paginate a, div.Pagination .wp-paginate span {
	border: none;
	background: none;
	text-decoration: none!important;
	color: <?php echo $gris_fonce; ?>!important;
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 16px;
	padding: 5px 8px;
	font-weight: bold;
	color: <?php echo $vert_fonce; ?>;
}

div.Pagination .wp-paginate a:hover {
	color: <?php echo $vert_fonce; ?>!important;
	text-decoration: none!important;
}

div.Pagination .wp-paginate a.next, div.Pagination .wp-paginate a.prev {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-weight: normal;
	color: <?php echo $gris_fonce; ?>!important;
	padding: 5px 0;
}

div.Pagination .wp-paginate a.next:hover, div.Pagination .wp-paginate a.prev:hover {
	color: <?php echo $vert_fonce; ?>!important;
}

div.Pagination .wp-paginate a.next {
	padding-left: 8px;
}

div.Pagination .wp-paginate a.prev {
	padding-right: 8px;
}

div.Pagination .wp-paginate span.current {
	color: <?php echo $blanc; ?>!important;
	background-color: <?php echo $vert_fonce; ?>;
}

div.Pagination .wp-paginate span.title {
	display: none;
}

/* Single Blog */

div#Content div.template-single-nouvelle {
	padding-bottom: 48px;
}

div#Content div#LeftContent {
	float: left;
	width: 26.7%;
	margin-bottom: 48px;
}

div#Content div.template-single-nouvelle div#LeftContent div.ItemImage {
	border: 1px solid <?php echo $gris_bordure; ?>;
	background-color: <?php echo $gris_fond_bte; ?>;
	width: 99.2126%;
	padding-bottom: 103%;
	height: 0px;
	margin-bottom: 24px;
}

div#Content div#LeftContent div.MoreInformationWrap {
	margin-left: 10.0%;
}


div#Content div#RightContent {
	float: right;
	width: 68.8%;
	padding-left: 4.2%;
	padding-top: 0;
}

div#Content div#RightContent div.PostDate {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 17px;
	text-transform: uppercase;
	color: <?php echo $gris_pale; ?>;
	margin-bottom: 16px;
}

div#Content div#RightContent div.PostDate span {
	color: <?php echo $vert_fonce; ?>;
}

div#Content div.ShareSocial {
	margin-bottom: 36px;
}

div#Content div.ShareSocial a {
	width: 24px;
	height: 24px;
	display: block;
	float: left;
	margin-right: 15px;
}

div#Content div.ShareSocial a.ShareFacebook {
	background: url("images/facebook-petit.png") no-repeat scroll 0px 0px transparent;
}

div#Content div.ShareSocial a.ShareFacebook:hover {
	background: url("images/facebook-petit.png") no-repeat scroll 0px -25px transparent;
}

div#Content div.ShareSocial a.ShareGooglePlus {
	background: url("images/googleplus-petit.png") no-repeat scroll 0px 0px transparent;
}

div#Content div.ShareSocial a.ShareGooglePlus:hover {
	background: url("images/googleplus-petit.png") no-repeat scroll 0px -25px transparent;
}

div#Content div.ShareSocial a.ShareLinkedIn {
	background: url("images/linkedin-petit.png") no-repeat scroll 0px 0px transparent;
}

div#Content div.ShareSocial a.ShareLinkedIn:hover {
	background: url("images/linkedin-petit.png") no-repeat scroll 0px -25px transparent;
}

div#Content div.template-single-nouvelle div#RightContent div.PreviousLink {
	float: left;
}

div#Content div.template-single-nouvelle div#RightContent div.NextLink {
	float: right;
}

div#Content div.template-single-nouvelle div#RightContent div.BlogPostWrap {
	border-bottom: 2px dotted <?php echo $gris_bordure; ?>;
	margin-bottom: 24px;
}

div#Content div.RetourBlogue {
	width: 100%;
	text-align: right;
	border-bottom: 2px solid <?php echo $gris_pale; ?>;
}

div#Content div.RetourBlogue a, div.TermNavigation div.NextLink a, div.TermNavigation div.PreviousLink a {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 14px;	
	line-height: 30px;
	text-decoration: none!important;
	color: <?php echo $gris_fonce; ?>;
}

div#Content div.RetourBlogue a, div.TermNavigation div.PreviousLink a {
	background: url("images/mini_previous.png") no-repeat left center transparent;
	padding-left: 10px;
}

div.TermNavigation div.NextLink a {
	background: url("images/mini_next.png") no-repeat right center transparent;
	padding-right: 10px;
}

div#Content div.template-single-nouvelle div.RetourBlogue a:hover, div.TermNavigation div.NextLink a:hover, div.TermNavigation div.PreviousLink a:hover {
	text-decoration: none;
	color: <?php echo $vert_hover; ?>!important;
}

div#Content div.SendAction a, div#Content div#LeftContent div.DownloadFile a {
	font-family: Arial, Helvetica, Sans-serif;
	font-size: 16px;	
	line-height: 36px;
	padding-left: 44px;
	color: <?php echo $vert_fonce; ?>;
	text-decoration: none;
	display: block;
	height: 36px;
	margin-bottom: 6px;
}

div#Content div#LeftContent div.DownloadFile {
	text-align: center;
	margin: 0 10px 44px 10px;
}

div#Content div#LeftContent div.DownloadFile a {
	padding-left: 0px;
	padding-right: 41px;
	background: url("images/download_pdf.png") no-repeat right center transparent;
	text-align: center;
}

div#Content div.SendEmail a {
	background: url("images/send_email.png") no-repeat left center transparent;
}

div#Content div.SendPrinter a {
	background: url("images/send_printer.png") no-repeat left center transparent;
}

div#Content div.SendAction a:hover, div#Content div#LeftContent div.DownloadFile a:hover {
	color: <?php echo $vert_fonce; ?>;
	text-decoration: underline;
}

div#Content a.FlecheSlide {
	background: url("images/mobile/bouton_fleche_sidebar.png") no-repeat left top transparent;
	display: none;
	width: 19px;
	height: 19px;
}

div#Content a.FlecheSlide.Open {
	background-position: right -20px;
}

div#Content div#LeftContent div.ShareSocial, div#Content div#LeftContent div.SendAction {
	display: none;
}

@media screen and (min-width: 641px) and (max-width: 949px) {
	
	div#Content div.MoreInformation {
		font-size: 13px;
	}
	
	div#Content div.PageButton {
		width: 100%;
	}
	
	div#Content div.PageButton a {
		width: 85%;
		padding-right: 15%;
		font-size: 12px;
		background-position: right top;
	}

}

@media screen and (min-width: 641px) and (max-width: 760px) {

	div#Content div#ListeItems div.ListeItem div.ItemImage {
		float: none;
		display: block;
		margin-bottom: 24px;
	}
	
	div#Content div#ListeItems div.ListeItem div.ItemContenu {
		padding-left: 0px;
	}

}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#PreContent div#LeftPreContent {
		width: 100%;
	}

	div#PreContent div#LeftPreContent h1, div#Content h2.ListTitle {
		width: 100%;
		border-bottom: 2px solid <?php echo $gris_footer; ?>;
		text-align: center;
		padding-bottom: 6px;
		margin-bottom: 24px;
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
		font-size: 20px;
		line-height: 24px;
		text-transform: uppercase;
		color: <?php echo $gris_fonce; ?>;		
	}

	div#PreContent div#RightPreContent {
		display: none;
	}

	div.PostsOf {
		display: none;
	}
	
	div#Content div#Sidebar div.InputBlock {
		display: none;
	}
	
	div#Content hr.SeparateurPlein {
		display: none;
	}
	
	div#Content div#Sidebar {
		width: 100%;
		max-width: 320px;
		float: none;
		margin: 0 auto;
	}
	
	div#Content div#Sidebar div.BlockTitle {
		padding-left: 18px;
		position: relative;
		font-size: 16px;
	}
	
	div#Content div#Sidebar div#CategoryBlock {
		background-color: <?php echo $gris_pale; ?>;
		padding-left: 0px;
	}
	
	div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap {
		padding: 18px;
		background-color: <?php echo $gris_fond_bte; ?>;
		display: none;
	}
	
	div#Content div#Sidebar div.FormWrap {
		display: none;
	}
	
	div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li a {
		font-size: 13px;
	}
	
	div#Content a.FlecheSlide { 
		display: block;
		position: absolute;
		top: 14px;
		right: 18px;
	}
	
	div#Content div#ArchivesBlock a.FlecheSlide {
		right: -10px;
	}
	
	div#Content div#Sidebar div#CategoryBlock div#CategoryListWrap ul li {
		display: inline-block;
		width: 50%;
	}
	
	div#Content div#Sidebar div#ArchivesBlock  {
		display: block;
	}
	
	div#RightContent {
		float: none!important;
		width: 100%!important;
		padding-left: 0px!important;
	}
	
	div#Content div#ListeItems div.ListeItem div.ItemImage {
		display: none;
	}
	
	div#Content div#ListeItems div.ListeItem div.ItemContenu div.CategoriePost {
		font-size: 14px;
	}
	
	div#Content div.ListeNouvelles div.ListeItem div.PostDate {
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	}
	
	div.MoreInformationWrap {
		display: none;
	}
	
	div#Content  div#LeftContent div.ItemImage {
		max-width: 254px;
	}
	
	div#Content  div#RightContent div.BlogPostWrap h1, div#Content div#RightContent div.BlogPostWrap div.PostDate {
		text-align: center;
	}
	
	div.NextLink {
		display: none;
	}
	
	div#Content div#RightContent div.BlogPostWrap {
		border-bottom: none!important;
		margin-bottom: 0px!important;
	}
	
	div#Content div#RightContent {
		margin-bottom: 24px!important;
		padding-top: 18px;
		/*border-top: 1px solid <?php echo $gris_footer; ?>;*/
	}
	
	div.BlogPostWrap div.ShareSocial, div#RightContent div.SendAction {
		display: none;
	}
	
	div#Content div#LeftContent {
		float: none;
		width: 100%;
		max-width: 640px;
		margin: 0 auto;
	}
	
	div#Content div#LeftContent div.ShareSocial, div#Content div#LeftContent div.SendAction {
		display: block;
	}
	
	div#Content div.template-single-nouvelle div#LeftContent div.ItemImage {
		padding-bottom: 0px;
		max-height: 264px;
		height: 100%;
		margin: 0 auto 24px;
	}
	
	div#Content div.template-single-nouvelle div#LeftContent div.DownloadFile {
		max-width: 250px;
		margin: 0 auto;
	}
	
	div#Content div.SendEmail a {
		max-width: 160px;
		margin: 0 auto 24px;
	}
	
	div#Content div#LeftContent div.SendAction {
		padding-top: 24px;
		border-top: 2px dotted <?php echo $gris_pale; ?>;
	}
	
	div#Content div#LeftContent div.ShareSocial {
		text-align: center;
		margin-bottom: 12px;
	}
	
	div#Content div#LeftContent div.ShareSocial a {
		display: inline-block;
		float: none;

	}
	
	div#Content div.RetourBlogue {
		display: none;
	}
	
	div#Content div#RightContent h4.Legende {
		padding-bottom: 12px;
		float: none;
		display: block;
		text-align: center;
	}
	
	div#Content div#RightContent div.ContainerLegende {
		width: 100%;
		max-width: 320px;
		margin: 0 auto 24px;
	}
	
	div#Content div#RightContent div.ContainerLegende div.ItemLegende{ 
		width: 50%;
		text-align: center;
		padding-left: 0px;
	}
	
	div#Content div#ListeItems div.ListeItem div.ItemContenu div.DateActivite {
		font-size: 14px;
	}
	
	div#Content div#ListeItems div.ListeItem div.ItemContenu div.LieuActivite {
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
		padding-right: 12px;
	}
	
	div#Content div#ListeItems div.ListeItem a.MoreDetails {
		display: none;
	}
	
	div#Content div#ListeItems div.ListeItemWrap {
		margin-bottom: 0px;
	}
	
	div#Content div.ListeEvenements div.ListeItem {
		padding: 12px 12px 24px 6px;
		border-right: none;
		border-left: none;
		border-bottom: none;
		border-top: 2px dotted <?php echo $gris_pale; ?>;
	}
	
	div#Content div#ListeItems div.ListeItemWrap div.ItemLabel {
		right: -1px;
		bottom: 20px;
		top: auto;
		left: auto;
	}
	
	div#Content div.ListeEvenements div.LastListItem {
		border-bottom: 2px dotted <?php echo $gris_pale; ?>;
		margin-bottom: 24px!important;
	}	
	
	div#Content div.template-equipe div#FullContent div.ListeEquipe div.ListeItem div.ItemImage {
		float: none;
		display: block;
		width: 45%;
		padding-bottom: 27.7%;
	}
	
	div#Content div#FullContent div.ListeEquipe div.ListeItem div.ItemContenu {
		float: none;
		width: 100%;
		padding-left: 0px;
	}
	
}

/*****************************************/
/* TEMPLATE COMMUN (INTERNE)						 */
/*****************************************/

div#Content div.template-commun {
	padding-bottom: 48px;
}

div#Content div.template-commun div.MoreInformationWrap {
	float: right;
	margin-bottom: 24px;
}

div#Content div.template-commun p, div#Content div.template-commun ul {
	width: 72%;
}

div#Content blockquote {
	width: 82.631%;
	padding: 24px 30px 1px 30px;
	margin-bottom: 24px;
	background-color: <?php echo $gris_fond_bte; ?>;
	position: relative;
}

div#Content blockquote p, div#Content div.template-commun blockquote p {
	width: 76.433%;
}

div#Content blockquote div.ImageCitation {
	position: absolute;
	top: -30px;
	right: 35px;
}

div#Content blockquote div.ImageCitation a {
	text-decoration: none!important;
}

div.HiddenImage {
	display: none!important;
}

/*****************************************/
/* TEMPLATE PLAN DE SITE								 */
/*****************************************/

div#Content div.template-plan-de-site {
	margin-bottom: 0px;
	padding-bottom: 0px;
}

/*****************************************/
/* FORMULAIRE ACTIVIT�					 */
/*****************************************/

div#Content div.BoutonFormulaire {
	display: none;
	background-color: <?php echo $gris_pale; ?>;
	width: 94%;
	height: 72px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 16px;
	line-height: 72px;
	padding-left: 6%;
	color: <?php echo $blanc; ?>;
	text-transform: uppercase;	
	position: relative;
}

div#Content div.BoutonFormulaire a.FlecheSlide {
	top: 26px!important;
}

div#Content div.Formulaire div.SectionEntreprise label, div#Content div.Formulaire div.SectionResponsable label {
	display: block;
}

div#Content div.Formulaire div.SectionEntreprise p, div#Content div.Formulaire div.SectionResponsable p {
	float: left;
	width: 40%;
	margin-right: 5%;
}

div#Content div.Formulaire div.SectionEntreprise input, div#Content div.Formulaire div.SectionResponsable input {
	width: 90%;
}

div#Content div.Formulaire div.SectionResponsable p.Salutation { 
	float: none;
	clear: both;
}

div#Content div.Formulaire textarea#commentaires {
	width: 81%;
	margin-bottom: 24px;
}

div#Content div.Formulaire p.Salutation input, div#Content div.Formulaire p.RParticipe input {
	margin-top: 2px;
	float: left;
	width: 4%;
	height: 14px;
}


div#Content div.Formulaire p.Salutation label, div#Content div.Formulaire p.RParticipe label  {
	float: left;
	margin-right: 24px;
}

div#Content div.Formulaire p.RParticipe input {
	margin-top: 14px;
}

div#Content div.Formulaire p.RParticipe label {
	margin-top: 12px;
	text-transform: uppercase;
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 16px;	
}

div#Content div.Formulaire div.SectionTarification {
	border: 1px solid transparent; 
}

div#Content div.Formulaire div.SectionTarification div.BoiteTarif {

	float: left;
	width: 20%;
	margin-right: 5%;
}

div#Content div.Formulaire div.SectionTarification label{
	display: block;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;	
	font-size: 14px;
	line-height: 16px;
	color: <?php echo $noir; ?>;	
		text-transform: uppercase;
		margin-bottom: 6px;
}

div#Content div.Formulaire div.SectionTarification div.FondInput {
	background: url("images/fond_tarif.png") no-repeat center top transparent;
	background-size: cover;
	background-image: none\9;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
	src='<?php echo $template_url; ?>/images/fond_tarif.png',
	sizingMethod='scale');	
	width: 100%;
	text-align: center;
	line-height: 42px;
}

div#Content div.Formulaire div.SectionTarification div.FondInput input {
	border: none!important;
}

div#Content div.Formulaire div.SectionResponsable p.DInput input.CTelephone {
	float: left;
	margin-right: 5%;
	width: 65%;
}

div#Content div.Formulaire div.SectionResponsable p.DInput input.CPoste {
	float: left;
	width: 14%;
}

div#Content div.Formulaire div.SectionEntreprise,div#Content div.Formulaire div.SectionResponsable {
	margin-bottom: 24px;
}

div#Content div.Formulaire div.ParticipantBox {
	background-color: #e1e1e1;
	border: 4px solid <?php echo $gris_footer; ?>;
	padding: 2%;
	position: relative;
	margin-bottom: 24px;
}

div#Content div.Formulaire div.ParticipantBox h3 {
	font-size: 14px;
	line-height: 16px;
	color: <?php echo $noir; ?>;
	float: left;
	margin-right: 24px;
}

div#Content div.Formulaire div.ParticipantBox div.ZoneChamps {
	/*height: 62px;
	margin-bottom: 12px;*/
}

div#Content div.Formulaire div.ParticipantBox div.ZoneChamps div.BoiteChampsParticipant {
	float: left;
	width: 27%;
	margin-right: 3%;
}

div#Content div.Formulaire div.ParticipantBox div.ZoneChamps label {
	display: block;
	width: 95%;
	padding: 0px 6px;
}

div#Content div.Formulaire div.ParticipantBox div.ZoneChamps input {
	width: 95%;
}

div#Content div.Formulaire div.ParticipantBox p.DInput {
	width: 41.5%;
}

div#Content div.Formulaire div.ParticipantBox  label.LabelTelephone {
	margin-right: 8%;
}

div#Content div.Formulaire div.ParticipantBox p.DInput input.CTelephone {
	float: left;
	margin-right: 7.5%;
	width: 60%;
}

div#Content div.Formulaire div.ParticipantBox p.DInput input.CPoste {
	float: left;
	width: 14.5%;
}

div#Content div.Formulaire p.BoutonSupression {
	position: absolute!important;
	top: 0px!important;
	right: 12px!important;
	width: 12px!important;
	height: 13px!important;
	min-height: 13px!important;
	margin-bottom: 0px;
}

div#Content div.Formulaire p.BoutonSupression a {
	background: url('./images/supression.png') no-repeat scroll left top transparent!important;
	width: 12px!important;
	height: 13px!important;
	padding: 2px!important
}

div#Content div.Formulaire p.BoutonAjout {
	height: 14px!important;
	margin-bottom: 10px;
}

div#Content div.Formulaire p.BoutonAjout a {
	background: url('./images/bouton_mini_plus.png') no-repeat scroll right top transparent!important;
	height: 14px!important;
	padding: 2px 18px 2px 2px!important
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 14px;	
	color: <?php echo $gris_fonce; ?>!important;
}

div#Content div#FormulaireActivite hr {
	margin-bottom: 24px;
	margin-top: 24px;
}

div#Content div.Formulaire p.Mini {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;	
	font-size: 12px;
	margin-bottom: 0px;
	text-transform: uppercase;
}

div#Content div.Formulaire p.Medium {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;	
	font-size: 13px;
	margin-bottom: 0px;	
	text-transform: uppercase;	
}

div#Content div.Formulaire p span.Total {
	font-family: OpenSansExtraBold, Arial, Helvetica, Sans-serif;	
	font-size: 14px;
	text-transform: uppercase;	
}

div#Content div.Formulaire p.ModePaiement input {
	float: left;
	height: 14px;
	margin-right: 24px;
	height: 46px;
	line-height: 46px;	
	clear: left;	
}

div#Content div.Formulaire p.ModePaiement label {
	float: left;

	width: 80%;
	height: 42px;
	margin-bottom: 18px;
	line-height: 50px;
	text-transform: uppercase;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;		
}

div#Content div.Formulaire p.ListingConference input {
	float: left;
	margin-right: 24px;
	width: 5%;
	clear: left;	
}

div#Content div.Formulaire div.ListingConference {
	border: 1px solid transparent;
}

div#Content div.Formulaire p.ListingConference label {
	float: left;

	margin-bottom: 12px;
	text-transform: uppercase;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;		
}

div#Content div.Formulaire div.ZonePaiement {
	margin-bottom: 24px;
	border: 1px solid transparent;
}

/* Page single activit�s */
div#Content div.template-single-activite {
	margin-bottom: 48px;
}

div#Content div.template-single-activite div#LeftContent div.ItemImage{
	width: 99%;
	height: 0px;
	padding-bottom: 80.5%;
	border: 1px solid <?php echo $gris_bordure; ?>;
	background-color: <?php echo $gris_fond_bte; ?>;
	position: relative;
}

div#Content div.template-single-activite div#LeftContent div.ImageCategorie {
	position: absolute;
	bottom: -2px;
	right: 0px;
}

div#Content div.template-single-activite div#LeftContent  div.LeftContentWrap {
	width: 88%;
	padding: 6%;
	background-color: <?php echo $gris_fond_bte; ?>;
}

div#Content div.template-single-activite div#LeftContent {
	text-transform: uppercase;
}


div#Content div.template-single-activite div.LeftContentWrap a {
	background: url('./images/bouton_vert.png') no-repeat right top <?php echo $vert_pale; ?>;
	display: inline-block;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 42px;
	height: 42px;
	font-size: 16px;
	color: <?php echo $blanc; ?>;
	text-decoration: none!important;
	padding-right: 36px;
	padding-left: 18px;
}

div#Content div.template-single-activite div.LeftContentWrap a:hover {
	background-color: <?php echo $gris_pale; ?>;
	background-position: right -43px;
	color: <?php echo $blanc; ?>;
	text-decoration: none!important;
}

div#Content div.template-single-activite div.SendEmail a{
	max-width: 220px!important;
}

div#Content div.template-single-activite div.ShareSocial {
	margin-bottom: 48px!important;
}

div#Content div.template-single-activite hr {
	border-bottom: 2px dotted <?php echo $gris_footer; ?>;
	background-color: transparent;
}

div#Content div.template-single-activite div.RetourBlogue {
	margin-bottom: 48px;
}

div#Content p.PFacture {
	display: none!important;
}

@media screen and (min-width: 640px) and (max-width: 949px) { 

	div#Content div.template-single-activite div#LeftContent div.LeftContentWrap p {
		font-size: 14px;
	}
	
	div#Content div.template-single-activite div#LeftContent a {
		font-size: 11px;
	}

}
	
@media screen and (min-width: 320px) and (max-width: 640px) { 

	div#Content div.template-single-activite div#RightContent h1, div#Content div.template-single-activite div#RightContent div.PostDate {
		text-align: center;
	}

	div#Content div.BoutonFormulaire { 
		display: block;
	}

	div#Content div.template-single-activite div#LeftContent {
		float: none;
		margin: 0 auto;
	}

	div#Content div.template-single-activite div#LeftContent div.ItemImage {
		float: none;
		margin: 0 auto;	
		padding-bottom: 0%;
		width: 254px;
		height: 206px;
	}
	
	div#Content div.template-single-activite div#LeftContent div.LeftContentWrap {
		background-color: transparent;
		text-align: center;
	}

	div#Content div.template-single-activite div.ShareSocial {
		display: none;
	}

	div#Content div.Formulaire textarea#commentaires {
		width: 96%;
		margin-bottom: 24px;
	}
	
	
	div#FormulaireActivite h2 {
		text-align: center;
	}
	
	div#Content div.Formulaire div.SectionEntreprise input, div#Content div.Formulaire div.SectionResponsable input {
		width: 96%;
	}

	div#Content div.Formulaire p.Salutation input, div#Content div.Formulaire p.RParticipe input {
		margin-top: 2px;
		float: left;
		width: 4%;
		height: 14px;
	}	
	
	div#Content div.Formulaire div.SectionResponsable p.DInput input.CTelephone {
		margin-right: 10%;
	}
	
	div#Content div.Formulaire label.LabelTelephone {
		margin-right: 14%;
	}
	
	div#Content div.Formulaire p.RParticipe input {
		margin-top: 14px;
	}
	
	div#Content div.Formulaire p.ListingConference input {
		float: left;
		height: 14px;
		margin-right: 24px;
		width: 5%;
		clear: left;	
	}

	div#Content div.Formulaire div.ListingConference {
		border: 1px solid transparent;
	}

	div#Content div.Formulaire p.ListingConference label {
		float: left;

		margin-bottom: 12px;
		text-transform: uppercase;
		font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;		
	}	
	
	div#Content div.Formulaire div.SectionTarification div.BoiteTarif {
		width: 100%;
		margin-right: 0%;
		background: url("images/fond_tarif.png") no-repeat right top transparent;
		background-size: cover;
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/fond_tarif.png',
		sizingMethod='scale');		
		margin-bottom: 24px;
	}
	
	div#Content div.Formulaire div.SectionTarification label{
		display: inline-block;
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;	
		font-size: 13px;
		line-height: 42px;
		color: <?php echo $blanc; ?>;	
		text-transform: uppercase;
		margin-bottom: 6px;
		padding-left: 24px;
	}

	div#Content div.Formulaire div.SectionTarification div.FondInput {
		background-image: none!important;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/false_positive.png',
		sizingMethod='scale');		
		width: 30px;
		height: 42px;
		float: right;
		margin-right: 24px;
	}
	
	div#Content div.Formulaire p.BoutonSupression {
		position: absolute!important;
		top: 6px!important;
		right: 4px!important;
		width: 12px!important;
		height: 13px!important;
		min-height: 13px!important;
		margin-bottom: 0px;
	}

	div#Content div.Formulaire p.BoutonSupression a {
		background: url('./images/supression.png') no-repeat scroll left top transparent!important;
		width: 12px!important;
		height: 13px!important;
		padding: 2px!important
	}

	div#Content div.Formulaire p.BoutonAjout {
		height: 14px!important;
		margin-bottom: 10px;
	}

	div#Content div.Formulaire p.BoutonAjout a {
		background: url('./images/bouton_mini_plus.png') no-repeat scroll right top transparent!important;
		height: 14px!important;
		padding: 2px 18px 2px 2px!important
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 13px;
		line-height: 14px;	
		color: <?php echo $gris_fonce; ?>!important;
		height: 24px;
	}	
	
	div#Content div.ChargementAjax {
		float: none!important;
		margin: 0 auto!important;
	}
	
	div#Content div.Formulaire p.Salutation label, div#Content div.Formulaire p.RParticipe label {
		margin-right: 6px;
	}
	
	div#Content div.Formulaire div.ParticipantBox h3 { 
		display: block;
		float: none;
	}
	
	div#Content div.Formulaire div.ParticipantBox div.ZoneChamps div.BoiteChampsParticipant {
		float: none;
		width: 100%
	}
	
	div#Content div.Formulaire div.ParticipantBox p.DInput {
		width: 100%;
	}
	
	div#Content div.Formulaire div.ParticipantBox p.DInput input.CPoste {
		width: 20%;
	}
	
	div#Content div#Template div#FormulaireActivite p.Bouton {
		margin-bottom: 24px;
	}
	
}

/*****************************************/
/* RESULTATS DE RECHERCHE				 */
/*****************************************/

div#Content div.template-search {
	padding-bottom: 48px; 
}

div#Content div.template-search h1 {
	text-transform: uppercase;
	margin-bottom: 30px;
}

div#Content div.template-search h2.KeyWords {
	color: <?php echo $gris_pale; ?>;
	margin-bottom: 30px;
}

div#Content div.template-search h2 {
	text-transform: uppercase;
}

div#Content div.template-search a {
	color: uppercase;
	color: <?php echo $gris_fonce; ?>;
	text-decoration: none;
}

div#Content div.template-search a:hover {
	color: uppercase;
	color: <?php echo $vert_hover; ?>;
	text-decoration: none;
}

div#Content div.template-search p {
	margin-bottom: 36px;
}

div#Content div.template-search hr {
	border: none;
	height: 1px;
	background-color: <?php echo $gris_bordure; ?>;
	margin-bottom: 8px;
}

@media screen and (min-width: 320px) and (max-width: 640px) { 

	div#Content div.template-search div.ZoneRecherche {
		text-align: center;
	}
	
	div.Pagination {
		float: none;
		text-align: center;
	}

}

/*****************************************/
/* PAGE 404						 */
/*****************************************/

div#Content div.template-404 {
	padding-bottom: 48px; 
}

div#Content div.template-404 h1 {
	text-transform: uppercase;
}

/*****************************************/
/* NOUS JOINDRE							 */
/*****************************************/

/* Captcha */

#recaptcha_area input[type="text"] {
	height: 14px!important;
	line-height: 16px!important;
}

/* **** MapPress **** */

div.template-contact {
	margin-bottom: 48px;
}	

div#Content div.mapp-container {
	margin-bottom: 24px!important;
}

div#mapp0_poweredby {
	display: none!important;
}

div#Content div.LeftContactContent {
	float: left;
	width: 38.31578%; /* 364/950 */
	margin-right: 5.05263%; /* 48/950 */
	margin-bottom: 24px;	
}

div#Content div.RightContactContent {
	float: left;
	width: 56.63157%; /* 538/950 */
	margin-bottom: 24px;
}


div#Content div.RightContactContent p.BoutonGoogleMapMobile {
	display: none!important;
}

div#Content div.RightContactContent div.CadreGoogleMap {
	border: 1px solid <?php echo $gris_footer; ?>;
	width: 100%;
}

div#Content div.Formulaire {
	/*margin-bottom: 48px!important;*/
}

div#Content div.Formulaire div.ZoneGauche {
	float: left;
	width: 35.07972%; /* 308/878 */
}

div#Content div.Formulaire div.ZoneMilieu {
	float: left;
	width: 35.07972%; /* 308/878 */
}

div#Content div.Formulaire div.ZoneDroite {
	float: left;
	width: 29.84054%; /* 262/878 */
	min-height: 316px;
	position: relative;	
}

div#Content div.Formulaire div.ZoneGauche label {
	display: block;
	margin-top: 3px;
}

div#Content div.Formulaire div.ZoneGauche div.selectbox {
	width: 84%!important;
}	

div#Content div.Formulaire div.ZoneGauche input {
	width: 80%;
}

div#Content div.Formulaire div.ZoneMilieu  textarea {
	width: 95%;
	height: 100px;
}

div#Content div.Formulaire div.ZoneDroite p.Bouton  {
	position: absolute;
	bottom: 0px;
	right: 0px;
}

/* Google Map */

div#Full_Google_Map {
	width: 100%;
	height: 380px;
	/*max-height: 825px;*/
}

div#Full_Google_Map iframe {
	width: 100%;
	height: 100%;
}

div#Full_Google_Map img {
    max-width: none !important;
}

div#Full_Google_Map div p {
	margin-bottom: 12px!important;
}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content div.LeftContactContent {
		float: none;
		width: 100%;
		text-align: center;
	}
	
	div#Content div.RightContactContent {
		float: none;
		width: 100%;
		text-align: center;	
	}
	
	div#Content div.RightContactContent p.BoutonGoogleMapMobile {
		display: block!important;
	}

	div#Content div.CadreGoogleMap, div#Full_Google_Map {
		display: none;
	}

	div#Content div.Formulaire div.ZoneGauche {
		float: none;
		width: 100%;
		margin-bottom: 24px;
	}
	
	div#Content div.Formulaire div.ZoneGauche  input {
		width: 97%!important;
	}
	
	div#Content div.Formulaire div.ZoneGauche div.selectbox {
		width: 100%!important;
	}

	div#Content div.Formulaire div.ZoneMilieu {
		float: none;
		width: 100%;
	}
	
	div#Content div.Formulaire div.ZoneMilieu textarea {
		width: 97%;
	}

	div#Content div.Formulaire div.ZoneDroite {
		float: none;
		width: 100%;
		min-height:	96px;
		height:	96px;
	}	
	
	div#Content div.Formulaire div.ZoneDroite p.Bouton {
		position: relative;	
		margin: 0 auto;
	}
	
}

/*****************************************/
/* NOUVELLES							 */
/*****************************************/

/*****************************************/
/* EMPLOIS								 */
/*****************************************/

/*****************************************/
/* FORMULAIRES							 */
/*****************************************/

div#Content div.template-pre-diagnostic {
	padding-bottom: 48px;
}

div#Content div.Formulaire {
	background-color: <?php echo $gris_fond_bte; ?>;
	padding: 36px;
}

div#Content form.wpcf7-form {
	background-color: <?php echo $gris_fond_bte; ?>;
	/*padding-bottom: 24px;*/
}

div#Content div.Formulaire div.DoubleLigne label {
	display: block;
}

div#Content div.Formulaire div.DoubleLigne p {
	float: left;
	width: 40%;
	margin-right: 5%;
}

div#Content div.Formulaire div.DoubleLigne input {
	width: 90%;
}

div#Content div.Formulaire div.DoubleLigne input#telephone {
	float: left;
	width: 65%;
	margin-right: 5%;
}

div#Content div.Formulaire  label.LabelTelephone{
	float: left;
	width: 65%;
	margin-right: 10%;
}

div#Content div.Formulaire div.DoubleLigne input#poste,  div#Content div.Formulaire label.LabelPoste {
	float: left;
	width: 15%;
}

div#Content div.Formulaire h1 {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
}

div#Content div.Formulaire h2 {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
}

div#Content  div.Formulaire label {
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 15px;
	line-height: 21px;
	color: <?php echo $gris_fonce; ?>;
}

div#Content div.template-pre-diagnostic div.Formulaire div.partie1 label, div#Content div.template-pre-diagnostic div.Formulaire div.partie2 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie3 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie4 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie5 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie6 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie7 label  {
	display: inline-block;
	width: 67.5%;
	margin-right: 5%;
	text-indent: -21px;
	margin-left: 2.5%;
	margin-bottom: 12px;
}

div#Content div.Formulaire select {
	float: right;
	width: 25%;
}

div#Content div.Formulaire hr {
	background-color: <?php echo $gris_footer; ?>;
	clear: both;
	border: none;
	width: 100%;
	height: 1px;
	margin: 12px 0px 36px 0px;
}

div#Content div.Formulaire textarea {
	width: 98%;
}

div#Content div.Formulaire div.selectbox {
	float: right;
	width: 23%!important;
}

div#Content div.template-contact div.Formulaire div.selectbox {
	float: left;
}

div#Content div.Formulaire p.DInput {
	height: 62px;
}

div#Content div.Formulaire p.DInput span {
	display: block;
}

div#Content div.Formulaire p.DInput span.telephone span.wpcf7-not-valid-tip {
	top: 28px!important;
}


@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content form.wpcf7-form {
		/*padding-bottom: 24px;*/
	}
	
	div#Content div.ContactForm7Wrap {
		display: none;
	}	
	
	div#Content div.Formulaire {
		width: 94%!important;
		padding: 3% 3% 6% 3%;
	}
	
	div#Content div.template-single-activite div.Formulaire {
		display: none;
	}
	
	div#Content div.template-pre-diagnostic div.Formulaire div.DoubleLigne p, div#Content div.Formulaire div.SectionEntreprise p, div#Content div.Formulaire div.SectionResponsable p  {
		float: none;
		width: 100%;
		margin-right: 0%;
		margin-bottom: 12px;
	}	
	
	div#Content div.template-pre-diagnostic div.Formulaire div.partie1 label, div#Content div.template-pre-diagnostic div.Formulaire div.partie2 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie3 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie4 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie5 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie6 label , div#Content div.template-pre-diagnostic div.Formulaire div.partie7 label  {
		display: inline-block;
		width: 95%;
		margin-right: 0px%;
		text-indent: -5%;
		margin-left: 5%;
		margin-bottom: 12px;
	}
	
	div#Content div.Formulaire div.selectbox {
		float: none;
		width: 100%!important;
	}
	
	div#Content div.Formulaire textarea {
		width: 97%;
	}
	
}

/*****************************************/
/* R�ALISATIONS							 */
/*****************************************/

div#Content div#Realisations {
	width: 960px;
}

div#Content div#Realisations div.RealisationContent {
	float: left;
	margin-bottom: 24px;
	margin-right: 24px;
}

div#Content div#Realisations div.EndDiv {
	margin-right: 0px!important;
}

div#Content div#Realisations div.RealisationContent div.RealisationImage {
	width: 176px;
	height: 176px;
	margin-bottom: 12px;
}

/*Page single */

div#Content div#SingleRealisation {
	margin-bottom: 24px;
}


/*****************************************/
/* FOOTER								 */
/*****************************************/

div#ZoneInfolettreWrap {
	background-color: <?php echo $vert_pale; ?>;
	width: 100%;
	height: 76px;
}

div#ZoneInfolettre {
	max-width: 950px;
	width: 100%;
	height: 72px;
	margin: 0 auto;
}

div#ZoneInfolettre form {
	float: right;
}

div#ZoneInfolettre label {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 14px;	
	line-height: 72px;
	text-transform: uppercase;
	color: <?php echo $blanc; ?>;
	margin-right: 12px;

}

div#ZoneInfolettre input#courriel_infolettre {
	border: none;
	width: 200px;
	padding: 0px 6px;	
	height: 35px;
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 35px;
	color: <?php echo $gris_moyen; ?>;
}

div#ZoneInfolettre a.SubmitSpecial {
	background: url('./images/bouton_fleche_infolettre.png') no-repeat scroll left top transparent;
	display: block;
	width: 40px;
	height: 35px;
	float: right;
	margin-top: 18.5px;
	cursor: pointer;
}

div#ZoneInfolettre a.SubmitSpecial:hover {
	background-position: 0px -36px;
}

div#FooterWrap {
	background: url('./images/bg_fond_footer.jpg') repeat center top transparent;
	width: 100%;
	height: 233px;
	position: relative;
	z-index: 500;
}

div#Footer {
	width: 100%;
	max-width: 950px;
	height: 179px;
	padding-top: 54px;
	margin: 0 auto;
	position: relative;
	z-index: 400;	
	/*padding-bottom: 48px;*/
}

div#FondFooter {
	background: url('./images/fond_footer_new.jpg') no-repeat center top transparent;
	height: 233px;
	width: 100%;
	position: absolute;
	left: 0px;
	bottom: 0px;
	z-index: 100;
}

div#Footer a.BoutonMobileContact {
	display: none;
}

div#Footer div#InfoFooter {
	padding-bottom: 42px;
	border-bottom: 1px solid <?php echo $gris_footer; ?>;
}

div#Footer div#LogoFooter {
	float: left;
	width: 235px;
	height: 62px;
	margin-right: 50px;
}

div#Footer div#LogoFooter a {
	background: url('./images/logo_nb_new.png') no-repeat left top transparent;
	display: block;
	width: 235px;
	height: 62px;	
}

div#Footer div#FooterAdresse {
	float: left;
	width: 200px;
	margin-right: 50px;
	font-family: OpenSansRegular, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 18px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;
}

div#Footer div#FooterAdresse div.NomCompagnie {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	margin-bottom: 8px;	
}

div#Footer div#FooterContact {
	float: left;
	padding: 26px 30px 24px 0px;
	margin-right: 30px;
	font-family: OpenSansRegular, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 18px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;
	border-right: 1px solid <?php echo $gris_footer; ?>;
}

div#Footer div#FooterContact a {
	color: <?php echo $vert_pale; ?>;
}

div#Footer div#FooterContact a:hover {
	color: <?php echo $vert_fonce; ?>;
	text-decoration: underline;
}

div#Footer div#FooterMedia {
	float: left;
}

div#Footer div#FooterMedia div.SuivezNousFooter {
	font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
	font-size: 13px;
	line-height: 18px;
	color: <?php echo $blanc; ?>;	
	text-transform: uppercase;
	text-align: center;
}

div#Footer div#FooterMedia a.BoutonLinkedin {
	background: url('./images/bouton_linkedin_footer.png') no-repeat left top transparent;
	display: block;
	float: left;
	width: 42px;
	height: 42px;
	margin: 20px 18px 10px 18px;
}

div#Footer div#FooterMedia a.BoutonLinkedin:hover {
	background-position: 0px -43px;
} 

div#Footer div#FooterMedia a.BoutonYoutube {
	background: url('./images/bouton_youtube_footer.png') no-repeat left top transparent;
	display: block;
	float: left;	
	width: 42px;
	height: 52px;
	margin: 12px 18px 14px 18px;	
}

div#Footer div#FooterMedia a.BoutonYoutube:hover {
	background-position: 0px -53px;
}

div#Footer div#FooterNotes {
	font-family: OpenSansRegular, Arial, Helvetica, Sans-serif;
	font-size: 11px;
	line-height: 28px;
	color: <?php echo $gris_footer; ?>;
}

div#Footer div#FooterNotes div#Rights {
	float:left;
}

div#Footer div#FooterNotes a#PlanDuSite {
	float:left;
}

div#Footer div#FooterNotes a {
	color: <?php echo $gris_footer; ?>;	
	text-decoration: underline;	
}

div#Footer div#FooterNotes a:hover {
	color: <?php echo $blanc; ?>;	
	text-decoration: underline;
}

div#Footer div#FooterNotes div.Nmedia {
	float: right;
}
.titre_nouvelle_Accueil{
	color: <?php echo $vert_pale; ?>;	
	text-decoration: none;
}

/* MEDIA QUERIES FOOTER */

@media screen and (min-width: 641px) and (max-width: 949px) {

	div#ZoneInfolettre form {
		margin-right: 12px;
	}
	
	div#Footer {
		width: 98%;
		margin: 0 auto;
	}
	
	div#Footer div#LogoFooter {
		/*display: none;*/
		width: 20%;
		margin-right: 24px;		
	}
	
	div#Footer div#LogoFooter a {
		background: url("images/logo_nb_new.png") no-repeat left top transparent;
		background-size: contain!important;
		background-image: none\9;
		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
		src='<?php echo $template_url; ?>/images/logo_nb_new.png',
		sizingMethod='scale');
		width: 100%;
		height: 0px;
		padding-bottom: 26.38297%;
	}
	
	div#Footer div#FooterAdresse {
		/*width: 21.48227%;*/
		margin-right: 24px;	
		font-size: 11px;
		line-height: 16px;
	}
	
	div#Footer div#FooterContact {
		padding-right: 24px;
		margin-right: 24px;
		font-size: 11px;
		line-height: 16px;	
	}
	
	div#Footer div#FooterMedia a.BoutonLinkedin {
		margin-left: 6px;
		margin-right: 6px;	
	}
	
	div#Footer div#FooterMedia a.BoutonYoutube {
		margin-left: 6px;
		margin-right: 6px;
	}

}

@media screen and (min-width: 641px) and (max-width: 760px) { 

	div#Footer div#LogoFooter {
		display: none;
	}

}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#ZoneInfolettreWrap {
		background: url('./images/mobile/bande_infolettre.png') no-repeat right top <?php echo $vert_pale; ?>;
		background-size: contain!important;		
	}

	div#ZoneInfolettre form {
		float: none;
		max-width: 320px;
		margin: 0 auto;
	}

	div#ZoneInfolettre label {
		display: block;
		font-size: 13px;
		line-height: 18px;
		text-align: center;
		padding-top: 6px;
		margin-bottom: 6px;
		margin-right: 0px;
	}

	div#ZoneInfolettre input#courriel_infolettre {
		margin-left: 10.75%;
		float: left;
	}
	
	div#ZoneInfolettre a.SubmitSpecial {
		float: left;
		margin-top: 0px;
	}	
	
	div#FooterWrap {
		height: 216px;
	}
	
	div#Footer {
		padding-top: 24px;
	}
	
	div#FondFooter {
		background: url('./images/mobile/fond_footer.jpg') no-repeat left top <?php echo $gris_pale; ?>;	
		height: 216px;
	}
	
	div#Footer div#InfoFooter {
		padding-bottom: 12px;
	}
	
	div#Footer div#LogoFooter,div#Footer  div#FooterAdresse,div#Footer div#FooterContact {
		display: none;
	}
	
	div#Footer a.BoutonMobileContact {
		background-color: <?php echo $vert_fonce; ?>;
		display: block;
		max-width: 260px;
		width: 80%;
		height: 42px;
		margin: 0 auto;
		font-family: OpenSansBold, Arial, Helvetica, Sans-serif;
		font-size: 13px;	
		line-height: 42px;
		text-transform: uppercase;
		color: <?php echo $blanc; ?>;	
		text-align: center;
		text-decoration: none;
	}

	div#Footer a.BoutonMobileContact:hover{
		background-color: <?php echo $gris_moyen; ?>;
		text-decoration: none;
		color: <?php echo $blanc; ?>;
	}	
	
	div#Footer div#FooterMedia {
		float: none;
		max-width: 260px;
		width: 80%;
		margin: 12px auto 0px;		
	}
	
	div#Footer div#FooterMedia div.SuivezNousFooter {
		float: left;
		font-size: 16px;
		line-height: 68px;
	}
	
	div#Footer div#FooterMedia div.BoutonMediaWrap {
		float: right;
	}
	
	div#Footer div#FooterMedia a.BoutonLinkedin {
		background: url('./images/mobile/bouton_linkedin_footer.png') no-repeat left top transparent;
		width: 26px;
		height: 26px;
		margin: 20px 14px 14px;
	}
	
	div#Footer div#FooterMedia a.BoutonLinkedin:hover {
		background-position: 0px -27px;
	}
	
	div#Footer div#FooterMedia a.BoutonYoutube {
		background: url('./images/mobile/bouton_youtube_footer.png') no-repeat left top transparent;
		width: 30px;
		height: 37px;	
		margin: 12px 0px 14px 14px;
	}

	div#Footer div#FooterMedia a.BoutonYoutube:hover {
		background-position: 0px -38px;
	}	
	
	div#Footer div#FooterNotes {
		max-width: 310px;
		margin: 0 auto;
		font-size: 10px;
		text-align: center;
	}
	
	div#Footer div#FooterNotes div.Nmedia {
		clear: left;
		float: left;
	}
	
}

/* MEDIA QUERIES FOOTER */

/*****************************************/
/* CONTACT FORM 7						 */
/*****************************************/

div.wpcf7{
	position: relative;
}

div.EmploiUnePage div.wpcf7{
	display: none;
}

div#Content div#Contacts div.wpcf7{
	display: block;
}

div.wpcf7 div.HiddenInput {
	display:none;
}

/* Pour bouton file  */
div.FileSpecial input.file {
	display: none!important;
}

div.FileSpecial div input{
	#width: 232px!important;
	#display: block!important;
}

div.FileSpecial span.resume {
	float: left;
	height: 24px;
	overflow: hidden;
}

div.FileSpecial span.text {
	float: left;
	padding-left: 24px;
}

div.FileSpecial {
	height: 24px;
	line-height: 24px;
	margin-bottom: 3px;
}

div.FileSpecial input.wpcf7-file {
	#right: 0px!important;
}


div#Content div.Formulaire span.wpcf7-not-valid-tip {
	left: 5px!important;
	top: -5px!important;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 13px;	
	line-height: 18px;
	color: <?php echo $gris_moyen; ?>;
	width: 220px!important;
}

div.wpcf7-response-output {
	background-color: <?php echo $gris_fond_bte; ?>;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 13px;	
	line-height: 18px;
	padding: 18px!important;
	margin: 0px 24px 0px 24px!important;
	border-color: red!important;
}

div.wpcf7-mail-sent-ok {
	border-color: #398F14!important;
}


/* Fin bouton file */

/*****************************************/
/* COMMONS								 */
/*****************************************/

div#Content div.ChargementAjax {
	background: url("images/chargement_ajax.gif") no-repeat left top transparent;
	background-size: contain!important;
	background-image: none\9;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
	src='<?php echo $template_url; ?>/images/chargement_ajax.gif',
	sizingMethod='scale');	
	width: 24px; 
	height: 24px;
	display: none;
	float: left;
	margin-top: 10px;
}

div#Content div.BoiteErreur {
	border: 1px solid red;
	padding: 3px;
	position: absolute;
	top: 28px;
	left: 6px;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 16px;	
}

div#Content div.BoiteErreurParticipant {
	border: 1px solid red;
	padding: 12px;
	top: 28px;
	left: 6px;
	font-family: CabinRegular, Arial, Helvetica, Sans-serif;
	font-size: 14px;
	line-height: 16px;	
}

/* Bouton */

p.Bouton{
	height: 24px;
	float: left;
}

p.BoutonDroite{
	float: right!important;
}

div#Content div#Template p.Bouton {
	font-family: OpenSansSemiBold, Arial, Helvetica, Sans-serif;
	line-height: 42px;
	height: 42px;
	font-size: 16px;
	color: <?php echo $blanc; ?>;
	text-transform: uppercase;
	display: block;
	cursor: pointer;
	float: right;
}

p.Bouton a{
	background: url('./images/bouton_vert.png') no-repeat right top <?php echo $vert_pale; ?>;
	display: inline-block;
	color: <?php echo $blanc; ?>!important;
	text-decoration: none!important;
	padding-right: 36px;
	padding-left: 18px;
}

p.Bouton a:hover {
	background-color: <?php echo $gris_pale; ?>;
	background-position: right -43px;
	color: <?php echo $blanc; ?>;
	text-decoration: none!important;
}

p.Bouton a:hover span {
	cursor: pointer;
}

@media screen and (min-width: 320px) and (max-width: 640px) {

	div#Content div#Template p.Bouton {
		width: 90%;
		float: none;
		margin: 0 auto;
		height: 72px;
		line-height: 72px;

	}
	
	div#Content div#Template p.Bouton a {
		background-image: none;
		background-color: <?php echo $vert_fonce; ?>;	
		display: block;
		text-align: center;
		height: 72px;
		padding-right: 18px!important;		
	}
	
	div#Content div#Template p.Bouton a:hover {
		background-color: <?php echo $gris_moyen; ?>;
	}	

}

/* style td */

td.CornerTopLeft {
	width: 6px;
	height: 6px;
	background: url('./images/corner-top-left.png') no-repeat bottom right transparent;
}

td.CornerTopRight {
	width: 6px;
	height: 6px;
	background: url('./images/corner-top-right.png') no-repeat bottom left transparent;
}

td.CornerBottomLeft {
	width: 6px;
	height: 6px;
	background: url('./images/corner-bottom-left.png') no-repeat top right transparent;
}

td.CornerBottomRight {
	width: 6px;
	height: 6px;
	background: url('./images/corner-bottom-right.png') no-repeat top left transparent;
}

td.SideTop {
	height: 6px;
	background: url('./images/border-top.png') repeat-x bottom left transparent;
}

td.SideBottom {
	height: 6px;
	background: url('./images/border-bottom.png') repeat-x top left transparent;
}

td.SideLeft {
	width: 6px;
	background: url('./images/border-left.png') repeat-y top right transparent;
}

td.SideRight {
	width: 6px;
	background: url('./images/border-right.png') repeat-y top left transparent;
}

td.MiddleCenter {
	background-color: <?php echo $blanc; ?>;
}

/* TIMYMCE */

.Capitale, .capitale {
	text-transform: uppercase;
}

/* autres */

.empty {
	line-height: 0px;
	font-size: 0px;
	overflow: hidden;
	width: 0;
	height: 0;
}

.clear {
	clear: both!important;
	line-height: 0px!important;
	font-size: 0px!important;
	overflow: hidden!important;
	width: 0!important;
	height: 0!important;
	float: none!important;
	margin: 0!important;
	padding: 0!important;
}	