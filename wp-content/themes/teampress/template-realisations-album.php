<?php
/*
Template Name: Realisations Album
*/
	get_header();
	the_post();
?>
<div id="ContentWrap">
		<div id="Content">	
			<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">

					<?php
						//apply_filters('the_title',the_title()); 	
						//apply_filters('the_content',the_content());
						
						// Save paged context
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						
						$args = array(
							'orderby'         => 'menu_order',
							'order'           => 'DESC',
							'post_type'       => 'les-realisations',
							'paged' => $paged
						 );
						 
						// Save old query
						
						$postsquery = new WP_Query($args);
						$postslist = $postsquery->posts;
						
						$ii = 1;
						$nb_image = count($postslist);

						$output = '';						
						$output .= '<div id="Realisations">';
						
						foreach( $postslist as $une_realisation ) {			
						
								//4 par ligne
								($ii % 4 == 0) ? $output .= '<div class="RealisationContent EndDiv">' : $output .= '<div class="RealisationContent">';
								
								//pour lire les images en popup link, utiliser tag : rel ... ne pas oublier de changer le single aussi!
								/*$output .= '<div class="RealisationImage"><a class="fancybox" rel="Groupe" href="'.get_permalink($une_realisation->ID,false).'">'. team_the_thumb(false,false,'','team_thumbnail_image0',176,176,1,1,$une_realisation->ID) .'</a></div>';
								$output .= '<div class="RealisationTitre"><a href="'.get_permalink($une_realisation->ID,false).'">'. apply_filters('the_title',$une_realisation->post_title) .'</a></div>';
								$output .= '<div class="RealisationContenu">'. apply_filters('the_content',$une_realisation->post_content ).'</div>';
								$output .= '</div>';*/							
								
								//Informations sur la realisations
								$output .= '<div class="RealisationImage"><a href="'.get_permalink($une_realisation->ID,false).'">'. team_the_thumb(false,false,'','team_thumbnail_image0',176,176,1,1,$une_realisation->ID) .'</a></div>';
								$output .= '<div class="RealisationTitre"><a href="'.get_permalink($une_realisation->ID,false).'">'. apply_filters('the_title',$une_realisation->post_title) .'</a></div>';
								$output .= '<div class="RealisationContenu">'. apply_filters('the_content',$une_realisation->post_content) .'</div>';
								$output .= '</div>';
								
								if ($ii == $nb_image) {
									$output .= '<div class="clear"></div>';
								}
								
								$ii++;
						}
						
						$output .= '</div>';
						echo $output;
					
						if(function_exists('wp_paginate')) {
							wp_paginate();
						}

					?>
			</div> <!-- Template -->
		</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>