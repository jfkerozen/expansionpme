<?php
/*
Template Name: Activités / Blogue
*/

	$posts_per_page = 6;

	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<div id="PreContent">
				<div id="LeftPreContent">
					<?php
					global $root_page_name;
					global $is_activites;
					
					$is_activites = is_activites();
					
					if($is_activites) {
						//team_the_wpml('Activites', 'coming_events');
						$root_page_name = 'activites';
					} else { // Blogue
						//team_the_wpml('Actualites', 'posts');
						$root_page_name = 'blogue';
					}
					?>
					<?php $current_page = team_get_page($root_page_name); ?>
					<?php
						
						$output = '';
						$output .= apply_filters('the_content', $current_page->post_content);
						
						echo $output;
					?>
				</div>
				<div id="RightPreContent">
					<div class="MoreInformation"><?php team_the_wpml('Contact', 'more_information'); ?></div>
					<?php
						$contactez_nous = team_get_page('contactez-nous');
					?>
					<div class="PageButton"><a href="<?php echo get_permalink($contactez_nous->ID); ?>"><?php echo apply_filters('the_title', $contactez_nous->post_title); ?></a></div>
				</div>
				<div class="clear"></div>
			</div>
			<h2 class="ListTitle"><?php
				if(is_activites()) {
					team_the_wpml('Activites', 'coming_events');
				} else { // Blogue
					team_the_wpml('Actualites', 'news');
				}
			?></h2>
			<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					if(team_is_page($root_page_name)) {
						if($is_activites) {
							query_posts(array('posts_per_page' => $posts_per_page, 'post_type' => 'les-activites', 'paged' => $paged));
						} else { // Blogue
							query_posts(array('posts_per_page' => $posts_per_page, 'post_type' => 'les-nouvelles', 'paged' => $paged));
						}
					}
			?>
			<div class="PostsOf"><?php echo get_posts_of($wp_query->found_posts, $posts_per_page, $paged); ?></div>
			<div class="clear"></div>
			<hr class="SeparateurPlein" />
				<?php get_sidebar('activites-blogue'); ?>
			<div id="RightContent">
				<?php if($is_activites) { ?>
					<h4 class="Legende"><?php team_the_wpml('Activites', 'expertise_fields_legend'); ?></h4>
					<?php
						$args = array('hide_empty' => false);
						
						$champs_expertises = get_terms('champs-expertises', $args);
						
						$output = '';
						$output .= '<div class="ContainerLegende">';
						foreach($champs_expertises as $champ_expertise) {
							$output .= '<div class="ItemLegende">' . $champ_expertise->name . team_the_thumb(false,false,get_taxonomy_image_url($champ_expertise->term_id)[0],'',18,18,1,1,'','png') . '</div>';
						}
						$output .= '<div class="clear"></div></div>';
						
						echo $output; ?>
					<div class="clear"></div>
				<?php } ?>
				<?php if (have_posts()) :?>
					<div id="ListeItems" class="<?php echo (($is_activites) ? 'ListeEvenements' : 'ListeNouvelles'); ?>">
					<?php while (have_posts()) : the_post();
						if($is_activites) {
							$terms_post = wp_get_post_terms($post->ID, 'champs-expertises');
						} else {
							$terms_post = wp_get_post_terms($post->ID, 'types-actualites');
						}
					?>
						<div class="ListeItemWrap">
							<div class="ListeItem">
								<?php if(get_post_meta($post->ID, 'team_thumbnail_image0', true)) { ?>
								<div class="ItemImage"><a href="<?php the_permalink(); ?>"><?php team_the_thumb(true, false,'','team_thumbnail_image0',176,143,1,1,$post->ID,'jpeg&q=100') ?></a></div>
								<?php } ?>
								<div class="ItemContenu">
									<?php
										if(!empty($terms_post)) {
											$output = '<div class="CategoriePost">' . $terms_post[0]->name . '</div>';
											echo $output;
										}
									?>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<?php
										if(!$is_activites) {
											$output = '<div class="PostDate">' . get_the_date() . ' par <span>' . get_the_author() . '</span></div>';
											echo $output;
										}
									?>
									<div class="ExtraitActivite"><?php the_excerpt(); ?></div>
									<div class="DateActivite"><?php echo get_post_meta($post->ID, 'DateActivite', true); ?></div>
									<div class="LieuActivite"><?php echo get_post_meta($post->ID, 'LieuActivite', true); ?></div>
								</div>
								<div class="clear"></div>
								<a class="MoreDetails" href="<?php the_permalink(); ?>"><?php team_the_wpml('Messages', 'more'); ?></a>
								<div class="clear"></div>
							</div>
							<?php
								if(!empty($terms_post)) {
									$image_url = get_taxonomy_image_url($terms_post[0]->term_id);
									$output = '<div class="ImageCategorie">' . team_the_thumb(false,false,$image_url[0],'',52,52,1,1,'','png') . '</div>';
									echo $output;
								}
								// Statuts d'activité spéciaux
								// Activité terminée: prioritaire
								if(get_post_meta($post->ID, 'DateLimite', true != '') && strtotime(get_post_meta($post->ID, 'DateLimite', true)) + 86400 <= time()) {
									$output = '<div class="ItemLabel LabelPast">' . team_the_wpml('Messages', 'past_event', false) . '</div>';
									echo $output;
									
								// Activité complète
								}elseif(get_post_meta($post->ID, 'StatutActivite', true) == 'complete') {
									$output = '<div class="ItemLabel LabelComplete">' . team_the_wpml('Messages', 'event_complete', false) . '</div>';
									echo $output;
								}								
							?>
						</div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<hr class="SeparateurPlein" />
				<div class="Pagination">
					<?php
						$args = array(
														'title' => '',
														'nextpage' => team_the_wpml('Pagination', 'next', false),
														'previouspage' => team_the_wpml('Pagination', 'previous', false)													
													);
						wp_paginate($args);
					?>
				</div>
				<div class="PostsOf"><?php echo get_posts_of($wp_query->found_posts, $posts_per_page, $paged); ?></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
