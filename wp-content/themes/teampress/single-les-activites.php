<?php 
	get_header(); 
	the_post();
?>

<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>	
		<div id="Template" class="template-single-activite">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<?php if (!have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>
		
					<?php
							$term_post = wp_get_post_terms( $post->ID, 'champs-expertises');						
							$image_url = get_taxonomy_image_url($term_post[0]->term_id);
					
					?>
		
					<div id="RightContent">
							<h1><?php the_title(); ?></h1>
							<div class="PostDate"><?php echo $term_post[0]->name; ?><br /><?php echo get_post_meta($post->ID,'DateActivite',true); ?></span></div>
							<div class="ShareSocial">
								<a class="ShareFacebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>"></a>
								<a class="ShareGooglePlus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>"></a>
								<a class="ShareLinkedIn" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&summary=<?php echo urlencode(get_the_title()); ?>&source=<?php echo urlencode(site_url()); ?>"></a>
								<div class="clear"></div>
							</div>
						<?php the_content(); ?>
						
						<div class="SendEmail SendAction"><a class="EmailThisPage" href="#"><?php team_the_wpml('Messages', 'send_email'); ?></a></div>
						<div class="SendPrinter SendAction"><a class="PrintThisPage" href="#"><?php team_the_wpml('Messages', 'print'); ?></a></div>
					</div>
					
					<div id="LeftContent">
						<div class="ItemImage">
						
						<?php
							echo '<div class="ImageCategorie">' . team_the_thumb(false,false,$image_url[0],'',52,52,1,1,'','png') . '</div>';
							
							// Statuts d'activité spéciaux
							// Activité terminée: prioritaire
							if(get_post_meta($post->ID, 'DateLimite', true != '') && strtotime(get_post_meta($post->ID, 'DateLimite', true)) + 86400 <= time()) {
								$output = '<div class="ItemLabel LabelPast">' . team_the_wpml('Messages', 'past_event', false) . '</div>';
								echo $output;
								
							// Activité complète
							}elseif(get_post_meta($post->ID, 'StatutActivite', true) == 'complete') {
								$output = '<div class="ItemLabel LabelComplete">' . team_the_wpml('Messages', 'event_complete', false) . '</div>';
								echo $output;
							}								
							
						?>
					
						<?php team_the_thumb(true, false,'','team_thumbnail_image0',254,206,1,1,$post->ID,'jpeg&q=100') ?></div>
						<div class="LeftContentWrap"><?php echo apply_filters('the_content', get_post_meta($post->ID,'SidebarActivite',true)); ?></div>
						
						<div class="SendEmail SendAction"><a class="EmailThisPage" href="#"><?php team_the_wpml('Messages', 'send_email'); ?></a></div>						
						<div class="ShareSocial">
							<a class="ShareFacebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>"></a>
							<a class="ShareGooglePlus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>"></a>
							<a class="ShareLinkedIn" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&summary=<?php echo urlencode(get_the_title()); ?>&source=<?php echo urlencode(site_url()); ?>"></a>
							<div class="clear"></div>
						</div>	
						
					</div>
					<div class="clear"></div>
					
					<div class="RetourBlogue"><a href="<?php team_the_permalink('activites'); ?>"><?php team_the_wpml('Messages', 'back_blog'); ?></a></div>
					

						<?php
							// Affiche le formulaire seulement si la date limite, si spécifiée, n'est pas dépassée et que l'activité n'est pas spécifiée complète
							// Affiche le formulaire toujours si l'utilisateur est connecté => is_user_logged_in()
							if((!(get_post_meta($post->ID, 'DateLimite', true != '') && strtotime(get_post_meta($post->ID, 'DateLimite', true)) + 86400 <= time()) && !(get_post_meta($post->ID, 'StatutActivite', true) == 'complete') && (get_post_meta($post->ID,'InscriptionActivite',true) == '1')) || is_user_logged_in()) {
								?>
								<div class="BoutonFormulaire"><?php team_the_wpml('Contenu', 'contenu_formulaire_titre'); ?><a href="#" class="FlecheSlide"></a></div>
								<div class="Formulaire">
								<h1><?php team_the_wpml('Contenu', 'contenu_formulaire_titre'); ?></h1>
								<p><?php team_the_wpml('Contenu', 'contenu_formulaire_obligatoire'); ?></p>
									<?php	affiche_formulaire_activite();	?>
								</div>
								<?php
							}
						?>
					

				<?php endwhile; ?>
			<?php endif; ?>
		</div> <!-- Template -->	
	</div><!-- Content -->
</div><!-- ContentWrap -->

<?php get_footer(); ?>