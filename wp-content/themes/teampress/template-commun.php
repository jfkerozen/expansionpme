<?php
/*
Template Name: Commun
*/
	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-<?php echo template_name(__FILE__); ?>">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<div class="MoreInformationWrap">
				<div class="MoreInformation"><?php team_the_wpml('Contact', 'more_information'); ?></div>
				<?php
					$contactez_nous = team_get_page('contactez-nous');
				?>
				<div class="PageButton"><a href="<?php echo get_permalink($contactez_nous->ID); ?>"><?php echo apply_filters('the_title', $contactez_nous->post_title); ?></a></div>
			</div>
			<?php if (have_posts()) :?>
				<?php while (have_posts()) : the_post(); ?>
				
					<?php	the_content(); ?>
					
				<?php endwhile; ?>
			<?php endif; ?>
			
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	if($attached_post = get_post_meta($post->ID, 'ImageCitation', true)) {
		$upload_dir = wp_upload_dir();
		$attached_file = $upload_dir['baseurl'] . '/' . get_post_meta($attached_post, '_wp_attached_file', true);
		$output = '<div class="HiddenImage"><div class="ImageCitation">' . team_the_thumb(false,false,$attached_file,'',114,114,0,0,'','png') . '</div></div>';
		echo $output;
	}

	get_footer(); 
?>
