<?php
	get_header();
?>
<div id="ContentWrap">
	<div id="Content">
		<div id="BandeVerte"></div>
		<div id="Template" class="template-404">
			<div id="Breadcrumbs"><?php team_the_breadcrumbs(); ?></div>
			<h1 class="PageNotFound"><?php echo icl_t('System', 'not_found', 'Page non-trouvée'); ?></h1>
			<p><?php echo icl_t('System', 'not_found_text', 'La page que vous recherchez a peut-être été effacée, son nom changé ou est temporairement indisponible.'); ?></p>	

			<div class="BackHome">
				<?php $back_link = get_home_url(); ?>
				<p><a href="<?php echo $back_link; ?>" class="ImgSubmitLeft"><?php echo icl_t('Product', 'back_home', 'Retour à l\'accueil'); ?></a></p>
			</div>			
		</div> <!-- Template -->
	</div><!-- Content -->
</div><!-- ContentWrap -->
<?php
	get_footer(); 
?>
