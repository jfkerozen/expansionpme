<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

        if (!empty($post->post_password)) { // if there's a password
            if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
				?>
				
				<p class="nocomments">This post is password protected. Enter the password to view comments.<p>
				
				<?php
				return;
            }
        }

		/* This variable is for alternating comment background */
		$oddcomment = 'alt';
?>

<!-- You can start editing here. -->

<?php 
	if ($comments) : ?>

	<div class="commenttitle">
		<h2 id="comments">Commentaires</h2>
		<div class="clear"></div>
	</div>

	<div class="commentlist">

	<?php
		$commentCount = 0;

		// 2010-01-12 NM : Du plus r�cent au plus ancien
		$comments = array_reverse($comments, true);

		foreach ($comments as $comment) :
		
			$commentCount++;
			if($commentCount != 1) {
				echo '<div class="DashedLine"></div>';
			}
	?>

		<div class="<?php echo $oddcomment; ?> LeComment" id="comment-<?php comment_ID() ?>">
			<div class="commentinfo">
				<span class="commentdate"><?php comment_date('j M. Y') ?></span><span class="commentby"><?php comment_author_link() ?></span>
			</div>
			
			<?php if ($comment->comment_approved == '0') : ?>
			<div class="awaitingapproval"><?php echo icl_t('Widgets', 'comment_awaiting_moderation', 'Votre commentaire est en attente d\'approbation.'); ?></div>
			<?php endif; ?>

			<div class="commenttext">
				<?php comment_text() ?>
			</div>
			<div class="clear"></div>
		</div>

	<?php /* Changes every other comment to a different class */	
		if ('alt' == $oddcomment) $oddcomment = '';
		else $oddcomment = 'alt';
	?>

	<?php endforeach; /* end for each comment */ ?>
	</div>
	
 <?php else : // this is displayed if there are no comments so far ?>

  <?php if ('open' == $post->comment_status) : ?> 
		<!-- If comments are open, but there are no comments. -->
		<div class="commenttitle">
			<h2 id="comments"><?php comments_number('Aucun '.icl_t('Widgets', 'comment', "commentaire pour l'instant."), '1 '.icl_t('Widgets', 'comment', 'Commentaire'), '% '.icl_t('Widgets', 'comments', 'Commentaires'));?></h2> 
		</div>
		
	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php echo icl_t('Widgets', 'comments_closed', 'Les commentaires sont ferm�s.'); ?></p>
		
	<?php endif; ?>
<?php endif; ?>
<div class="clear"></div>
<div class="Separator"></div>

<?php if ('open' == $post->comment_status) : ?>

<div class="addcomment" id="respond">
	<h3 id="respond">Faites-nous parvenir vos commentaires ou suggestions !</h3>

	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>
	<?php else : ?>

	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

	<p>
		<label for="email"><?php if ($req) echo '<span class="input_required">*</span>'; ?><?php echo icl_t('Widgets', 'email', 'Votre adresse courriel:'); ?></label>
		<input class="text" type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="40" tabindex="1" />
	</p>

	<p>
		<label for="author"><?php if ($req) echo '<span class="input_required">*</span>'; ?><?php echo icl_t('Widgets', 'name', 'Nom:'); ?></label>
		<input class="text" type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="40" tabindex="2" />
	</p>

	<p>
		<label for="url"><?php echo icl_t('Widgets', 'website', 'Site web:'); ?></label>
		<input class="text" type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="40" tabindex="3" />
	</p>

	<p>
		<label for="secteur"><?php echo icl_t('Widgets', 'area', 'Secteur:'); ?></label>
		<?php do_shortcode("[secteur_dropdown default='$comment->extra_secteur']"); ?>
	</p>	
	
	<!--<p><small><strong>XHTML:</strong> You can use these tags: <?php echo allowed_tags(); ?></small></p>-->

	<p>
		<label for="comment"><?php if ($req) echo '<span class="input_required">*</span>'; ?><?php echo icl_t('Widgets', 'comments', 'Commentaires:'); ?></label>
		<textarea name="comment" id="comment" cols="31" rows="10" tabindex="4"></textarea>
	</p>

	<p><input class="submit" name="submit" type="submit" id="submit" tabindex="5" value="<?php echo icl_t('Widgets', 'submit', 'Envoyer'); ?>" />
	<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
	</p>
	<?php do_action('comment_form', $post->ID); ?>

	</form>
	<div class="Separator" style="margin-bottom:10px;"></div>
	<p><span class="input_required">* Obligatoire</span></p>
	<?php endif; // If registration required and not logged in ?>

	<?php endif; // if you delete this the sky will fall on your head ?>
</div>