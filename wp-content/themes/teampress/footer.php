	<div class="clear"></div>
	
	<div id="ZoneInfolettreWrap">
		<div id="ZoneInfolettre">
		<form method='get' accept-charset='UTF-8' name='oi_form' action='http://suivi.lnk01.com/oi/1/9b2579090bdf3348068ea435835e6bcd' >
			<label for="courriel_infolettre"><?php team_the_wpml('Accueil', 'accueil_inscrivezinfolettre'); ?></label><input type='text' id="courriel_infolettre" name='email'onClick="_gaq.push(['_trackEvent', 'infolettre', 'inscription',,, false]);"/>
			<!-- Prenom: <input type='text' name='Prenom' /><br />
			Nom: <input type='text' name='Nom' /><br />
			salutation: <input type='text' name='salutation' /><br />
			entreprise: <input type='text' name='entreprise' /><br /> -->
			<input  type='hidden' name='goto' value='' />
			<input type='hidden' name='iehack' value='&#9760;' />
			<input class="NoDisplay" type='submit' value='Inscrire' />
			<a class="SubmitSpecial"></a>
			<div class="clear"></div>
		</form>
		</div>
	</div>	
	
	<div id="FooterWrap">	
		<div id="FondFooter"></div>		
		<div id="Footer">
		
			<a href="<?php team_the_permalink('contactez-nous'); ?>" class="BoutonMobileContact"><?php team_the_wpml('Accueil', 'accueil_btn_contact'); ?></a>	
		
			<div id="InfoFooter">
				<div id="LogoFooter"><a title="Logo" href="<?php echo icl_get_home_url(); ?>"></a></div>			
			
					<?php
						$output .= '';
						$output .= '<div id="FooterAdresse">';
						$output .= '<div class="NomCompagnie">Expansion PME</div>';
						$output .= team_adresse('adresse',false);
						$output .= '</div>';
						$output .= '<div id="FooterContact">';
						$output .= team_adresse('telephone',false) .'<br/>';
						$output .= team_adresse('fax',false).'<br/>';
						$output .= '<a href="mailto:info@expansionpme.org" onClick="_gaq.push(["_trackEvent", "contact", "courriel",,, false]);">'.team_adresse('courriel',false) .'</a>';
						$output .= '</div>';
						echo $output;
					?>
				
				<div id="FooterMedia">
					<div class="SuivezNousFooter"><?php team_the_wpml('Accueil', 'exemple_suiveznous'); ?></div>
					<div class="BoutonMediaWrap">
					<a href="http://www.linkedin.com/company/expansion-pme" target="_blank" class="BoutonLinkedin"></a>
					<a href="http://www.youtube.com/user/ExpansionPME" target="_blank" class="BoutonYoutube"></a>	
					<div class="clear"></div>
					</div>
				</div>
				
				<div class="clear"></div>
			</div>
			
		
				
			<div id="FooterNotes">
				<div id="Rights">
					&copy; <?php echo '<a href="'.icl_get_home_url().'">'. get_bloginfo('name'); echo '</a>&nbsp;'. date_i18n('Y') . '&nbsp;-&nbsp;'; echo icl_t('Footer', 'all_rights_reserved', 'Tous droits r&eacute;serv&eacute;s.'); ?> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
				<a href="<?php team_the_permalink('plan-de-site'); ?>" id="PlanDuSite"><?php team_the_wpml('Footer', 'footer_sitemap'); ?></a>
				<div class="Nmedia"><?php team_the_wpml('Footer', 'footer_realisation'); ?><a href="http://www.nmediasolutions.com/accueil" target="_blank" rel="external" title="Nmédia Solutions">Nmédia Solutions</a></div>
				<div class="clear"></div>
			</div>
			
			<?php wp_footer(); ?>
			<div class="clear"></div>
		</div> <!-- Footer -->
	</div> <!-- FooterWrap -->
		<div id="FontLoad">Team Marketing.Web.Design</div>
</body>
</html>