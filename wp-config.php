<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/Editing_wp-config.php Modifier
 * wp-config.php} (en anglais). C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */
// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'expansionpme');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'dev');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '12dube');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/*
switch($_SERVER["HTTP_HOST"]) {
    default:
        define('DB_NAME', 'exppme_wp');
        define('DB_USER', 'exppme');
        define('DB_PASSWORD', 'NuBEb37wsQCccXCr');
        define('DB_HOST', 'localhost');
        define('DB_CHARSET', 'utf8');
        break;
}
*/
/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mOEq9Y<Q05<sXqLln Bx=X%0Y)CD[.Bersk2 }9UvB]ql,9GqQ+MhU)yu:j<d2/A'); 
define('SECURE_AUTH_KEY',  '<GM]A%=2IH@rFz8NVTt4xN:6dw,;NJOz;;6Jg>B,07ep;A!rvs|}dG?M9Ky5FPMd'); 
define('LOGGED_IN_KEY',    'P.?Gwy{;Y7<q!@ni.n@q7zr&Gw5y xw=/6q6PNmoU1ke%)cNqfY=k]p.2YXtdA>A'); 
define('NONCE_KEY',        '|PdZEN<AQPvu,IHKy:JNwF>D!y&|, jL|#L>os,iN*7L%4qNny;WDNj>RbY&-&GC'); 
define('AUTH_SALT',        'N@4d-!Z.6&k/V28IN_KiLf=~nqD._+,JxY:Z0R?VF2H2_<W,2v?k-jZVi^l#D5AR'); 
define('SECURE_AUTH_SALT', '*4z5W`?5P-4xh`zvgW4hBpMRl:0=Y@6?U$Qy4eE_Em>8>Cra(~A|oK;D0p7+**Ni'); 
define('LOGGED_IN_SALT',   '0[|UC98`;M-*uWI;Q=7CRSiMW8mVLI>M.y2Q+Uye6@S#@<Wlj%R}848>HZ59<,)0'); 
define('NONCE_SALT',       '0yV@}}W?~EqqS1>hDSU9eyK  YM)$cZrh6l?Y@4(UtjYDtIp3Q~n/Xx#k9 FP1wl'); 
/**#@-*/

// 2013-04-11 NM : désactive la possibilité de modifier des fichiers PHP dans WordPress
define('DISALLOW_FILE_EDIT', true);

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');